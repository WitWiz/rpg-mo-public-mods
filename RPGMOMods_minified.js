Player.Mods = Player.Mods || new Object();
var Mods = Player.Mods;

Mods.version = "3.2a-2014-10-24";
Mods.modOptions = Mods.modOptions || new Object();
Mods.Load = new Object();
Mods.time = timestamp();

var disable_options = true,
    Load = Mods.Load,
    modOptions = Mods.modOptions;

// Switch world bug
var switchWorldBugFix = function(){};

function getElem (id, content) {
    if (typeof id !== "string") return null;
    var div = document.getElementById(id);
    if (div === null) return null;
    if (typeof content === "undefined") return div;
    for (var i in content) {
        if (i == "id" || i == "className" || i == "cssFloat" || i == "innerHTML")
                                                                div[i] = content[i];
        if (typeof content[i] === "string")                     div.setAttribute(i, content[i]);
        if (typeof content[i] === "function")                   div[i] = content[i];
        if (typeof content[i] === "number")                     div[i] = "" + content[i];
        if (typeof content[i] === "object")
            for (var j in content[i]) {
            if (i == "style")                                   div.style[j] = content[i][j];
            if (i == "setAttributes")                           div.setAttribute(j, "javascript: " + content[i][j]);
            if (i == "setFunctions")                            div[j] = content[i][j];
        }
    }
    return div;
}

function createElem (type, attachTo, content) {
    if (typeof type === "undefined" || attachTo === "undefined") return Mods.consoleLog("createElem error: no type or attachTo") ? null : null;
    var div = document.createElement(type);
    if (typeof content != "undefined") for (var i in content) {
        if (i == "id" || i == "className" || i == "cssFloat" || i == "innerHTML")
                                                                div[i] = content[i];
        if (typeof content[i] === "string")                     div.setAttribute(i, content[i]);
        if (typeof content[i] === "function")                   div[i] = content[i];
        if (typeof content[i] === "number")                     div[i] = "" + content[i];
        if (typeof content[i] === "object")
            for (var j in content[i]) {
            if (i == "style")                                   div.style[j] = content[i][j];
            if (i == "setAttributes")                           div.setAttribute(j, "" + content[i][j]);
            if (i == "setFunctions")                            div[j] = content[i][j];
        }
    }
    if (typeof attachTo === "string")                           getElem(attachTo).appendChild(div);
    else if (typeof attachTo === "object")                      attachTo.appendChild(div);
    else return null
    return div;
}

if (players[0].name == "dendrek" || players[0].name == "witwiz") disable_options = false;

Mods.initialize = function () {
    // This object determines the styles/appearance of the different options that appear in the Mod Options "options" menu;
    Mods.modOptionsTypes = {
        text: {
            type: "text",
            createElement: "span",
            closeElement: "span",
            opt_span: "all",
            style: {
                "": ""
            }
        },
        checkbox: {
            createElement: "input type='checkbox'",
            style: {
                width: ".8em",
                height: ".8em",
                margin: "0px",
                "margin-right": "6px"
            }
        },
        radio: {
            type: "radio",
            createElement: "input type='radio'",
            style: {
                width: ".8em",
                height: ".8em",
                margin: "0px",
                "margin-right": "6px"
            }
        },
        button: {
            type: "button",
            createElement: "button",
            closeElement: "button",
            className: "market_select pointer",
            opt_span: "all",
            style: {
                margin: "0px",
                "font-size": "1em"
            }
        },
        block_color: {
            createElement: "div",
            closeElement: "div",
            opt_span: "all",
            style: {
                border: "1px solid #666666",
                width: "100px",
                height: "20px"
            }
        }
    };
    // This function populates the Mods.modOptions object with updated values. It has conditions that if it's loaded twice, it will take on new values (if any exist) as long as they're not booleans;
    Mods.modOptionsVersion = function () {
        var modOptions = {};
        //modOptions["translator"] = {
        //    id: "translator",
        //    name: "Chat Translator",
        //    shortname: "Chat Translator",
        //    description1: "Allows you to translate chat messages.",
        //    description2: "This mod allows you to translate your messages in a different language or to tranlate other people chat messages.",
        //    load: false,
        //    loaded: false,
        //    newmod: false,
        //    updated: false,
        //    time: 0
        //;

        modOptions["expmonitor"] = {
            id: "expmonitor",
            name: "x2 Experience Monitor",
            shortname: "x2 Experience Monitor",
            description1: "Shows a timer during x2 events.",
            description2: "This mod shows a visibe timer whenever a x2 event is started.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["fullscreen"] = {
            id: "fullscreen",
            name: "FullScreen Mode",
            shortname: "FullScreen Mode",
            description1: "Enable full-screen mode.",
            description2: "This mod allows to display the game on the whole screen. It is only suggested on PC (no mobile devices). WARNING: on slow devices, it may affect game performance. After loading the mod, enable full screen mode in the options menu.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["autocast"] = {
            id: "autocast",
            name: "Auto Cast",
            shortname: "Auto Cast",
            description1: "Enable auto-casting equipped magic.",
            description2: "This mod enables auto-casting magic (which becomes automatic when engaging in combat). It is disabled by default, to turn it on enable Autocast in game options.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["newmap"] = {
            id: "newmap",
            name: "Enhanced Map",
            shortname: "Enhanced Map",
            description1: "Enhances game map with several added details.",
            description2: "Map now shows current position and details, including travel signs, mobs, bosses, resource spots and POI. In dungeons, fellow players are shown in the full map. Mimimap shows bigger dots, yellow-colored for friends.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["newmarket"] = {
            id: "newmarket",
            name: "Enhanced Market",
            shortname: "Enhanced Market",
            description1: "Adds various helpers for market interface.",
            description2: "Allows resubmit or edit of market offers, display target player for transactions, highlights offers directed to the player and other market improvements. Adds Trade channel.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["tabs"] = {
            id: "tabs",
            name: "Tabbed Chat",
            shortname: "Tabbed Chat",
            description1: "Adds tabs to the chat interface.",
            description2: "Every chat tab can have different filters and subscribed channels. Filters are now applied only on active chat. Subscribed channels can be filtered from tabs via right-click menu. Tabs can also be renamed/deleted via context menu.",
            requires: "Chatmd",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };

        modOptions["kbind"] = {
            id: "kbind",
            name: "Keybinding Extensions",
            shortname: "Keybinding Extensions",
            description1: "Adds an iterface to manage custom keybindings for various actions.",
            description2: "From the game menu, a new 'keybindings' item allows access to mod customization.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };

        modOptions["expbar"] = {
            id: "expbar",
            name: "Experience Bar",
            shortname: "Experience Bar",
            description1: "Shows an experience bar for your chosen active skill.",
            description2: "Open your skill\'s menu and click any skill to have the experience bar show that skill.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "attached",
                    name: "Lock exp bar above the health bar.",
                    description: "If you move the health bar, the exp bar will move with it.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;attached&apos;);"
                },
                "1": {
                    id: "toggle",
                    name: "Allow exp bar to be toggled on/off.",
                    description: "If on, clicking the exp bar will cause it to hide. Click a skill to un-hide it.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;toggle&apos;);"
                },
                "2": {
                    id: "color",
                    name: "Change the color of the exp bar.",
                    type: Mods.modOptionsTypes.text,
                    options: {
                        "0": {
                            id: "color",
                            type: Mods.modOptionsTypes.radio,
                            value: "0",
                            background: "",
                            onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;color&apos;);"
                        },
                        "1": {
                            id: "color",
                            type: Mods.modOptionsTypes.radio,
                            value: "1",
                            background: "",
                            onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;color&apos;);"
                        },
                        "2": {
                            id: "color",
                            type: Mods.modOptionsTypes.radio,
                            value: "2",
                            background: "",
                            onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;color&apos;);"
                        },
                        "3": {
                            id: "color",
                            type: Mods.modOptionsTypes.radio,
                            value: "3",
                            background: "",
                            onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;color&apos;);"
                        },
                        "4": {
                            id: "color",
                            type: Mods.modOptionsTypes.radio,
                            value: "4",
                            background: "",
                            onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;color&apos;);"
                        },
                        "5": {
                            id: "color",
                            type: Mods.modOptionsTypes.radio,
                            value: "5",
                            background: "",
                            onclick: "javascript: Mods.modOptions_options(&apos;expbar&apos;,&apos;color&apos;);"
                        }
                    }
                }
            }
        };
        modOptions["petinv"] = {
            id: "petinv",
            name: "Pet Inventory",
            shortname: "Pet Inventory",
            description1: "Attaches the pet inventory to the main one.",
            description2: "You will see the pet's inventory beneath your main inventory. You will also be able to transfer items between the two inventories very easily. By default, left-clicking items will send them from your inventory to your pet's and shift+clicking will cause you to use/equip items. Additional features include: (unload) and (load) to unload/load all pet-inventory items quickly.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "shiftclick",
                    name: "Allow shift+click to send items to pet chest.",
                    description: "If toggled on, shift+click sends items from your inventory to the pet chest, while left-click uses/equips items. If toggled off, shift+click uses/equips items, while left-click sends items from your inventory to the pet chest.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;petinv&apos;,&apos;shiftclick&apos;);"
                },
                "1": {
                    id: "petexp",
                    name: "Hide the pet's exp/evolution on the pet chest.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;petinv&apos;,&apos;petexp&apos;);"
                },
                "2": {
                    id: "pettext",
                    name: "Hide text/information above pet inventory.",
                    description: "If toggled off, the 'Pet/'s chest title, as well as the greyed text and checkbox will be hidden.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;petinv&apos;,&apos;pettext&apos;);",
                    options: {
                        "0": {
                            id: "0",
                            name: "Hide 'Pet/'s chest'.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;petinv&apos;,&apos;pettext&apos;,&apos;0&apos;);"
                        },
                        "1": {
                            id: "1",
                            name: "Hide grey text for (click to close) and shift+click.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;petinv&apos;,&apos;pettext&apos;,&apos;1&apos;);"
                        },
                        "2": {
                            id: "2",
                            name: "Hide shift+click checkbox.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;petinv&apos;,&apos;pettext&apos;,&apos;2&apos;);"
                        }
                    }
                }
            }
        };
        modOptions["mosmob"] = {
            id: "mosmob",
            name: "Mouseover Stats",
            shortname: "Mouseover Stats",
            description1: "When you mouseover a mob, an item, or an object, you'll be able to see its stats.",
            description2: "The stats shown are 'A' for accuracy, 'S' for strength, 'D' for defence, and 'Hp' for health. Objects show a description or required levels. A game options allows to tweak panel appearance.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "appearance",
                    name: "Choose how the mob's stats appear.",
                    description: "When you mouseover a mob, you will see its stats: accuracy, strength, defense and health. You can decide here how those values are shown.",
                    type: Mods.modOptionsTypes.text,
                    options: {
                        "0": {
                            id: "appearance",
                            name: "(A0, S0, D0, H5)",
                            type: Mods.modOptionsTypes.radio,
                            value: "0",
                            onclick: "javascript: Mods.modOptions_options(&apos;mosmob&apos;,&apos;appearance&apos;);"
                        },
                        "1": {
                            id: "appearance",
                            name: "(Acc0, Str0, Def0, Hp5)",
                            type: Mods.modOptionsTypes.radio,
                            value: "1",
                            onclick: "javascript: Mods.modOptions_options(&apos;mosmob&apos;,&apos;appearance&apos;);"
                        },
                        "2": {
                            id: "appearance",
                            name: "(0 / 0 / 0 | 5)",
                            type: Mods.modOptionsTypes.radio,
                            value: "2",
                            onclick: "javascript: Mods.modOptions_options(&apos;mosmob&apos;,&apos;appearance&apos;);"
                        }
                    }
                },
                "1": {
                    id: "twolines",
                    name: "Show the mob's stats below the mob's name.",
                    description: "When toggled on, you'll see two lines of text: 1) level and name, 2) stats. When toggled off, the name and stats all appear on one line.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;mosmob&apos;,&apos;twolines&apos;);"
                },
                "2": {
                    id: "color",
                    name: "Set color of mob's stats based on difficulty.",
                    description: "When toggled on, the mob's stats will be compared to your own. If it's stronger, the stat will be red, if much weaker, the stat will be green. Stats within your combat range will be yellow.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;mosmob&apos;,&apos;color&apos;);"
                }
            }
        };
        modOptions["health"] = {
            id: "health",
            name: "Updated Health Bar",
            shortname: "Health Bar",
            description1: "Will now display health values for you and your target.",
            description2: "You'll see current health values on your and your target's health bars that adjust as you take/deal damage or heal.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "capitalize",
                    name: "Capitalize your name.",
                    description: "The game's default is to show your name in lower case. Choose this option to have your name shown in proper case.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;health&apos;,&apos;capitalize&apos;);"
                },
                "1": {
                    id: "appearance",
                    name: "Choose how the name/health/level appear.",
                    type: Mods.modOptionsTypes.text,
                    options: {
                        "0": {
                            id: "appearance",
                            name: "L1 White Rat (5)",
                            description: "The mob's level will be shown in front of its name, and its current health will be shown in parenthesis.",
                            type: Mods.modOptionsTypes.radio,
                            value: "0",
                            onclick: "javascript: Mods.modOptions_options(&apos;health&apos;,&apos;appearance&apos;);"
                        },
                        "1": {
                            id: "appearance",
                            name: "White Rat (5/5)",
                            description: "The mob's level is hidden. Its current and max health are shown in parenthesis as ([current]/[max]).",
                            type: Mods.modOptionsTypes.radio,
                            value: "1",
                            onclick: "javascript: Mods.modOptions_options(&apos;health&apos;,&apos;appearance&apos;);"
                        },
                        "2": {
                            id: "appearance",
                            name: "White Rat (5/5 100%)",
                            description: "The mob's level is hidden. Its current, max and percent health are shown in parenthesis as ([current]/[max] [percent]%).",
                            type: Mods.modOptionsTypes.radio,
                            value: "2",
                            onclick: "javascript: Mods.modOptions_options(&apos;health&apos;,&apos;appearance&apos;);"
                        }
                    }
                }
            }
        };
        modOptions["forgem"] = {
            id: "forgem",
            name: "Forging Interface",
            shortname: "Forge Interface",
            description1: "The forging UI is greatly improved to be easier to use.",
            description2: "You can now learn recipes (by using them once) and then will be able to quickly place/remove mats with a button click.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "reset",
                    name: "Forget All Recipes",
                    description: "This will delete your entire recipes list, causing you to start with zero recipes known. (You can always relearn any recipe by creating that item again.)",
                    type: Mods.modOptionsTypes.button,
                    onclick: "javascript: Mods.modOptions_options(&apos;forgem&apos;,&apos;reset',true);"
                }
            }
        };
        modOptions["chestm"] = {
            id: "chestm",
            name: "Chest Interface",
            shortname: "Chest Interface",
            description1: "New options for sorting, withdrawing and depositing.",
            description2: "You have several options for how to sort items, including 'inventory first'; you can also use withdraw 'All' to fill your inventory quickly, or deposit 'All+' to deposit all unequipped items at once. (If you Ctrl+click an item in your inventory, All+ will also ignore that item; this is useful for items that you cannot equip.)",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "sortinv",
                    name: "When sorting the chest, sort inventory items to the top.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortinv&apos;);",
                    options: {
                        "0": {
                            id: "0",
                            name: "Choose highlight color.",
                            type: Mods.modOptionsTypes.text,
                            description: "Inventory items in your chest will have the border color you choose.",
                            options: {
                                "0": {
                                    id: "chestm_sortinv_color",
                                    name: "No highlight color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "0",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortinv&apos;,&apos;color&apos;);"
                                },
                                "1": {
                                    id: "chestm_sortinv_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "1",
                                    border: "#FFFFFF",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortinv&apos;,&apos;color&apos;);"
                                },
                                "2": {
                                    id: "chestm_sortinv_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "2",
                                    border: "#00FF00",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortinv&apos;,&apos;color&apos;);"
                                },
                                "3": {
                                    id: "chestm_sortinv_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "3",
                                    border: "#FF00FF",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortinv&apos;,&apos;color&apos;);"
                                },
                                "4": {
                                    id: "chestm_sortinv_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "4",
                                    border: "#FFFF00",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortinv&apos;,&apos;color&apos;);"
                                }
                            }
                        }
                    }
                },
                "1": {
                    id: "sortfav",
                    name: "When sorting the chest, sort favorited items to the top.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortfav&apos;);",
                    options: {
                        "0": {
                            id: "0",
                            name: "Choose highlight color.",
                            type: Mods.modOptionsTypes.text,
                            description: "Favorited items in your chest will have the border color you choose.",
                            options: {
                                "0": {
                                    id: "chestm_sortfav_color",
                                    name: "No highlight color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "0",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortfav&apos;,&apos;color&apos;);"
                                },
                                "1": {
                                    id: "chestm_sortfav_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "1",
                                    border: "#FFFFFF",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortfav&apos;,&apos;color&apos;);"
                                },
                                "2": {
                                    id: "chestm_sortfav_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "2",
                                    border: "#00FF00",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortfav&apos;,&apos;color&apos;);"
                                },
                                "3": {
                                    id: "chestm_sortfav_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "3",
                                    border: "#FF00FF",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortfav&apos;,&apos;color&apos;);"
                                },
                                "4": {
                                    id: "chestm_sortfav_color",
                                    type: Mods.modOptionsTypes.radio,
                                    value: "4",
                                    border: "#FFFF00",
                                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;sortfav&apos;,&apos;color&apos;);"
                                }
                            }
                        }
                    }
                },
                "2": {
                    id: "hidecheckbox",
                    name: "Hide additional sorting option checkboxes.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;hidecheckbox&apos;);"
                },
                "3": {
                    id: "gearsort",
                    name: "Choose how gear is sub-sorted.",
                    description: "Gear is already sorted by chategory (armor vs weapon vs jewelry). But can be further sub-sorted based on one of the following parameters.",
                    type: Mods.modOptionsTypes.text,
                    options: {
                        "0": {
                            id: "gearsort",
                            name: "Sort by minimum level requirement.",
                            type: Mods.modOptionsTypes.radio,
                            value: "0",
                            onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;gearsort&apos;);"
                        },
                        "1": {
                            id: "gearsort",
                            name: "Sort by armor type.",
                            type: Mods.modOptionsTypes.radio,
                            value: "1",
                            onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;gearsort&apos;);"
                        },
                        "2": {
                            id: "gearsort",
                            name: "Sort by primary stat bonus.",
                            type: Mods.modOptionsTypes.radio,
                            value: "2",
                            onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;gearsort&apos;);"
                        }
                    }
                },
                "4": {
                    id: "allplus",
                    name: "Hide the All+ deposit option.",
                    type: Mods.modOptionsTypes.checkbox,
                    onclick: "javascript: Mods.modOptions_options(&apos;chestm&apos;,&apos;allplus&apos;);"
                }
            }
        };
        modOptions["rclick"] = {
            id: "rclick",
            name: "Right-Click Menu Extensions",
            shortname: "RightClick Menu Extensions",
            description1: "Right-clicking on mobs: 'Drops', 'Combat Analysis' and wiki access.",
            description2: "These are new menu options when you right click on mobs. Item Drops shows all items the mob is able to drop (and accurate drop rates); Combat Analysis shows the expected amount of damage that you and the mob will do. Right click on items/mobs allows wiki search. On a player, allows whispering. On inventory items allow to destroy all similar items and to search wiki. Additionally, this mod will show gathering success rates when used on mining nodes, trees, and fishing spots.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0,
            options: {
                "0": {
                    id: "dropitem",
                    name: "Choose what the item 'Drops' option shows.",
                    type: Mods.modOptionsTypes.text,
                    options: {
                        "0": {
                            id: "0",
                            name: "Show 'Wiki' drop rates.",
                            description: "The drop rates shown on the wiki are not accurate because they are not adjusted to the accurate in-game values. By default, this mod shows the adjusted drop rates. However, turning this option on will show the un-adjusted (aka 'wiki') rates.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;rclick&apos;,&apos;dropitem&apos;,&apos;0&apos;);"
                        }
                    }
                },
                "1": {
                    id: "combat",
                    name: "Choose what the 'Combat Analysis' option shows.",
                    type: Mods.modOptionsTypes.text,
                    options: {
                        "0": {
                            id: "0",
                            name: "Average damage",
                            description: "This shows the actual average damage (over a large number of fights) that can be expected with your current gear. It takes into account that damage can never be less than 0. For example, a mob that normally hits you for 0, will sometimes hit you for more than 0. So its average damage has to be greater than 0.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;rclick&apos;,&apos;combat&apos;,&apos;0&apos;);"
                        },
                        "1": {
                            id: "1",
                            name: "Chance to hit for zero",
                            description: "A 'miss' is a hit of zero (when you or the enemy take 0 damage). The higher this rate is, the more likely a miss will occur.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;rclick&apos;,&apos;combat&apos;,&apos;1&apos;);"
                        },
                        "2": {
                            id: "2",
                            name: "Max damage",
                            description: "Max damage is the highest possible melee hit that you or your enemy can do. You should avoid fighting a mob whose max hit is greater than your current health.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;rclick&apos;,&apos;combat&apos;,&apos;2&apos;);"
                        },
                        "3": {
                            id: "3",
                            name: "Ave. hits to kill",
                            description: "Assuming you and the enemy start at full health, this is approximately how many hits it will normally take for one of you to kill the other.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;rclick&apos;,&apos;combat&apos;,&apos;3&apos;);"
                        },
                        "4": {
                            id: "4",
                            name: "Ave. time to kill",
                            description: "Assuming you and the enemy start at full health, this is approximately how many seconds it will normally take for one of you to kill the other.",
                            type: Mods.modOptionsTypes.checkbox,
                            onclick: "javascript: Mods.modOptions_options(&apos;rclick&apos;,&apos;combat&apos;,&apos;4&apos;);"
                        }
                    }
                }
            }
        };
        modOptions["magicm"] = {
            id: "magicm",
            name: "Magic Damage Interface",
            shortname: "Magic Interface",
            description1: "Magic damage done now appears over the enemy.",
            description2: "When you cast spells, you will see the amount of damage they do appear over the enemy's head. Additionally, new keybinds are available for magic spells: 7 8 9 0 as well as the number pad 1 2 3 4.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["wikimd"] = {
            id: "wikimd",
            name: "In-Game Wiki",
            shortname: "In-Game Wiki",
            description1: "An in-game wiki will now be available in this menu.",
            description2: "You can use the wiki to browse the game's database for items/monsters/vendors to see information like stats, drops, vendor availability/prices, and craft recipes. There are plenty of options for searching the wiki (such as by name, by min-skill requirement, by type, etc) to make navigating it and finding what you're looking for easier. On crafting recipes, you can look at crafting formula or learn the formula for later use in the Forge Mod.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["miscmd"] = {
            id: "miscmd",
            name: "Miscellaneous Improvements",
            shortname: "Miscellaneous",
            description1: "Various improvements of the game's UI.",
            description2: "These are 'small' mods that didn't require individual load options. Included at the moment: 1) Indicators for items that will be saved upon death, 2) Toolbar at the top showing various useful information.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["chatmd"] = {
            id: "chatmd",
            name: "Chat Extensions",
            shortname: "Chat Extensions",
            description1: "Adds chat filters and commands.",
            description2: "A new chat filter (found in the Filters menu) has been added that blocks 'spam' messages, including: 'I think I'm missing something.' 'Cannot do that yet.' 'You are under attack!' 'You feel a bit better.' and 'It's a [object name]'; in addition, when you do /online, your friends will be yellow colored, and mods/admins as well with green/orange colors. Another option allows to enable links in chat. The mod also adds newbie tips, shown every 10 minutes (can be disabled from game options). Also, you can right-click a player's name in chat window to ignore, add/remove as friend. Also chat commands (ping, played, wiki...) are added.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        modOptions["farming"] = {
            id: "farming",
            name: "Farming Improvements",
            shortname: "Farming Mod",
            description1: "Adds the ability to \'queue\' farming actions.",
            description2: "Queued farming actions (seeding, harvesting and raking) will occur automatically once you are no longer busy with the previous action. You can queue one plot at a time, or the entire farm, if you like. Additional keybinds include: Ctrl (to queue actions) and Space (to toggle between Active and Paused). Also, the Island Deed now sends you on a path straight to the sign for a quick exist.",
            load: false,
            loaded: false,
            newmod: false,
            updated: false,
            time: 0
        };
        if (Mods.modOptions != modOptions) {
            for (var i in modOptions) {
                if (Mods.modOptions[i] == null) {
                    Mods.modOptions[i] = modOptions[i];
                    Mods.modOptions[i].newmod = true;
                };
                for (var j in modOptions[i]) {
                    if (Mods.modOptions[i][j] == null) {
                        Mods.modOptions[i][j] = modOptions[i][j];
                        Mods.modOptions[i].updated = true;
                    };
                    if (Mods.modOptions[i][j] != modOptions[i][j] && typeof modOptions[i][j] != "boolean") {
                        Mods.modOptions[i][j] = modOptions[i][j];
                        Mods.modOptions[i][j].updated = true;
                    };
                };
            };
        };
    };

    Mods.modOptionsVersion();

    // This function sets the "load" value for Mods.modOptions based on the user's preferences (based on whether the user normally loads those mods);
    // This function also determines if a mod is "new" to the user. If the mod is new, it will display in the mod's list as such.
    Mods.modOptionsLoad = function () {
        var temp_load = {};
        var temp_load_test = {};
        var temp_newmod = {};
        var temp_newmod_test = {};
        for (var i in Mods.modOptions) {
            temp_load[Mods.modOptions[i].id] = Mods.modOptions[i].load;
            temp_newmod[Mods.modOptions[i].id] = Mods.modOptions[i].newmod;
        };
        localStorage["modOptionsLoad"] = localStorage["modOptionsLoad"] || JSON.stringify(temp_load);
        localStorage["modOptionsNewmod"] = localStorage["modOptionsNewmod"] || JSON.stringify(temp_newmod);
        temp_load_test = JSON.parse(localStorage.modOptionsLoad);
        temp_newmod_test = JSON.parse(localStorage.modOptionsNewmod);
        for (var j in temp_load) {
            Mods.modOptions[j].load = (typeof temp_load_test[j] == "boolean") ? temp_load_test[j] : temp_load[j];
            Mods.modOptions[j].newmod = (typeof temp_newmod_test[j] == "boolean") ? temp_newmod_test[j] : temp_newmod[j];
        };
    };
    Mods.modOptionsLoad();
    // This function checks or unchecks all mods in the "Load" menu based on the button clicked;
    Mods.loadModsSelectAll = function (value) {
        for (var i in Mods.modOptions) {
            getElem("checkbox_" + Mods.modOptions[i].id).checked = value;
        };
    };
    // This is called when the "Load Selected" button is pressed. It checks to be sure that the mod isn't already loaded before calling the actual Mods.loadMod() function
    Mods.loadSelectedMods = function (modID) {

       if (typeof modID != "undefined" && Mods.modOptions[modID] != undefined) {
            if (!Mods.modOptions[modID].loaded) {
                Mods.failedLoad(modID);
                Timers.set("failed_load_mods", function () {Mods.failedLoad(true)}, 1500);
                Mods.loadMod(modID);
                Mods.modOptions[modID].loaded = true;
                var mod = capitaliseFirstLetter(modID);
                Mods.loadedMods.push(mod);
            };
            Mods.modOptions[modID].newmod = false;
            Mods.modOptions[modID].updated = false;

            //IECOMPAT
            var tr = getElem("row_" + modID);
            while (tr.firstChild) {
                tr.removeChild(tr.firstChild);
            }
            var newtd = createElem("td", tr, {
                style: "padding-top: 8px; padding-bottom: 0px; font-weight: bold;",
                innerHTML: Mods.modOptions[modID].name + "<span style='color:#FFFFFF; font-weight:normal;'>" + (Mods.modOptions[modID].loaded ? " (loaded)" : "") + (Mods.modOptions[modID].newmod ? " ** New **" : Mods.modOptions[modID].updated ? " ** Updated **" : "") + "</span>",
                setAttributes: {
                    colSpan: "3"
                }
            });
            getElem("mods_menu_newmods").style.display = "none";
            getElem("mods_menu_updated").style.display = "none";
            var temp_load = JSON.parse(localStorage.modOptionsLoad);
            var temp_newmods = JSON.parse(localStorage.modOptionsNewmod);
            temp_load[modID] = true;
            temp_newmods[modID] = false;
            localStorage["modOptionsLoad"] = JSON.stringify(temp_load);
            localStorage["modOptionsNewmod"] = JSON.stringify(temp_newmods);
        } else {
            var temp_load = {};
            var temp_newmods = {};
            for (var i in Mods.modOptions) {
                if (getElem("checkbox_" + Mods.modOptions[i].id).checked && Mods.loadedMods.indexOf(capitaliseFirstLetter(i)) == -1 && (!Mods.modOptions[i].requires || Mods.loadedMods.indexOf(Mods.modOptions[i].requires) != -1)) {
                    Mods.failedLoad(i)
                    Timers.set("failed_load_mods", function () {Mods.failedLoad(true)}, 1500)
                    Mods.loadMod(Mods.modOptions[i].id);
                    Mods.modOptions[i].loaded = true;
                    var mod = capitaliseFirstLetter(i);
                    Mods.loadedMods.push(mod);
                };
                temp_load[Mods.modOptions[i].id] = getElem("checkbox_" + Mods.modOptions[i].id).checked;
                Mods.modOptions[i].newmod = false;
                temp_newmods[Mods.modOptions[i].id] = false;
                Mods.modOptions[i].updated = false;

                var tr = getElem("row_" + Mods.modOptions[i].id);
                while (tr.firstChild) {
                    tr.removeChild(tr.firstChild);
                }
                var newtd = createElem("td", tr, {
                    style: "padding-top: 8px; padding-bottom: 0px; font-weight: bold;",
                    innerHTML: Mods.modOptions[i].name + "<span style='color:#FFFFFF; font-weight:normal;'>" + (Mods.modOptions[i].loaded ? " (loaded)" : "") + (Mods.modOptions[i].newmod ? " ** New **" : Mods.modOptions[i].updated ? " ** Updated **" : "") + "</span>",
                    setAttributes: {
                        colSpan: "3"
                    }
                });
            };
            getElem("mods_menu_newmods").style.display = "none";
            getElem("mods_menu_updated").style.display = "none";
            localStorage["modOptionsLoad"] = JSON.stringify(temp_load);
            localStorage["modOptionsNewmod"] = JSON.stringify(temp_newmods);
        };

    };

    // This function will call to load the mods and will load them based on the player's choices. There will also be protections against loading a mod more than once;
    Mods.loadMod = function (modID) {
        if (!Mods.modOptions[modID].loaded) {
            Load[modID]();
            Mods.modOptions[modID].loaded = true;
            Mods.modOptionsOptionsDisplay(modID);
        }
    };

    // These functions will load the menus for "Load" and "Options" when those buttons are clicked;
    Mods.loadModMenu_options = function () {
        getElem("mod_load_options").style.display = "none";
        getElem("mod_load_mods_options").style.display = "none";
        getElem("mod_options_options").style.display = "block";
        getElem("mod_options_mods_options").style.display = "block";
        Mods.modOptionsOptionsDisplay("expbar");
    };
    Mods.loadModMenu_load = function () {
        getElem("mod_load_options").style.display = "block";
        getElem("mod_load_mods_options").style.display = "block";
        getElem("mod_options_options").style.display = "none";
        getElem("mod_options_mods_options").style.display = "none";
    };

    // This function shows the "options" of the currently selected mod under Mod Options;
    Mods.modOptionsOptionsDisplay = function (modID) {
        for (var i in Mods.modOptions) {
            getElem("mod_options_options_" + Mods.modOptions[i].id).style.display = "none";
        };
        getElem("mod_options_options_" + modID).style.display = "block";
        if (Mods.modOptions[modID].loaded) {
            getElem("mod_options_options_" + modID + "_loaded").style.display = "block";
            getElem("mod_options_options_" + modID + "_notloaded").style.display = "none";
        } else {
            getElem("mod_options_options_" + modID + "_loaded").style.display = "none";
            getElem("mod_options_options_" + modID + "_notloaded").style.display = "block";
        }
    };

    // This function will check new mods if "Always enable new mods" is checked;
    Mods.loadNewMods = function () {
        var checked = getElem("checkbox_enable_newmods").checked;
        if (checked) {
            for (var i in Mods.modOptions) {
                if (Mods.modOptions[i].newmod) getElem("checkbox_" + Mods.modOptions[i].id).checked = true;
            }
        }
        localStorage.loadNewMods = JSON.stringify(checked);
    };

    // This function creates the Mods Info "Load" menu;
    Mods.loadModOptions = function () {
        createElem("div", wrapper, {
            id: "mods_form",
            className: "menu scrolling_allowed",
            style: "z-index: 99999; position: absolute; left: 50%; top: 50%; width: 400px; margin-left: -200px; height: 300px; margin-top: -150px; overflowY: auto; overflow-x: hidden",
            innerHTML: "<span id='mods_form_top' class='common_border_bottom' style='margin-bottom:4px;'><span style='float:left; font-weight: bold; color:#FFFF00; margin-bottom:3px;'>Mods Info</span><span id='mods_menu_load' class='common_link' onclick='javascript:Mods.loadModMenu_load();' style='float:left; margin:0px; margin-left:42px;'>Load Mods</span><span id='mods_menu_options' class='common_link' onclick='javascript:Mods.loadModMenu_options(); 'style='float:left; margin:0px; margin-left:46px;'>Mod Options</span><span id: 'mod_options_close' class='common_link' style='margin: 0px; margin-bottom: 2px;' onclick='javascript:addClass(getElem(&apos;mods_form&apos;),&apos;hidden&apos;);'>Close</span></span>"
        });

        // This first set is for the "Load" menu,
        createElem("div", "mods_form", {
            id: "mod_load_mods_options",
            className: "common_border_bottom",
            style: "width: 100%; height: 24px; margin-bottom: 5px; font-size: .8em; display: block",
            innerHTML: "<button id='mods_menu_load_all' class='market_select pointer' onclick='javascript:Mods.loadModsSelectAll(true);' style='float:left; margin:0px; margin-left:5px;'>Select All</button><button id='mods_menu_load_none' class='market_select pointer' onclick='javascript:Mods.loadModsSelectAll(false);' style='float:left; margin:0px; margin-left:6px;'>Select None</button><button id ='mods_menu_load_selected' class='market_select pointer' style='margin: 0px; margin-bottom: 2px;' onclick='javascript:Mods.loadSelectedMods();'>Load Selected</button>"
        });
        createElem("div", "mods_form", {
            id: "mod_load_options",
            className: "scrolling_allowed",
            style: "display: block",
            innerHTML: "<span style='color:yellow; font-size:.8em; font-weight:bold; padding-left:4px;'>Dendrek &amp; WitWiz Mods Pack version " + Mods.version + "</span><table id='mod_options_table' cellspacing='0' style='font-size: 0.8em; width:100%; margin-top:5px;'><tr id='mods_menu_newmods' style='color:#00FF00; font-weight:bold; display:none;'><td colspan='3'>** New Mod(s) Available! **<span style='color:#FFFFFF; float:right; font-weight:normal; margin-top:2px;'>Always enable new mods</span><input type='checkbox' id='checkbox_enable_newmods' style='width:.8em; height:.8em; float:right;' onclick='javascript:Mods.loadNewMods();'></td></tr><tr id='mods_menu_updated' style='color:#00FF00; font-weight:bold; display:none;'><td colspan='3'>** Mod(s) Updated! ** &nbsp;&nbsp;&nbsp;<span style='font-weight:normal; color:#FFFFFF; float:right;'>(reload your browser to enable the updates)</span></td></tr></table>"
        })

        // This second set is for the "Options" menu,
        createElem("div", "mods_form", {
            id: "mod_options_options",
            className: "common_border_right",
            style: "width: 26%; padding-bottom: 4px; font-size: .8em; float: left; display: none; border-top: 1px solid #666666; border-right: 1px solid #666666; border-bottom: 1px solid #666666"
        });
        createElem("div", "mods_form", {
            id: "mod_options_mods_options",
            className: "scrolling_allowed",
            style: "width: 72.4%; min-height: 100%; margin-left: 1%; display: none; float: left; border-top: 1px solid #666666; border-left: 1px solid #666666; border-bottom: 1px solid #666666"
        });
        createElem("span", "mod_options_options", {
            id: "mod_options_name_title",
            style: "size: .8em; display: inline-block; float: left; clear: left; margin: 0px; margin-bottom: 14px; width: 98px; padding-top: 10px; padding-left: 6px; font-weight: bold; color: yellow",
            innerHTML: "Mod Options"
        });

        var shade = true;
        var bgcolor = "";// this will be used to color the background of every other mod in the load mod's menu.
        // This for statement is used to create the rows/columns in the table;
        for (var i in Mods.modOptions) {

            // These two if statements are used to determine if the warnings that there are either new or updated mods should appear;
            if (Mods.modOptions[i].newmod) getElem("mods_menu_newmods").style.display = "";
            if (Mods.modOptions[i].updated && !Mods.modOptions[i].newmod) getElem("mods_menu_updated").style.display = "";

            shade = false;
            if (shade) bgcolor = "#242424";
            else  bgcolor = "";

            // This object is used to make loading these rows/columns easier to manage;
            var tr;

            // This first set is for the "Load" table;
            //row1
            createElem("tr", "mod_options_table", {
                id: "row_" + Mods.modOptions[i].id,
                style: "font-size: 1em; color: #FF0; background-color: " + bgcolor
            });
            createElem("td", "row_" + Mods.modOptions[i].id, {
                colSpan: "3",
                style: "font-weight: bold; padding: 6px 6px 0px 6px; border-top: 1px solid #666;",
                innerHTML: Mods.modOptions[i].name + "<span style='color:#FFFFFF; font-weight:normal;'>" + (Mods.modOptions[i].loaded ? " (loaded)" : "") + (Mods.modOptions[i].newmod ? " ** New **" : Mods.modOptions[i].updated ? " ** Updated **" : "") + "</span>"
            });

            var r2t1 = "r2_td1_" + Mods.modOptions[i].id;
            var d_r2t1 = "getElem(&apos;" + r2t1 + "&apos;).style.paddingBottom";
            var r2t2 = "r2_td2_" + Mods.modOptions[i].id;
            var d_r2t2 = "getElem(&apos;" + r2t2 + "&apos;).style.paddingBottom";
            var r3t2 = "row3_" + Mods.modOptions[i].id;
            var d_r3t2 = "getElem(&apos;" + r3t2 + "&apos;).style.display";

            //row2
            createElem("tr", "mod_options_table", {
                id: "row2_" + Mods.modOptions[i].id,
                style: "font-size: 1em; color: #FFF; font-weight: normal; background-color: " + bgcolor
            });
            createElem("td", "row2_" + Mods.modOptions[i].id, {
                id: "r2_td1_" + Mods.modOptions[i].id,
                style: "width: 15px; padding: 0px 0px 6px 6px;",
                innerHTML: "<input id='checkbox_" + Mods.modOptions[i].id + "' type='checkbox' style='width:.8em; height:.8em; margin-right:6px;'>"
            });
            createElem("td", "row2_" + Mods.modOptions[i].id, {
                id: "r2_td2_" + Mods.modOptions[i].id,
                colSpan: "2",
                style: "padding: 0px 0px 6px 6px;",
                innerHTML: Mods.modOptions[i].description1 + "<span class='common_link' onclick='javascript: ( " + d_r3t2 + " == &apos;none&apos; ) ? ( " + d_r3t2 + " = &apos;&apos;, " + d_r2t1 + " = " + d_r2t2 + " = &apos;0px&apos; ) : ( " + d_r3t2 + " = &apos;none&apos;, " + d_r2t1 + " = " + d_r2t2 + " = &apos;6px&apos; );' style='float:right; margin:0px;'>(more info)</span>"
            });

            //row 3
            createElem("tr", "mod_options_table", {
                id: "row3_" + Mods.modOptions[i].id,
                style: "color: #FFF; background-color: " + bgcolor + "; display: none;"
            });
            createElem("td", "row3_" + Mods.modOptions[i].id);
            createElem("td", "row3_" + Mods.modOptions[i].id, {
                colSpan: "2",
                style: "padding: 0px 0px 6px 0px;",
                innerHTML: Mods.modOptions[i].description2
            });

            // This second set is for the "Options" column;
            createElem("span", "mod_options_options", {
                id: "mod_options_name_" + Mods.modOptions[i].id,
                className: "common_link",
                style: "size: .8em; display: inline-block; float: left; clear: left; margin: 0px; padding: 8px 0px 10px 6px; width: 90%; border-top: 1px solid #666;",
                innerHTML: Mods.modOptions[i].shortname,
                onclick: function () { javascript: Mods.modOptionsOptionsDisplay(Mods.modOptions[i].id); }
            });
            createElem("div", "mod_options_mods_options", {
                id: "mod_options_options_" + Mods.modOptions[i].id,
                style: "padding-top: 10px; padding-left: 5%; width: 95%; height: 95%; font-size: .8em; display: none;",
                innerHTML: "<span style='color:yellow; font-weight:bold; float:left;'>" + Mods.modOptions[i].name + " Options</span><span id='mod_options_options_" + Mods.modOptions[i].id + "_notloaded' style='margin-top:41px; width:100%; float:left; clear:left;'><span style='float:left; margin-bottom:4px; text-align:center; width:91%; '>The " + Mods.modOptions[i].name.toLowerCase() + " mod is not loaded...</span><button id='mod_options_options_load_" + Mods.modOptions[i].id + "' class='market_select pointer' type='button' style='font-size:1.25em; margin:0px; width:80px; left:45%; margin-left:-40px; float:left; clear:left; display:block; position:relative;' onclick='javascript:Mods.loadSelectedMods(&apos;" + i + "&apos;);'>Load Mod</button></span><span id='mod_options_options_" + Mods.modOptions[i].id + "_loaded' style='margin-top:6px; margin-bottom:6px; width:100%; float:left; clear:left; display:none;'><table id='mod_options_options_" + Mods.modOptions[i].id + "_table' style='font-size: 0.8em; width:100%;'><tr><td style='width:17px;'></td><td style='width:17px;'></td><td style='width:17px;'></td><td></td></table></span>"
            });

            // Switches the shade value of the rows;
            shade = !shade;

            // This checks the load-mod checkboxes if the user's preferences show that they should be;
            if (getElem("checkbox_enable_newmods").checked && Mods.modOptions[i].newmod) Mods.modOptions[i].load = true;
            getElem("checkbox_" + Mods.modOptions[i].id).checked = Mods.modOptions[i].load;
        };
        // This for/if combination makes sure that only one of the Mod Options "options" are displayed.
        var optNamesOptionsDisplay = false;
        for (var i in Mods.modOptions) {
            if (optNamesOptionsDisplay) getElem("mod_options_options_" + Mods.modOptions[i].id).style.display = "none";
            if (getElem("mod_options_options_" + Mods.modOptions[i].id).style.display == "block") optNamesOptionsDisplay = true;
        }
        if (!optNamesOptionsDisplay) getElem("mod_options_options_expbar").style.display = "block";

        // Makes sure the wiki menu appears if it's loaded;
        if (Mods.modOptions.wikimd.loaded) {
            getElem("mods_form_top").innerHTML = "<span style='float:left; font-weight: bold; color:#FFFF00; margin-bottom:3px;'>Mods Info</span><span id='mods_menu_load' class='common_link' onclick='javascript:Mods.loadModMenu_load();' style='float:left; margin:0px; margin-left: 42px;'>Load</span><span id='mods_menu_options' class='common_link' onclick='javascript:Mods.loadModMenu_options(); ' style='float:left; margin:0px; margin-left: 45px;'>Options</span><span id='mods_menu_load' class='common_link' onclick='javascript:Mods.loadModMenu_wiki();' style='float:left; margin:0px; margin-left: 41px;'>Wiki</span><span id='mod_options_close' class='common_link' style='margin: 0px; margin-bottom: 2px;' onclick='javascript:addClass(getElem(&apos;mods_form&apos;),&apos;hidden&apos;);'>Close</span>";
            getElem("mods_form").style.width = "464px";
            getElem("mods_form").style.marginLeft = "-225px";
            Mods.Wikimd.loadDivs();
            Mods.loadModMenu_options = function () {
                getElem("mod_load_options").style.display = "none";
                getElem("mod_load_mods_options").style.display = "none";
                getElem("mod_options_options").style.display = "block";
                getElem("mod_options_mods_options").style.display = "block";
                getElem("mod_wiki_options").style.display = "none";
                getElem("mod_wiki_mods_options").style.display = "none";
                Mods.modOptionsOptionsDisplay("expbar");
            };
            Mods.loadModMenu_load = function () {
                getElem("mod_load_options").style.display = "block";
                getElem("mod_load_mods_options").style.display = "block";
                getElem("mod_options_options").style.display = "none";
                getElem("mod_options_mods_options").style.display = "none";
                getElem("mod_wiki_options").style.display = "none";
                getElem("mod_wiki_mods_options").style.display = "none";
            };
            Mods.loadModMenu_wiki = function () {
                getElem("mod_load_options").style.display = "none";
                getElem("mod_load_mods_options").style.display = "none";
                getElem("mod_options_options").style.display = "none";
                getElem("mod_options_mods_options").style.display = "none";
                getElem("mod_wiki_options").style.display = "block";
                getElem("mod_wiki_mods_options").style.display = "block";
            };
        };

        // Disable the Options menu for now;
        if (disable_options) {
            getElem("mods_menu_options").className = "";
            getElem("mods_menu_options").onclick = function () { };
            getElem("mods_menu_options").style.fontWeight = "bold";
            getElem("mods_menu_options").style.color = "#999999";
        };
    };

    Mods.initializeOptionsMenu = function () {
        var id = "";
        var opt = Mods.modOptions;
        var option = "";
        var pre_colspan = 0;
        for (var a in opt) {
            if (opt[a].options != undefined) {
                var opt1 = opt[a].options;
                for (var b in opt1) {
                    if (opt1[b].id != undefined) {
                        id = a + "_" + opt1[b].id;
                        option = opt1[b];
                        pre_colspan = 0;
                        Mods.populateOptionsMenu(id, option, pre_colspan, opt[a].id);
                        if (opt1[b].options != undefined) {
                            var opt2 = opt1[b].options;
                            for (var c in opt2) {
                                if (opt2[c].id != undefined) {
                                    id = a + "_" + b + "_" + opt2[c].id;
                                    option = opt2[c];
                                    pre_colspan = 1;
                                    Mods.populateOptionsMenu(id, option, pre_colspan, opt[a].id);
                                    if (opt2[c].options != undefined) {
                                        var opt3 = opt2[c].options;
                                        for (var d in opt3) {
                                            if (opt3[d].id != undefined) {
                                                id = a + "_" + b + "_" + c + "_" + opt3[d].id;
                                                option = opt3[d];
                                                pre_colspan = 2;
                                                Mods.populateOptionsMenu(id, option, pre_colspan, opt[a].id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                id = a + "_0options";
                option = null;
                pre_colspan = 0;
                Mods.populateOptionsMenu(id, option, pre_colspan, opt[a].id);
            }
        }
    };

    Mods.populateOptionsMenu = function (id, option, pre_colspan, mod) {
        if (option != null) {
            var type = option.type;
            var tr_id = "mod_options_options_row_" + id;
            var type_id = "id='mod_options_options_" + id + "' ";
            var type_open = "<" + type.createElement + (option.value != undefined ? " value='" + option.value + "' " : " ");
            var type_class = type["class"] != undefined ? "class='" + type["class"] + "' " : "";
            var type_style = "style='' ";
            var type_click = option.onclick != undefined ? "onclick='" + option.onclick + "'>" : "'>";
            var type_close = type.closeElement != undefined ? "</" + type.closeElement + "></td>" : "</td>";
            var type_html = "";
            var span_html = "";
            var info_click = "";
            var descript_id = "mod_options_options_row2_" + id + (option.value != undefined ? "_" + option.value : "");
            var descript_html = "";
            var colspan = 4;
            var opt_colspan = 0;
            var pst_colspan = 0;
            for (var i in type.style) type_style = type_style.slice(0, -2) + i + ":" + type.style[i] + ";' ";
            if (type.opt_span == "all") opt_colspan = colspan - pre_colspan;
            else {
                opt_colspan = 1;
                pst_colspan = colspan - (pre_colspan + opt_colspan);
            };
            if (option.description != undefined) {
                info_click = "<span class='common_link' onclick='javascript:getElem(&apos;" + descript_id + "&apos;).style.display = getElem(&apos;" + descript_id + "&apos;).style.display == &apos;none&apos;? &apos;&apos;:&apos;none&apos;' style='float:right; margin:0px;'>(more info)</span></td>";
                descript_html = "<td colspan='" + pre_colspan + "'></td><td colspan='" + (colspan - pre_colspan) + "'><span>" + option.description + "</span></td>";
            };
            if (type.type == "text") type_html = option.name + info_click;
            else if (type.type == "button") {
                type_html = option.name;
                span_html = info_click + "</td>";
                type_close = type.closeElement != undefined ? "</" + type.closeElement + ">" : "";
            } else if (option.border != undefined) span_html = "<div style='height:10px; width:40px; margin-top:1px; border:1px solid " + option.border + ";'></div>";
            else if (option.background != undefined) span_html = "<div style='height:11px; width:40px; margin-top:1px; background:" + option.border + ";'></div>";
            else span_html = "<td colspan='" + pst_colspan + "'><span>" + option.name + "</span>" + info_click;
            if (pre_colspan > 0) pre_colspan = "<td colspan='" + pre_colspan + "'><td colspan='" + opt_colspan + "'>";
            else pre_colspan = "<td colspan='" + opt_colspan + "'>";
        } else {
            var tr_id = "mod_options_options_row_" + id;
            var type_id = "mod_options_options_" + id;
            var type_open = "<span ";
            var type_class = "";
            var type_style = "style='' ";
            var type_click = ">";
            var type_close = "</span></td>";
            var type_html = "This mod does not have any options that can be changed.";
            var span_html = "";
            var info_click = "";
            var descript_id = "mod_options_options_row2_" + id;
            var descript_html = "";
            var pre_colspan = "<td colspan='4'>";
        };
        var tr_optOptions = document.createElement("tr");
        tr_optOptions.id = tr_id;
        tr_optOptions.innerHTML = pre_colspan + type_open + type_id + type_class + type_style + type_click + type_html + type_close + span_html;
        tr_optOptions.style.marginTop = "12px";
        getElem("mod_options_options_" + mod + "_table").appendChild(tr_optOptions);
        var tr2_optOptions = document.createElement("tr");
        tr2_optOptions.id = descript_id;
        tr2_optOptions.innerHTML = descript_html;
        tr2_optOptions.style.display = "none";
        getElem("mod_options_options_" + mod + "_table").appendChild(tr2_optOptions);
        var tr3_optOptions = document.createElement("tr");
        tr3_optOptions.innerHTML = "&nbsp;";
        tr3_optOptions.style.fontSize = ".3em";
        getElem("mod_options_options_" + mod + "_table").appendChild(tr3_optOptions);
    };

    Mods.loadOptionsMenu = function (modID) {
        var checkall = true;
        if (Mods.modOptions[modID] != undefined) { checkall = false };
        for (var a in Mods.modOptions) {
            var opt = Mods.modOptions[a];
            if ((!checkall && a == modID || checkall) && opt.options != undefined && opt.loaded) {
                var opt1 = opt.options;
                var options1 = JSON.parse(localStorage[a + "_options"]);
                for (var b in opt1) {
                    if (opt1[b].type.type == "radio") {
                        getElem("mod_options_options_" + a + "_" + opt1[b].id).value = options1[a][opt1[b].id] || 0;
                        Mods.modOptions_options(a, opt1[b].id);
                    } else if (opt1[b].type.type == "checkbox") {
                        getElem("mod_options_options_" + a + "_" + opt1[b].id).checked = options1[a][opt1[b].id] || false;
                        Mods.modOptions_options(a, opt1[b].id);
                    }
                    if (opt1.options != undefined) {
                        var opt2 = opt1.options;
                        for (var c in opt2) {
                            if (opt2[c].type.type == "radio") {
                                getElem("mod_options_options_" + a + "_" + b + "_" + opt2[c].id).value = options1[a][b][opt2[c].id] || 0;
                                Mods.modOptions_options(a, opt1[b].id, opt2[c].id);
                            } else if (opt2[c].type.type == "checkbox") {
                                getElem("mod_options_options_" + a + "_" + b + "_" + opt2[c].id).checked = options1[a][b][opt2[c].id] || false;
                                Mods.modOptions_options(a, opt1[b].id, opt2[c].id);
                            }
                        }
                    }
                }
            }
        }
    };

    Mods.modOptions_options = function (modID, opt1, opt2) {

    };

    Load.variables();
    Load.functions();
};

Load.functions = function () {

	Mods.consoleLog = function(input) {
		// replacement for Mods.consoleLog function so that it's not used on devices that don't support it
		if (!iOS && !Android) console.log(input)
	}
    
	Mods.failedLoad = function (modID) {
        if (modID === true) {
            var loadAll = true;
            var text = "";
            for (var a in Mods.failedToLoad) if (modOptions[a]) {
                loadAll = false;
                text += modOptions[a].name + ", ";
            }
            if (loadAll) text = "Mods loaded and ready: RPG MO Dendrek and WitWiz Mods Pack version " + Mods.version;
            else text = "Mod failed to load: " + text.slice(0, -2) + ". Please inform the mod developers (Dendrek or WitWiz). Try reloading the game and do not load this mod until this issue can be fixed.";
            addChatText(text, void 0, COLOR.TEAL);
        } else Mods.failedToLoad[modID] = 1;
    }

    Mods.timestamp = function (mod) {
        delete Mods.failedToLoad[mod];
        Mods.consoleLog(mod + " loaded (" + Math.round((timestamp() - modOptions[mod].time) / 10) / 100 + "s)")
    };

    Mods.findWithAttr = function (array, attr, value) {
        for (var i in array) if (array[i][attr] == value) return i;
    };

    Chat.set_hidden = function () {
        if (loadedMods.indexOf("Chatmd") != -1 && Mods.Chatmd.set_hidden() == false) return false;
        if (Mods.set_hidden() == false) return false;
        if (loadedMods.indexOf("Tabs") != -1) {
            var tabs = getElem("tabs");
            tabs.style.visibility = "hidden";
            getElem("chat_resize").style.visibility = "hidden";
        }
    }

    Mods.cleanText = function (text, twice) {
        if (twice) text = text.replace(/['"]/g,"*");
        else text = text.replace(/'/g,"&apos;").replace(/"/g,"&quot;");
        return text
    };

    Mods.timeConvert = function (input, delta, base) {
        // base == 60: input is in minutes; base == 3600: input is in hours; base == 1000 || base == 1/1000; input is in miliseconds; else input is in seconds.
        var input, delta, base, i, vals, text;
        vals = {year: 31536000, day: 86400, hour: 3600, minute: 60, second: 1};
        input = input > 0 ? input : 0;
        delta = delta > 0 ? delta : 0;
        input = input - delta;
        text = "";
        input = base === 1000 || base === 0.001 ? input / 1000 : base === 60 || base === 3600 ? input * base : input;
        for (i in vals) if (input / vals[i] >= 1) {
            delta = Math.floor(input / vals[i]);
            input -= delta * vals[i];
            text += delta + " " + i + sOrNoS(delta) + ", ";
        }
        text = text.slice(0, -2);
        return text;
    }

    Mods.UpdateBase = Mods.UpdateBase || updateBase;
    updateBase = function () {
        Mods.UpdateBase();
        setCanvasSize();
    }

    //reposition custom context menu in screen when falling offscreen
    Mods.CustomMenu = Mods.CustomMenu || ActionMenu.custom_create;
    ActionMenu.custom_create = function (a, b) {
        Mods.CustomMenu(a, b);
        var d = getElem("action_menu");
        if (d.offsetTop + d.offsetHeight > window.innerHeight) d.style.top = window.innerHeight - d.offsetHeight + "px";
    };

    setCanvasSize = function (a) {
        Mods.oldCanvasSize(a);
        Mods.setCanvasSize();
    };

    Mods.setCanvasSize = function () {
        for (var font = 0; font < 3; font++) Mods.fontSize[font] = Math.min(1, Math.round((1 + ((font - 1) * 0.4)) * current_ratio_y * 10) / 10) + "em";
        for (var mod in loadedMods) {
            var modID = loadedMods[mod];
            if (typeof Mods[modID] != "undefined") if (typeof Mods[modID].setCanvasSize != "undefined") Mods[modID].setCanvasSize();
        };

        //CHG: object_selector_info restyling
        getElem("object_selector_info", {
            style: {
                left: Math.ceil(320 * current_ratio_x) + "px",
                width: "20%",
                height: "auto",
                textAlign: "center"
            }
        });

        //make sure market popup is hidded if market is closed
        if (getElem("market_offer_popup") && (hasClass(getElem("market"), "hidden"))) Mods.Newmarket.hidedetails();

        //CHG: chat resize changes
        var cw = getElem("chat", {
            style: {
                maxHeight: last_updated.set_canvas_size_new_height * 80 / 100 + "px",
                height: Mods.Tabs.chat_size_percent * last_updated.set_canvas_size_new_height * 80 / 100 + "px"
            }
        });

        //CHG: tabs reposition
        var list = getElem("tabs");
        if (list != undefined) { list.style.top = cw.offsetTop - 49 + "px" };

        var resdiv = getElem("chat_resize");

        if (resdiv) resdiv.style.top = cw.offsetTop - 4 + "px";
    };

    Inventory.equip = function (a, b, d) {
        var block = Mods.inventoryEquip(a, b, d) || false;
        if (!block) return Mods.oldInventoryEquip(a, b, d);
    };

    Mods.inventoryEquip = function (a, b, d) {
        var block = false;
        var c;
        for (var mod in loadedMods) {
            var modID = loadedMods[mod];
            if (typeof Mods[modID] != "undefined" && typeof Mods[modID].inventoryEquip != "undefined") block = Mods[modID].inventoryEquip(a, b, d) === true || block === true ? true : false;
        }
        return block;
    };

    socket.on("message", function (message) {
        if (typeof message === "object" && message.action) Mods.socketOn(message.action, message.data, message)
    })
    var switchWorldBugFixOld = switchWorldBugFix;
    switchWorldBugFix = function(){
        switchWorldBugFixOld();
        socket.on("message", function (message) {
            if (typeof message === "object" && message.action) Mods.socketOn(message.action, message.data, message)
        });
    }

    document.addEventListener("keyup", function (key) {
        Mods.eventListener("keyup", key.keyCode, keyMap.action(key))
    })

    document.addEventListener("keydown", function (key) {
        Mods.eventListener("keydown", key.keyCode, keyMap.action(key))
    })

    Mods.socketOn = function (action, data, message) {
        var index;
        for (index in loadedMods)
            if (Mods[loadedMods[index]].socketOn !== undefined && Mods[loadedMods[index]].socketOn.actions.indexOf(action) !== -1)
                Mods[loadedMods[index]].socketOn.fn(action, data, message);
    }

    Mods.eventListener = function (type, keyCode, keyMap) {
        var index;
        for (index in loadedMods)
            if (Mods[loadedMods[index]].eventListener !== undefined && Mods[loadedMods[index]].eventListener.keys[type] && (Mods[loadedMods[index]].eventListener.keys[type][0] === true || Mods[loadedMods[index]].eventListener.keys[type].indexOf(keyCode) !== -1 || Mods[loadedMods[index]].eventListener.keys[type].indexOf(keyMap) != -1))
                Mods[loadedMods[index]].eventListener.fn(type, keyCode, keyMap)
    }
};

Load.variables = function () {
    ////// general options
    Mods.loadedMods = Mods.loadedMods || [];
    loadedMods = Mods.loadedMods;

    localStorage["enableNewMods"] = localStorage["enableNewMods"] || "false";
    var forge_formula = forge_formula || -1;
    Mods.disableInvClick = false;
    KEY_ACTION.CTRL = 145;
    keyMap.keys[1][17] = KEY_ACTION.CTRL;
    keyMap.keys[1][91] = KEY_ACTION.CTRL;
    Mods.oldCanvasSize = Mods.oldCanvasSize || setCanvasSize;
    Mods.oldInventoryEquip = Mods.oldInventoryEquip || Inventory.equip;
    Mods.failedToLoad = Mods.failedToLoad || {};
    Mods.set_hidden = Mods.set_hidden || Chat.set_hidden;
    Mods.fontSize = Mods.fontSize || { 0: 0.7, 1: 1, 2: 1.3 };

    ////// health
    Mods.Health = Mods.Health || {};
    Mods.Health.old_inAFight = Mods.Health.old_inAFight || BigMenu.in_a_fight;

    ////// rclick
    Mods.Rclick = Mods.Rclick || {};
    Mods.Rclick.oldActionMenu = Mods.Rclick.oldActionMenu || ActionMenu.create;
    Mods.Rclick.oldInvMenu = Mods.Rclick.oldInvMenu || InvMenu.create;

    ////// mosmob
    localStorage["infopanelmode"] = localStorage["infopanelmode"] || 0;
    Mods.Mosmob = Mods.Mosmob || {};
    Mods.regular_onmousemove = Mods.regular_onmousemove || regular_onmousemove;

    //CHG Tab intercept join/leave
    Mods.Tabs = Mods.Tabs || {};
    Mods.Newmarket = Mods.Newmarket || {};
    Mods.Newmarket.submitHolder = Mods.Newmarket.submitHolder || {};
    Mods.Newmarket.submitSorted = Mods.Newmarket.submitSorted || [];
    Mods.Newmarket.submitQueued = Mods.Newmarket.submitQueued || false;
    Mods.Newmap = Mods.Newmap || {};
    Mods.Kbind = Mods.Kbind || {};
    Mods.Tabs.oldremove_channel = Mods.Tabs.oldremove_channel || Contacts.remove_channel;
    Mods.Tabs.oldadd_channel = Mods.Tabs.oldadd_channel || Contacts.add_channel;

    Mods.Tabs.wwMaxTabs = 8;
    Mods.Tabs.wwCurrentTabs = Mods.Tabs.wwCurrentTabs || [];
    Mods.Tabs.wwTabContent = Mods.Tabs.wwTabContent || [];

    Mods.Tabs.chat_size_percent = 0.3;
    Mods.Tabs.chat_resize_timestamp = timestamp();

    ////// chestm
    localStorage["chestInv_color"] = localStorage["chestInv_color"] || JSON.stringify("#C000FF");
    localStorage["chestFav_color"] = localStorage["chestFav_color"] || JSON.stringify("#FF8000");
    localStorage["chest_colCheck"] = localStorage["chest_colCheck"] || "true";
    localStorage["chest_colCheckF"] = localStorage["chest_colCheckF"] || "true";
    localStorage["sortFav_check"] = localStorage["sortFav_check"] || "false";
    localStorage["sortInv_check"] = localStorage["sortInv_check"] || "false";
    localStorage["chestArmorPriority"] = localStorage["chestArmorPriority"] || "false";
    localStorage["chestCraftPriority"] = localStorage["chestCraftPriority"] || "false";
    localStorage["chestPricePriority"] = localStorage["chestPricePriority"] || "false";
    if (localStorage.chestPlayerPriorities && localStorage.chestPlayerPriorities == "{object Object}") delete localStorage.chestPlayerPriorities;
    localStorage["chestPlayerPriorities"] = localStorage["chestPlayerPriorities"] || JSON.stringify({});
    localStorage["avoidAll"] = localStorage["avoidAll"] || JSON.stringify({});

    Mods.Chestm = Mods.Chestm || {};
    Mods.Chestm.chest_item_id = Mods.Chestm.chest_item_id || 0;
    Mods.Chestm.tempChest = Mods.Chestm.tempChest || {};
    Mods.Chestm.inv_select_color = JSON.parse(localStorage.chestInv_color);
    Mods.Chestm.fav_select_color = JSON.parse(localStorage.chestFav_color);
    Mods.Chestm.chest_colCheck = JSON.parse(localStorage.chest_colCheck);
    Mods.Chestm.chest_colCheckF = JSON.parse(localStorage.chest_colCheckF);
    Mods.Chestm.chest_sort_hidden = true;
    Mods.Chestm.sortFav_check = JSON.parse(localStorage.sortFav_check);
    Mods.Chestm.sortInv_check = JSON.parse(localStorage.sortInv_check);
    Mods.Chestm.armorPriority = JSON.parse(localStorage.chestArmorPriority);
    Mods.Chestm.craftPriority = JSON.parse(localStorage.chestCraftPriority);
    Mods.Chestm.pricePriority = JSON.parse(localStorage.chestPricePriority);
    Mods.Chestm.playerPriorities = JSON.parse(localStorage.chestPlayerPriorities);
    Mods.Chestm.currentChestPage = 1;
    Mods.Chestm.chestArmorPriorities = {
        5: 0, // weapons;
        0: 1, // armors;
        2: 2, // jewelry;
        1: 3, // food;
        7: 4, // pets;
        6: 5, // spells;
        3: 6, // materials;
        8: 7, // house;
        4: 8 // tools;
    };
    Mods.Chestm.chestCraftPriorities = {
        3: 0, // materials;
        8: 1, // house;
        4: 2, // tools;
        7: 3, // pets;
        5: 4, // weapons;
        0: 5, // armors;
        2: 6, // jewelry;
        1: 7, // food;
        6: 8 // spells;
    };
    Mods.Chestm.materialsPriorities = {
        "Jewel": 0,
        "Cut": 1,
        "Uncut": 2,
        "ore": 3,
        "Chunk": 3,
        "Sand": 3,
        "Clay": 3,
        "Copper": 3,
        "Zinc": 3,
        "Coal": 3,
        "Bar": 4,
        "Mould": 4,
        "Log": 5,
        "Wood": 5,
        "Vial": 6,
        "Raw": 7,
        "Feather": 8,
        "Egg": 9,
        "Scale": 10,
        "Eye": 11,
        "Soil": 12,
        "Leaf": 12,
        "Seed": 12,
        "Grass": 12,
        "Hay": 12,
        "Wheat": 12,
        "Enchant": 13,
        "Teleport": 14
    };
    Mods.Chestm.ctrlPressed = false;
    Mods.Chestm.avoidAll = Mods.Chestm.avoidAll || JSON.parse(localStorage.avoidAll);


    ////// forgem
    localStorage["RECIPE_LIST"] = localStorage["RECIPE_LIST"] || JSON.stringify([]);
    localStorage["RECIPE_U_LIST"] = localStorage["RECIPE_U_LIST"] || JSON.stringify({});

    Mods.Forgem = Mods.Forgem || {};
    Mods.Forgem.toForgeItem = Mods.Forgem.toForgeItem || -1;
    //Forging var: Mods.Forgem.FORGE_MATERIALS_LIST;
    //Creates a list of each unique forging material, used later to determine if the player has the necessary materials;
    Mods.Forgem.FORGE_MATERIALS_LIST = Mods.Forgem.FORGE_MATERIALS_LIST || [];
    Mods.Forgem.FORGE_MAT_LIST = Mods.Forgem.FORGE_MAT_LIST || {};
    Mods.Forgem.RECIPE_LIST = JSON.parse(localStorage.RECIPE_LIST) || [];
    Mods.Forgem.RECIPE_U_LIST = JSON.parse(localStorage.RECIPE_U_LIST) || {};
    forge_formula = forge_formula || 0;
    Mods.Forgem.oldDrop = Mods.Forgem.oldDrop || Forge.drop;
    Mods.Forgem.oldSelect = Mods.Forgem.oldSelect || Forge.select;
    Mods.Forgem.oldOpen = Mods.Forgem.oldOpen || Forge.forge_open;

    ////// expbar
    localStorage["expSkillSet"] = localStorage["expSkillSet"] || "health";

    Mods.Expbar = Mods.Expbar || {};
    Mods.Expbar.set_skill = localStorage.expSkillSet;

    /////WIZ MODS
    localStorage["autocastenabled"] = localStorage["autocastenabled"] || JSON.stringify(1);
    var of = getElem("options_form");

    Mods.Autocast = Mods.Autocast || {};
    Mods.Autocast.lastFullCast = timestamp();
    Mods.Autocast.enabled = JSON.parse(localStorage["autocastenabled"]);
    Mods.Autocast.enabled -= 1;
    Mods.Kbind = Mods.Kbind || {};
    Mods.showBag = Mods.showBag == undefined ? false : Mods.showBag;
    Mods.Kbind.lastfoodeaten = timestamp();

    ////// expmonitor
    Mods.Expmonitor = Mods.Expmonitor || {};
    Mods.Expmonitor.XPEventSeconds = Mods.Expmonitor.XPEventSeconds || 0;

    ////// newmarket
    localStorage["announceBlock"] = localStorage["announceBlock"] || JSON.stringify([]);
    localStorage["tradechatmode"] = localStorage["tradechatmode"] || 0;
    localStorage["marketpopup"] = localStorage["marketpopup"] || false;
    Mods.Newmarket.times = Mods.Newmarket.times || JSON.parse(localStorage.announceBlock);
    Mods.Newmarket.announceList = Mods.Newmarket.announceList || {};
    Mods.Newmarket.max_lines = 2 + 2 * players[0].params.market_offers/5;
    Mods.Newmarket.announces = Mods.Newmarket.announces || {
        messages: [],
        count: 0
    };
    Mods.Newmarket.states = Mods.Newmarket.states || {};
    Mods.Newmarket.submitHolder = [];
    Mods.Newmarket.tradechatmode = JSON.parse(localStorage["tradechatmode"]);
    Mods.Newmarket.tradechatmode -= 1;
    Mods.Newmarket.infopannelmode = JSON.parse(localStorage["infopanelmode"]);
    Mods.Newmarket.infopannelmode -= 1;
    Mods.Newmarket.popup = JSON.parse(localStorage["marketpopup"]);

    ////// fullscreen
    localStorage["fullscreenenabled"] = localStorage["fullscreenenabled"] ? localStorage["fullscreenenabled"] : JSON.stringify(1);
    Mods.Fullscreen = Mods.Fullscreen || {};
    Mods.Fullscreen.iMapBegin = Mods.Fullscreen.iMapBegin || iMapBegin;
    Mods.Fullscreen.jMapBegin = Mods.Fullscreen.jMapBegin || jMapBegin;
    Mods.Fullscreen.iMapTo = Mods.Fullscreen.iMapTo || iMapTo;
    Mods.Fullscreen.jMapTo = Mods.Fullscreen.jMapTo || jMapTo;
    Mods.Fullscreen.astarsearchOld = Mods.Fullscreen.astarsearchOld || astar.search;
    Mods.Fullscreen.enabled = JSON.parse(localStorage["fullscreenenabled"]);
    Mods.Fullscreen.enabled -= 1;

    ////// petinv
    localStorage["enableShiftClick"] = localStorage["enableShiftClick"] || false;

    Mods.Petinv = Mods.Petinv || {};
    Mods.Petinv.enableShiftClick_check = JSON.parse(localStorage.enableShiftClick);
    Mods.Petinv.invSendItem = false; //if true, items are transfered to the pet inventory;
    Mods.Petinv.petInv_toggle = players[0].pet.enabled ? true : false;

    ////// magicm
    Mods.Magicm = Mods.Magicm || {};
    Mods.Magicm.enemy = Mods.Magicm.enemy || new Object();
    Mods.Magicm.magic_damage_timers = Mods.Magicm.magic_damage_timers || { 0: 0, 1: 0, 2: 0, 3: 0 };

    ////// wikimd
    Mods.Wikimd = Mods.Wikimd || {};
    Mods.Wikimd.newWikiLoad = Mods.Wikimd.newWikiLoad || {};
    Mods.Wikimd.oldWikiLoad = Mods.Wikimd.oldWikiLoad || {};
    Mods.Wikimd.item_formulas = Mods.Wikimd.item_formulas || {};
    Mods.Wikimd.item_slots = {
        0: "Helm",
        1: "Cape",
        2: "Chest",
        3: "R.Hand",
        4: "L.Hand",
        5: "Glove",
        6: "Boots",
        7: "Neck",
        8: "Ring",
        9: "none",
        10: "Magic",
        11: "Pants"
    };
    Mods.Wikimd.oldSortList = Mods.Wikimd.oldSortList || [];
    Mods.Wikimd.oldSortValue = Mods.Wikimd.oldSortValue || "";
    Mods.Wikimd.newSortValue = Mods.Wikimd.newSortValue || "";
    Mods.Wikimd.oldSort = Mods.Wikimd.oldSort || {};
    Mods.Wikimd.oldSort = {
        item: Mods.Wikimd.oldSort.item || "name",
        monster: Mods.Wikimd.oldSort.monster || "name",
        vendor: Mods.Wikimd.oldSort.vendor || "name",
        craft: Mods.Wikimd.oldSort.craft || "name",
        pet: Mods.Wikimd.oldSort.pet || "name",
        spell: Mods.Wikimd.oldSort.spell || "name",
        enchant: Mods.Wikimd.oldSort.enchant || "name"
    };
    Mods.Wikimd.formulas = Mods.Wikimd.formulas || {};
    // This is used to determine the dimensions of the tables within the wiki. "c" represents "colspan" and "r" represents "rowspan". There are 6 columns and two rows in each table. This object is used for those rows/columns that should be larger than normal.
    Mods.Wikimd.span = {
        item: {
            wiki_r1_c0: { c: 2, r: 2 },
            wiki_r1_c3: { c: 2 }
        },
        monster: {
            //wiki_r1_c0: { c: 2, r: 2 },
            wiki_r2_c1: { c: 5 }
        },
        vendor: {
            wiki_r1_c0: { c: 2 },
            wiki_r1_c1: { c: 5 }
        },
        craft: {
            wiki_r1_c0: { c: 2 },
            wiki_r2_c0: { c: 2 },
            wiki_r2_c1: { c: 5 }
        },
        pet: {
            wiki_r2_c1: { c: 5 }
        },
        spell: {
            wiki_r1_c0: { r: 2 }
        },
        enchant: {
            //wiki_r1_c0: { c: 2 },
            wiki_r2_c2: { c: 5 }
        }
    };
    Mods.Wikimd.mouse = { x: 0, y: 0 };
    Mods.Wikimd.pet_family = Mods.Wikimd.pet_family || {};
    Mods.Wikimd.family = Mods.Wikimd.family || {};
    var ClassValue = 0;
    LazyLoad.css(cdn_url+"mod.css?v="+mod_version, function () {
        Mods.consoleLog("CSS Loaded")
    });

    ////// miscmd;
    localStorage["activeQuest"] = localStorage["activeQuest"] || JSON.stringify(false);
    localStorage["penalty_bonus"] = localStorage["penalty_bonus"] || JSON.stringify("health");
    localStorage["audioVolume"] = localStorage["audioVolume"] || "50";

    Mods.Miscmd = Mods.Miscmd || {};
    Mods.Miscmd.ideath = Mods.Miscmd.ideath || {};
    Mods.Miscmd.ideath.inventory = [];
    Mods.Miscmd.ideath.oldInventoryClick = Mods.Miscmd.ideath.oldInventoryClick || inventoryClick;
    Mods.Miscmd.ideath.bgColor = "#3A6";
    Mods.Miscmd.ideath.brColor = "inherit";
    Mods.Miscmd.toolbar = Mods.Miscmd.toolbar || {};
    Mods.Miscmd.toolbar.oldDrawMap = Mods.Miscmd.toolbar.oldDrawMap || drawMap;
    Mods.Miscmd.toolbar.xpmessage = ["Current experience rate is 2x"];
    Mods.Miscmd.toolbar.ids = {
        playerLocation: "td_location",
        playerStats: "td_stats",
        questData: "td_quests",
        currentTime: "td_time",
        invSlots: "td_inventory",
        dpsinfo: "td_dpsinfo"
    };
    Mods.Miscmd.toolbar.oldInventoryAdd = Mods.Miscmd.toolbar.oldInventoryAdd || Inventory.add;
    Mods.Miscmd.toolbar.oldInventoryRemove = Mods.Miscmd.toolbar.oldInventoryRemove || Inventory.remove;
    Mods.Miscmd.toolbar.activeQuest = Mods.Miscmd.toolbar.activeQuest || JSON.parse(localStorage.activeQuest);
    Mods.Miscmd.toolbar.oldQuestsShowActive = Mods.Miscmd.toolbar.oldQuestsShowActive || Quests.show_active;
    Mods.Miscmd.penalty = Mods.Miscmd.penalty || JSON.parse(localStorage.penalty_bonus);
    Mods.Miscmd.oldPenaltyBonus = Mods.Miscmd.oldPenaltyBonus || penalty_bonus;
    Mods.Miscmd.potions = Mods.Miscmd.potions || {};
    Mods.Miscmd.adps = new Array();
    Mods.Miscmd.maxtime = 180000;
    Mods.Miscmd.avgdps = 0;
    Mods.Miscmd.maxdps = 0;
    Mods.Miscmd.avgexp = 0;
    Mods.Miscmd.dpsmode = false;
    Mods.Miscmd.lastSkill = new Object();
    Mods.Miscmd.changeVolume = Mods.Miscmd.changeVolume || {};

    ////// chatmd;
    localStorage["colorChannel"] = localStorage["colorChannel"] || JSON.stringify(false);
    localStorage["highlightFriends"] = localStorage["highlightFriends"] || JSON.stringify(true);
    localStorage["timer"] = localStorage["timer"] && typeof JSON.parse(localStorage["timer"]) == "object" && localStorage["timer"] || JSON.stringify({ start: {}, set: {} });
    localStorage["tipsenabled"] = localStorage["tipsenabled"] ? localStorage["tipsenabled"] : JSON.stringify(0);
    localStorage["enableallchatts"] = localStorage["enableallchatts"] ? localStorage["enableallchatts"] : JSON.stringify(0);

    Mods.Chatmd = Mods.Chatmd || {};
    Mods.Chatmd.addChatText = Mods.Chatmd.addChatText || addChatText;
    Mods.Chatmd.ping = 0;
    Mods.Chatmd.colors = {
        "EN": "#FFFFFF",
        "18": "#99FFC6",
        "$$": "#F2A2F2",
        "{M}": "#EAE330",
        "default": "#FFDFC0",
        "none": "#DDDD69"
    };
    Mods.Chatmd.default_channels = {
        "$$": 1,
        "18": 1,
        "BR": 1,
        "CZ": 1,
        "DE": 1,
        "EN": 1,
        "ES": 1,
        "ET": 1,
        "FI": 1,
        "FR": 1,
        "HU": 1,
        "IT": 1,
        "JP": 1,
        "KO": 1,
        "NL": 1,
        "NO": 1,
        "PL": 1,
        "RO": 1,
        "RU": 1,
        "TH": 1,
        "TR": 1,
        "TW": 1
    };
    Mods.Chatmd.runTimer = Mods.Chatmd.runTimer || JSON.parse(localStorage.timer);
    Mods.Chatmd.ModCh = Mods.Chatmd.ModCh || {};
    //Mods.Chatmd.ModCh.mods = ["margus", "katt", "kemikaalikeijo", "paxli", "roryajax", "woofy", "nox", "dendrek", "roase", "bmw9191", "witwiz", "trishula", "foxy"];
    Mods.Chatmd.ModCh.delay = Mods.Chatmd.ModCh.delay || false;
    Mods.Chatmd.ModCh.channel = "{M}";
    Mods.Chatmd.ModCh.regular_onclick = Mods.Chatmd.ModCh.regular_onclick || regular_onclick;
    chat_filters["mod"] = false;
    Mods.Chatmd.afkHolder = Mods.Chatmd.afkHolder || {};
    Mods.Chatmd.afkMessage = Mods.Chatmd.afkMessage || "";
    Mods.Chatmd.whispNames = Mods.Chatmd.whispNames || [];
    Mods.Chatmd.cycleWhisper = Mods.Chatmd.cycleWhisper || true;
    Mods.Chatmd.oldDrawObject = Mods.Chatmd.oldDrawObject || drawObject;
    Mods.Chatmd.mooDelay = Mods.Chatmd.mooDelay || {};
    Mods.Chatmd.blockCommand = false;
    Mods.Chatmd.enableallchatts = JSON.parse(localStorage["enableallchatts"]);
    Mods.Chatmd.enableallchatts -= 1;
    Mods.Chatmd.tipsenabled = JSON.parse(localStorage["tipsenabled"]);
    Mods.Chatmd.tipsenabled -= 1;


    ////// tabs
    Mods.Tabs.set_visible = Mods.Tabs.set_visible || Chat.set_visible;
    Mods.Tabs.chattoggle = Mods.Tabs.chattoggle || ChatSystem.toggle;

    ////// farming
    Mods = Mods || {};
    Mods.Farming = Mods.Farming || {};
    Mods.Farming.queue = Mods.Farming.queue || {};
    Mods.Farming.sortedQueue = Mods.Farming.sortedQueue || [];
    Mods.Farming.ctrlPressed = Mods.Farming.ctrlPressed || false;
    Mods.Farming.queueHidden = Mods.Farming.queueHidden || false;
    Mods.Farming.queuePaused = Mods.Farming.queuePaused || false;
    // holders for the old DEFAULT_FUNCTIONS for rake, seed and harvest, and holders for Inventory.equip and moveInPath.
    Mods.Farming.oldDefault = Mods.Farming.oldDefault || {rake: DEFAULT_FUNCTIONS.rake, seed: DEFAULT_FUNCTIONS.seed, harvest: DEFAULT_FUNCTIONS.harvest};
    Mods.Farming.oldInventoryEquip = Mods.Farming.oldInventoryEquip || Inventory.equip;
    Mods.Farming.oldMoveInPath = Mods.Farming.oldMoveInPath || moveInPath;
    // farming options
    localStorage["farming_options"] = localStorage["farming_options"] || JSON.stringify({});
    Mods.Farming.options = Mods.Farming.options || JSON.parse(localStorage.farming_options);
    // templates for the queue window and queue'd events
    Mods.Farming.farming_queue_template = Handlebars.compile("<span style='width: 100%; display: block; float: left; color: #FF0; font-weight: bold; border-bottom: 1px solid #DDD; padding-bottom: 5px; margin-bottom: 2px;'>Farming Queue <span class='common_link' style='margin: 0; font-weight: normal; float: right;' onclick='Mods.Farming.queueOptions(true);'>(options)</span></span><span id='mods_farming_queue' style='width: 100%; float: left; display: block; margin-bottom: 2px; padding-bottom: 2px; overflow-y: hidden;'><span style='width: 100%; float: left; display: inline-block; font-weight: bold; color: #999;'><span style='float: left;'>Action:&nbsp;&nbsp;Object</span><span style='float: right;'>Coords</span></span></span><span style='width: 100%; float: left; display: inline-block; border-bottom: 1px solid #DDD; margin-bottom: 2px; padding-bottom: 4px;'><span style='float: left;'>Queued:&nbsp;<span id='mods_farming_total'>0</span></span><span class='common_link' style='margin: 0; float: right; display: block; font-weight: normal;' onclick='Mods.Farming.cancelQueue()'>(clear)</span></span><span style='color: #FF0;'>Action: <span id='mods_farming_action' style='color: #FFF; font-weight: normal;'>Active</span></span><span id='farming_queue_button' class='common_link' style='margin: 0; float: right; display: block; font-weight: normal;' onclick='Mods.Farming.pauseQueue(this)'>(queue)</span>");
    Mods.Farming.farming_queue_action_template = Handlebars.compile("<span id='mods_farming_{{slot}}' style='width: 100%; float: left; display: inline-block; font-weight: normal;'><span>{{action}}:&nbsp;&nbsp;{{object}}</span><span style='float: right;'>({{i}}, {{j}})</span></span>");
    Mods.Farming.farming_queue_option_template = Handlebars.compile("<span style='color: #FF0; font-weight: bold; width: 100%; float: left; margin-bottom: 2px; padding-bottom: 5px; border-bottom: 1px solid #DDD;'>Farming Options</span><table style='color: #DDD;'><tr><td colspan='2'><div id='mods_farming_opt_hide' class='common_link' style='margin: 4px;' onclick='Mods.Farming.hideQueue()'>Hide queued window</div></td></tr><tr><td><input type='checkbox' id='mods_farming_opt_equipped' style='width: .8em; height: .8em;'></td><td><div style='margin: 3px;'>Meet requirements to queue action</div></td></tr><tr><td><input type='checkbox' id='mods_farming_opt_stop' style='width: .8em; height: .8em;'></td><td><div style='margin: 3px;'>Stop movement while queuing</div></td></tr><tr><td><input type='checkbox' id='mods_farming_opt_save' style='width: .8em; height: .8em;'></td><td><div style='margin: 3px;'>Save queue when leaving island</div></td></tr><tr><td colspan='2'><div style='margin: 3px;'>Ctrl: Toggle Queuing</div></td></tr><tr><td colspan='2'><div style='margin: 3px;'>Space: Toggle Active/Paused</div></td></tr></table>");

};

Load.rclick = function () {

    modOptions.rclick.time = timestamp();

    ActionMenu.mobDrops = function (selectedID, selectedType) {
        var itemString = "";
        // Campfires, fishing spots, altars, dungeon chests, etc.
        if (selectedType == BASE_TYPE.OBJECT) {
            var name = object_base[selectedID].name;
            addChatText(name + " drops:", void 0, COLOR.ORANGE);
            for (var result in object_base[selectedID].params.results) {
                var compiledItems = {};
                var reducedRate, drops, name;
                var average_xp = 0;
                reducedRate = 0;
                drops = object_base[selectedID].params.results[result].returns;
                for (var i in drops) {
                    // Takes potion effects into account (checks .current instead of .level)
                    var drop_chance =
                            drops[i].chance ||
                            skills[0][object_base[selectedID].params.results[result].skill].current >= drops[i].level && drops[i].max_chance != undefined && Math.min(drops[i].base_chance + (skills[0][object_base[selectedID].params.results[result].skill].current - drops[i].level) / 100, drops[i].max_chance) ||
                            Math.min(skills[0][object_base[selectedID].params.results[result].skill].current >= drops[i].level && drops[i].base_chance + (skills[0][object_base[selectedID].params.results[result].skill].current - drops[i].level) / 100, 1) ||
                            0;
                    compiledItems[item_base[drops[i].id].name] = {
                        "percent": drop_chance * (1 - reducedRate),
                        "xp": typeof drops[i].xp == "undefined" ? "" : "(" + drops[i].xp + "xp) "
                    };
                    if(typeof drops[i].xp != "undefined") // Does not calculate if xp is 0 or undefined.
                        average_xp = average_xp + drops[i].xp * drop_chance * (1 - reducedRate);
                    reducedRate = reducedRate + compiledItems[item_base[drops[i].id].name].percent;
                }
                var trate = 0;
                for (var i in compiledItems) {
                    itemString = itemString + i + " " + Math.round(compiledItems[i].percent * 10000) / 100 + "% " + compiledItems[i].xp + "- ";
                    trate = trate + compiledItems[i].percent;
                };
                if (Math.round((1 - trate) * 10000) / 100 > 0)
                    itemString = itemString + "No loot " + Math.round((1 - trate) * 10000) / 100 + "%";
                else
                    itemString = itemString.slice(0, -3);
                if(average_xp > 0) {
                    average_xp = Math.round(100*average_xp)/100;
                    itemString = itemString + "; avg: " + average_xp + "xp";
                }
                itemString = itemString + ". ";
                addChatText(itemString, void 0, COLOR.WHITE);
                itemString = "";
            }
        }
        // Shops and Mobs
        else if (selectedType == BASE_TYPE.NPC) {
            if (npc_base[selectedID].type == OBJECT_TYPE.SHOP) {
                var compiledItems = {};
                var name = npc_base[selectedID].name;
                for (var content in npc_base[selectedID].temp.content) {
                    var item = npc_base[selectedID].temp.content[content];
                    if (item.count > 0)
                        compiledItems[item_base[item.id].name] = {
                            "stock": item.count
                        };
                };
                for (var i in compiledItems)
                    itemString = itemString + i + " (" + compiledItems[i].stock + ") - ";
                if (itemString === "")
                    itemString = "Nothing.";
                else
                    itemString = itemString.slice(0, -3) + ".";
                addChatText(name + " sells:", void 0, COLOR.ORANGE);
                addChatText(itemString, void 0, COLOR.WHITE);
            }
            else {
                var compiledItems = {};
                var average_xp = 0;
                var reducedRate = 0;
                var drops = npc_base[selectedID].params.drops;
                var name = npc_base[selectedID].name;
                for (var i in drops) {
                    var drop_chance =
                            drops[i].chance ||
                            skills[0][object_base[selectedID].params.results[0].skill].level >= drops[i].level && drops[i].max_chance != undefined && Math.min(drops[i].base_chance + (skills[0][object_base[selectedID].params.results[0].skill].level - drops[i].level) / 100, drops[i].max_chance) ||
                            Math.min(skills[0][object_base[selectedID].params.results[0].skill].level >= drops[i].level && drops[i].base_chance + (skills[0][object_base[selectedID].params.results[0].skill].level - drops[i].level) / 100, 1) ||
                            0;
                    compiledItems[item_base[drops[i].id].name] = {
                        "percent": drop_chance * (1 - reducedRate),
                        "xp": typeof drops[i].xp == "undefined" ? "" : "(" + drops[i].xp + "xp) "
                    };
                    if(typeof drops[i].xp != "undefined") // Does not calculate if xp is 0 or undefined.
                        average_xp = average_xp + drops[i].xp * drop_chance * (1 - reducedRate);
                    reducedRate = reducedRate + compiledItems[item_base[drops[i].id].name].percent;
                }
                var trate = 0;
                for (var i in compiledItems) {
                    itemString = itemString + i + " " + Math.round(compiledItems[i].percent * 10000) / 100 + "% " + compiledItems[i].xp + "- ";
                    trate = trate + compiledItems[i].percent;
                };
                if (Math.round((1 - trate) * 10000) / 100 > 0)
                    itemString = itemString + "No loot " + Math.round((1 - trate) * 10000) / 100 + "%";
                else
                    itemString = itemString.slice(0, -3);
                addChatText(name + " drops:", void 0, COLOR.ORANGE);
                if(average_xp > 0) {
                    average_xp = Math.round(100*average_xp)/100;
                    itemString = itemString + "; avg: " + average_xp + "xp";
                }
                itemString = itemString + ". ";
                addChatText(itemString, void 0, COLOR.WHITE);
            }
        }
    };

    // Prints a report of expected combat outcomes with the selected mob;
    ActionMenu.combatCheck = function (mobID) {
        var blk = 0;
        var dmg = 0;
        var Ave_Damage = 0;
        var Zero_Count = 0;
        var str = 0;
        var o_def = 0;
        var acc = 0;
        var Pl_aveDmg = 0;
        var Pl_zroCnt = 0;
        var Pl_maxDmg = 0;
        var En_aveDmg = 0;
        var En_zroCnt = 0;
        var En_maxDmg = 0;
        // This for() is being used just to repeat the function, first for the player (i == 0) then for the enemy (i == 1);
        for (var i = 0; i < 2; i++) {
            if (i == 0) {
                str = players[0].temp.total_strength;
                o_def = npc_base[mobID].temp.total_defense;
                acc = players[0].temp.total_accuracy;
            };
            if (i == 1) {
                str = npc_base[mobID].temp.total_strength;
                o_def = players[0].temp.total_defense;
                acc = npc_base[mobID].temp.total_accuracy;
            };
            dmg = Math.ceil(str / 5);
            blk = Math.max(Math.ceil(o_def - acc), 0);
            if (blk == 0) {
                Ave_Damage = 0.5 * dmg + 1;
                Zero_Count = 0;
            } else if ((blk - 1) / dmg > 1) {
                Ave_Damage = (Math.pow(dmg, 2) + 3 * dmg + 3) / (6 * blk);
                Zero_Count = 1 - (dmg + 2) / (2 * blk);
            } else { //((blk - 1)/dmg < 1) things become more complicated
                Ave_Damage = 1/2 * dmg + 1 - 1/2 * blk - Math.pow(1 - blk, 3) / (6 * dmg * blk);
                Zero_Count = (Math.pow(blk, 2) - 2 * blk + 1) / (2 * blk * dmg);
            };
            if (i == 0) {
                Pl_aveDmg = Ave_Damage;
                Pl_zroCnt = Zero_Count;
                Pl_maxDmg = dmg + 1;
            };
            if (i == 1) {
                En_aveDmg = Ave_Damage;
                En_zroCnt = Zero_Count;
                En_maxDmg = dmg + 1;
            };
        };

        var spell_slots = players[0].params.magic_slots;
        var total_magic_damage = 0;
        var max_magic_damage = 0;
        var staff_cooldown = 1 - players[0].params.cooldown;
        var secperround = 3;
        var enemy_magic_block_lets_through = 1-((npc_base[mobID].temp.magic_block || 0)/100);
        if (spell_slots > 0) {
            //magic stats
            var mag_msg = "";
            for (var sp = 0; sp < spell_slots; sp++) {
                if (sp < players[0].params.magics.length) {
                    var curr_spell = players[0].params.magics[sp].id;
                    var hit_coeff = Math.min(1, (players[0].temp.magic / 1.2 + skills[0].magic.current + Magic[curr_spell].params.penetration) / (npc_base[mobID].temp.total_defense));
                    var magic_dmg = enemy_magic_block_lets_through * Math.round((0.5 / 1.333 + 0.25) * Magic[curr_spell].params.basic_damage * hit_coeff) / (Magic[curr_spell].params.cooldown * staff_cooldown / 1000);
                    var max_magic_hits_per_round = Math.ceil(secperround / (Magic[curr_spell].params.cooldown * staff_cooldown / 1000));
                    var magic_dmgm = enemy_magic_block_lets_through * max_magic_hits_per_round * Math.round((1 / 1.333 + 0.25) * Magic[curr_spell].params.basic_damage * hit_coeff);
                    mag_msg = mag_msg + Magic[curr_spell].name + " (" + Math.round(hit_coeff * 100) / 100 + ")" + ": " + Math.round(magic_dmg * 100) / 100 + "/s; ";
                    total_magic_damage += magic_dmg;
                    max_magic_damage += magic_dmgm;
                }
            }
        }

        var enemy_spell_slots = npc_base[mobID].temp.magics && npc_base[mobID].temp.magics.length || 0;
        var enemy_total_magic_damage = 0;
        var enemy_max_magic_damage = 0;
        var enemy_staff_cooldown = 1 - npc_base[mobID].temp.cooldown;
        var magic_block_lets_through = 1-((players[0].temp.magic_block || 0)/100);
        if (enemy_spell_slots > 0) {
            //magic stats
            var enemy_mag_msg = "Enemy magic: ";
            for (var sp = 0; sp < enemy_spell_slots; sp++) {
                if (sp < enemy_spell_slots) {
                    var curr_spell = npc_base[mobID].temp.magics[sp].id;
                    var hit_coeff = Math.min(1, (npc_base[mobID].temp.magic / 1.2 + Magic[curr_spell].params.penetration) / (players[0].temp.total_defense));
                    var times_per_turn = 1/Math.ceil(Magic[curr_spell].params.cooldown * enemy_staff_cooldown / 1000 / secperround);
                    var magic_dmg = magic_block_lets_through * Math.round((0.5 / 1.333 + 0.25) * Magic[curr_spell].params.basic_damage * hit_coeff * times_per_turn / secperround*100)/100;
                    var max_magic_hits_per_round = Math.min(1,Math.ceil(secperround / (Magic[curr_spell].params.cooldown * enemy_staff_cooldown / 1000)));
                    var magic_dmgm = magic_block_lets_through * max_magic_hits_per_round * Math.round((1 / 1.333 + 0.25) * Magic[curr_spell].params.basic_damage * hit_coeff);
                    enemy_mag_msg = enemy_mag_msg + Magic[curr_spell].name + " (" + Math.round(hit_coeff * 100) / 100 + ")" + ": " + Math.round(magic_dmg * 100) / 100 + "/s; ";
                    enemy_total_magic_damage += magic_dmg;
                    enemy_max_magic_damage += magic_dmgm;
                }
            }
        }

        var En_combatString = "ENEMY: average damage = " + Math.round(En_aveDmg * 100) / 100 + " + " + Math.round(enemy_total_magic_damage * secperround * 100) / 100 + "M = " + Math.round((En_aveDmg + (enemy_total_magic_damage * secperround)) * 100) / 100 + ", chance to hit = " + Math.round((1 - En_zroCnt) * 10000) / 100 + "%, max damage: " + En_maxDmg + " + " + enemy_max_magic_damage + "M = " + (En_maxDmg + enemy_max_magic_damage);
        var Pl_combatString = "YOU: average damage = " + Math.round(Pl_aveDmg * 100) / 100 + " + " + Math.round(total_magic_damage * secperround * 100) / 100 + "M = " + Math.round((Pl_aveDmg + (total_magic_damage * secperround)) * 100) / 100 + ", chance to hit = " + Math.round((1 - Pl_zroCnt) * 10000) / 100 + "%, max damage: " + Pl_maxDmg + " + " + max_magic_damage + "M = " + (Pl_maxDmg + max_magic_damage);

        // The combat report prints out to three lines. It's easier to read this way;
        addChatText("Combat Analysis: " + npc_base[mobID].name, void 0, COLOR.ORANGE);
        addChatText(En_combatString, void 0, COLOR.WHITE);
        addChatText(Pl_combatString, void 0, COLOR.WHITE);

        if (spell_slots > 0) {
            addChatText(mag_msg, void 0, COLOR.TEAL);
            addChatText("Total magic damage: " + Math.round(total_magic_damage * 100) / 100 + "/s", void 0, COLOR.TEAL);
        }

        if (enemy_spell_slots > 0) {
            addChatText(enemy_mag_msg, void 0, COLOR.TEAL);
            addChatText("Total magic damage from enemy: " + Math.round(enemy_total_magic_damage * 100) / 100 + "/s", void 0, COLOR.TEAL);
        }

        var trounds = Math.ceil(npc_base[mobID].temp.health / (Pl_aveDmg + (total_magic_damage * secperround)));
        var tdamage = (trounds - 1) * (En_aveDmg+(enemy_total_magic_damage * secperround));

        var timeforkill = trounds * secperround;
        var timeforkills = Math.floor(timeforkill / 60) + ":" + (parseInt(timeforkill % 60, 10) < 10 ? "0" : "") + parseInt(timeforkill % 60, 10);

        var rounds = "Average per fight: hits: " + Math.round(trounds * 100) / 100 + ", damage received: " + Math.round(tdamage * 100) / 100 + ", time to kill: " + timeforkills;
        addChatText(rounds, void 0, COLOR.WHITE);

    };

    //CHG: OVERRIDES for standard join functions
    Contacts.add_channel = function (a) {
        if(!Contacts.can_join_channel(a)){
            return;
        }
        //add channel for all tabs
        if (loadedMods.indexOf("Tabs") > -1) {
            for (var i = 0; i < Mods.Tabs.wwCurrentTabs.length; i++) {
                Mods.Tabs.wwCurrentTabs[i].channels[a] = true;
            }
        }
        Mods.Tabs.oldadd_channel(a);
    }

    Contacts.remove_channel = function (a) {
        //remove channel for all tabs
        if (loadedMods.indexOf("Tabs") > -1) {
            for (var i = 0; i < Mods.Tabs.wwCurrentTabs.length; i++) {
                delete (Mods.Tabs.wwCurrentTabs[i].channels[a]);
            }
        }
        Mods.Tabs.oldremove_channel(a);
    }

    InvMenu.create = function (a) {
        Mods.Rclick.oldInvMenu(a);


        var b = players[0].temp.inventory[a];
        if (typeof b != "undefined") {
            var b = item_base[b.id];
            d = getElem("action_menu");

            //check if keybind mod is loaded
            if (loadedMods.indexOf("Kbind") > -1) {
                d.innerHTML += "<span class='line' onclick='Mods.destroyallitems(" + b.b_i + ")'>Destroy All<span class='item'>" + Mods.cleanText(b.name) + "</span></span>";
            };

            //right-click wiki CHECK IF WIKI LOADED && CHAT LOADED
            if (loadedMods.indexOf("Wikimd") > -1 && loadedMods.indexOf("Chatmd") > -1) {
                d.innerHTML += "<span class='line' onclick='Mods.Chatmd.chatCommands(&apos;\/wiki item name " + Mods.cleanText(b.name, true) + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)'>Check Wiki<span class='item'>ITEM</span></span>" + "<span class='line' onclick='Mods.Chatmd.chatCommands(&apos;\/wiki craft item " + Mods.cleanText(b.name, true) + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)'>Check Wiki<span class='item'>CRAFT</span></span>" + "<span class='line' onclick='Mods.Chatmd.chatCommands(&apos;\/wiki npc item " + Mods.cleanText(b.name, true) + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)'>Check Wiki<span class='item'>NPC</span></span>";
            }
        }


    }

    Mods.destroyallitems = function (itemID) {
        InvMenu.hide();
        typeof itemID != "undefined" && Popup.prompt("Do you want to destroy all " + item_base[itemID].name + "?", function () {
            Socket.send("inventory_destroy", {
                item_id: itemID,
                all: true
            })
        })
    };

    ActionMenu.create = function (a, b, d) {
        Mods.Rclick.oldActionMenu(a, b, d);
        if (!d) {
            d = getElem("action_menu");
            addClass(d, "hidden");
            d.style.top = a.clientY + 10 + "px";
            d.style.left = a.clientX + "px";
            var e = "";
            var f = "";
            var g = "";
            //CHG: whisper player
            for (var at in players) if (at != "0" && players[at].i == selected_object.i && players[at].j == selected_object.j) if (players[at].b_t == BASE_TYPE.PLAYER) {

                g = "<span class='line' onclick=\"ChatSystem.whisper('" + players[at].name.sanitize() + "');addClass(getElem(&apos;action_menu&apos;), ";
                g += "&apos;hidden&apos;)\">Whisper <span class='item'>" + players[at].name.sanitize() + "</span></span>";
                break;

            };
            if ((selected_object.b_t == 1 && selected_object.params.results != undefined) || selected_object.b_t == 4) {
                e = "<span class='line' onclick='ActionMenu.mobDrops(" + selected_object.b_i + "," + selected_object.b_t + ");addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)' style='margin-left:-5px;'><span class='item'>" + selected_object.name + "</span>Drops</span>" + (selected_object.b_t == "4" ? "<span class='line' onclick='ActionMenu.combatCheck(" + selected_object.b_i + ");addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)'>Combat Analysis</span>" : "");
                if (modOptions.chatmd.loaded) {
                    if (selected_object.type == OBJECT_TYPE.SHOP) {
                        f = "<span class='line' onclick='Mods.Chatmd.chatCommands(&apos;\/wiki npc name " + selected_object.name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)'>Check Wiki<span class='item'>NPC</span></span>";
                    }
                    else if (selected_object.type == OBJECT_TYPE.ENEMY) {
                        f = "<span class='line' onclick='Mods.Chatmd.chatCommands(&apos;\/wiki mob name " + selected_object.name.replace("[BOSS]", "") + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)'>Check Wiki<span class='item'>MOB</span></span>";
                    }
                }
            }

            d.innerHTML = ActionMenu.action(selected_object, 0) + ActionMenu.action(selected_object, 1) + e + f + g;
            d.innerHTML.length > 0 && removeClass(d, "hidden");
        };
    };

    Mods.timestamp("rclick");
};

Load.chestm = function () {

    modOptions.chestm.time = timestamp();

    function temp () {

        for (var i = 0; i < 10; i++) {
            if (typeof getElem("chest").childNodes[1].childNodes[0] != "undefined") {
                getElem("chest").childNodes[1].removeChild(getElem("chest").childNodes[1].childNodes[0]);
            } else { break }
        }
        createElem("span", getElem("chest").childNodes[1], {
            id: "chest_chest",
            style: "float: left; font-weight: bold;",
            innerHTML: "Chest"
        });
        createElem("span", getElem("chest").childNodes[1], {
            id: "chest_sort",
            className: "link",
            style: "float: left; font-weight: bold; margin: 0px; margin-left: 10px;",
            innerHTML: "Sort"
        });
        createElem("span", getElem("chest").childNodes[1], {
            id: "chest_market",
            className: "link",
            style: "margin-right: 72px; margin-top: 5px;",
            innerHTML: "Market",
            onclick: function () { javascript: Market.open(); }
        });
        createElem("span", getElem("chest").childNodes[1], {
            id: "chest_close",
            className: "link",
            style: "margin: 0px; padding: 0px; float: right;",
            innerHTML: "Close",
            onclick: function () { javascript: addClass(getElem("chest"), "hidden"); }
        });

        for (var i = 0; i < 5; i++) createElem("span", getElem("chest").childNodes[1], {
            id: "chest_page_" + (5 - i),
            className: "chest_pages link",
            style: "margin: 0px; padding-right: 9px; float: right;",
            onclick: function () {
                for (var vv = 0; vv < 5; vv++) getElem("chest_page_" + (vv + 1)).style.color = "";
                var num = parseInt(this.id.replace("chest_page_", ""));
                getElem("chest_page_" + num).style.color = "orange";
                Chest.change_page(num);
            },
            innerHTML: (5 - i)
        });

        getElem("chest_withdraw").style.display = "none";
        getElem("chest_withdraw_8").style.display = "none";
        getElem("chest_destroy").style.display = "none";
        getElem("chest_deposit").style.display = "none";
        getElem("chest_deposit_all").style.display = "none";
        getElem("chest_item_name").style.display = "none";

        // holders for each section so that stuff is properly grouped;
        createElem("span", "chest", {
            id: "chest_sort_holder",
            style: "width: 359px; min-height: 22px; display: none; margin-bottom: 3px; margin-top: 3px; padding-bottom: 4px; color: #FFF; border-bottom: 1px solid #666;"
        });
        createElem("span", "chest", {
            id: "chest_btn_holder",
            style: "font-weight: bold; color: #555;"
        });

        // sort Holder content;
        createElem("select", "chest_sort_holder", {
            id: "chest_sort_category",
            className: "market_select",
            style: "width: 144px; float: left; margin: 1px 12px 3px 5px;",
            onchange: function () { Chest.sort_player_modChest(); },
            innerHTML: "<option value='-1'>-- Sort Inventory --</option><option value='0'>Sort: Equipment</option><option value='1'>Sort: Materials</option><option value='2'>Sort: Vendor Price</option>"
        });
        createElem("div", "chest_sort_holder", {
            id: "chest_divInv",
            style: "display: inline-block; float: left;"
        });
        createElem("input", "chest_divInv", {
            id: "chest_invCheck",
            type: "checkbox",
            style: "float: left; margin: 1px 7px 0px 0px; width: 0.7em; height: 0.7em;",
            onchange: function () { Chest.sort_player_modChest(); }
        });
        createElem("span", "chest_divInv", {
            id: "chest_invCheck_name",
            style: "float: left; font-size: 0.7em; margin-top: -1px;",
            innerHTML: "Inventory Items"
        });
        createElem("input", "chest_divInv", {
            id: "chest_colCheck",
            type: "checkbox",
            style: "float: left; clear: left; margin: 1px 7px 0px 0px; width: 0.7em; height: 0.7em;",
            onchange: function () { BigMenu.update_chest(chest_content); }
        });
        createElem("span", "chest_divInv", {
            id: "chest_colCheck_name",
            style: "float: left; font-size: 0.7em;",
            innerHTML: "Highlight"
        });
        createElem("div", "chest_sort_holder", {
            id: "chest_divFav",
            style: "display: inline-block; float: left;"
        });
        createElem("input", "chest_divFav", {
            id: "chest_favCheck",
            type: "checkbox",
            style: "float: left; margin: 1px 7px 0px 14px; width: 0.7em; height: 0.7em;",
            onchange: function () { Chest.sort_player_modChest(); }
        });
        createElem("span", "chest_divFav", {
            id: "chest_favCheck_name",
            style: "float: left; font-size: 0.7em; margin-top: -1px;",
            innerHTML: "Favorited Items"
        });
        createElem("input", "chest_divFav", {
            id: "chest_colCheckF",
            type: "checkbox",
            style: "float: left; clear: left; margin: 1px 7px 0px 14px; width: 0.7em; height: 0.7em;",
            onchange: function () { BigMenu.update_chest(chest_content); }
        });
        createElem("span", "chest_divFav", {
            id: "chest_colCheck_nameF",
            style: "float: left; font-size: 0.7em;",
            innerHTML: "Highlight"
        });

        if (navigator.userAgent.search("Chrome") > -1) {
            createElem("input", "chest_divInv", {
                id: "chest_invColor",
                type: "color",
                style: "float: left; margin: -2px 0px 0px 3px; width: .95em; height: 1.25em; border: none; background: none; background-color: transparent;",
                onchange: function () { BigMenu.update_chest(chest_content); }
            });
            createElem("input", "chest_divFav", {
                id: "chest_favColor",
                type: "color",
                style: "float: left; margin: -2px 0px 0px 3px; width: .95em; height: 1.25em; border: none; background: none; background-color: transparent;",
                onchange: function () { BigMenu.update_chest(chest_content); }
            });
        };

        // btn holder content;
        createElem("span", "chest_btn_holder", {
            id: "chest_withdraw_item",
            style: "float: left; color: #FF0; font-weight: normal; margin: 5px 6px;",
            innerHTML: "Withdraw"
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_withdraw_item_1",
            className: "link",
            style: "float: left; margin: 3px;",
            innerHTML: "1",
            onclick: function () { Chest.withdraw(1); }
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_withdraw_item_8",
            className: "link",
            style: "float: left; margin: 3px;",
            innerHTML: "8",
            onclick: function () { Chest.withdraw(8); }
        });
            //CHG: withdraw 19
        createElem("span", "chest_btn_holder", {
            id: "chest_withdraw_item_19",
            className: "link",
            style: "float: left; margin: 3px;",
            innerHTML: "19",
            onclick: function () { Chest.withdraw(19) }
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_withdraw_item_all",
            className: "link",
            style: "float: left; margin: 4px 6px;",
            innerHTML: "All",
            onclick: function () { Chest.withdraw(99); }
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_deposit_item",
            style: "float: right; color: #FF0; font-weight: normal; margin: 5px 6px;",
            innerHTML: "Deposit"
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_deposit_item_1",
            className: "link",
            style: "float: right; margin: 4px 6px;",
            innerHTML: "1",
            onclick: function () { Chest.deposit(1); }
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_deposit_item_all",
            className: "link",
            style: "float: right; margin: 4px 6px;",
            innerHTML: "All",
            onclick: function () { Chest.deposit(99); }
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_deposit_item_pet",
            className: "link",
            style: "float: right; margin: 4px 5px;",
            innerHTML: "All+",
            onclick: function () { Chest.deposit_all(); }
        });
        createElem("span", "chest_btn_holder", {
            id: "chest_destroy_item",
            className: "link",
            style: "float: left; margin: 4px 0px 0px 20px;",
            innerHTML: "Destroy",
            onclick: function () { Chest.destroy(); Chest.sort_player_modChest(); }
        });

        // New functions, onmouseover/out, to highlight items in chest that are moused over if their border color changes.

        for (var i = 0; i < 60; i++) {
            getElem("chest_" + i).onmouseover = new Function("Chest.mouseoverColor(this,true);");
            getElem("chest_" + i).onmouseout = new Function("Chest.mouseoverColor(this,false);");
            getElem("chest_" + i).onclick = /*new Function("selected_chest ='" + i + "'; Chest.button_enable_check(); BigMenu.update_chest_selection();");*/ function () { };
        };

        // Show/hide Sort options;
        getElem("chest_colCheck").checked = Mods.Chestm.chest_colCheck;
        getElem("chest_colCheckF").checked = Mods.Chestm.chest_colCheckF;
        if (navigator.userAgent.search("Chrome") > -1) {
            getElem("chest_invColor").value = Mods.Chestm.inv_select_color;
            getElem("chest_favColor").value = Mods.Chestm.fav_select_color;
        };

        getElem("chest_sort").onclick = function () {
            Mods.Chestm.chest_sort_hidden = !Mods.Chestm.chest_sort_hidden;
            if (Mods.Chestm.chest_sort_hidden) {
                getElem("chest_sort_holder").style.display = "none";
                getElem("chest_sort").innerHTML = "Sort";
            } else {
                getElem("chest_sort_holder").style.display = "inline-block";
                getElem("chest_sort").innerHTML = "Sort (hide)";
            }
        };

        getElem("chest_invCheck").checked = Mods.Chestm.sortInv_check;
        getElem("chest_favCheck").checked = Mods.Chestm.sortFav_check;
        getElem("chest_sort_category").value = Mods.Chestm.armorPriority ? 0 : Mods.Chestm.craftPriority ? 1 : Mods.Chestm.pricePriority ? 2 : -1;

        getElem("chest").onclick = function (a) {
            var target = a.target;
            var id = target.id;
            var inv = /chest_[0-9]/.test(id);
            if (!inv) {
                var temp = target.parentNode;
                temp = temp.id;
                if (/chest_[0-9]/.test(temp)) {
                    id = temp;
                    inv = true;
                }
            };
            if (inv) id = id.replace("chest_", "");
            var key = Mods.Chestm.ctrlPressed;
            if (inv) {
                if (key) {
                    var item = parseInt(id) + (parseInt(chest_page) - 1) * 60;
                    item = chests[0][item] && chests[0][item].id || false;
                    if (item) {
                        if (Mods.Chestm.playerPriorities[item]) { delete Mods.Chestm.playerPriorities[item] }
                        else { Mods.Chestm.playerPriorities[item] = true }
                    }
                } else {
                    selected_chest = id;
                    Chest.button_enable_check();
                    //BigMenu.update_chest_selection();
                }
            }
            localStorage.chestPlayerPriorities = JSON.stringify(Mods.Chestm.playerPriorities);
            Chest.sort_player_modChest();
        };

        getElem("inventory").onclick = function (a) {
            var target = a.target;
            var id = target.id;
            var inv = /inv_[0-9]/.test(id);
            if (inv) id = id.replace("inv_", "");
            var key = Mods.Chestm.ctrlPressed;
            if (inv && key) {
                var item = players[0].temp.inventory[id] && players[0].temp.inventory[id].id || false;
                if (item) {
                    if (Mods.Chestm.avoidAll[item]) { delete Mods.Chestm.avoidAll[item] }
                    else { Mods.Chestm.avoidAll[item] = true }
                    var avoid = Mods.Chestm.avoidAll[item] || false;
                    var color = avoid && "#00FF00" || "#FF0000";
                    target.style.borderColor = color;
                    Timers.set("slot_border_" + id, function () {
                        target.style.borderColor = "";
                        Mods.Chestm.ctrlPressed && Mods.Chestm.showAvoidAll(true);
                    }, 1000);
                    Mods.Chestm.showAvoidAll(true);
                }
            }
            localStorage.avoidAll = JSON.stringify(Mods.Chestm.avoidAll);
        };

    }

    Chest.open = function (a, b, d) {
        chest_content = a;
        chests[0] = a;
        // clears then creates a new object (2x array) to store the initial chest item positions;
        for (var j in Mods.Chestm.tempChest) delete Mods.Chestm.tempChest[j];
        for (var i = 0; i < chests[0].length; i++) {
            Mods.Chestm.tempChest[chest_content[i].id] = i;
        };
        b && Chest.change_page(chest_page);
        d && Carpentry.init(d);
        Chest.sort_player_modChest();
    };

    // Changed: added code;
    BigMenu.update_chest = function (a) {
        if (navigator.userAgent.search("Chrome") > -1) {
            Mods.Chestm.inv_select_color = getElem("chest_invColor").value;
            Mods.Chestm.fav_select_color = getElem("chest_favColor").value;
        };
        Mods.Chestm.chest_colCheck = getElem("chest_colCheck").checked;
        Mods.Chestm.chest_colCheckF = getElem("chest_colCheckF").checked;
        chest_content = a;
        getElem("chest_coins_amount").innerHTML = thousandSeperate(players[0].temp.coins);
        for (var b = chest_page - 1, d = b * 60, f = Math.min(a.length, b * 60 + 60) ; d < (b * 60 + 60) ; d++) {
            var e = getElem("chest_" + (d - b * 60));
            var q, g, k;
            if (d < f) {
                g = item_base[a[d].id];
                //g.b_i == Mods.Chestm.chest_item_id && (selected_chest = d - b * 60);
                k = IMAGE_SHEET[g.img.sheet];
                q = a[d].id;
                e.style.background = 'url("' + k.url + '") no-repeat scroll ' + -g.img.x * k.tile_width + "px " + -g.img.y * k.tile_height + "px transparent";
                for (e = e.childNodes[0], g = a[d].count, k = Inventory.get_item_count(players[0], a[d].id), l = "", o = 0, h = 6 - g.toString().length - k.toString().length; o < h; o++) l = l + "&nbsp;";
                e.innerHTML = g + l + k;
            } else q = -1;
            e = getElem("chest_" + (d - b * 60));
            // adding border to items in inventory or are favorited;
            if (Inventory.get_item_count(players[0], q) > 0 && Mods.Chestm.chest_colCheck) e.style.borderColor = Mods.Chestm.inv_select_color;
            else if (Mods.Chestm.chest_colCheckF && Mods.Chestm.playerPriorities[q]) e.style.borderColor = Mods.Chestm.fav_select_color;
            else  e.style.borderColor = "";
            localStorage.chestInv_color = JSON.stringify(Mods.Chestm.inv_select_color);
            localStorage.chestFav_color = JSON.stringify(Mods.Chestm.fav_select_color);
            localStorage.chest_colCheck = JSON.stringify(Mods.Chestm.chest_colCheck);
            localStorage.chest_colCheckF = JSON.stringify(Mods.Chestm.chest_colCheckF);
            // end of additional code;
        }
        BigMenu.update_chest_selection();
        Chest.button_enable_check();
    };

    Chest.mouseoverColor = function (source, over) {
        var item = source.id;
        item = parseInt(item.replace("chest_", "")) + (parseInt(chest_page) - 1) * 60;
        item = chests[0][item] && chests[0][item].id || false;
        if (source.style.borderColor != "" && over == true) source.style.borderColor = "#1DEDFF";
        if (source.style.borderColor != "" && over == false) source.style.borderColor = Inventory.get_item_count(players[0], item) > 0 && Mods.Chestm.chest_colCheck && Mods.Chestm.inv_select_color || Mods.Chestm.chest_colCheckF && Mods.Chestm.playerPriorities[item] && Mods.Chestm.fav_select_color || "";
    };

    // ********** Disable buttons that can't be used;
    Chest.button_enable_check = function () {
        var b = chest_page - 1;
        var slot = parseInt(selected_chest) + b * 60;
        if (typeof chest_content[slot] != "undefined") {
            var item = chest_content[slot].id;
            var a = getElem("chest_withdraw_item_1");
            var b = getElem("chest_withdraw_item_8");
            //CHG: with 19
            var b1 = getElem("chest_withdraw_item_19");
            var c = getElem("chest_withdraw_item_all");
            var d = getElem("chest_deposit_item_1");
            var e = getElem("chest_deposit_item_all");
            var f = getElem("chest_deposit_item_pet");
            var g = getElem("chest_destroy_item");
            if (chest_content[slot].count == 0) {
                a.className = "";
                b.className = "";
                b1.className = "";
                c.className = "";
                g.className = "";
                a.onclick = function () { };
                b.onclick = function () { };
                b1.onclick = function () { };
                c.onclick = function () { };
                g.onclick = function () { };
            } else {
                a.className = "link";
                b.className = "link";
                b1.className = "link";
                c.className = "link";
                g.className = "link";
                a.onclick = function () { Chest.withdraw(1); };
                b.onclick = function () { Chest.withdraw(8); };
                b1.onclick = function () {
                    Chest.withdraw(19)
                };
                c.onclick = function () { Chest.withdraw(99); };
                g.onclick = function () { Chest.destroy(); };
            };
            if (Inventory.get_item_count(players[0], item) == 0) {
                d.className = "";
                e.className = "";
                d.onclick = function () { };
                e.onclick = function () { };
            } else {
                d.className = "link";
                e.className = "link";
                d.onclick = function () { Chest.deposit(1); };
                e.onclick = function () { Chest.deposit(99); };
            };
            var n = false;
            for (var i = 0; i < players[0].temp.inventory.length; i++) {
                if (!players[0].temp.inventory[i].selected) { n = true; break }
            }
            if (!n) {
                f.className = "";
                f.onclick = function () { };
            } else {
                f.className = "link";
                f.onclick = function () { Chest.deposit_all(); };
            };
        };
    };

    // 0 = armors, 1 = foods, 2 = jewelry, 3 = materials, 4 = tools, 5 = weapons, 6 = spells, 7 = pets, 8 = house;
    Chest.sort_player_modChest = function () {

        //var fav = chestPlayerPriorities;
        Mods.Chestm.armorPriority = getElem("chest_sort_category").value == "0" ? true : false;
        Mods.Chestm.craftPriority = getElem("chest_sort_category").value == "1" ? true : false;
        Mods.Chestm.pricePriority = getElem("chest_sort_category").value == "2" ? true : false;

        chests[0].sort(function (a, b) {
            Mods.Chestm.sortFav_check = getElem("chest_favCheck").checked;
            Mods.Chestm.sortInv_check = getElem("chest_invCheck").checked;
            var pricea = item_base[a.id].params.price;
            var priceb = item_base[b.id].params.price;
            var params_a = item_base[a.id].params;
            var params_b = item_base[b.id].params;
            var namea = item_base[a.id].name;
            var nameb = item_base[b.id].name;
            var fav = Mods.Chestm.playerPriorities;
            var params_min =
                (params_b.min_defense || params_b.min_accuracy || params_b.min_health || params_b.min_forging || params_b.min_jewelry ||
                params_b.min_cooking || params_b.min_carpentry || params_b.min_fishing || params_b.min_alchemy || params_b.min_magic || 0) -
                (params_a.min_defense || params_a.min_accuracy || params_a.min_health || params_a.min_forging || params_a.min_jewelry ||
                params_a.min_cooking || params_a.min_carpentry || params_a.min_fishing || params_a.min_alchemy || params_a.min_magic || 0);
            var typea = item_base[a.id].b_t;
            var typeb = item_base[b.id].b_t;
            if (Mods.Chestm.armorPriority == true) {
                typea = Mods.Chestm.chestArmorPriorities[typea];
                typeb = Mods.Chestm.chestArmorPriorities[typeb];
            } else if (Mods.Chestm.craftPriority == true) {
                typea = Mods.Chestm.chestCraftPriorities[typea];
                typeb = Mods.Chestm.chestCraftPriorities[typeb];
            } else if (Mods.Chestm.pricePriority == true) {
                typea = -item_base[a.id].params.price;
                typeb = -item_base[b.id].params.price;
            };

            if (Mods.Chestm.sortFav_check == true && typeof fav[a.id] != "undefined" && typeof fav[b.id] == "undefined") return -1;
            if (Mods.Chestm.sortFav_check == true && typeof fav[a.id] == "undefined" && typeof fav[b.id] != "undefined") return 1;
            if (Mods.Chestm.sortInv_check == true && Inventory.get_item_count(players[0], a.id) > 0 && Inventory.get_item_count(players[0], b.id) == 0) return -1;
            if (Mods.Chestm.sortInv_check == true && Inventory.get_item_count(players[0], a.id) == 0 && Inventory.get_item_count(players[0], b.id) > 0) return 1;
            // next sort by parameter type like armor/craft/price
            if (typea == typeb) {
                if (item_base[a.id].b_t == 3 && !Mods.Chestm.pricePriority) {
                    var itemNameStr_a = item_base[a.id].name;
                    var itemNameStr_b = item_base[b.id].name;
                    var itemNameMatch_a = -1;
                    var itemNameMatch_b = -1;
                    for (var i in Mods.Chestm.materialsPriorities) {
                        if (itemNameStr_a.search(i) > -1) itemNameMatch_a = Mods.Chestm.materialsPriorities[i];
                        if (itemNameStr_b.search(i) > -1) itemNameMatch_b = Mods.Chestm.materialsPriorities[i];
                    }
                    if (itemNameMatch_a != -1 && itemNameMatch_b == -1) return -1;
                    if (itemNameMatch_a == -1 && itemNameMatch_b != -1) return 1;
                    if (itemNameMatch_a < itemNameMatch_b) return -1;
                    if (itemNameMatch_a > itemNameMatch_b) return 1;

                }
                if (params_min > 0) return 1;
                if (params_min < 0) return -1;
                if (pricea > priceb) return -1;
                if (pricea < priceb) return 1;
                if (namea > nameb) return -1;
                if (namea < nameb) return 1;
                else return 0;
            }
            if (typea < typeb) return -1;
            if (typea > typeb) return 1;
            if (namea > nameb) return -1;
            if (namea < nameb) return 1;
            else return 0;
        });
        // a call to update the chest visually.
        BigMenu.update_chest(chests[0], Mods.Chestm.currentChestPage);
        // resave stored variables;
        localStorage["sortFav_check"] = Mods.Chestm.sortFav_check;
        localStorage["sortInv_check"] = Mods.Chestm.sortInv_check;
        localStorage["chestArmorPriority"] = Mods.Chestm.armorPriority;
        localStorage["chestCraftPriority"] = Mods.Chestm.craftPriority;
        localStorage["chestPricePriority"] = Mods.Chestm.pricePriority;
        //    localStorage["chestPlayerPriorities"] = chestPlayerPriorities;
    };

    // ********** Adjust destroy / deposit / withdraw functions;
    Chest.deposit = function (a) {
        var b = chest_page - 1;
        b = parseInt(selected_chest) + b * 60;
        var id = chests[0][b].id;
        b = Mods.Chestm.tempChest[id];
        Mods.Chestm.chest_item_id = id;
        Socket.send("chest_deposit", {
            item_id: id,
            item_slot: b,
            target_id: chest_npc.id,
            target_i: chest_npc.i,
            target_j: chest_npc.j,
            amount: a
        })
    };

    Chest.destroy = function () {
        var b = chest_page - 1;
        b = parseInt(selected_chest) + b * 60;
        var id = chests[0][b].id;
        b = Mods.Chestm.tempChest[id];
        Mods.Chestm.chest_item_id = id;
        Popup.prompt("Do you want to destroy " + item_base[Mods.Chestm.chest_item_id].name + "?", function () {
            Socket.send("chest_destroy", {
                item_id: id,
                item_slot: b,
                target_id: chest_npc.id
            })
        })
    };

    Chest.withdraw = function (a) {
        var b = chest_page - 1;
        b = parseInt(selected_chest) + b * 60;
        if (a > chest_content[b].count) a = chest_content[b].count;
        var id = chests[0][b].id;
        b = Mods.Chestm.tempChest[id];
        Mods.Chestm.chest_item_id = id;
        Socket.send("chest_withdraw", {
            item_id: id,
            item_slot: b,
            target_id: chest_npc.id,
            target_i: chest_npc.i,
            target_j: chest_npc.j,
            amount: a
        })
    };

    //CHG: withdrawfavs
    Chest.withdrawfavs = function (a) {
        /*var b = chest_page - 1;
        b = parseInt(selected_chest) + b * 60;
        if (a > chest_content[b].count) a = chest_content[b].count;
        var c = chests[0][b].id;
        b = Mods.Chestm.tempChest[c];
        Mods.Chestm.chest_item_id = c;
        Socket.send("chest_withdraw", {
            item_id: c,
            item_slot: b,
            target_id: chest_npc.id,
            amount: a
        })*/
    };

    Chest.deposit_all = function () {
        var c = players[0].temp.inventory;
        var d = Inventory.get_item_counts(players[0]);
        if(Timers.running("deposit_all")){
            return false;
        }
        Timers.set("deposit_all", null_function, 1000);
        var a = 0;
        var n = 0;
        for (var i in d) {
            a = Inventory.get_item_count(players[0], i);
            for (var j in c) if (parseInt(c[j].id) == i && c[j].selected) a = a - 1;
            if (a != 0 && !Mods.Chestm.avoidAll[i]) {
                (function(n,i,a){
                    setTimeout(function(){
                        Socket.send("chest_deposit", {
                            item_id: i,
                            item_slot: Mods.Chestm.tempChest[i],
                            target_id: chest_npc.id,
                            target_i: chest_npc.i,
                            target_j: chest_npc.j,
                            amount: a
                        });
                    }, n*75);
                })(n,i,a);
                n += 1;
            }
        }
    };

    Mods.Chestm.eventListener = {
        keys: {"keydown": [KEY_ACTION.CTRL], "keyup": [KEY_ACTION.CTRL]},
        fn: function (type, keyCode, keyMap) {
            if (type == "keydown") {
                if (keyMap === KEY_ACTION.CTRL) {
                    Mods.Chestm.ctrlPressed = true;
                    Mods.disableInvClick = true;
                    Mods.Chestm.showAvoidAll(true);
                    Timers.set("clear_ctrl_chest", function () {
                        Mods.Chestm.eventListener.fn("keyup", false, KEY_ACTION.CTRL);
                    }, 1000)
                }
            }
            if (type == "keyup") {
                if (keyMap === KEY_ACTION.CTRL) {
                    Mods.Chestm.ctrlPressed = false;
                    Mods.disableInvClick = false;
                    Mods.Chestm.showAvoidAll(false);
                }
            }
        }
    }

    Mods.Chestm.showAvoidAll = function (show) {
        var show = show || false;
        for (var a = 0; a < 40; a++) {
            var inv = getElem("inv_" + a);
            var item = players[0].temp.inventory[a] && players[0].temp.inventory[a].id || false;
            var select = players[0].temp.inventory[a] && players[0].temp.inventory[a].selected || false;
            var color = inv.style.borderColor || "#FFFFFF";
            var clear = inv.style.borderColor != "#FFFFFF" && inv.style.borderColor != "rgb(255, 255, 255)" ? inv.style.borderColor : "";
            if (item && show && Mods.Chestm.avoidAll[item]) inv.style.borderColor = color;
            else inv.style.borderColor = clear
        }
    };

    temp();
    Mods.timestamp("chestm");
};

Load.forgem = function () {

    modOptions.forgem.time = timestamp();

    function temp () {

        getElem("forging_menu").style.minHeight = "204px";
        getElem("forging_components").style.bottom = "17px";

        createElem("select", "forging_menu", {
            id: "forge_search",
            className: "market_select",
            style: "width: 142px; position: absolute; top: 25px; left: 3px; display: inline-block;",
            onchange: "Mods.Forgem.forgeButtonShow();"
        });

        createElem("option", "forge_search", {
            id: "forge_recipe_select",
            value: "-1",
            innerHTML: "-- Select Recipe --"
        });

        for (var a = 0; a < Mods.Forgem.RECIPE_LIST.length; a++) {
            if (FORGE_FORMULAS[Mods.Forgem.RECIPE_LIST[a]]) createElem("option", "forge_search", {
                id: "forge_options_" + Mods.Forgem.RECIPE_LIST[a],
                value: Mods.Forgem.RECIPE_LIST[a],
                innerHTML: item_base[FORGE_FORMULAS[Mods.Forgem.RECIPE_LIST[a]].item_id].name
            });
            else {
                delete Mods.Forgem.RECIPE_U_LIST[Mods.Forgem.RECIPE_LIST[a]];
                Mods.Forgem.RECIPE_LIST.splice(a, 1);
                localStorage["RECIPE_LIST"] = JSON.stringify(Mods.Forgem.RECIPE_LIST);
                localStorage["RECIPE_U_LIST"] = JSON.stringify(Mods.Forgem.RECIPE_U_LIST);
            }
        };

        createElem("div", "forging_menu", {
            id: "forge_btn_container",
            style: "display: inline-block; position: absolute; width: 60px; height: 50px; top: 25px; left: 150px;"
        });
        createElem("button", "forge_btn_container", {
            id: "forge_btnRemove",
            className: "market_select pointer",
            style: "position: absolute; top: 23px; font-weight: bold; min-width: 55px; display: none;",
            innerHTML: "Clear",
            onclick: function () { Mods.Forgem.remove_mats(); }
        });
        createElem("button", "forge_btn_container", {
            id: "forge_btnSelect",
            className: "market_select pointer",
            style: "position: absolute; top: 145px; font-weight: bold; min-width: 55px; display: none;",
            innerHTML: "Select",
            onclick: function () { Mods.Forgem.place_mats(true); }
        });
        createElem("button", "forge_btn_container", {
            id: "forge_btnForget",
            className: "market_select pointer",
            style: "position: absolute; top: 0px; font-weight: bold; min-width: 55px; display: none;",
            innerHTML: "Forget",
            onclick: function () { Mods.Forgem.forget_recipe(true); }
        });

        if (Mods.Forgem.RECIPE_LIST.length == 0) getElem("forge_recipe_select").innerHTML = "No Recipes Known";

        getElem("forge_make").onclick = function () { Forge.attempt(); Mods.Forgem.newRecipe(); };

    }

    Mods.Forgem.forgeMatList = function () {
        var n = [];
        var a = FORGE_FORMULAS[Mods.Forgem.toForgeItem];
        var c = Mods.Forgem.FORGE_MATERIALS_LIST;
        Mods.Forgem.FORGE_MATERIALS_LIST.splice(0, c.length);
        if (a.item_id != undefined && Mods.Forgem.toForgeItem != "-1") {
            for (var e = 0; e < a.pattern.length; e++) for (var f = 0; f < a.pattern[e].length; f++) {
                n.push(a.pattern[e][f]);
                Mods.Forgem.countUnique(n, Mods.Forgem.FORGE_MATERIALS_LIST);
                for (var i in Mods.Forgem.FORGE_MAT_LIST) delete Mods.Forgem.FORGE_MAT_LIST[i];
                for (var g = 0; g < Mods.Forgem.FORGE_MATERIALS_LIST.length; g++) Mods.Forgem.FORGE_MAT_LIST[Mods.Forgem.FORGE_MATERIALS_LIST[g]] = FORGE_FORMULAS[Mods.Forgem.toForgeItem].materials[Mods.Forgem.FORGE_MATERIALS_LIST[g]];
            }
        } else for (var i in Mods.Forgem.FORGE_MAT_LIST) delete Mods.Forgem.FORGE_MAT_LIST[i];
    };

    Mods.Forgem.countUnique = function (a, b) {
        b.splice(0, b.length);
        var v = {};
        for (var d = 0; d < a.length; d++) if (!v.hasOwnProperty(a[d]) && a[d] != "-1") {
            b.push(a[d]);
            v[a[d]] = 1;
        }
    };

    Mods.Forgem.forgeButtonShow = function () {

        // This first part checks if the Select button should show;
        var a = getElem("forge_btnSelect");
        var haveEnough = true;
        Mods.Forgem.toForgeItem = getElem("forge_search").value;
        if (Mods.Forgem.toForgeItem != -1 && FORGE_FORMULAS[Mods.Forgem.toForgeItem].item_id != undefined) {
            Mods.Forgem.forgeMatList();
            for (var n in Mods.Forgem.FORGE_MAT_LIST) {
                if (Mods.Forgem.FORGE_MAT_LIST[n] != undefined && Inventory.get_item_count(players[0], n) >= Mods.Forgem.FORGE_MAT_LIST[n] && haveEnough == true) haveEnough = true;
                else haveEnough = false;
            }
        } else haveEnough = false;
        if (haveEnough) a.style.display = "block";
        else a.style.display = "none";

        // This second part checks if the Clear button should show;
        var hasSlotFull = false;
        var a = getElem("forge_btnRemove");
        for (var i = 0; i < 4; i++) for (var j = 0; j < 4; j++) if (getElem("forg_slot_" + i + "_" + j).style.background != "" || hasSlotFull) hasSlotFull = true;
        if (hasSlotFull) a.style.display = "block";
        else a.style.display = "none";

        // This third part checks if the Forget button should show;
        var a = parseInt(getElem("forge_search").value);
        var b = getElem("forge_btnForget");
        if (typeof a == "number" && a != -1) b.style.display = "block";
        else b.style.display = "none";
    };

    Mods.Forgem.place_mats = function (haveEnough) {
        var a = Mods.Forgem.FORGE_MATERIALS_LIST;
        var a_c = Mods.Forgem.FORGE_MAT_LIST;
        var b = FORGE_FORMULAS[Mods.Forgem.toForgeItem];
        Mods.Forgem.remove_mats();
        if (haveEnough) for (var e = 0; e < b.pattern.length; e++) for (var f = 0; f < b.pattern[e].length; f++) for (var g = 0; g < players[0].temp.inventory.length; g++) {
            if (players[0].temp.inventory[g].id == b.pattern[e][f]) {
                var select = getElem("forg_inv_" + g);
                select.srcElement = select;
                select.preventDefault = function () { };
                Forge.select(select, false);
                Forge.switcher(forge_selected, getElem("forg_slot_" + e + "_" + f), true);
            }
        };
        Mods.Forgem.forgeButtonShow();
    };

    Mods.Forgem.remove_mats = function () {
        for (var a = 0; a < 4; a++) for (var b = 0; b < 4; b++) for (var c = 0; c < 40; c++) {
            if (getElem("forg_slot_" + a + "_" + b).style.background != "" && getElem("forg_inv_" + c).style.background == "") {
                var select = getElem("forg_slot_" + a + "_" + b);
                select.srcElement = select;
                select.preventDefault = function () { };
                Forge.select(select, false);
                Forge.switcher(forge_selected, getElem("forg_inv_" + c), true);
            }
        };
        Mods.Forgem.forgeButtonShow();
    };

    Mods.Forgem.forget_recipe = function () {
        var a = parseInt(getElem("forge_search").value);
        getElem("forge_search").value = "-1";
        delete Mods.Forgem.RECIPE_U_LIST[a];
        for (var i = 0; i < Mods.Forgem.RECIPE_LIST.length; i++) if (Mods.Forgem.RECIPE_LIST[i] == a) Mods.Forgem.RECIPE_LIST.splice(i, 1);
        addChatText("You have forgotten an old recipe... (" + item_base[FORGE_FORMULAS[a].item_id].name + ")", null, COLOR.ORANGE);
        localStorage.RECIPE_LIST = JSON.stringify(Mods.Forgem.RECIPE_LIST);
        localStorage.RECIPE_U_LIST = JSON.stringify(Mods.Forgem.RECIPE_U_LIST);
        getElem("forge_search").removeChild(getElem("forge_options_" + a));
        if (Mods.Forgem.RECIPE_LIST.length == 0) getElem("forge_recipe_select").innerHTML = "No Recipes Known";
        Mods.Forgem.forgeButtonShow();
    };

    Mods.Forgem.newRecipe = function (b) {
        if (typeof b == "number") var a = b;
        else var a = parseInt(forge_formula);
        if (typeof a == "number") {
            getElem("forge_recipe_select").innerHTML = "-- Select Recipe --";
            if (!Mods.Forgem.RECIPE_U_LIST.hasOwnProperty(a) || Mods.Forgem.RECIPE_LIST.length == 0) {
                Mods.Forgem.RECIPE_LIST.push(a);
                Mods.Forgem.RECIPE_U_LIST[a] = 1;
                localStorage.RECIPE_LIST = JSON.stringify(Mods.Forgem.RECIPE_LIST);
                localStorage.RECIPE_U_LIST = JSON.stringify(Mods.Forgem.RECIPE_U_LIST);
                addChatText("You have discovered a new recipe! (" + item_base[FORGE_FORMULAS[a].item_id].name + ")", null, COLOR.ORANGE);
                var opt_newRecipe = document.createElement("option");
                opt_newRecipe.id = "forge_options_" + a;
                opt_newRecipe.value = a;
                opt_newRecipe.innerHTML = item_base[FORGE_FORMULAS[a].item_id].name;
                getElem("forge_search").appendChild(opt_newRecipe);
            }
        };
        Mods.Forgem.forgeButtonShow();
    };

    Forge.drop = function (a, b) {
        Mods.Forgem.oldDrop(a, b);
        Mods.Forgem.forgeButtonShow();
    };

    Forge.select = function (a, b) {
        Mods.Forgem.oldSelect(a, b);
        Mods.Forgem.forgeButtonShow();
    };

    Forge.forge_open = function () {
        Mods.Forgem.oldOpen();
        Mods.Forgem.forgeButtonShow();
    };

    temp();
    Mods.timestamp("forgem");
};

Load.health = function () {

    modOptions.health.time = timestamp();

    BigMenu.in_a_fight = function (a, b) {
        var afk = loadedMods.indexOf("Chatmd") != -1 && Mods.Chatmd.afkMessage != "" ? "<span class='pointer' title='" + Mods.Chatmd.afkMessage + "' style='color: #F00;' onclick='javascript:Mods.Chatmd.chatCommands(&apos;/afk&apos;)'>*</span>" : "";
        Mods.Health.old_inAFight(a, b);
        if (typeof a !== "undefined") {
            skills[0].health.current = a.temp.health;
            getElem("player_health_name").innerHTML = afk + capitaliseFirstLetter(a.name) + " (" + a.temp.health + "/" + skills[0].health.level + ")";
        };
        if (typeof b != "undefined") getElem("enemy_health_name").innerHTML = b.name + " (" + b.temp.health + ")";
    };

    Player.update_healthbar = function () {
        var afk = loadedMods.indexOf("Chatmd") != -1 && Mods.Chatmd.afkMessage != "" ? "<span class='pointer' title='" + Mods.Chatmd.afkMessage + "' style='color: #F00;' onclick='javascript:Mods.Chatmd.chatCommands(&apos;/afk&apos;)'>*</span>" : "";
        players[0].temp.healthbar && removeClass(getElem("player_healthbar"), "hidden");
        getElem("player_health_name").innerHTML = afk + capitaliseFirstLetter(players[0].name) + " (" + skills[0].health.current + "/" + skills[0].health.level + ")";
        if (players[0].temp.target_id == -1) getElem("player_health").style.width = Math.round(skills[0].health.current / skills[0].health.level * 100) + "%";
    };

    Player.update_healthbar();
    Mods.timestamp("health")
};

Load.mosmob = function () {

    modOptions.mosmob.time = timestamp();

    function temp () {

        getElem("wrapper").onmousemove = regular_onmousemove;

        createElem("div", "options_game", {
            innerHTML: "<span class='wide_link pointer' id='settings_infopanel' onclick='InfoPaneltoggle();'>Info Panel: full</span>"
        })

        Mods.Mosmob.createDiv();
        InfoPaneltoggle();

        wrapper.onmouseover = Mods.Mosmob.findID

    }

    regular_onmousemove = function (a) {
        Mods.regular_onmousemove(a);
        if (socket_status == 2) {
            var b = translateMousePositionToScreenPosition(a.clientX,
                a.clientY);
            if (b.x < 100 && b.y < 100 || minimap) getElem("object_selector_info").style.display = 'block';
        }
    }

    updateMouseSelector = function updateMouseSelector(a) {
        if (!hasClass(getElem("mods_tooltip_holder"), "hidden")) return;
        var os = getElem("object_selector_info");
        os.style.pointerEvents = "none";
        if (!mouse_over_magic) {
            a.clientX = a.clientX || a.pageX || a.touches && a.touches[0].pageX;
            a.clientY = a.clientY || a.pageY || a.touches && a.touches[0].pageY;

            var sw = Math.round(Math.min(window.innerWidth, width * max_scale));
            var sh = Math.round(Math.min(window.innerWidth / d, height * max_scale));
            var fs = Math.min(16, Math.round(16 * current_ratio_y));

            var b = translateMousePosition(a.clientX, a.clientY);
            var a = "";
            var d = "#FFFF00";

            if (b && map_visible(b.i, b.j) && on_map[current_map] && on_map[current_map][b.i] && on_map[current_map][b.i]) {
                var f;
                if (!(f = obj_g(on_map[current_map][b.i][b.j]))) f = player_map[b.i] && player_map[b.i][b.j] ? player_map[b.i][b.j][0] : false
                if (f && f.name && f.name != "Name") {
                    a = f.name;
                    if (f.b_t == BASE_TYPE.PLAYER) {
                        d = "#FFFFFF";
                        a = capitaliseFirstLetter(a) + " (" + FIGHT.calculate_monster_level(f) + ")";
                    }
                    if (f.b_t == BASE_TYPE.PET) a = pets[f.id].name + "<br/><span style='font-size:" + (fs * 0.7) + "px'>" + capitaliseFirstLetter(a) + "</span>";
                    if (f.b_t == BASE_TYPE.NPC) {
                        if (f.type == OBJECT_TYPE.SHOP) a = a + " (NPC)";
                        else {
                            if (editor_enabled) a = a + " (ID:" + f.b_i + ")";
                            a = a + " (" + FIGHT.calculate_monster_level(f) + ")" + "<br/><span style='font-size:" + (fs * 0.7) + "px'>(" + npc_base[f.b_i].temp.total_accuracy + "A, " + npc_base[f.b_i].temp.total_strength + "S, " + npc_base[f.b_i].temp.total_defense + "D, " + npc_base[f.b_i].params.health + "Hp)</span>";
                            if (Mods.Newmarket.infopannelmode == 0) a = a + (f.params.aggressive ? "<br/><span style='color:#FF0000;font-size:" + (fs * 0.7) + "px'>Aggressive</span>" : "<br/><span style='color:#FFFFFF;font-size:" + (fs * 0.7) + "px'>Passive</span>");
                        }
                    }
                    if (f.params.desc && Mods.Newmarket.infopannelmode == 0) a = a + "<br/><span style='font-size:" + (fs * 0.7) + "px'><i>" + f.params.desc + "</i></span>"
                    if (editor_enabled) {
                        a = a + ("<br/>(i: " + b.i + ", j:" + b.j + ")");
                        f.blocking && (a = a + "(B)")
                    }
                }
            }
            if (editor_enabled) {
                f = b.i - dx;
                b = b.j - dy;
                if (b > 10 && f < 11 && f > 1) {
                    b = f - 2 + 9 * (16 - b) + editor_page * editor_page_size;
                    if (b < BASE_TYPE[tileset].length && b >= 0 && b < (editor_page + 1) * editor_page_size) {
                        a = BASE_TYPE[tileset][b].name;
                        BASE_TYPE[tileset][b].blocking && (a = a + "(B)")
                    }
                }
            }
            os.innerHTML = a;
            os.style.color = d;
        }
        if (a == "") os.style.display = 'none';
        else {
            if (Mods.Newmarket.infopannelmode == 2) {
                os.style.border = "none";
                os.style.backgroundColor = '';
            } else {
                os.className = "menu";
                os.style.borderRadius = '14px';
                os.style.padding = "7px";
                os.style.border = '2px #666 solid';
                os.style.MozBorderRadius = '10px';
                //os.style.backgroundColor = 'rgba(100,100,100,0.7)';
            }
            os.style.display = 'block';
        };
    };

    InfoPaneltoggle = function () {
        var s = getElem("settings_infopanel");

        switch (Mods.Newmarket.infopannelmode) {
            case 0:
                s.innerHTML = "Info Panel: no inspect";
                Mods.Newmarket.infopannelmode = 1;
                break;
            case 1:
                s.innerHTML = "Info Panel: transparent";
                Mods.Newmarket.infopannelmode = 2;
                break;
            default:
                s.innerHTML = "Info Panel: full";
                Mods.Newmarket.infopannelmode = 0;
                break;
        }
        //save to localstorage
        localStorage["infopanelmode"] = JSON.stringify(Mods.Newmarket.infopannelmode);

    }

    Mods.Mosmob.createDiv = function () {
        if (getElem("mods_tooltip_holder") != null) return false;
        createElem("div", wrapper, {
            id: "mods_tooltip_holder",
            className: "menu hidden",
            style: "position: absolute; left: 50%; margin-left: -122px; top: 3.4%; font-size: 0.7em; padding: 7px; border: 2px solid #666; border-radius: 14px; z-index: 999; max-width: 230px;"
        });

        Mods.Mosmob.holder_html_template = Handlebars.compile("<div id='mods_tooltip_name' style='position: relative; width: 100%; float: left; text-align: center;'>&nbsp;</div><div id='mods_tooltip_content' style='position: relative; width: 100%; float: left; clear: left; padding: 3px 6px 1px 0px;'>&nbsp;</div>");
        getElem("mods_tooltip_holder").innerHTML = Mods.Mosmob.holder_html_template();
    };

    Mods.Mosmob.gatherParams = function (itemID) {
        var params = {};
        var item = item_base[itemID];
        if (item == undefined || item.params == undefined) return false;
        var i_params = item.params,
            s;
        // gather name
        params.name = item.name;
        // gather amount owned
        var inv_count = 0;
        params.owned = "";
        inv_count += Inventory.get_item_count(players[0], itemID);
        for (var i in players[0].pet.chest) if (players[0].pet.chest[i] == itemID) inv_count += 1;
        for (var i in chests[0]) if (chests[0][i].id == itemID) {
            if (chests[0][i].count > 0) inv_count += chests[0][i].count;
            break;
        };
        var color = inv_count == 0 ? "#F00" : "#0F0";
        params.owned += "<span style='color: #FFF'>" + thousandSeperate(inv_count) + "</span>";
        // gather magic values (for pouches/spells)
        if (i_params.min_magic && (i_params.magic_slots || i_params.slot == 10 || i_params.cooldown)) {
            var spell = i_params.slot == 10 ? Magic[i_params.magic].params : false;
            params.spell = spell == false ? false : spell.basic_damage + " <span style='color: #AAA;'>Dmg</span>, " +
                (spell.cooldown/1000) + "s <span style='color: #AAA;'>CD</span>, " +
                spell.xp + " <span style='color: #AAA;'>Exp/Cast</span><br>" +
                spell.penetration + " <span style='color: #AAA;'>Spell Pen</span>, " +
                spell.uses + " <span style='color: #AAA;'>Uses/Scroll</span>";
        } else params.spell = false;
        // gather source information
        var presents = {"Good": {1: 2500, 2: 80000}, "Great": {1: 8000, 2: 170000}, "Best": {1: 34999, 2: 450000}, "Legendary": {1: 150000, 2: 1500000}, "Rare": {1: 0, 2: 1}};
        params.price = i_params.price == undefined ? "span style='color: #F00;'>0</span>" : "<span style='color: #FFF;'>" + thousandSeperate(i_params.price) + "</span> <span style='color: #AAA'>coins</span>";
        params.obtained = "";
        if (modOptions.wikimd.loaded) {
            params.obtained = "";
            var formula = Mods.Wikimd.item_formulas[itemID];
            if (formula.craft && formula.craft.level) params.obtained += "<span style='text-align: center'>Craft <span style='color: #AAA;'>(" + capitaliseFirstLetter(formula.craft.source.skill) + ")</span>,&nbsp;</span>";
            if (formula.enchant && formula.enchant.from_enchant) params.obtained += "<span style='text-align: center'>Craft <span style='color: #AAA;'>(Enchant)</span>,&nbsp;</span>";
            if (formula.drop && formula.drop.sources) params.obtained += "<span style='text-align: center'>Drop,&nbsp;</span>";
            if (formula.sold && formula.sold.sources) {
                params.obtained += "<span style='text-align: center'>"
                var buy = false;
                for (var s in formula.sold.sources) if (formula.sold.sources[s].spawn) {
                    buy = true;
                    break;
                }
                if (buy) params.obtained += "Buy from ";
                else params.obtained += "Sale to ";
                params.obtained += "<span style='color: #AAA;'>(NPC)</span>,&nbsp;</span>";
            };
        };
        if (i_params.no_present == undefined) {
            var pre = "";
            for (var type in presents) if (i_params.price >= presents[type][1] && i_params.price <= presents[type][2]) pre += type + ", ";
            pre = pre.slice(0, -2);
            if (pre.length != 0) params.obtained += "<span style='text-align: center'>Present <span style='color: #AAA;'>(" + pre + ")</span>,&nbsp;</span>";
        };
        var isMOS = false
        for (var i in ItemPacks) if (!isMOS && ItemPacks[i].items) {
            for (var j in ItemPacks[i].items) if (ItemPacks[i].items[j].id == itemID) {
                params.obtained += "<span style='text-align: center'>MOS,&nbsp;</span>";
                isMOS = true;
                break
            }
        } else break;
        if (params.obtained.length > 0) {
            params.obtained = "<span style='text-align: center;'>Obtained: <span style='color: #FFF'><span style='width: 60%;'>" + params.obtained.slice(0, -14);
            params.obtained += "</span></span></span>";
        } else params.obtained = false;
        return params;
    };

    Mods.Mosmob.compileInfo = function (itemID) {
        var params = Mods.Mosmob.gatherParams(itemID);
        if (params == false) return false;
        var order = ["owned", "price", "enchant", "spell", "obtained"]
        var text = "<div style='color: #FF0; padding: 0px 10px 2px 10px; text-align: center;'>Owned: " + params.owned + "<span style='color: #AAA'>,</span> Value: " + params.price + "<br>";
        text += params.obtained ? params.obtained + "<br>" : "";
        text += params.spell ? "Spell Info: <span style='color: #FFF;'>" + params.spell + "</span><br>" : "";
        text += params.enchant ? "Enchant Info: <span style='color: #FFF;'>" + params.enchant + "</span>": "";
        text += "</div>";
        return text;
    };

    Mods.Mosmob.updateTooltip = function (itemID) {
        if (itemID === false || !(itemID > -1)) addClass(getElem("mods_tooltip_holder"), "hidden");
        else {
            var item = item_base[itemID];
            var img = IMAGE_SHEET[item.img.sheet];
            removeClass(getElem("mods_tooltip_holder"), "hidden");
            getElem("mods_tooltip_name").innerHTML = "<div style='width: 100%; text-align: center; padding-bottom: 4px; padding-top: 1px;'><span style='color: #FF0; font-weight: bold; padding-bottom: 3px; padding-left: 3px; font-size: 1.2em;'>" + Mods.cleanText(item_base[itemID].name) + "</span><br><span style='color: #FF0; padding: 1px 3px 3px 3px; font-style: italic; text-align: center'>" + Items.info(itemID) + "</span>";
            //getElem("mods_tooltip_img").style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
            getElem("mods_tooltip_content").innerHTML = Mods.Mosmob.compileInfo(itemID);
            getElem("object_selector_info").style.display = "none";
        }
    };

    Mods.Mosmob.findID = function (location) {
        var target = location.target || location.srcElement;
        var id = target.id;
        var vals = /(chest_|cabinet_chest_|shop_|forg_slot_|inv_|pet_inv_|pet_chest_|wiki_row\d_col\d_div_)(\d{1,3})(_(\d{1,2}))?/.exec(id);
        var item_id = target.item_id || target.getAttribute("item_id");

        var itemID = false;
        if (id == "forge_result") {
            if (typeof forge_formula != 'undefined') {
                item_id = -1;
                itemID = FORGE_FORMULAS[forge_formula];
                itemID = itemID != undefined ? itemID.item_id : false;
            }
            else {
                Mods.Mosmob.updateTooltip(false);
                return;
            }
        } else if (!vals && (!(item_id > -1) || item_id == null)) {
            Mods.Mosmob.updateTooltip(false);
            return;
        };
        if (vals) {
            if (vals[1] == "chest_") {
                itemID = parseInt(vals[2]);
                itemID = itemID + (chest_page - 1) * 60;
                itemID = chests[0][itemID];
                itemID = itemID != undefined ? itemID.id : false;
            } else if (vals[1] == "cabinet_chest_") {
                itemID = parseInt(vals[2]);
                var itemOnMap = on_map[current_map][last_cabinet.i][last_cabinet.j].params.items;
                itemID = itemOnMap != undefined ? itemOnMap[itemID] : false;
            } else if (vals[1] == "shop_") {
                itemID = parseInt(vals[2]);
                itemID = shop_npc.temp.content[itemID];
                itemID = itemID != undefined ? itemID.id : false;
            } else if (vals[1] == "forg_slot_") {
                itemID = parseInt(vals[2]);
                var itemID2 = parseInt(vals[4]);
                itemID = forge_components[itemID][itemID2];
                itemID = itemID != undefined ? itemID.id : false;
            } else if (vals[1] == "inv_") {
                itemID = parseInt(vals[2]);
                itemID = players[0].temp.inventory[itemID];
                itemID = itemID != undefined ? itemID.id : false;
            } else if (vals[1] == "pet_inv_" || vals[1] == "pet_chest_") {
                itemID = parseInt(vals[2]);
                itemID = players[0].pet.chest[itemID];
                itemID = parseInt(itemID);
            }
        } else if (item_id > -1) itemID = item_id;
        if (!(itemID > -1)) Mods.Mosmob.updateTooltip(false);
        else Mods.Mosmob.updateTooltip(itemID);
    };

    temp();
    Mods.timestamp("mosmob");
};

Load.expbar = function () {

    modOptions.expbar.time = timestamp();

    function temp () {

        createElem("div", wrapper, {
            id: "player_xp_bar",
            className: "xp_bar",
            style: "position: absolute; top: 0%; left: 0%; width: 100%; height: 2.8%; z-index: 9999;",
            innerHTML:
                '<div id="player_xp_bar_back" class="xp_bar_back" style="position: absolute; width: 100.3%; height: 100%; background: #000; top: 0%; left: 0%; opacity: 0.4;">\
                </div>\
                <div id="player_xp_bar_front" class="xp_bar_front" style="position: absolute; width: 20%; height: 100%; top: 0%; left: 0.3%; background: -moz-linear-gradient(top,  rgb(0, 184, 192) 0%,rgb(2, 49, 71) 50%,rgb(0, 46, 60) 51%,rgb(0, 21, 44) 100%); background: -webkit-linear-gradient(top,  rgb(0, 184, 192) 0%,rgb(2, 49, 71) 50%,rgb(0, 46, 60) 51%,rgb(0, 21, 44) 100%); background: -o-linear-gradient(top,  rgb(0, 184, 192) 0%,rgb(2, 49, 71) 50%,rgb(0, 46, 60) 51%,rgb(0, 21, 44) 100%); background: -ms-linear-gradient(top,  rgb(0, 184, 192) 0%,rgb(2, 49, 71) 50%,rgb(0, 46, 60) 51%,rgb(0, 21, 44) 100%); background: linear-gradient(top,  rgb(0, 184, 192) 0%,rgb(2, 49, 71) 50%,rgb(0, 46, 60) 51%,rgb(0, 21, 44) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#bfd255\', endColorstr=\'#9ecb2d\',GradientType=0 )">\
                    <div id="player_xp_name" class="xp_name" style="font-size: .8em; padding-top: 1px; color: #FFF; position: absolute; hight: inherit; top: inherit; left: 5px; white-space: nowrap;">40 Strength: 20,000 / 100,000 (20%)\
                    </div>\
                </div>'
        });

        for (var s in skills[0]) getElem("skill_" + s).parentElement.onclick = new Function("Mods.Expbar.set_skill = '" + s + "';Mods.Expbar.updateExpInfo();");

        Mods.Expbar.updateExpInfo();

        getElem("magic_slots").style.right = "";
        getElem("magic_slots").style.left = "2px";

        setCanvasSize();

    }

    Mods.Expbar.updateExpInfo = function () {
        var expForNextLevel = Math.round(Level.xp_for_level(skills[0][Mods.Expbar.set_skill].level + 1) - skills[0][Mods.Expbar.set_skill].xp);
        var expBetweenLevels = Math.round(Level.xp_for_level(skills[0][Mods.Expbar.set_skill].level + 1) - Level.xp_for_level(skills[0][Mods.Expbar.set_skill].level));
        var expGained = expBetweenLevels - expForNextLevel;
        var percentToGoal = Math.round(expGained / expBetweenLevels * 100);
        var xpNameFontSize = Mods.fontSize[0];
        var xp_bar_positionLeft = getElem("player_healthbar").style.left;
        var xp_bar_width = getElem("player_healthbar").style.width;
        localStorage.expSkillSet = Mods.Expbar.set_skill;
        getElem("player_xp_name").innerHTML = skills[0][Mods.Expbar.set_skill].level + " " + capitaliseFirstLetter(Mods.Expbar.set_skill) + ": " + expGained + " / " + expBetweenLevels + " (" + percentToGoal + "%)";
        getElem("player_xp_bar_front").style.width = percentToGoal + "%";
        getElem("player_xp_name").style.fontSize = xpNameFontSize;
        getElem("player_xp_bar").style.left = xp_bar_positionLeft;
        getElem("player_xp_bar").style.width = xp_bar_width;
    };

    Mods.Expbar.socketOn = {
        actions: ["skills"],
        fn: function () {
            Mods.Expbar.updateExpInfo();
        }
    }

    Mods.Expbar.setCanvasSize = function () {
        getElem("magic_slots").style.top = Math.ceil(127 * current_ratio_y) + "px";
        Mods.Expbar.updateExpInfo();
    };

    temp();
    Mods.timestamp("expbar");
};

Load.fullscreen = function () {

    modOptions.fullscreen.time = timestamp();

    function temp() {

        createElem("div", "options_video", {
            innerHTML: "<span class='wide_link pointer' id='settings_fullscreen' onclick='Mods.Fullscreen.toggle();'>Fullscreen Mode (off)</span>"
        });

        getElem("my_text").style.zIndex = "90";

        Mods.Fullscreen.toggle();

    }

    Mods.Fullscreen.toggle = function () {
        var s = getElem("settings_fullscreen");
        var opt = getElem("settings_game_grid");

        switch (Mods.Fullscreen.enabled) {
            case 0:
                s.innerHTML = "Fullscreen Mode (off)";
                Mods.Fullscreen.enabled = 1;
                opt.onclick = function () { toggleGridSize(); };
                opt.style.color = "";

                map_increase = 4;
                //enable menu option

                break;
            default:
                s.innerHTML = "Fullscreen Mode (on)<br/><span style='color:red;font-size:10px;'>WARNING: May impact game performance.</span>";
                Mods.Fullscreen.enabled = 0;

                opt.onclick = "";
                opt.style.color = "#AAA";


                map_increase = 6;
                //disable menu option

                break;
        }
        //save to localstorage
        localStorage["fullscreenenabled"] = JSON.stringify(Mods.Fullscreen.enabled);

        resetMapShift();
        drawMap();
        setCanvasSize(true);

    }

    iMapBegin = function () {
        if (Mods.Fullscreen.enabled == 0)
            return -6;
        else {
            return Mods.Fullscreen.iMapBegin();
        }
    };

    jMapBegin = function () {
        if (Mods.Fullscreen.enabled == 0)
            return -9;
        else {
            return Mods.Fullscreen.jMapBegin();
        }

    };

    iMapTo = function () {
        if (Mods.Fullscreen.enabled == 0)
            return minimap ? 99 : 24;
        else {
            return Mods.Fullscreen.iMapTo();
        }
    };

    jMapTo = function () {
        if (Mods.Fullscreen.enabled == 0)
            return minimap ? 99 : 21;
        else {
            return Mods.Fullscreen.jMapTo();
        }
    };

    astar.search = function (a, b, d, e, f) {
        if (Mods.Fullscreen.enabled == 0)
            return Mods.Fullscreen.astarsearchNew(a, b, d, e, f);
        else {
            return Mods.Fullscreen.astarsearchOld(a, b, d, e, f);
        }
    }

    Mods.Fullscreen.astarsearchNew = function (a, b, d, e, f) {
        var g;
        if (map_increase == 6)
            g = 15;
        else
            g = 5 + map_increase / 2;

        astar.init(a, b, g + 1);
        var f = f || astar.manhattan,
            e = !!e,
            k = astar.heap();
        for (k.push(b) ; 0 < k.size() ;) {
            var l = k.pop();
            if (l === d) {
                a = l;
                for (b = []; a.parent;) b.push(a),
                a = a.parent;
                return b.reverse()
            }
            l.closed = !0;
            for (var o = astar.neighbors(a, l, e, b, g), h = 0, q = o.length; h < q; h++) {
                var s = o[h];
                if (!s.closed && !s.isWall()) {
                    var u = l.g + s.cost,
                        r = s.visited;
                    if (!r || u < s.g) s.visited = !0, s.parent = l, s.h = s.h || f(s.pos, d.pos), s.g = u, s.f = s.g + s.h, r ? k.rescoreElement(s) : k.push(s)
                }
            }
        }
        return []
    }

    temp();
    Mods.timestamp("fullscreen");
}

Load.autocast = function () {

    modOptions.autocast.time = timestamp();

    function temp () {

        createElem("div", "options_game", {
            innerHTML: "<span class='wide_link pointer' id='settings_autocast' onclick='Mods.Autocast.toggle();'>Autocast (off)</span>"
        });

        Mods.Autocast.toggle();

    }

    Mods.Autocast.toggle = function () {
        var s = getElem("settings_autocast");

        switch (Mods.Autocast.enabled) {
            case 0:
                s.innerHTML = "Autocast (off)";
                Mods.Autocast.enabled = 1;
                break;
            default:
                s.innerHTML = "Autocast (on)";
                Mods.Autocast.enabled = 0;
                break;
        }
        //save to localstorage
        localStorage["autocastenabled"] = JSON.stringify(Mods.Autocast.enabled);
    }

    Mods.Autocast.socketOn = {
        actions: ["attack"],
        fn: function (action, data, message) {
            if (Mods.Autocast.enabled == 0 && players[0].params.magic_slots > 0 && action === "attack") {
                if (message.defender == "0" || message.attacker == "0") {
                    //start autocast
                    setTimeout(function () {
                        Mods.Autocast.TryCast();
                    }, 175);
                }
            }
        }
    }

    Mods.Autocast.TryCast = function () {
        if (inAFight && Mods.Autocast.enabled == 0) {
            for (var castn = 0; castn < players[0].params.magic_slots; castn++) {
                if (players[0].params.magics[castn] && players[0].params.magics[castn].ready)
                    Player.client_use_magic(castn);
            }
            Mods.Autocast.lastFullCast = timestamp();
            setTimeout(function () {
                Mods.Autocast.TryCast();
            }, 190);
        }
    };

    temp();
    Mods.timestamp("autocast")
}

Load.expmonitor = function () {

    modOptions.expmonitor.time = timestamp();

    function temp () {

        createElem("div", wrapper, {
            id: "xp_timer_holder",
            style: "position: absolute; min-width: 50px; height: 25px; z-index: 49; border: 1px solid #F00; border-radius: 4px; pointer-events: none; text-align: center; line-height: 25px; color: #FFF; background: orange; font-family: ariel; font-size: 18px; padding: 0px 4px; visibility: hidden;",
            innerHTML: "<span style='left: -25px; line-height: 10px; color: #000; font-weight: bold; font-size: .8em; padding-top: 4px; text-align: right; position: absolute;'>2x<br><span style='font-size: 0.7em;'>exp</span></span><span id='xp_timer'></span>"
        });

        //"Time left: xx minutes"
        //config.xp_multiplier
        //color: COLOR.TEAL

        //text: a,
        //user: b,
        //color: d,
        //lang: f,
        //type: e
    }

    Mods.Expmonitor.socketOn = {
        actions: ["message", "login"],
        fn: function (action, data, message) {
            if (action == "message" && data.message && data.color === COLOR.TEAL) {
                if (data.message === "Current experience rate is 2x") {
                    Mods.Expmonitor.XPEventSeconds = Mods.Expmonitor.XPEventSeconds < timestamp() ? timestamp() : Mods.Expmonitor.XPEventSeconds;
                    Mods.Expmonitor.XPEventSeconds += 1800000
                } else if (data.message === "Current experience rate is 1x") {
                    Mods.Expmonitor.XPEventSeconds = 0
                } else if (data.message.indexOf("Time left: ") === 0) {
                    var match = data.message.match(/Time left: (\d+\.\d+)\s*minute/);
                    //replace value only if result is > or < 60 secs
                    if (match.length > 0 && Math.abs((match[1] * 60) - Mods.Expmonitor.XPEventSeconds) > 60)
                        Mods.Expmonitor.XPEventSeconds = timestamp() + Math.round(match[1] * 60) * 1000;
                } else if (data.message === "Event not running or time unknown") {
                    Mods.Expmonitor.XPEventSeconds = 0
                }
                if (Mods.Expmonitor.XPEventSeconds > timestamp() && getElem("xp_timer_holder").style.visibility === "hidden") {
                    //enable display of xp_timer div and start showing timer
                    Mods.Expmonitor.Timer();
                    getElem("xp_timer").innerHTML = "";
                    getElem("xp_timer_holder").style.visibility = "";
                }
            };
            if (action == "login" && message.status == "ok") {
                Socket.send("message", {
                    data: "/xp"
                })
            }
        }
    }

    Mods.Expmonitor.Timer = function() {
        var xpdiv = getElem("xp_timer");
        var time = timestamp();
        time = Mods.Expmonitor.XPEventSeconds - time;
        time = Math.round(time / 1000)
        var text = "";
        if (time <= 0) getElem("xp_timer_holder").style.visibility = "hidden";
        else {
            text = Math.floor(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + (time % 60);
            xpdiv.innerHTML = text;
            Timers.set("check_2x", function () { Mods.Expmonitor.Timer() }, 1000)
        }
    }

    Mods.Expmonitor.setCanvasSize = function () {
        var d = wrapper.style.width.replace("px", "") / width,
            e = wrapper.style.height.replace("px", "") / height,
            a = ((players[0].pet.enabled ? 32:0) + (players[0].map == 300 || players[0].map == 16 ? 32:0));
        var tmr = getElem("xp_timer_holder");
        tmr.style.right = ((100 + a) * d) + "px";
        tmr.style.top = (23 * e) + "px";
    }

    //on mod load, send /xp command
    Socket.send("message", {
        data: "/xp",
        lang: getElem("current_channel").value
    });

    // Improves the showCaptchaBonus function:
    // Sets more appropriate values as the default number of captcha points to be assigned for exp reward:
    // If a 2x exp event is going on, the default is the total number of points saved (0 if the total is negative);
    // Otherwise the default is 1, unless the total available points is less than 1, in which case the default is 0.
    showCaptchaBonus = function() {
        document.getElementById("captcha_bonus_assign").style.display = "block";
        document.getElementById("penalty_points_bonus").innerHTML = -players[0].params.penalty;
        removeClass(document.getElementById("penalty_points_bonus"),"green");
        removeClass(document.getElementById("penalty_points_bonus"),"red");
        removeClass(document.getElementById("penalty_points_bonus"),"orange");
        addClass(document.getElementById("captcha_red"),"hidden");
        addClass(document.getElementById("captcha_green"),"hidden");
        if(0 < players[0].params.penalty) {
            addClass(document.getElementById("penalty_points_bonus"),"red");
            removeClass(document.getElementById("captcha_red"),"hidden");
            document.getElementById("penalty_bonus_points").value=0;
        }
        else {
            if(-players[0].params.penalty == 5){
                addClass(document.getElementById("penalty_points_bonus"),"orange");
            } else {
                addClass(document.getElementById("penalty_points_bonus"),"green");
            }
            removeClass(document.getElementById("captcha_green"),"hidden");
            document.getElementById("captcha_green").innerHTML = "* Assign points to get experience";
            var value = 0;
            // 2x exp on?
            if(typeof Mods !== 'undefined' && typeof Mods.Expmonitor !== 'undefined' && typeof Mods.Expmonitor.XPEventSeconds !== 'undefined' && Mods.Expmonitor.XPEventSeconds > 0){
                value = -players[0].params.penalty;
            } else {
                if(-players[0].params.penalty > 0){
                    value = 1;
                }
            }
            document.getElementById("penalty_bonus_points").value = value;
        }
    };


    temp();
    Mods.timestamp("expmonitor");
}

Load.kbind = function () {

    modOptions.kbind.time = timestamp();

    function temp () {

        Mods.Kbind.AKbind = [{ value: 0, enabled: false },
            { value: 0, enabled: false },
            { value: 0, enabled: false },
            { value: 0, enabled: false },
            { value: 0, enabled: false },
            { value: 0, enabled: false },
            { value: 0, enabled: false },
            { value: 66, enabled: true },
            { value: 0, enabled: false }];

        keylist = {0: "<option value=0>(none)</option><option value=8>[BackSpace]</option> \
        <option value=9>[Tab]</option><option value=13>[Enter]</option> \
        <option value=27>[Esc]</option><option value=33>[PgUp]</option> \
        <option value=34>[PgDwn]</option><option value=35>[End]</option> \
        <option value=36>[Home]</option><option value=45>[Ins]</option> \
        <option value=46>[Delete]</option><option value=66>B</option> \
        <option value=67>C</option> \
        <option value=69>E</option><option value=70>F</option> \
        <option value=71>G</option><option value=72>H</option> \
        <option value=73>I</option><option value=74>J</option> \
        <option value=75>K</option><option value=76>L</option> \
        <option value=77>M</option><option value=78>N</option> \
        <option value=79>O</option><option value=80>P</option> \
        <option value=81>Q</option><option value=82>R</option> \
        <option value=84>T</option><option value=85>U</option> \
        <option value=86>V</option><option value=88>X</option> \
        <option value=89>Y</option><option value=90>Z</option> \
        <option value=96>[Numpad0]</option> \
        <option value=97>[Numpad1]</option><option value=98>[Numpad2]</option> \
        <option value=99>[Numpad3]</option><option value=100>[Numpad4]</option> \
        <option value=101>[Numpad5]</option><option value=102>[Numpad6]</option> \
        <option value=103>[Numpad7]</option><option value=104>[Numpad8]</option> \
        <option value=105>[Numpad9]</option>"};

        createElem("div", wrapper, {
            id: "keybinding_form",
            className: "menu",
            style: "position: absolute; display: none; z-index: 300; width: 330px; height: 245px; top: 50%; left: 50%; margin-left: -115px; margin-top: -122px;",
            innerHTML: "<span class='common_border_bottom'><span style='float:left; font-weight: bold;color:#FFFF00;'>Keybindings</span><span class='common_link' style='margin:0px;margin-bottom:2px;' onclick='javascript:Mods.Kbind.Init();getElem(\"keybinding_form\").style.display=\"none\";'>Close</span></span><div style='padding-top: 8px;'><table width='100%'><tr><td><input type='checkbox' id='kbinding_0' onclick='void(0);'></td><td><select id='kbind_0' class='market_select'>" + keylist[0] + "</select></td><td>Deposit All+ in chest</td></tr><tr><td><input type='checkbox' id='kbinding_1' onclick='void(0);'></td><td><select id='kbind_1' class='market_select'>" + keylist[0] + "</select></td><td>Unload pet inventory</td></tr><tr><td><input type='checkbox' id='kbinding_5' onclick='void(0);'></td><td><select id='kbind_5' class='market_select'>" + keylist[0] + "</select></td><td>Load pet inventory</td></tr><tr><td><input type='checkbox' id='kbinding_2' onclick='void(0);'></td><td><select id='kbind_2' class='market_select'>" + keylist[0] + "</select></td><td>Cast all magic.</td></tr><tr><td><input type='checkbox' id='kbinding_3' onclick='void(0);'></td><td><select id='kbind_3' class='market_select'>" + keylist[0] + "</select></td><td>Run from fight</td></tr><tr><td><input type='checkbox' id='kbinding_4' onclick='void(0);'></td><td><select id='kbind_4' class='market_select'>" + keylist[0] + "</select></td><td>Destroy all ores in bag</td></tr><tr><td><input type='checkbox' id='kbinding_6' onclick='void(0);'></td><td><select id='kbind_6' class='market_select'>" + keylist[0] + "</select></td><td>Eat food in inventory</td></tr><tr><td><input type='checkbox' id='kbinding_7' onclick='void(0);'></td><td><select id='kbind_7' class='market_select'>" + keylist[0] + "</select></td><td>Toggle inventory</td></tr><tr><td><input type='checkbox' id='kbinding_8' onclick='void(0);'></td><td><select id='kbind_8' class='market_select'>" + keylist[0] + "</select></td><td>Withdraw 1 or All</td></tr></table></div>"
        });

        //load localstorage
        //Mods.consoleLog(AKbind);
        var AKbind = Mods.Kbind.AKbind;
        localStorage["AKbind"] = localStorage["AKbind"] || JSON.stringify(AKbind);
        var holder = JSON.parse(localStorage["AKbind"]);
        //Mods.consoleLog(holder);
        for (var i = 0; i < AKbind.length; i++) holder[i] = holder[i] != undefined ? holder[i] : AKbind[i];
        AKbind = Mods.Kbind.AKbind = holder;
        //Mods.consoleLog(AKbind);
        localStorage["AKbind"] = JSON.stringify(AKbind);

        for (var cn = 0; cn < AKbind.length; cn++) {
            getElem("kbinding_" + cn).checked = AKbind[cn].enabled;
            getElem("kbind_" + cn).value = AKbind[cn].value;
        }

        var f = document.createElement("span");
        f.className = "wide_link";
        f.id = "keybinding_link";
        f.style.cssFloat = "left";
        f.onclick = function () { getElem("keybinding_form").style.display = "block"; };
        f.innerHTML = "Keybindings";

        var md = getElem("mods_link");
        getElem("settings").insertBefore(f, md);

        Mods.Kbind.Init();

        //CHG: add cast all button
        CompiledTemplate.magic_slots = Handlebars.compile("{{#each this.magics}}<div class='magic_outer pointer' style='{{magic_image this.id}};'><div class='magic_inner' id='magic_slot_{{this.i}}' onclick='Player.client_use_magic({{this.i}})' onmouseover='mouseOverMagic({{this.i}})' onmouseout='mouseOutMagic({{this.i}})'>{{this.count}}</div></div>{{/each}}<div class='magic_outer'><div class='magic_inner pointer' style='font-size:10px;text-align: center;background-color: rgba(0, 0, 0, 0.8);' id='magic_slot_all' onclick='Mods.Kbind.CastAll()'>Cast All</div></div>");
    }

    Mods.Kbind.Init = function () {
        var AKbind = Mods.Kbind.AKbind;
        for (var cn = 0; cn < AKbind.length; cn++) {
            if (getElem("kbinding_" + cn).checked) {
                AKbind[cn].value = getElem("kbind_" + cn).value;
                AKbind[cn].enabled = true;
            } else { AKbind[cn].value = 0; AKbind[cn].enabled = false; }
        }
        //save localstorage
        localStorage["AKbind"] = JSON.stringify(AKbind);
    }

    Mods.Kbind.CastAll = function () {
        if (inAFight && timestamp() - Mods.Autocast.lastFullCast > 150 && GAME_STATE != GAME_STATES.CHAT) {
            for (var castn = 0; castn < players[0].params.magic_slots; castn++) {
                if (players[0].params.magics[castn] && players[0].params.magics[castn].ready)
                    Player.client_use_magic(castn);
            }
            Mods.Autocast.lastFullCast = timestamp()
        }
    };

    Mods.Kbind.Process = function (type, keyCode) {
        //CHG: process hey handlers
        //if chatline is open (GAME_STATE != GAME_STATES.CHAT), captchas, wiki or market open, ignore presses
        //Mods.consoleLog(GAME_STATE);
        var AKbind = Mods.Kbind.AKbind;
        if (GAME_STATE != GAME_STATES.CHAT && (hasClass(getElem("market"), "hidden")) && !captcha && hasClass(getElem("mods_form"), "hidden")) {
            //Mods.consoleLog("perform");
            if (AKbind[0].enabled && keyCode == AKbind[0].value && !players[0].temp.busy) Chest.deposit_all()
            if (AKbind[1].enabled && keyCode == AKbind[1].value && !players[0].temp.busy) Mods.Petinv.unload()
            if (AKbind[2].enabled && keyCode == AKbind[2].value) if (inAFight && timestamp() - Mods.Autocast.lastFullCast > 150) {
                for (var castn = 0; castn < players[0].params.magic_slots; castn++) if (players[0].params.magics[castn].ready) Player.client_use_magic(castn);
                Mods.Autocast.lastFullCast = timestamp()
            }
            if (AKbind[3].enabled && keyCode == AKbind[3].value) if (inAFight && timestamp() - lastRunAwayAttempt > 150) Socket.send("run_from_fight", {}); lastRunAwayAttempt = timestamp();
            if (AKbind[4].enabled && keyCode == AKbind[4].value && !players[0].temp.busy) Mods.Kbind.DestroyOres();
            if (AKbind[5].enabled && keyCode == AKbind[5].value && !players[0].temp.busy) Mods.Petinv.load();
            if (AKbind[6].enabled && keyCode == AKbind[6].value && !players[0].temp.busy) Mods.Kbind.eatfood();
            if (AKbind[7].enabled && keyCode == AKbind[7].value) {
                getElem("inventory").style.zIndex = "199";
                Mods.showBag = !Mods.showBag.valueOf();
                if (Mods.showBag)  getElem("inventory").style.display = "block";
                else getElem("inventory").style.display = "";
            }
            if (AKbind[8].enabled && keyCode == AKbind[8].value) {
                var chest = getElem("chest");
                if (!hasClass(chest,"hidden")) {
                    var item = (parseInt(chest_page) - 1) * 60 + parseInt(selected_chest);
                    var itemID = chests[0][item].id;
                    var item = item_base[itemID];
                    var params = item.params;
                    var type = item.b_t;
                    var w_all = false;
                    if ((params.min_cooking || params.min_forging || params.min_jewelry || params.min_alchemy || params.min_farming || item.name.indexOf("Enchant Scroll") > -1 || params.min_magic > 1 && (type != 5 && type != 0 && item.name.indexOf("Teleport") == -1) || type == 1) && type != 4 && params.slot != 14) w_all = true;
                    w_all && Chest.withdraw(99);
                    !w_all && Chest.withdraw(1);
                }
            }
        }
    }

    Mods.Kbind.eatfood = function () {
        if (GAME_STATE != GAME_STATES.CHAT && skills[0].health.level > skills[0].health.current && timestamp() - Mods.Kbind.lastfoodeaten > 250) {
            var bsuc = false;
            for (var b = 0; b < players[0].temp.inventory.length; b++) if (typeof item_base[players[0].temp.inventory[b].id].params.heal != "undefined") {
                if (inventoryClick(b)) Mods.Kbind.lastfoodeaten = timestamp();
                bsuc = true;
                break;
            }
            //no more food
            if (!bsuc) addChatText("You have no food in inventory!", void 0, COLOR.WHITE);
        }
    }

    //CHG:destroy ores
    Mods.Kbind.DestroyOres = function (c) {
        if (GAME_STATE == GAME_STATES.CHAT) return;
        var c = typeof c == "number" ? c : -1;
        if (c == -1) Popup.prompt("Do you want to destroy all ores in your bag?", function () {
            Mods.Kbind.DestroyOres(0);
        });
        else if (c < players[0].temp.inventory.length) {
            //check inventory item at "c" position if ID=one of ores
            switch (parseInt(players[0].temp.inventory[c].id)) {
                //if ore found, destroy ore, call function again from same inv spot with 50ms timer
                case 184:
                    //gold
                    Socket.send("inventory_destroy", { item_id: 184, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 185:
                    //silver
                    Socket.send("inventory_destroy", { item_id: 185, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 484:
                    //wg
                    Socket.send("inventory_destroy", { item_id: 484, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 373:
                    //plat
                    Socket.send("inventory_destroy", { item_id: 373, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 657:
                    //firestone
                    Socket.send("inventory_destroy", { item_id: 657, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 31:
                    //iron
                    Socket.send("inventory_destroy", { item_id: 31, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 383:
                    //azure
                    Socket.send("inventory_destroy", { item_id: 383, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 186:
                    //coal
                    Socket.send("inventory_destroy", { item_id: 186, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 32:
                    //copper
                    Socket.send("inventory_destroy", { item_id: 32, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 33:
                    //tin
                    Socket.send("inventory_destroy", { item_id: 33, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;
                case 30:
                    //clay
                    Socket.send("inventory_destroy", { item_id: 30, all: true});
                    Timers.set("destroy cycle " + c, function () {
                        Mods.Kbind.DestroyOres(c)
                    }, 50);
                    break;

                default:
                    //else, call function immediately for c+1
                    c++;
                    Mods.Kbind.DestroyOres(c);
                    break;
            }
        }
    };

    Mods.Kbind.eventListener = {
        keys: {"keyup": [true]},
        fn: function (type, keyCode) {
            Mods.Kbind.Process(type, keyCode)
        }
    }

    temp();
    Mods.timestamp("kbind")
}

Load.petinv = function () {

    modOptions.petinv.time = timestamp();

    function temp() {
        //adds new html divs for pet inventory slots under the main inventory window;

        getElem("inventory").style.paddingBottom = "114px";

        createElem("div", "inventory", {
            id: "inv_pet_chest",
            style: "border-top: 1px solid #666; position: absolute; top: 203px; padding-top: 2px; width: 288px; margin-bottom: 17px; color #FF0; display: block;"
        });
        createElem("div", "inventory", {
            id: "inv_pet_settings",
            style: "position: absolute; top: 297px; text-size: 80%;",
            innerHTML: "Pet has reached its maximum level."
        });
        createElem("div", "inv_pet_chest", {
            id: "pet_inv_expand",
            style: "height: 17px; width: 265px; top: 0px; vertical-align: middle;",
            innerHTML: "Pet's chest <span style='color:grey;font-size:80%;vertical-align:middle;'>(click to close)</span><br>"
        });
        createElem("input", "inv_pet_chest", {
            id: "shift_click",
            type: "checkbox",
            style: "position: absolute; right: 0px; top: 0px;"
        });
        createElem("div", "inv_pet_chest", {
            id: "inv_checkbox",
            style: "position: absolute; right: 21px; top: 4px; font-size: .8em; color: grey;",
            innerHTML: "use items = shift+click"
        });

        //CHG:load pet inv
        createElem("span", "inventory", {
            id: "pet_inv_load",
            className: "common_link",
            style: "color: #999; font-size: .8em; font-weight: normal; margin: 0px; padding: 0px 5px 2px 0px; position: absolute; bottom: 0%; right: 42px;",
            innerHTML: "(load)",
            onclick: "Mods.Petinv.load()"
        });

        //Adds button to unload all items in pet chest;
        createElem("span", "inventory", {
            id: "pet_inv_unload",
            className: "common_link",
            style: "color: #999; font-size: .8em; font-weight: normal; margin: 0px; padding: 0px 5px 2px 0px; position: absolute; bottom: 0%; right: 0%;",
            innerHTML: "(unload)",
            onclick: "Mods.Petinv.unload()"
        });

        Mods.Petinv.spawnInvPetChest();
        getElem("shift_click").checked = Mods.Petinv.enableShiftClick_check;

        for (a = 0; a < 40; a++) getElem("inv_" + a).onclick = Mods.Petinv.createFunc(a);

        for (a = 0; a < 16; a++) getElem("inv_pet_chest_" + a).onclick = (function (n) {
            return function () {
                Pet.menu_remove(n);
            };
        })(a);

        getElem("pet_inv_expand").onclick = new Function("Mods.Petinv.petInv_toggle = !Mods.Petinv.petInv_toggle;Mods.Petinv.invHeight();");

        Mods.Petinv.invHeight();
        Mods.Petinv.init_menuInv();

        if (Mods.Petinv.enableShiftClick_check) {
            getElem("inv_checkbox").innerHTML = "send items = shift+click";
            getElem("shift_click").checked = true;
        } else {
            getElem("inv_checkbox").innerHTML = "use items = shift+click";
            getElem("shift_click").checked = false;
        };

        getElem("shift_click").onclick = function () {
            Mods.Petinv.enableShiftClick_check = !Mods.Petinv.enableShiftClick_check;
            localStorage.enableShiftClick = Mods.Petinv.enableShiftClick_check;
            if (Mods.Petinv.enableShiftClick_check) getElem('inv_checkbox').innerHTML = "send items = shift+click";
            else getElem('inv_checkbox').innerHTML = "use items = shift+click";
        };
    }

    Mods.Petinv.invHeight = function () {
        var a = getElem("inventory");
        if (players[0].pet.enabled) var b = pets[players[0].pet.id].params.inventory_slots;
        var c = getElem("inv_pet_settings");
        var d = getElem("pet_inv_expand");
        var k = getElem("pet_inv_unload");
        var k1 = getElem("pet_inv_load");
        if (!players[0].pet.enabled) {
            a.style.paddingBottom = "2px";
            addClass(c, "hidden");
            addClass(d, "hidden");
            addClass(k, "hidden");
            addClass(k1, "hidden");
            getElem("inv_pet_chest").style.borderTop = "";
            getElem("shift_click").style.display = "none";
            getElem("inv_checkbox").style.display = "none";
            for (var p = 0; p < 16; p++) {
                getElem("inv_pet_chest_" + p).style.display = "none";
            };
        } else if (players[0].pet.enabled && Mods.Petinv.petInv_toggle) {
            getElem("shift_click").style.display = "block";
            getElem("inv_checkbox").style.display = "block";
            d.innerHTML = "Pet's chest <span style='color:grey;font-size:80%;vertical-align:middle;'>(click to close)</span><br>";
            getElem("inv_pet_chest").style.borderTop = "1px solid #666666";
            Mods.Petinv.init_menuInv();
            if (b < 9 && players[0].pet.enabled && Mods.Petinv.petInv_toggle) {
                a.style.paddingBottom = "76px";
                c.style.top = "258px";
                removeClass(c, "hidden");
                removeClass(d, "hidden");
                removeClass(k1, "hidden");
                removeClass(k, "hidden");
            } else if (b > 8 && players[0].pet.enabled && Mods.Petinv.petInv_toggle) {
                a.style.paddingBottom = "114px";
                c.style.top = "297px";
                removeClass(c, "hidden");
                removeClass(d, "hidden");
                removeClass(k1, "hidden");
                removeClass(k, "hidden");
            }
        } else if (players[0].pet.enabled && !Mods.Petinv.petInv_toggle) {
            getElem("shift_click").style.display = "none";
            getElem("inv_checkbox").style.display = "none";
            a.style.paddingBottom = "23px";
            addClass(c, "hidden");
            removeClass(d, "hidden");
            removeClass(k, "hidden");
            removeClass(k1, "hidden");
            getElem("inv_pet_chest").style.borderTop = "1px solid #666666";
            for (p = 0; p < 16; p++) {
                getElem("inv_pet_chest_" + p).style.display = "none";
            };
            d.innerHTML = "Pet's chest <span style='color:grey;font-size:80%;vertical-align:middle;'>(click to expand)</span><br>";
        }
    };

    Mods.Petinv.spawnInvPetChest = function () {
        for (a = 0; a < 16; a++) createElem("div", "inv_pet_chest", {
            id: "inv_pet_chest_" + a,
            className: "inv_item",
            innerHTML: "&nbsp;"
        });
    };

    Mods.Petinv.init_menuInv = function () {
        for (var a = 0; a < 16; a++) {
            if (players[0].pet.enabled) getElem("inv_pet_chest_" + a).style.display = pets[players[0].pet.id].params.inventory_slots > a ? "inline-block" : "none";
            else getElem("inv_pet_chest_" + a).style.display = "none";
        };
        if (players[0].pet.enabled) {
            for (a = 0; a < pets[players[0].pet.id].params.inventory_slots; a++) {
                var b = getElem("inv_pet_chest_" + a);
                removeClass(b, "selected");
                if (typeof players[0].pet.chest[a] != "undefined") {
                    var d = item_base[players[0].pet.chest[a]];
                    var f = IMAGE_SHEET[d.img.sheet];
                    b.style.background = 'url("' + f.url + '") no-repeat scroll ' + -d.img.x * f.tile_width + "px " + -d.img.y * f.tile_height + "px transparent";
                } else b.style.background = "";
            };
            var a = "";
            a = pets[players[0].pet.id];
            a = a.params.xp_required ? "Pet experience " + Math.round(players[0].pet.xp) + " / " + a.params.xp_required + " (" + Math.round(players[0].pet.xp / a.params.xp_required * 100) + "%)" : a.params.requires_stone ? "Pet needs Stone of Evolution to evolve." : "Pet has reached its maximum level.";
            getElem("inv_pet_settings").innerHTML = a;
        }
        for (a = 0; a < 40; a++) {
            var b = getElem("inv_" + a);
            if (typeof players[0].temp.inventory[a] != "undefined") {
                var d = item_base[players[0].temp.inventory[a].id];
                var f = IMAGE_SHEET[d.img.sheet];
                b.style.background = 'url("' + f.url + '") no-repeat scroll ' + -d.img.x * f.tile_width + "px " + -d.img.y * f.tile_height + "px transparent";
            } else b.style.background = "";
        }
    };

    Mods.Petinv.invSendItemCheck = function (shiftHeldDown) {
        enableShiftClick = getElem("shift_click").checked;
        if (Mods.Petinv.petInv_toggle == false || players[0].pet.enabled == false) return false;
        else if ((shiftHeldDown == true && enableShiftClick == true) || (shiftHeldDown == false && enableShiftClick == false)) return true;
        else return false;
    };

    Mods.Petinv.createFunc = function (i) {
        return function (event) {
            if (Mods.disableInvClick) return;
            if (Mods.Petinv.invSendItemCheck(event.shiftKey)) Pet.menu_add(i);
            else {
                inventoryClick(i);
                left_click_cancel=true;
            }
        }
    };

    Mods.Petinv.socketOn = {
        actions: ["my_pet_data", "skills"],
        fn: function (action, data) {
            if (action == "my_pet_data" || action == "skills") {
                Mods.Petinv.init_menuInv();
                Mods.Petinv.invHeight();
            }
        }
    }

    //Adds function unload();
    Mods.Petinv.unload = function (slot) {
        if (players[0].pet.enabled) var b = pets[players[0].pet.id].params.inventory_slots;
        var slot = typeof slot == "number" ? slot : 0;
        if (slot < b) {
            var inv = "inv_pet_chest_" + a;
            Pet.menu_remove(slot);
            if (players[0].temp.inventory.length < 40 && players[0].pet.chest.length > 0) Timers.set("unload pet inv" + slot, function () {
                Mods.Petinv.unload(slot)
            }, 81)
        }
    };
    //CHG: load pet inv
    Mods.Petinv.load = function (c) {
        if (players[0].pet.enabled) var b = pets[players[0].pet.id].params.inventory_slots;
        var c = typeof c == "number" ? c : players[0].temp.inventory.length - 1;
        if (c > 0) if (players[0].pet.chest.length < b) {
            if (!players[0].temp.inventory[c].selected) {
                Pet.menu_add(c);
                Timers.set("load pet inv" + c, function () {
                    Mods.Petinv.load(c - 1)
                }, 81)
            }
            else Mods.Petinv.load(c - 1);
        }
    };

    temp();
    Mods.timestamp("petinv");
};

Load.magicm = function () {

    modOptions.magicm.time = timestamp();

    socket.on("message", function (a) {
        if (typeof a === "object" && a.action === "message" && a.data.color == COLOR.GREEN && a.data.message.search("magic damage") > -1) {
            var damage = a.data.message.substr(1, a.data.message.search(" magic")-1)
            if(!/locked/.test(damage)){
                Mods.Magicm.show_magic_damage(damage);
            }
        }
    });

    var switchWorldBugFixOld = switchWorldBugFix;
    switchWorldBugFix = function(){
        switchWorldBugFixOld();
        socket.on("message", function (a) {
            if (typeof a === "object" && a.action === "message" && a.data.color == COLOR.GREEN && a.data.message.search("magic damage") > -1) {
                var damage = a.data.message.substr(1, a.data.message.search(" magic")-1)
                if(!/locked/.test(damage)){
                    Mods.Magicm.show_magic_damage(damage);
                }
            }
        });
    }

    Mods.Magicm.show_magic_damage = function (dmg) {
        var f = getElem("enemy_hit").cloneNode(true);
        var b = Mods.Magicm.enemy;
        var c = Mods.Magicm.magic_damage_timers;
        var d = c[0] == 0 ? 0 : c[1] == 0 ? 1 : c[2] == 0 ? 2 : c[3] == 0 ? 3 : c[0] <= c[1] ? 0 : c[1] <= c[2] ? 1 : c[2] <= c[3] ? 2 : 3;
        var e = {
            i: d == 0 || d == 2 ? (-1 * (d / 2) - 0.5) * half_tile_width_round : (d / 2) * half_tile_width_round,
            j: -2 * half_tile_height_round
        };
        var q = players[0].temp.target_id;
        q = objects_data[q] || players[q];
        if (b != q) {
            var L = Mods.Magicm.magic_damage_timers;
            L[0] = L[1] = L[2] = L[3] = 0;
            b = Mods.Magicm.enemy = q;
        };
        if (b) {
            var g = translateTileToCoordinates(b.i, b.j);
            var h = translateTileToCoordinates(dx, dy);
            f.id = "magic_" + d + b.id + (new Date).getTime();
            removeClass(f, "hidden");
            f.innerHTML = getElem("enemy_hit").innerHTML;
            f.childNodes[1].innerHTML = dmg;
            wrapper.appendChild(f);
            f.style.left = (g.x + 16 + players[0].mx + e.i) * current_ratio_x + "px";
            f.style.top = (g.y - 40 + players[0].my + e.j) * current_ratio_y + "px";
            addClass(f, "opacity_100");
            c[d] = 100;
            setTimeout(function () {
                decreaseOpacity(f, 150, 10);
                Mods.Magicm.decreaseMagic(d, 150, 10);
            }, 150)
        }
    };

    Mods.Magicm.decreaseMagic = function (a, b, d) {
        if (Mods.Magicm.magic_damage_timers[a] > 0) {
            Mods.Magicm.magic_damage_timers[a] = Math.max(Mods.Magicm.magic_damage_timers[a] - d, 0);
            setTimeout(function () {
                Mods.Magicm.decreaseMagic(a, b, d)
            }, b)
        }
    };

    Mods.timestamp("magicm");
};

Load.wikimd = function () {

    modOptions.wikimd.time = timestamp();

    function temp () {

        getElem("mods_form_top").innerHTML = "<span style='float:left; font-weight: bold; color:#FFFF00; margin-bottom:3px;'>Mods Info</span><span id='mods_menu_load' class='common_link' onclick='javascript:Mods.loadModMenu_load();' style='float:left; margin:0px; margin-left: 42px;'>Load</span><span id='mods_menu_options' class='common_link' onclick='javascript:Mods.loadModMenu_options(); ' style='float:left; margin:0px; margin-left: 45px;'>Options</span><span id='mods_menu_load' class='common_link' onclick='javascript:Mods.loadModMenu_wiki();' style='float:left; margin:0px; margin-left: 41px;'>Wiki</span><span id='mod_options_close' class='common_link' style='margin: 0px; margin-bottom: 2px;' onclick='javascript:addClass(getElem(&apos;mods_form&apos;),&apos;hidden&apos;);'>Close</span>";
        getElem("mods_form", {
            style: {
                width: "464px",
                marginLeft: "-225px"
            }
        });
        Mods.Wikimd.loadDivs();
        Mods.elemClass("scrolling_allowed", "mod_wiki_mods_options");
        Mods.elemClass("scrolling_allowed", "mod_wiki_options");

        if (disable_options) getElem("mods_menu_options", {
            className: "",
            onclick: "",
            style: {
                fontWeight: "bold",
                color: "#999"
            }
        });

        getElem("mod_wiki_search").onmouseover = function (a) { Mods.Wikimd.mouse.x = a.clientX; Mods.Wikimd.mouse.y = a.clientY };

        Mods.Wikimd.populate_item_formulas();
        Mods.Wikimd.populate_pets();
        Mods.Wikimd.populate_family();

    }

    // This function creates the object item_formulas, which contain the crafting formulas for every item, and the source information (which enemies drop it and which npcs sell/buy it)
    Mods.Wikimd.populate_item_formulas = function () {

        Mods.Wikimd.item_formulas = {};
        for (var a in item_base) {
            var item = item_base[a];
            var id = item.b_i;
            var type = ITEM_CATEGORY[item.b_t];
            var name = item.name;
            var params = item.params;
            var temp = item.temp;
            var img = item.img;
            var slot = Mods.Wikimd.item_slots[params.slot] || "none";
            var price = thousandSeperate(params.price);
            var skill = "none";
            var level = "none";
            var enchant = params.enchant_id;
            var magic = params.slot == 10 ? Magic[params.magic].params : false;
            if(slot == "L.Hand" && params.disable_slot == 3){
                slot = "2 Hands";
            }
            for (var b in skills[0]) if (typeof params["min_" + b] != "undefined") {
                skill = capitaliseFirstLetter(b);
                level = item_base[a].params["min_" + b];
            };
            if (typeof params.heal != "undefined") {
                skill = "Food";
                level = params.heal;
            };
            Mods.Wikimd.item_formulas[id] = Mods.Wikimd.item_formulas[id] || {};
            Mods.Wikimd.item_formulas[id].id = id;
            Mods.Wikimd.item_formulas[id].type = type;
            Mods.Wikimd.item_formulas[id].name = name;
            Mods.Wikimd.item_formulas[id].params = params;
            Mods.Wikimd.item_formulas[id].temp = temp;
            Mods.Wikimd.item_formulas[id].img = img;
            Mods.Wikimd.item_formulas[id].skill = skill;
            Mods.Wikimd.item_formulas[id].level = level;
            Mods.Wikimd.item_formulas[id].slot = slot;
            Mods.Wikimd.item_formulas[id].price = price;
            if (enchant) {
                Mods.Wikimd.item_formulas[id].enchant = Mods.Wikimd.item_formulas[id].enchant || {};
                Mods.Wikimd.item_formulas[id].enchant.to_enchant = enchant;
                var ench_type = params.min_accuracy ? Forge.enchantingChancesWeapon : params.min_defense ? Forge.enchantingChancesArmor : Forge.enchantingChancesJewelry;
                Mods.Wikimd.item_formulas[id].enchant.low = Math.min(1, (params.enchant_bonus || 0) + (ench_type[176] && ench_type[176](level) || ench_type[64] && ench_type[64](level) || ench_type[1125] && ench_type[1125](level) || 0));
                Mods.Wikimd.item_formulas[id].enchant.med = Math.min(1, (params.enchant_bonus || 0) + (ench_type[177] && ench_type[177](level) || ench_type[173] && ench_type[173](level) || ench_type[1126] && ench_type[1126](level) || 0));
                Mods.Wikimd.item_formulas[id].enchant.high = Math.min (1, (params.enchant_bonus || 0) + (ench_type[178] && ench_type[178](level) || ench_type[174] && ench_type[174](level) || ench_type[1127] && ench_type[1127](level) || 0));
                Mods.Wikimd.item_formulas[id].enchant.sup = Math.min(1, (params.enchant_bonus || 0) + (ench_type[179] && ench_type[179](level) || ench_type[175] && ench_type[175](level) || ench_type[1128] && ench_type[1128](level) || 0));
                Mods.Wikimd.item_formulas[enchant] = Mods.Wikimd.item_formulas[enchant] || {};
                Mods.Wikimd.item_formulas[enchant].enchant = Mods.Wikimd.item_formulas[enchant].enchant || {};
                Mods.Wikimd.item_formulas[enchant].enchant.from_enchant = id;
            };
            if (magic) Mods.Wikimd.item_formulas[id].magic = magic;
        };
        for (var a in object_base) {
            var o = object_base[a];
            if (typeof o.params.results != "undefined") {
                var results = o.params.results;
                for (var b in results) {
                    var returns = results[b].returns;
                    for (var c in returns) {
                        var item = returns[c];
                        var id = item.id;
                        var source = {};
                        source.name = o.name;
                        source.id = o.b_i;
                        source.type = o.type;
                        source.img = o.img;
                        source.b_t = o.b_t;
                        var skill = results[b].skill;
                        var requires = results[b].requires;
                        var level = item.level;
                        var base_chance = item.base_chance || null;
                        var max_chance = item.max_chance || null;
                        var xp = item.xp || null;
                        var consumes = item.consumes || null;
                        var img = item_base[id].img;

                        //IGNORE CHESTS!
                        if (skill != 'health') {
                            Mods.Wikimd.item_formulas[id].craft = Mods.Wikimd.item_formulas[id].craft || {};
                            Mods.Wikimd.item_formulas[id].craft.level = level;
                            Mods.Wikimd.item_formulas[id].craft.xp = xp;
                            Mods.Wikimd.item_formulas[id].craft.source = Mods.Wikimd.item_formulas[id].craft.source || {};
                            Mods.Wikimd.item_formulas[id].craft.source.object = source;
                            Mods.Wikimd.item_formulas[id].craft.source.skill = skill;
                            Mods.Wikimd.item_formulas[id].craft.source.patterns = Mods.Wikimd.item_formulas[id].craft.source.patterns || {};
                            var i = 0;
                            var isUnique = true;
                            for (var p in Mods.Wikimd.item_formulas[id].craft.source.patterns) {
                                var patterns = Mods.Wikimd.item_formulas[id].craft.source.patterns[p];
                                if (patterns.base_chance == base_chance && patterns.max_chance == max_chance) {
                                    isUnique = false;
                                    for (var q in patterns.requires) {
                                        for (var r in requires) if (q == r && patterns.requires[q] != requires[r]) isUnique = true;
                                        if (isUnique) { break };
                                    }
                                }
                                if (isUnique) { i++ }
                            }
                            if (isUnique) Mods.Wikimd.item_formulas[id].craft.source.patterns[i] = {
                                consumes: consumes,
                                requires: requires,
                                base_chance: base_chance,
                                max_chance: max_chance,
                                source: source
                            }
                        }
                    }
                }
            }
        };
        var forgeFormulas = {};
        for (var i in FORGE_FORMULAS) {
            var item = FORGE_FORMULAS[i];
            var id = item.item_id;
            var o = object_base[36];
            var source = {};
            source.name = o.name;
            source.id = o.b_i;
            source.type = o.type;
            source.img = o.img;
            source.b_t = o.b_t;
            var skill = "forging";
            var level = item.level;
            var chance = item.chance;
            var pattern = item.pattern || null;
            var img = item_base[id].img;
            var xp = item.xp;
            var requires = item.materials;
            var consumes = {};
            for (var a in requires) { consumes[a] = requires[a] }
            delete consumes[36];
            requires[36] = 1;
            Mods.Wikimd.item_formulas[id].craft = Mods.Wikimd.item_formulas[id].craft || {};
            Mods.Wikimd.item_formulas[id].craft.level = level;
            Mods.Wikimd.item_formulas[id].craft.xp = xp;
            Mods.Wikimd.item_formulas[id].craft.source = Mods.Wikimd.item_formulas[id].craft.source || {};
            Mods.Wikimd.item_formulas[id].craft.source.object = source;
            Mods.Wikimd.item_formulas[id].craft.source.skill = skill;
            Mods.Wikimd.item_formulas[id].craft.source.patterns = Mods.Wikimd.item_formulas[id].craft.source.patterns || {};
            Mods.Wikimd.item_formulas[id].craft.source.patterns[i] = {
                pattern: pattern,
                requires: requires,
                chance: chance,
                consumes: consumes
            }
        };
        for (var i in CARPENTRY_FORMULAS) {
            var carp = CARPENTRY_FORMULAS[i];
            var source = {};
            source.name = "House";
            source.type = i;
            var chance = 1;
            var skill = "Carpentry";
            for (var j in carp) {
                var item = carp[j];
                var id = item.item_id;
                var level = item.level;
                var consumes = item.consumes;
                var requires = item.requires;
                var img = item_base[id].img;
                var xp = 0;
                for (var a in consumes) if (a != "length") { xp += CARPENTRY_MATERIAL_XP[consumes[a].id] * consumes[a].count };
                var requires = consumes;
                Mods.Wikimd.item_formulas[id].craft = Mods.Wikimd.item_formulas[id].craft || {};
                Mods.Wikimd.item_formulas[id].craft.level = level;
                Mods.Wikimd.item_formulas[id].craft.xp = xp;
                Mods.Wikimd.item_formulas[id].craft.source = Mods.Wikimd.item_formulas[id].craft.source || {};
                Mods.Wikimd.item_formulas[id].craft.source.object = source;
                Mods.Wikimd.item_formulas[id].craft.source.skill = skill;
                Mods.Wikimd.item_formulas[id].craft.source.patterns = Mods.Wikimd.item_formulas[id].craft.source.patterns || {};
                Mods.Wikimd.item_formulas[id].craft.source.patterns[j] = {
                    requires: requires,
                    chance: chance,
                    consumes: consumes
                }
            };
        };
        for (var a in npc_base) {
            var o = npc_base[a];
            if (typeof o.params.drops != "undefined") {
                var drops = o.params.drops;
                for (var b in drops) {
                    var item = drops[b];
                    var id = item.id;
                    var name = o.name;
                    var item_id = o.b_i;
                    var type = o.b_t;
                    var img = o.img;
                    var chance = item.chance;
                    var level = o.level;
                    Mods.Wikimd.item_formulas[id].drop = Mods.Wikimd.item_formulas[id].drop || {};
                    Mods.Wikimd.item_formulas[id].drop.sources = Mods.Wikimd.item_formulas[id].drop.sources || {};
                    Mods.Wikimd.item_formulas[id].drop.sources[a] = {
                        name: name,
                        id: item_id,
                        level: level,
                        type: type,
                        chance: chance,
                        img: img
                    }
                }
            };
            if (typeof o.temp.content != "undefined") {
                var sold = o.temp.content;
                for (var b in sold) {
                    var item = sold[b];
                    var id = item.id;
                    var name = o.name;
                    var item_id = o.b_i;
                    var type = o.b_t;
                    var img = o.img;
                    var count = item.count || 0;
                    var spawn = item.spawn || false;
                    Mods.Wikimd.item_formulas[id].sold = Mods.Wikimd.item_formulas[id].sold || {};
                    Mods.Wikimd.item_formulas[id].sold.sources = Mods.Wikimd.item_formulas[id].sold.sources || {};
                    Mods.Wikimd.item_formulas[id].sold.sources[a] = {
                        name: name,
                        id: item_id,
                        count: count,
                        type: type,
                        spawn: spawn,
                        img: img
                    }
                }
            }
        }
        Mods.Wikimd.populate_formulas();
    };

    Mods.Wikimd.populate_formulas = function () {
        var item_formulas = Mods.Wikimd.item_formulas;
        var formulas = Mods.Wikimd.formulas = {};
        var i = 0;
        var req, requires, con, consumes, req_length, con_length, c;
        for (var a in item_formulas) if (typeof item_formulas[a].craft != "undefined") {
            var patterns = item_formulas[a].craft.source.patterns;
            for (var b in patterns) {
                formulas[i] = {};
                formulas[i].id = item_formulas[a].id;
                formulas[i].img = item_formulas[a].img;
                formulas[i].name = item_formulas[a].name;
                formulas[i].skill = item_formulas[a].craft.source.skill;
                formulas[i].object = patterns[b].source || item_formulas[a].craft.source.object;
                formulas[i].pattern = patterns[b];
                formulas[i].xp = item_formulas[a].craft.xp;
                formulas[i].level = item_formulas[a].craft.level;
                req = {};
                requires = formulas[i].pattern.requires;
                con = {};
                consumes = formulas[i].pattern.consumes;
                for (c in consumes) {
                    if (typeof consumes[c] == "object") { con[consumes[c].id] = consumes[c].count }
                    else { con[c] = consumes[c] }
                };
                for (c in requires) {
                    if (typeof requires[c] == "object") {
                        if (typeof requires[c].id != "undefined" && typeof requires[c].count != "undefined") { req[requires[c].id] = requires[c].count }
                        else { req[requires[c]] = 1 }
                    } else if (formulas[i].object.name != "Anvil") { req[requires[c]] = 1 }
                    else { req[c] = requires[c] }
                };
                req_length = 0;
                con_length = 0;
                for (c in req) if (c != "length") req_length += parseInt(req[c]);
                for (c in con) if (c != "length") con_length += parseInt(con[c]);
                delete formulas[i].pattern.consumes;
                delete formulas[i].pattern.requires;
                formulas[i].pattern.requires = req;
                formulas[i].pattern.consumes = con;
                formulas[i].pattern.requires.length = req_length;
                formulas[i].pattern.consumes.length = con_length;
                i++;
            }
        }
    };

    Mods.Wikimd.populate_pets = function () {
        var pet_family = Mods.Wikimd.pet_family = {};
        var i = 0;
        var length = pets.length;
        for (i = 0; i < 2; i++) {
            for (var a in pets) {
                var n = length - a;
                var pet = pets[n].params.item_id;
				var level = pets[n].params.level;
                pet_family[pet] = pet_family[pet] || {};
                pet_family[pet][level] = pet;
                var next = pets[n].params.next_pet_item_id;
                if (next != undefined) pet_family[pet][level + 1] = next;
                for (var b in pet_family[pet]) for (var c in pet_family[pet]) {
                    pet_family[pet_family[pet][b]] = pet_family[pet_family[pet][b]] || {};
                    pet_family[pet_family[pet][b]][c] = pet_family[pet][c];
                };
                if (i == 1) for (b in Mods.Wikimd.formulas) if (Mods.Wikimd.formulas[b].id == pet && Mods.Wikimd.formulas[b].object.name != "Big Treasure Chest") {
                    pet_family[pet] = Mods.Wikimd.formulas[b];
                    break;
                }
            }
        }
    };

    Mods.Wikimd.populate_family = function () {
        var pet_family = Mods.Wikimd.pet_family;
        var family = Mods.Wikimd.family = {};
        for (var a in pet_family) {
            var child = pet_family[a][1] != undefined ? pet_family[a][1] : pet_family[a][2] != undefined ? pet_family[a][2] : pet_family[a][3] != undefined ? pet_family[a][3] : pet_family[a][4] != undefined ? pet_family[a][4] : pet_family[a][5];
            if (typeof child == "number" && child > 0) {
                var child_name = pets[item_base[child].params.pet].name;
                child_name = child_name.replace(/ ?(Baby|\[|\]|Ancient|Legendary|Rare|Common) ?/gi,"");
                family[a] = child_name;
            }
        }
    };

    // This function creates the wiki tables. There is only one set of tables, which are all created initially with this; later, as the wiki is used, this single set of tables is modified (using colspan/rowspan, and other table properties) so that the table is reformatted based on the content that will appear in it.
    Mods.Wikimd.loadDivs = function () {

        function load_div (id, row, col) {
            var span_content = (id == 0 ? "<div style='padding-top: 3px' " : "<span ") + "class='scrolling_allowed' id='wiki_row";
            var div_content = "<div class='scrolling_allowed' id='wiki_row";
            var tbl_div = div_content + row + "_col" + col + "_div_" + id + "'>";
            if (col == 0) { tbl_div += div_content + row + "_col" + col + "_img_" + id + "'>&nbsp;</div>" }
            tbl_div += span_content + row + "_col" + col + "_text_" + id + (id == 0 ? "'>&nbsp;</div>" : "'>&nbsp;</span>");
            tbl_div += "</div>";
            return tbl_div;
        }

        function load_col (id, row) {
            var col_content = "<td class='scrolling_allowed' id='wiki_row";
            var tbl_col = "";
            for (var col = 0; col < 6; col++) tbl_col += col_content + row + "_col" + col + "_" + id + "'>" + load_div(id, row, col) + "</td>";
            return tbl_col;
        }

        function load_row (maxID) {
            var row_content = "<tr class='scrolling_allowed hidden' id='wiki_row";
            var row0_content = "<tr class='scrolling_allowed wiki_f2 hidden' id='wiki_row";
            var tbl_row = "";
            for (var id = 0; id < maxID; id++) for (var row = 0; row < 3; row++) {
                if (row == 0) tbl_row += row0_content + row + "_" + id + "'><td colspan='7'>&nbsp;</td></tr>";
                else tbl_row += row_content + row + "_" + id + "'>" + load_col(id, row) + "</tr>";
            }
            return tbl_row;
        }

        function load_tbl (maxID) {
            var tbl_content = "<table id='mod_wiki_search_items_table' class='scrolling_allowed' cellspacing='0' cellpadding='0' style='font-size: 0.8em; width:100%; margin-top:5px; padding-right:1px;'>";
            var table = tbl_content + load_row(maxID) + "</table>";
            return table;
        }

        // These variables determine the formatting each wiki filter (each drop-down option and text box).
        var wikiType =             "<select     id='mods_wiki_type'                  class='market_select scrolling_allowed'             style='float:left;     margin:0px;                         width:70px;'></select>";
        var wikiItem =             "<select     id='mods_wiki_type_item'             class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiMnst =             "<select     id='mods_wiki_type_monster'          class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiVend =             "<select     id='mods_wiki_type_vendor'           class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiCrft =             "<select     id='mods_wiki_type_craft'            class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiPets =             "<select     id='mods_wiki_type_pet'              class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiSpel =             "<select     id='mods_wiki_type_spell'            class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiEnch =             "<select     id='mods_wiki_type_enchant'          class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:70px;         display:none;                        '></select>";
        var wikiItemType =         "<select     id='mods_wiki_type_item_type'        class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:80px;         display:none;                        '></select>";
        var wikiItemSkll =         "<select     id='mods_wiki_type_item_skill'       class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:80px;         display:none;                        '></select>";
        var wikiCrftSkll =         "<select     id='mods_wiki_type_craft_skill'      class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:80px;         display:none;                        '></select>";
        var wikiCrftSrce =         "<select     id='mods_wiki_type_craft_source'     class='market_select scrolling_allowed'             style='float:left;     margin:0px;     margin-left:6px;     width:80px;         display:none;                        '></select>";
        var wikiName =             "<input      id='mods_wiki_name' type='text'      class='market_select scrolling_allowed'             style='float:left;                     margin-left:6px;     width:200px;        display:none;     height:16px;       '></input>";
        var wikiGo =               "<button     id='mods_wiki_load'                  class='market_select pointer scrolling_allowed'     style='                margin:0px;                                                                margin-bottom:2px; '>Go!</button>";
		
		createElem("div", "mods_form", {
            id: "mod_wiki_mods_options",
            className: "common_border_bottom scrolling_allowed",
            style: "width: 100%; height: 24px; margin-bottom: 5px; font-size: .8em; display: none;",
            innerHTML: wikiType + wikiItem + wikiMnst + wikiVend + wikiCrft + wikiEnch + wikiPets + wikiSpel + wikiItemType + wikiItemSkll + wikiCrftSkll + wikiCrftSrce + wikiName + wikiGo
        });
		createElem("span", "mod_wiki_mods_options", {
            id: "mods_wiki_range_separate",
            className: "scrolling_allowed",
            style: "float: left; margin-left: 6px; height: 20px; display: none; border-left: 1px solid #FFF;",
            onclick: "javascript:Mods.Wikimd.loadWikiType(false);",
            innerHTML: "<select id='mods_wiki_range' class='market_select scrolling_allowed' style='float:left; margin-left:6px; width:70px; display:none; margin-top:0px;'></input>"
        });
        createElem("span", "mod_wiki_mods_options", {
            id: "mods_wiki_level",
            className: "scrolling_allowed",
            style: "float: left; margin-left: 6px; display: none;",
            onclick: "javascript:Mods.Wikimd.loadWikiType(false);",
            innerHTML: "<input type='text' id='mods_wiki_level_low' class='market_select scrolling_allowed' style='width: 25px;margin-right: 5px; height:16px; float:left;'><span class='scrolling_allowed' style='float:left; margin-top:3px;'>to</span><input type='text' id='mods_wiki_level_high' class='market_select scrolling_allowed' style='width: 25px;margin-left: 5px; height:16px; float:left;'>"
        });
        createElem("div", "mods_form", {
            id: "mod_wiki_options",
            className: "scrolling_allowed",
            style: "display: block;",
            innerHTML: "<span class='scrolling_allowed' id='mod_wiki_search'>" + load_tbl(300) + "</span>"
        });
        createElem("div", wrapper, {
            id: "wiki_recipe_form",
            className: "menu",
            style: "position: relative; left: 50%; top: 30%; z-index: 99999999; width: 145px; height: 163px; display: none;"
        });
        createElem("span", "wiki_recipe_form", {
            id: "wiki_recipe_top",
            innerHTML: "<span class='pointer' style='font-weight: bold; color: #FFFFFF; float: right;' onclick='javascript: getElem(&apos;wiki_recipe_form&apos;).style.display = &apos;none&apos;'>Close</span>"
        });
        createElem("div", "wiki_recipe_form", {
            id: "wiki_recipe_holder",
            style: "position: relative; display: inline-block; float: left;"
        });

        for (var a = 0; a < 4; a++) for (var b = 0; b < 4; b++) createElem("div", "wiki_recipe_holder", {
            id: "wiki_formula_" + a + "_" + b,
            className: "inv_item",
            style: "display: inline-block; width: 32px; height: 32px; border: 1px solid #999; margin: 1px; float: left;",
            innerHTML: "&nbsp;"
        });

        // Options for the dropdown menues. These are the filter options; adding to them adds more options to be searched/filtered. The function (below) Mods.Wikimd.loadWikiType() adjusts the "Range" filter.
        getElem("mods_wiki_type").innerHTML =                 "<option value='-1'>Select</option>          <option value='item'>ITEM</option>                <option value='monster'>MOB</option>              <option value='vendor'>NPC</option>                  <option value='craft'>CRAFT</option>                <option value='enchant'>ENCHANT</option>          <option value='spell'>SPELL</option>             <option value='pet'>PET</option>";
        getElem("mods_wiki_type_item").innerHTML =            "<option value='all'>All</option>            <option value='skill'>Skill</option>              <option value='type'>Type</option>                <option value='name'>Name</option>";
        getElem("mods_wiki_type_item_type").innerHTML =       "<option value='-1'>Select</option>          <option value='weapons'>Weapon</option>           <option value='r.hand armors'>Shield</option>     <option value='chest'>Chest</option>                 <option value='helm'>Helm</helm>                    <option value='pants'>Pants</option>              <option value='glove'>Gloves</option>            <option value='boots'>Boots</option>                    <option value='cape'>Cape</option>               <option value='jewelry'>Jewelry</option>        <option value='magic'>Magic</option>            <option value='materials'>Material</option>         <option value='tools'>Tool</option>                  <option value='foods'>Food</option>";
        getElem("mods_wiki_type_item_skill").innerHTML =      "<option value='-1'>Select</option>          <option value='accuracy'>Accuracy</option>        <option value='strength'>Strength</option>        <option value='defense'>Defense</option>             <option value='health'>Health</option>              <option value='magic'>Magic</option>              <option value='alchemy'>Alchemy</option>         <option value='woodcutting'>Woodcut</option>            <option value='farming'>Farming</option>         <option value='fishing'>Fishing</option>        <option value='cooking'>Cooking</option>        <option value='jewelry'>Jewelry</option>            <option value='carpentry'>Carpentry</option>         <option value='forging'>Forging</option>        <option value='mining'>Mining</option>";
        getElem("mods_wiki_type_monster").innerHTML =         "<option value='all'>All</option>            <option value='name'>Name</option>                <option value='item'>Item</option>";
        getElem("mods_wiki_type_vendor").innerHTML =          "<option value='all'>All</option>            <option value='name'>Name</option>                <option value='item'>Item</option>";
        getElem("mods_wiki_type_craft").innerHTML =           "<option value='all'>All</option>            <option value='skill'>Skill</option>              <option value='source'>Source</option>            <option value='item'>Item</option>";
        getElem("mods_wiki_type_craft_skill").innerHTML =     "<option value='-1'>Select</option>          <option value='alchemy'>Alchemy</option>          <option value='woodcutting'>Woodcut</option>      <option value='farming'>Farming</option>             <option value='fishing'>Fishing</option>            <option value='cooking'>Cooking</option>          <option value='jewelry'>Jewelry</option>         <option value='carpentry'>Carpentry</option>            <option value='forging'>Forging</option>         <option value='mining'>Mining</option>          <option value='magic'>Magic</option>";
        getElem("mods_wiki_type_craft_source").innerHTML =    "<option value='-1'>Select</option>          <option value='furnace'>Furnace</option>          <option value='anvil'>Anvil</option>              <option value='campfire'>Campfire</option>           <option value='carpentry'>House/Farm</option>       <option value='kettle'>Kettle</option>            <option value='gather'>Gathering</option>        <option value='other'>Other</option>";
        getElem("mods_wiki_type_pet").innerHTML =             "<option value='all'>All</option>            <option value='name'>Name</option>                <option value='family'>Family</option";
        getElem("mods_wiki_type_enchant").innerHTML =         "<option value='all'>All</option>            <option value='item'>Item</option>";
        getElem("mods_wiki_type_spell").innerHTML =           "<option value='all'>All</option>            <option value='name'>Name</option>";
    
		var wiki_elements = {'type': 0, 'type_item': 1, 'type_monster': 1, 'type_vendor': 1, 'type_craft': 1, 'type_pet': 1, 'type_spell': 1, 'type_enchant': 1, 'type_item_type': 2, 'type_item_skill': 2, 'type_craft_skill': 2, 'type_craft_source': 2},
			w_events = ['onchange'];
		
		for (var i in wiki_elements) for (var j in w_events) {
			getElem('mods_wiki_' + i).setAttribute(w_events[j], "javascript: Mods.Wikimd.loadWikiType(" + wiki_elements[i] + ");");
		}
		getElem('mods_wiki_name').setAttribute("onclick", "javascript:Mods.Wikimd.loadWikiType(false);")
		getElem('mods_wiki_load').setAttribute("onclick", "javascript:Mods.Wikimd.populateWiki(true);")
	};

    Mods.Wikimd.nameMenu = function (type, id) {
        typeof type !== "string" && (type = false);
        typeof id !== "number" && (id = false);
        var obj = type == "item" || type == "craft" || type == "pet" || type == "spell" || type == "enchant" ? item_base : type == "monster" ? npc_base : false;
        if (type && id && obj) {
            var d = getElem("action_menu");
            addClass(d, "hidden");
            var mouse = Mods.Wikimd.mouse;
            d.style.top = mouse.y + 10 + "px";
            d.style.left = mouse.x + "px";
            if ((type == "item" || type == "craft" || type == "pet" || type == "spell" || type == "enchant") && modOptions.chatmd.loaded) {
                var message = "Mods.Chatmd.chatCommands";
                var name = item_base[id].name;
                var item = message + "(&apos;\/wiki item name " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    item = "<span class='line' onclick='" + item + "' style='margin-left:-5px;'>Check Wiki<span class='item'>ITEM</span></span>";
                var mob = message + "(&apos;\/wiki mob item " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    mob = "<span class='line' onclick='" + mob + "' style='margin-left:-5px;'>Check Wiki<span class='item'>MOB</span></span>";
                var npc = message + "(&apos;\/wiki npc item " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    npc = "<span class='line' onclick='" + npc + "' style='margin-left:-5px;'>Check Wiki<span class='item'>NPC</span></span>";
                var craft = message + "(&apos;\/wiki craft item " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    craft = "<span class='line' onclick='" + craft + "' style='margin-left:-5px;'>Check Wiki<span class='item'>CRAFT</span></span>";
                var enchant = message + "(&apos;\/wiki enchant item " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    enchant = "<span class='line' onclick='" + enchant + "' style='margin-left:-5px;'>Check Wiki<span class='item'>ENCHANT</span></span>";
                var pet = message + "(&apos;\/wiki pet name " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    pet = item_base[id].params.pet > -1 ? "<span class='line' onclick='" + pet + "' style='margin-left:-5px;'>Check Wiki<span class='item'>PET</span></span>" : "";
                var spell = message + "(&apos;\/wiki spell name " + name + "&apos;);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)";
                    spell = item_base[id].params.slot == 10 ? "<span class='line' onclick='" + spell + "' style='margin-left:-5px;'>Check Wiki<span class='item'>SPELL</span></span>" : "";
                d.innerHTML = "<div style='padding-left: 8px;'>" + item + mob + npc + craft + enchant + pet + spell + "<span class='line' onclick='addClass(getElem(&apos;action_menu&apos;),&apos;hidden&apos;)' style='margin-left:-5px;'>Close</span></div>";
            } else if (type == "monster" && modOptions.rclick.loaded) {
                var name = npc_base[id].name;
                d.innerHTML = "<div style='padding-left: 3px;'><span class='line' onclick='ActionMenu.mobDrops(" + id + ",4);addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;)' style='margin-left:-5px;'><span class='item'>" + name + "</span>Drops</span><span class='line' onclick='ActionMenu.combatCheck(" + id + ");addClass(getElem(&apos;action_menu&apos;), &apos;hidden&apos;);'>Combat Analysis</span><span class='line' onclick='addClass(getElem(&apos;action_menu&apos;),&apos;hidden&apos;)'>Close</span></div>";
            }
            d.innerHTML.length > 0 && removeClass(d, "hidden")
        }
    };

    // This function inputs all the values and images that appear in the wiki.
    Mods.Wikimd.populateWiki = function (list, sortlist) {
        var type, sortlist, list, a, b, d, e, i, j, k, content, m, n, o, p, q, r, type, item, img, mins, drops, images, min_chance, max_chance, n_onclick, data, div_r, div_c, div_d, div_i, div_t, value, for_id, magic;
        var oldwiki = Mods.Wikimd.oldSortValue;
        if (list == true) {
            Mods.Wikimd.newSortValue = Mods.Wikimd.currentSort();
            list = Mods.Wikimd.populateWikiList()
        } else list = list || Mods.Wikimd.populateWikiList();

        if (loadedMods.indexOf("Chatmd") != -1) {
            var go = getElem("mods_wiki_load");
            go.innerHTML = "Back!"
            go.setAttribute("onclick", "javascript:Mods.Chatmd.chatCommands(\'/wiki " + oldwiki + "\')");
        }

        type = getElem("mods_wiki_type").value;
        sortlist = sortlist || Mods.Wikimd.sortWiki(list,type,Mods.Wikimd.oldSort[type]);
        content = false;
        // This insures that the list of results has at least 1 result.
        for (a in list) { content = true; break };
        // This clears the wiki (by hiding the tables) so it can be repopulated without having old values still appear. (This is useful if a new search will populate fewer tables than the previous search populated: example, old search has 300 items, new search has 100 items; without this for statement, the last 200 of the old search's values would still appear)
        for (j = 0; j < 300; j++) for (a = 0; a < 3; a++) addClass(getElem("wiki_row" + a + "_" + j), "hidden");

        if (content) {

            d = {
                hidden: "scrolling_allowed hidden",
                name: "scrolling_allowed wiki_name",
                img: "scrolling_allowed wiki_img",
                nameT: "scrolling_allowed wiki_nameText",
                b1nameT: "scrolling_allowed wiki_base1 wiki_nameText",
                b1nameI: "scrolling_allowed wiki_base1 wiki_nameImg",
                p13: "scrolling_allowed wiki_p13",
                p26: "scrolling_allowed wiki_p26",
                p35: "scrolling_allowed wiki_p35",
                p65: "scrolling_allowed wiki_p65",
                row1: "scrolling_allowed wiki_row1",
                row2: "scrolling_allowed wiki_row2",
                a1: "scrolling_allowed wiki_a1",
                a2: "scrolling_allowed wiki_a2",
                b2p2bLbTmRmL: "scrolling_allowed market_select wiki_base2 wiki_pad2 wiki_bT wiki_bL wiki_mR wiki_mL",
                b1h35bL: "scrolling_allowed market_select wiki_h35 wiki_base1 wiki_bL",
                b1h17bLbT: "scrolling_allowed market_select wiki_h17 wiki_base1 wiki_h17 wiki_bT wiki_bL",
                b1h35h17bLbT: "scrolling_allowed market_select wiki_h35 wiki_base1 wiki_h17 wiki_bT wiki_bL",
                b1h35bLbT: "scrolling_allowed market_select wiki_base1 wiki_h35 wiki_bT wiki_bL",
                b1h35: "scrolling_allowed market_select wiki_h35 wiki_base1",
                b1h17bT: "scrolling_allowed market_select wiki_h17 wiki_base1 wiki_bT",
                b0h17bR: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bR",
                b0h17bRbL: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bR wiki_bL",
                b0h17bL: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bL",
                b0h17bRbT: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bT wiki_bR",
                b0h17bRbLbT: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bT wiki_bR wiki_bL",
                b0h17bLbT: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bT wiki_bL",
                b0h17bRmL: "scrolling_allowed market_select wiki_base0 wiki_h17 wiki_bR wiki_mL",
                pb1h35: "scrolling_allowed market_select pointer wiki_h35 wiki_base1",
                pb1h35bT: "scrolling_allowed market_select pointer wiki_h35 wiki_base1 wiki_bT",
                pb1h17bT: "scrolling_allowed market_select pointer wiki_h17 wiki_base1 wiki_bT",
                pb1h17: "scrolling_allowed market_select pointer wiki_h17 wiki_base1",
                pb1p2: "scrolling_allowed market_select pointer wiki_base1 wiki_pad2",
                pb0h17bR: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bR",
                pb0h17bRbL: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bR wiki_bL",
                pb0h17bL: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bL",
                pb0h17bRbT: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bT wiki_bR",
                pb0h17bRbLbT: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bT wiki_bR wiki_bL",
                pb0h17bLbT: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bT wiki_bL",
                pb0h17bRmL: "scrolling_allowed market_select pointer wiki_base0 wiki_h17 wiki_bR wiki_mL"
            };

            e = {
                item: {
                    1: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h35],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "name") }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.name],
                                innerHTML: ["Item Name"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bR, d.b0h17bR],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "level") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Level"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "skill") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Skill"]
                            }
                        },
                        3: {
                            className: [d.p26],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "price") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Price"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bL, d.b0h17bL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "slot") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Slot"]
                            }
                        }
                    },
                    2: {
                        className: [d.row2, d.a2],
                        0: {
                            className: [d.hidden]
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbT, d.b0h17bRbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "power") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Power"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbLbT, d.b0h17bRbLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "aim") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Aim"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbLbT, d.b0h17bRbLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "armor") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Armor"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbLbT, d.b0h17bRbLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "magic") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Magic"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bLbT, d.b0h17bLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "speed") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Speed"]
                            }
                        }
                    }
                },
                monster: {
                    1: {
                        className: [d.row2, d.a2],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb0h17bR, d.b0h17bR],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "moster", "respawn") }]
                            },
                            img: {
                                className: [d.hidden]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Respawn Time"]
                            }
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRmL, d.b0h17bRmL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "monster", "level") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Level"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "monster", "health") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Health"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "monster", "accuracy") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["ACC"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "monster", "strength") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["STR"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bL, d.b0h17bL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "monster", "defense") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["DEF"]
                            }
                        }
                    },
                    2: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h17bT, d.pb1h35bT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "monster", "name") }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.name],
                                innerHTML: ["Monster Name"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                        className: [d.p65],
                            div: {
                                className: [d.b1h17bLbT, d.b2p2bLbTmRmL],
                                onclick: [function () { }]
                            },
                            text: {
                                className: [d.b1nameI, "scrolling_allowed"],
                                innerHTML: ["Item Drops"]
                            }
                        }
                    }
                },
                vendor: {
                    1: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h35, d.pb1p2],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "vendor", "name") }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.name],
                                innerHTML: ["Vendor Name"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                            className: [d.p65],
                            div: {
                                className: [d.b1h35bL, "scrolling_allowed market_select wiki_base1 wiki_pad2 wiki_bL"],
                                onclick: [function () { }]
                            },
                            text: {
                                className: [d.b1nameI, "scrolling_allowed"],
                                innerHTML: ["Buys / Sells"]
                            }
                        }
                    }
                },
                craft: {
                    1: {
                        className: [d.row2, d.a2],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb0h17bR, d.b0h17bR],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "location") }]
                            },
                            img: {
                                className: [d.hidden]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Craft Location"]
                            }
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRmL, d.b0h17bRmL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "level") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Level"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "skill") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Skill"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "base_chance") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Min%"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "max_chance") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Max%"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bL, d.b0h17bL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "xp") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Exp"]
                            }
                        }
                    },
                    2: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h17bT, d.pb1h35bT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "craft", "name") }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.name],
                                innerHTML: ["Craft Name"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                            className: [d.p65],
                            div: {
                                className: [d.b1h35h17bLbT, d.b1h35bLbT],
                                onclick: [function () { }]
                            },
                            text: {
                                className: [d.b1nameI, "scrolling_allowed"],
                                innerHTML: ["Required Materials"]
                            }
                        }
                    }
                },
                pet: {
                    1: {
                        className: [d.row2, d.a2],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb0h17bR, d.pb1h17],
                                onclick: [function () { }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.b1nameT, d.nameT],
                                innerHTML: ["<span style='font-weight: normal color: #999 font-size: 1.05em'>Req to level: &nbsp;</span>" + "<span onclick='Mods.Wikimd.sortWiki(null, &apos;pet&apos;, &apos;stones&apos;)'>SoE</span>" + "&nbsp; | &nbsp;" + "<span onclick='Mods.Wikimd.sortWiki(null, &apos;pet&apos;, &apos;xp_required&apos;)'>Exp</span>"]
                            }
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRmL, d.b0h17bRmL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "pet", "aim") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Aim"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "pet", "power") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Power"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "pet", "armor") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Armor"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "pet", "magic") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Magic"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bL, d.b0h17bL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "pet", "speed") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Speed"]
                            }
                        }
                    },
                    2: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h17bT, d.pb1h35bT],
                                onclick: [function () { }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.b1nameT, d.name],
                                innerHTML: ["<span class='pointer' onclick='Mods.Wikimd.sortWiki(null, &apos;pet&apos;, &apos;name&apos;)'>Name</span>" + "&nbsp; | &nbsp;" + "<span class='pointer'  onclick='Mods.Wikimd.sortWiki(null, &apos;pet&apos;, &apos;family&apos;)'>Family</span>" + "&nbsp; | &nbsp;" + "<span class='pointer'  onclick='Mods.Wikimd.sortWiki(null, &apos;pet&apos;, &apos;inventory_slots&apos;)'>Slots</span>"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                            className: [d.p65],
                            div: {
                                className: [d.b1h35h17bLbT, d.b2p2bLbTmRmL],
                                onclick: [function () { }]
                            },
                            text: {
                                className: [d.b1nameI, "scrolling_allowed"],
                                innerHTML: ["Evolution Chain"]
                            }
                        }
                    }
                },
                spell: {
                    1: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h35],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "name") }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.name],
                                innerHTML: ["Spell Name"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bR, d.b0h17bR],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "level") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Level"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "cooldown") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["CD"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "price") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Price"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "casts") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Casts"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bL, d.b0h17bL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "cost_s") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Cost/S"]
                            }
                        }
                    },
                    2: {
                        className: [d.row2, d.a2],
                        0: {
                            className: [d.hidden]
                        },
                        1: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbT, d.b0h17bRbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "damage") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Dmg"]
                            }
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbLbT, d.b0h17bRbLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "exp") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Exp"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbLbT, d.b0h17bRbLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "spell", "penetration") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Pen"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbLbT, d.b0h17bRbLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "dmg_s") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Dmg/S"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bLbT, d.b0h17bLbT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "item", "exp_s") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Exp/S"]
                            }
                        }
                    }
                },
                enchant: {
                    1: {
                        className: [d.row2, d.a2],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb0h17bR, d.b0h17bR],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "enchant", "enchant") }]
                            },
                            img: {
                                className: [d.hidden]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Enchanted Item"]
                            }
                        },
                        1: {
                            className: [d.hidden]
                        },
                        2: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRmL, d.b0h17bRmL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "enchant", "low") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Low %"]
                            }
                        },
                        3: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "enchant", "med") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Med %"]
                            }
                        },
                        4: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bRbL, d.b0h17bRbL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "enchant", "high") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["High %"]
                            }
                        },
                        5: {
                            className: [d.p13],
                            div: {
                                className: [d.pb0h17bL, d.b0h17bL],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "enchant", "sup") }]
                            },
                            text: {
                                className: [d.b1nameT],
                                innerHTML: ["Sup %"]
                            }
                        }
                    },
                    2: {
                        className: [d.row1, d.a1],
                        0: {
                            className: [d.p35],
                            div: {
                                className: [d.pb1h17bT, d.pb1h35bT],
                                onclick: [function () { Mods.Wikimd.sortWiki(null, "enchant", "name") }]
                            },
                            img: {
                                className: [d.hidden, d.img]
                            },
                            text: {
                                className: [d.name],
                                innerHTML: ["Item Name"],
                                style: [{marginTop: "-8px"}]
                            }
                        },
                        1: {
                            className: [d.hidden]
                        },
                        2: {
                            className: [d.p65],
                            div: {
                                className: [d.b1h17bLbT, d.b2p2bLbTmRmL],
                                onclick: [function () { }]
                            },
                            text: {
                                className: [d.b1nameI, "scrolling_allowed"],
                                innerHTML: ["Enchant Chain"]
                            }
                        }
                    }
                }
            };

            for (i = 0; i < 300; i++) {

                a = i - 1;
                removeClass(getElem("wiki_row0_" + i), "hidden");

                if (!sortlist || i > sortlist.length) return;
                if (i > 0) {
                    if (sortlist[a] == undefined) return;
                    data = Mods.Wikimd.tableData(a, list, sortlist);
                    if (!data) return;
                    item = data.item;
					drops = data.drops;
                    img = data.img;
                    items = data.items;
                    images = data.images;
                    max_chance = data.max_chance;
                    min_chance = data.min_chance;
                    n_onclick = data.n_onclick;
                    mins = data.mins;
                    magic = data.magic;
                    for_id = item.object && item.object.name == "Anvil" ? Mods.findWithAttr(FORGE_FORMULAS, "item_id", Mods.Wikimd.formulas[sortlist[a]].id) : 0;
                };

                for (m = 1; m < 3; m++) {
                    div_r = getElem("wiki_row" + m + "_" + i);
                    if (e[type][m] == undefined) div_r.className = d.hidden;
                    else {
                        div_r.className = i == 0 ? e[type][m].className[0] : e[type][m].className[1];
                        for (n = 0; n < 6; n++) {
                            div_c = getElem("wiki_row" + m + "_col" + n + "_" + i);
                            if (e[type][m][n] == undefined) div_c.className = d.hidden;
                            else {
                                div_c.className = e[type][m][n].className;
                                div_d = getElem("wiki_row" + m + "_col" + n + "_div_" + i);
                                div_i = getElem("wiki_row" + m + "_col" + n + "_img_" + i);
                                div_t = getElem("wiki_row" + m + "_col" + n + "_text_" + i);
                                j = {"div": div_d, "img": div_i, "text": div_t};
                                for (k in j) if (j[k] != null && e[type][m][n][k] != undefined) {
                                    if (i == 0) {
                                        j[k].className = e[type][m][n][k].className[0];
                                        e[type][m][n][k].innerHTML && (j[k].innerHTML = e[type][m][n][k].innerHTML[0]);
                                        e[type][m][n][k].onclick && (j[k].onclick = e[type][m][n][k].onclick[0]);
                                        e[type][m][n][k].style && (j[k].style.marginTop = e[type][m][n][k].style[0].marginTop);
                                    } else {
                                        if (e[type][m][n][k].className[1] != undefined) value = e[type][m][n][k].className[1];
                                        else value = e[type][m][n][k].className[0];
                                        j[k].className = value;
                                    }
                                }
                            }
                        }
                    }
                }

                function enclose (text) {
                    var txt = "<div style='padding-top: 3px'>" + text + "</div>";
                    return txt;
                }

                if (i > 0) {
                    if (type == "item") {

                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "Mods.Wikimd.nameMenu(\'item\'," + item.id + ")");
                        getElem("wiki_row1_col0_div_" + i).style.height = "";
                        getElem("wiki_row1_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        getElem("wiki_row1_col0_img_" + i).item_id = item.id;
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( item.name );
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = item.name.length < 18 ? "-8px" : "-15px";
                        getElem("wiki_row1_col1_div_" + i).style.height = "";
                        getElem("wiki_row1_col1_text_" + i).innerHTML = enclose( item.level );
                        getElem("wiki_row1_col2_text_" + i).innerHTML = enclose( item.skill );
                        getElem("wiki_row1_col3_text_" + i).innerHTML = enclose( item.price + " coins" );
                        getElem("wiki_row1_col4_text_" + i).innerHTML = enclose( item.slot );

                        getElem("wiki_row2_col1_div_" + i).style.height = "";
                        getElem("wiki_row2_col1_text_" + i).innerHTML = enclose( item.params.power || "-" );
                        getElem("wiki_row2_col2_div_" + i).style.height = "";
                        getElem("wiki_row2_col2_text_" + i).innerHTML = enclose( item.params.aim || "-" );
                        getElem("wiki_row2_col3_text_" + i).innerHTML = enclose( item.params.armor || "-" );
                        getElem("wiki_row2_col4_text_" + i).innerHTML = enclose( item.params.magic || "-" );
                        getElem("wiki_row2_col5_text_" + i).innerHTML = enclose( item.params.speed || "-" );

                    } else if (type == "monster") {

                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "");
                        getElem("wiki_row1_col0_div_" + i).style.height = "";
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = "";
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( "Respawn: " + mins + " Minute" + (mins > 1 ? "s" : "") );
                        getElem("wiki_row1_col1_div_" + i).style.height = "";
                        getElem("wiki_row1_col1_text_" + i).innerHTML = enclose( FIGHT.calculate_monster_level(item) );
                        getElem("wiki_row1_col2_text_" + i).innerHTML = enclose( item.params.health );
                        getElem("wiki_row1_col3_text_" + i).innerHTML = enclose( item.temp.total_accuracy );
                        getElem("wiki_row1_col4_text_" + i).innerHTML = enclose( item.temp.total_strength );
                        getElem("wiki_row1_col5_text_" + i).innerHTML = enclose( item.temp.total_defense );

                        getElem("wiki_row2_col0_" + i).setAttribute("oncontextmenu", "Mods.Wikimd.nameMenu(\'monster\'," + item.b_i + ")");
                        getElem("wiki_row2_col0_div_" + i).style.height = drops.length < 8 ? "38px" :  drops.length < 15 ? "73px" : ((Math.ceil(drops.length/7) - 2)*36 + 73) + "px";

                        if(img){
                            getElem("wiki_row2_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        } else {
                            getElem("wiki_row2_col0_img_" + i).style.background = 'url("' + getBodyImgNoHalo(item.img.hash).toDataURL("image/png") + '") no-repeat scroll -12px -10px transparent';
                        }
                        getElem("wiki_row2_col0_img_" + i).item_id = false;
                        getElem("wiki_row2_col0_text_" + i).innerHTML = enclose( item.name );
                        getElem("wiki_row2_col0_text_" + i).style.marginTop = item.name.length < 18 ? "-8px" : "-15px";
                        getElem("wiki_row2_col1_div_" + i).style.height = drops.length < 8 ? "36px" : drops.length < 15 ? "71px" : ((Math.ceil(drops.length/7) - 2)*36 + 71) + "px";
                        getElem("wiki_row2_col1_text_" + i).innerHTML = images;

                    } else if (type == "vendor") {

                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "");
                        getElem("wiki_row1_col0_div_" + i).style.height = drops.length < 8 ? "36px" : drops.length < 15 ? "72px" : ((Math.ceil(drops.length/7) - 2)*36 + 72) + "px";
                        if(img){
                            getElem("wiki_row1_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        } else {
                            getElem("wiki_row1_col0_img_" + i).style.background = 'url("' + getBodyImgNoHalo(item.img.hash).toDataURL("image/png") + '") no-repeat scroll -12px -10px transparent';
                        }
                        getElem("wiki_row1_col0_img_" + i).item_id = false;
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( item.name );
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = item.name.length < 18 ? "-8px" : "-15px";
                        getElem("wiki_row1_col1_div_" + i).style.height = drops.length < 8 ? "36px" : drops.length < 15 ? "72px" : ((Math.ceil(drops.length/7) - 2)*36 + 72) + "px";
                        getElem("wiki_row1_col1_text_" + i).innerHTML = images;

                    } else if (type == "craft") {

                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "");
                        getElem("wiki_row1_col0_div_" + i).style.height = "";
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = "";
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( item.object.name );
                        getElem("wiki_row1_col1_div_" + i).style.height = "";
                        getElem("wiki_row1_col1_text_" + i).innerHTML = enclose( item.level );
                        getElem("wiki_row1_col2_text_" + i).innerHTML = enclose( capitaliseFirstLetter(item.skill.slice(0, 7)) );
                        getElem("wiki_row1_col3_text_" + i).innerHTML = enclose( min_chance );
                        getElem("wiki_row1_col4_text_" + i).innerHTML = enclose( max_chance );
                        getElem("wiki_row1_col5_text_" + i).innerHTML = enclose( item.xp || "-" );

                        getElem("wiki_row2_col0_" + i).setAttribute("oncontextmenu", "Mods.Wikimd.nameMenu(\'item\'," + item.id + ")");
                        getElem("wiki_row2_col0_div_" + i).setAttribute("onclick", n_onclick);
                        getElem("wiki_row2_col0_div_" + i).style.height = items.length < 8 ? "36px" : items.length < 15 ? "72px" : ((Math.ceil(items.length/7) - 2)*36 + 72) + "px";
                        getElem("wiki_row2_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        getElem("wiki_row2_col0_img_" + i).item_id = item.id;
                        getElem("wiki_row2_col0_text_" + i).innerHTML = enclose( item.name + (item.object.name == "Anvil" ? "<br/><span class='common_link' style='font-size:.8em;color:#999999;'>(Formula)</span>" + (loadedMods.indexOf("Forgem") > -1 ? "<span class='common_link' style='font-size:.8em;color:#999999;' onclick='Mods.Forgem.newRecipe(" + for_id + ");event.stopPropagation();'>(Learn)</span>" : "") : "") );
                        getElem("wiki_row2_col0_text_" + i).style.marginTop = item.name.length < 18 ? "-8px" : "-15px";
                        getElem("wiki_row2_col1_div_" + i).style.height = items.length < 8 ? "36px" : items.length < 15 ? "72px" : ((Math.ceil(items.length/7) - 2)*36 + 72) + "px";
                        getElem("wiki_row2_col1_text_" + i).innerHTML = images;

                    } else if (type == "pet") {

                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "");
                        getElem("wiki_row1_col0_div_" + i).style.height = "";
                        getElem("wiki_row1_col0_img_" + i).style.background = "";
                        getElem("wiki_row1_col0_img_" + i).item_id = false;
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( pets[item.params.pet].params.stones > 0 ? "Stones of Evolution: " + pets[item.params.pet].params.stones : pets[item.params.pet].params.xp_required > 0 ? "Exp: " + thousandSeperate(parseInt(pets[item.params.pet].params.xp_required)) : "Cannot be leveled" );
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = "";
                        getElem("wiki_row1_col1_div_" + i).style.height = "";
                        getElem("wiki_row1_col1_text_" + i).innerHTML = enclose( item.params.aim ? item.params.aim : "-" );
                        getElem("wiki_row1_col2_text_" + i).innerHTML = enclose( item.params.power ? item.params.power : "-" );
                        getElem("wiki_row1_col3_text_" + i).innerHTML = enclose( item.params.armor ? item.params.armor : "-" );
                        getElem("wiki_row1_col4_text_" + i).innerHTML = enclose( item.params.magic ? item.params.magic : "-" );
                        getElem("wiki_row1_col5_text_" + i).innerHTML = enclose( item.params.speed ? item.params.speed : "-" );

                        getElem("wiki_row2_col0_" + i).setAttribute("oncontextmenu", "Mods.Wikimd.nameMenu(\'" + type + "\'," + item_base[sortlist[a]].b_i + ")");
                        getElem("wiki_row2_col0_div_" + i).style.height = drops.length < 6 ? "38px" : drops.length < 11 ? "73px" : ((Math.ceil(drops.length/5) - 2)*36 + 73) + "px";
                        getElem("wiki_row2_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        getElem("wiki_row2_col0_img_" + i).item_id = item.b_i;
                        var name = pets[item.params.pet].name.replace(/\[(Ancient|Legendary|Rare|Common)\]/gi, function (blank, name) { 
							var color = {Ancient: COLOR.RED, Legendary: COLOR.ORANGE, Rare: COLOR.YELLOW, Common: COLOR.GREEN};
							return "[<span style='color: " + color[name] + ";;'>"+  name.slice(0,1) + "</span>]";
						});
						getElem("wiki_row2_col0_text_" + i).innerHTML = enclose( name + "<span style='color:#999; font-size:.9em'><br>(" + (Mods.Wikimd.family[item.b_i] != undefined ? Mods.Wikimd.family[item.b_i] : "Crafted") + ")</span>" + "<span style='position: absolute; right: 0px; bottom: 0px; font-size: .8em; color: #CCC'>" + pets[item.params.pet].params.inventory_slots + " slots</span>" );
                        getElem("wiki_row2_col0_text_" + i).style.marginTop = pets[item.params.pet].name.length < 20 ? "-12px" : "-17px";
                        getElem("wiki_row2_col1_div_" + i).style.height = drops.length < 6 ? "36px" : drops.length < 11 ? "71px" : ((Math.ceil(drops.length/5) - 2)*36 + 71) + "px";
                        getElem("wiki_row2_col1_text_" + i).innerHTML = images;

                    } else if (type == "spell") {

                        var color = "#DD8";
                        var color2 = "#8CD";
                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "Mods.Wikimd.nameMenu(\'item\'," + item.id + ")");
                        getElem("wiki_row1_col0_div_" + i).style.height = "";
                        getElem("wiki_row1_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        getElem("wiki_row1_col0_img_" + i).item_id = item.id;
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( item.name );
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = item.name.length < 18 ? "-8px" : "-15px";
                        getElem("wiki_row1_col1_div_" + i).style.height = "";
                        getElem("wiki_row1_col1_text_" + i).innerHTML = enclose( magic.min_level );
                        getElem("wiki_row1_col2_text_" + i).innerHTML = enclose( Math.round(magic.cooldown/100)/10 + " <span style='color: " + color2 + "'>secs</span>" );
                        getElem("wiki_row1_col3_text_" + i).innerHTML = enclose( thousandSeperate(item.params.price) );
                        getElem("wiki_row1_col4_text_" + i).innerHTML = enclose( magic.uses );
                        getElem("wiki_row1_col5_text_" + i).innerHTML = enclose( Math.round(item.params.price/(magic.cooldown/1000 * magic.uses) * 10)/10 + " <span style='color: " + color2 + "'>cps</span>" );

                        getElem("wiki_row2_col1_div_" + i).style.height = "";
                        getElem("wiki_row2_col1_text_" + i).innerHTML = enclose( magic.basic_damage + " <span style='color: " + color + "'>dmg</span>" );
                        getElem("wiki_row2_col2_div_" + i).style.height = "";
                        getElem("wiki_row2_col2_text_" + i).innerHTML = enclose( magic.xp + " <span style='color: " + color + "'>exp</span>" );
                        getElem("wiki_row2_col3_text_" + i).innerHTML = enclose( magic.penetration );
                        getElem("wiki_row2_col4_text_" + i).innerHTML = enclose( Math.round(magic.basic_damage/(magic.cooldown/1000) * 10)/10 + " <span style='color: " + color + "'>dps</span>" );
                        getElem("wiki_row2_col5_text_" + i).innerHTML = enclose( Math.round(magic.xp/(magic.cooldown/1000) * 10)/10 + " <span style='color: " + color + "'>eps</span>" );

                    } else if (type == "enchant") {

                        getElem("wiki_row1_col0_" + i).setAttribute("oncontextmenu", "");
                        getElem("wiki_row1_col0_div_" + i).style.height = "";
                        getElem("wiki_row1_col0_text_" + i).style.marginTop = "";
                        getElem("wiki_row1_col0_text_" + i).innerHTML = enclose( item_base[item.enchant.to_enchant].name.replace("Enchanted", "Ench").replace("Necklace", "Neck") );
                        getElem("wiki_row1_col2_text_" + i).innerHTML = enclose( Math.round(item.enchant.low * 100) + "%" );
                        getElem("wiki_row1_col3_text_" + i).innerHTML = enclose( Math.round(item.enchant.med * 100) + "%" );
                        getElem("wiki_row1_col4_text_" + i).innerHTML = enclose( Math.round(item.enchant.high * 100) + "%" );
                        getElem("wiki_row1_col5_text_" + i).innerHTML = enclose( Math.round(item.enchant.sup * 100) + "%" );

                        getElem("wiki_row2_col0_" + i).setAttribute("oncontextmenu", "Mods.Wikimd.nameMenu(\'item\'," + item.id + ")");
                        getElem("wiki_row2_col0_div_" + i).style.height = "38px";
                        getElem("wiki_row2_col0_img_" + i).style.background = 'url("' + img.url + '") no-repeat scroll ' + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent";
                        getElem("wiki_row2_col0_img_" + i).item_id = item.id;
                        getElem("wiki_row2_col0_text_" + i).innerHTML = enclose( item.name );
                        getElem("wiki_row2_col0_text_" + i).style.marginTop = item.name.length < 25 ? "-8px" : "-15px";
                        getElem("wiki_row2_col2_div_" + i).style.height = "36px";
                        getElem("wiki_row2_col2_text_" + i).innerHTML = images;

                    }
                }
                Mods.Wikimd.setSpan(type, i)
            }
        }
        //Mods.Wikimd.newSortValue = Mods.Wikimd.currentSort();
    };

    Mods.Wikimd.tableData = function (a, list, sortlist) {
        var item, items, img, drops, image, images, images1, images2, mins, a, b, c, d,
            min_chance, max_chance, n_click, i_item, i_img, list, sortlist, type, toomany,
            requires, consumes, pet_family, data, bg, magic;
        type = getElem("mods_wiki_type").value;
        item = list[sortlist[a]];
        if (!item) return false;
        img = IMAGE_SHEET[item.img.sheet];
        images = "";
        images1 = "";
        images2 = "";

        drops = {};
        items = 0;
        max_chance = "";
        min_chance = "";
        n_onclick = "";
        mins = 0;
        magic = {};

        if (type == "monster") {
            drops = item.params.drops;
            for (b in drops) {
                i_item = item_base[drops[b].id];
                i_img = IMAGE_SHEET[i_item.img.sheet];
                images = images + "<a title='" + Mods.cleanText(i_item.name) + "(" + drops[b].chance * 100 + "%)" + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
            };
            mins = Math.round((item.params.health + 60) / 60);
        } else if (type == "vendor") {
            drops = item.temp.content;
            for (b in drops) {
                i_item = item_base[drops[b].id];
                i_img = IMAGE_SHEET[i_item.img.sheet];
                if (drops[b].spawn) images1 = images1 + "<a title='" + Mods.cleanText(i_item.name) + " (buys \& sells) value " + thousandSeperate(i_item.params.price) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; background-color: #666666; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                else images2 = images2 + "<a title='" + Mods.cleanText(i_item.name) + " (buys) value " + thousandSeperate(i_item.params.price) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
            }
            images = images1 + images2;
        } else if (type == "craft") {
            drops = item.pattern.requires;
            items = 0;
            tooMany = function (test) {
                return (Math.max(drops[test] || 0, item.pattern.consumes[test] || 0) > 1) && true || false;
            };
            for (b in drops) if (b != "length" && typeof item_base[b] != "undefined") for (c = 0; c < Math.max(drops[b],item.pattern.consumes[b] || 0); c++) {
                i_item = item_base[b];
                i_img = IMAGE_SHEET[i_item.img.sheet];
                if (typeof item.pattern.consumes[b] == "undefined") {
                    images1 = images1 + "<a title='" + Mods.cleanText(i_item.name) + " (not consumed) x" + Math.max(drops[b],item.pattern.consumes[b] || 0) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; background-color: #666666; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                    tooMany(b) && (c = Math.max(drops[b],item.pattern.consumes[b] || 0)) && (images1 += "<div style='width:32px; height:20px; margin:2px; margin-left: 4px; margin-top: 14px; display:inline-block; float:left; text-align: center;'> x" + c + "</div>");
                } else {
                    images2 = images2 + "<a title='" + Mods.cleanText(i_item.name) + " (consumed) x" + Math.max(drops[b],item.pattern.consumes[b] || 0) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                    tooMany(b) && (c = Math.max(drops[b],item.pattern.consumes[b] || 0)) && (images2 += "<div style='width:32px; height:20px; margin:2px; margin-left: 4px; margin-top: 14px; display:inline-block; float:left; text-align: center;'> x" + c + "</div>");
                }
            };
            for (b in item.pattern.consumes) if (b != "length" && typeof item_base[b] != "undefined" && typeof drops[b] == "undefined") for (c = 0; c < item.pattern.consumes[b]; c++) {
                i_item = item_base[b];
                i_img = IMAGE_SHEET[i_item.img.sheet];
                images2 = images2 + "<a title='" + Mods.cleanText(i_item.name) + " (consumed) x" + item.pattern.consumes[b] + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                tooMany(b) && (c = item.pattern.consumes[b]) && (images2 += "<div item_id='" + i_item.b_i + "' style='width:32px; height:20px; margin:2px; margin-left: 4px; margin-top: 14px; display:inline-block; float:left; text-align: center;'> x" + c + "</div>");
            };
            for (b in drops) if (b != "length") { c = Math.max(drops[b], item.pattern.consumes[b] || 0); items += c <= 1 && c || 2 };
            for (b in item.pattern.consumes) if (typeof drops[b] == "undefined" && b != "length") { c = parseInt(item.pattern.consumes[b]); items += c <= 1 && c || 2 };
            images = images1 + images2;
            min_chance = item.pattern.base_chance || item.pattern.chance || "-";
            min_chance = typeof min_chance == "number" ? Math.round(10000 * min_chance) / 100 + "%" : min_chance;
            max_chance = item.pattern.max_chance || item.pattern.chance || 1;
            max_chance = typeof max_chance == "number" ? Math.round(10000 * max_chance) / 100 + "%" : max_chance;
            n_onclick = "";
            if (item.object.name == "Anvil") { n_onclick = "Mods.Wikimd.showFormula(\'" + sortlist[a] + "\');" }
        } else if (type == "pet") {
            drops = Mods.Wikimd.pet_family[item.b_i]
            drops.length = 0;
            for (b in drops) if (typeof parseInt(b) == "number" && typeof item_base[drops[b]] != "undefined" && item_base[drops[b]].params.pet != undefined) {
                if (Mods.Wikimd.pet_family[item.b_i].id == undefined) {
                    i_item = item_base[drops[b]];
                    drops.length += 1;
                    i_img = IMAGE_SHEET[i_item.img.sheet];
					if (images != "") images += "<span style='font-size: .8em; height: 32px; width: 10px; margin: 2px; display:inline-block; float:left; position: relative; color: #999;'><span style='position: absolute; width: 100%; top: 40%; right: 10%;'>&gt;</span></span>";
                    if (i_item.b_i == item.b_i) images += "<a title='" + Mods.cleanText(i_item.name) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px #666; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                    else images += "<a title='" + Mods.cleanText(i_item.name) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                } else {
                    pet_family = Mods.Wikimd.pet_family[item.b_i].pattern;
                    consumes = pet_family.consumes;
                    requires = pet_family.requires;
                    for (c in requires) if (typeof parseInt(c) == "number" && item_base[c] != undefined && item_base[c].img != undefined && consumes[c] == undefined) for (d = 0; d < requires[c]; d++) {
                        i_item = item_base[c];
                        drops.length += 1;
                        i_img = IMAGE_SHEET[i_item.img.sheet];
                        if (images != "") images += "<span style='font-size: .8em; height: 32px; width: 10px; margin: 2px; display:inline-block; float:left; position: relative; color: #999;'><span style='position: absolute; width: 100%; top: 40%; right: 10%;'>+</span></span>";
                        images += "<a title='" + Mods.cleanText(i_item.name) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px #666; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                    };
                    for (c in consumes) if (typeof parseInt(c) == "number" && item_base[c] != undefined && item_base[c].img != undefined) for (d = 0; d < consumes[c]; d++) {
                        i_item = item_base[c];
                        drops.length += 1;
                        i_img = IMAGE_SHEET[i_item.img.sheet];
                        if (images != "") images += "<span style='font-size: .8em; height: 32px; width: 10px; margin: 2px; display:inline-block; float:left; position: relative; color: #999;'><span style='position: absolute; width: 100%; top: 40%; right: 10%;'>+</span></span>";
                        images += "<a title='" + Mods.cleanText(i_item.name) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                    };
                    i_item = item_base[item.b_i];
                    drops.length += 1;
                    i_img = IMAGE_SHEET[i_item.img.sheet];
                    images += "<span style='font-size: .8em; height: 32px; width: 10px; margin: 2px; display:inline-block; float:left; position: relative; color: #999;'><span style='position: absolute; width: 100%; top: 40%; right: 10%;'>=</span></span>";
                    images += "<a title='" + Mods.cleanText(i_item.name) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px #666; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
                }
            }
        } else if (type == "enchant") {
            drops = [item.enchant.from_enchant, item.id, item.enchant.to_enchant];
            for (d = 0; d < 3; d++) if (drops[d]) {
                i_item = item_base[drops[d]];
                i_img = IMAGE_SHEET[i_item.img.sheet];
                bg = drops[d] == item.id ? "#666" : "transparent";
                if (images != "") images += "<span style='font-size: .8em; height: 32px; width: 10px; margin: 2px; display:inline-block; float:left; position: relative; color: #999;'><span style='position: absolute; width: 100%; top: 40%; right: 10%;'>&gt;</span></span>";
                images += "<a title='" + Mods.cleanText(i_item.name) + "'><div item_id='" + i_item.b_i + "' style='background:url(&apos;" + i_img.url + "&apos;) no-repeat scroll " + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px " + bg + "; width:32px; height:32px; margin:2px; margin-left: 4px; display:inline-block; float:left;' oncontextmenu='Mods.Wikimd.nameMenu(&apos;item&apos;," + i_item.b_i + ")'>&nbsp;</div></a>";
            }
        } else if (type == "spell") magic = item.magic;

        data = {
            item: item,
            img: img,
            drops: drops,
            items: items,
            images: images,
            max_chance: max_chance,
            min_chance: min_chance,
            n_onclick: n_onclick,
            mins: mins,
            magic: magic
        };
        return data;
    };

    // This is used to display Forge formulas for items that have forge formulas.
    Mods.Wikimd.showFormula = function (item) {
        if (typeof item != "undefined") {
            var pattern = Mods.Wikimd.formulas[item].pattern.pattern;
            getElem("wiki_recipe_form").style.display = "block";
            for (var a = 0; a < 4; a++) for (var b = 0; b < 4; b++) {
                var slot = getElem("wiki_formula_" + a + "_" + b);
                slot.style.background = "";
                if (typeof pattern[a] != "undefined" && typeof pattern[a][b] != "undefined" && typeof item_base[pattern[a][b]] != "undefined") {
                    var i_item = item_base[pattern[a][b]];
                    var i_img = IMAGE_SHEET[i_item.img.sheet];
                    slot.style.background = 'url("' + i_img.url + '") no-repeat scroll ' + -i_item.img.x * i_img.tile_width + "px " + -i_item.img.y * i_img.tile_height + "px transparent";
                }
            }
        }
    };

    // This sets up the colspan and rowspan for the tables according to how they were designed.
    Mods.Wikimd.setSpan = function (type, id) {
        var s = Mods.Wikimd.span;
        var node, span, colspan, rowspan;
        for (var row = 1; row < 3; row++) for (var col = 0; col < 6; col++) {
            node = getElem("wiki_row" + row + "_col" + col + "_" + id);
            if (s[type] != undefined) {
                span = s[type]["wiki_r" + row + "_c" + col];
                if (typeof span != "undefined") {
                    colspan = span.c || "";
                    rowspan = span.r || "";
                } else {
                    colspan = "";
                    rowspan = "";
                }
                node.setAttribute("colspan", colspan);
                node.setAttribute("rowspan", rowspan);
            }
        }
    };

    // This function sorts the list based on which sort option is clicked.
    Mods.Wikimd.sortWiki = function (list, type, value) {
        if (typeof type != "undefined" && typeof value != "undefined") {
            var populateWiki = false;
            if (typeof list != "object" || list == null) populateWiki = true;
            var sortReverse = false;
            if (Mods.Wikimd.oldSortValue == Mods.Wikimd.newSortValue && !Mods.Wikimd.sortReverse) sortReverse = true;
            else if (Mods.Wikimd.oldSortValue != Mods.Wikimd.newSortValue) Mods.Wikimd.sortReverse = false;
            Mods.Wikimd.oldSortValue = Mods.Wikimd.newSortValue;
            var list = list == null ? Mods.Wikimd.newWikiLoad : list;
            var type = getElem("mods_wiki_type").value;
            var value = value || Mods.Wikimd.oldSort[type];
            if (typeof list == "object") {
                var sortlist = new Array();
                for (var a in list) sortlist.push(a);
                var oldsort = Mods.Wikimd.oldSort || {};
                var reverse = false;

                function sortItems (to_sort) {
                    to_sort.sort(function (a, b) {

                        var sort1_a, sort2_a, sort3_a, sort1_b, sort2_b, sort3_b, c_value, n_value, type;
                        type = getElem("mods_wiki_type").value;
                        n_value = function (n) {var value = n == "damage" ? "basic_damage" : n == "exp" ? "xp" : n == "casts" ? "uses" : n; return value};

                        if (type == "item") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" || c == "level" || c == "skill" || c == "slot" ? list[i][c] : list[i].params[c] || 0; return value};
                        else if (type == "monster") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" ? list[i][c] : c == "level" ? FIGHT.calculate_monster_level(list[i]) : c == "health" || c == "respawn" ? list[i].temp.health : list[i].temp["total_" + c]; return value};
                        else if (type == "craft") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "base_chance" || c == "max_chance" ? list[i].pattern[c] || list[i].pattern.chance || 1 : c == "location" ? list[i].object.name || 0 : c == "xp" ? parseInt(list[i].xp) || 0 : c == "level" ? parseInt(list[i].level) || 0 : list[i][c]; return value};
                        else if (type == "pet") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" ? pets[list[i].params.pet][c] : c == "xp_required" || c == "stones" || c == "inventory_slots" ? pets[list[i].params.pet].params[c] : c == "family" ? Mods.Wikimd.family[i] : list[i].params[c]; return value};
                        else if (type == "spell") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var n = n_value(c); var m = list[i].magic; var value = n == "dmg_s" ? Math.round(m.basic_damage/(m.cooldown/1000) * 100)/100 : n == "cost_s" ? Math.round(list[i].params.price/(m.uses * m.cooldown/1000) * 100)/100 : n == "exp_s" ? Math.round(m.xp/(m.cooldown/1000) * 100)/100 : n == "level" ? m.min_level : n == "name" ? list[i].name : m[n]; return value};
                        else if (type == "enchant") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" ? list[i].name : c == "enchant" ? item_base[list[i].enchant.to_enchant].name : list[i].enchant[c]; return value};

                        if (type == "vendor") {
                            if (list[b][value] > list[a][value] && !reverse || list[b][value] < list[a][value] && reverse) return 1;
                            else return -1;
                        } else {
                            sort1_a = c_value(a, value);
                            sort2_a = c_value(a, vold);
                            sort3_a = c_value(a, last);
                            sort1_b = c_value(b, value);
                            sort2_b = c_value(b, vold);
                            sort3_b = c_value(b, last);

                            if (sort1_a > sort1_b) { return -1 };
                            if (sort1_a < sort1_b) { return 1 };
                            if (sort2_a > sort2_b) { return -1 };
                            if (sort2_a < sort2_b) { return 1 };
                            if (sort3_a > sort3_b) { return -1 };
                            if (sort3_a < sort3_b) { return 1 };
                            return 0
                        }
                    })

                };

                if (type == "item") {
                    vold = typeof oldsort.item != "undefined" ? oldsort.item : value != "name" ? "name" : "level";
                    last = value != "name" && vold != "name" ? "name" : value != "level" && vold != "level" ? "level" : "price";
                } else if (type == "monster") {
                    vold = oldsort.monster ? oldsort.monster : value != "name" ? "name" : "level";
                    last = value != "name" && vold != "name" ? "name" : value != "level" && vold != "level" ? "health" : "health";
                } else if (type == "craft") {
                    vold = value != "xp" ? "xp" : "level";
                    last = value != "xp" && vold != "xp" ? "xp" : value != "level" && vold != "level" ? "level" : "name";
                } else if (type == "pet") {
                    vold = oldsort.pet ? oldsort.pet : value != "family" ? "family" : "inventory_slots";
                    last = value != "family" && vold != "family" ? "family" : value != "inventory_slots" && vold != "inventory_slots" ? "inventory_slots" : "name";
                } else if (type == "enchant") {
                    vold = oldsort.enchant ? oldsort.enchant : value != "low" ? "low" : "name";
                    last = value != "low" && vold != "low" ? "low" : value != "name" && vold != "name" ? "name" : "enchant";
                } else if (type == "spell") {
                    vold = oldsort.spell ? oldsort.spell : value != "level" ? "level" : "name";
                    last = value != "level" && vold != "level" ? "level" : value != "name" && vold != "name" ? "name" : "exp";
                }

                (oldsort[type] == value) && ( (sortReverse) && (reverse = true) && (Mods.Wikimd.sortReverse = true)) || (oldsort[type] = value) && (Mods.Wikimd.sortReverse = false);

                if (type == "vendor") sortItems(sortlist);
                else {
                    var subsort1 = [],
                        subsort2 = [],
                        sort_a = "";
                    for (var a in sortlist) {
                        var n_value, c_value;
                        n_value = function (n) {var value = n == "damage" ? "basic_damage" : n == "exp" ? "xp" : n == "casts" ? "uses" : n; return value};

                        if (type == "item") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" || c == "level" || c == "skill" || c == "slot" ? list[i][c] : list[i].params[c] || 0; return value};
                        else if (type == "monster") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" ? list[i][c] : c == "level" ? FIGHT.calculate_monster_level(list[i]) : c == "health" || c == "respawn" ? list[i].temp.health : list[i].temp["total_" + c]; return value};
                        else if (type == "craft") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "base_chance" || c == "max_chance" ? list[i].pattern[c] || list[i].pattern.chance || 1 : c == "location" ? list[i].object.name || 0 : c == "xp" ? parseInt(list[i].xp) || 0 : c == "level" ? parseInt(list[i].level) || 0 : list[i][c]; return value};
                        else if (type == "pet") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" ? pets[list[i].params.pet][c] : c == "xp_required" || c == "stones" || c == "inventory_slots" ? pets[list[i].params.pet].params[c] : c == "family" ? Mods.Wikimd.family[i] : list[i].params[c]; return value};
                        else if (type == "spell") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var n = n_value(c); var m = list[i].magic; var value = n == "dmg_s" ? Math.round(m.basic_damage/(m.cooldown/1000) * 100)/100 : n == "cost_s" ? Math.round(list[i].params.price/(m.uses * m.cooldown/1000) * 100)/100 : n == "exp_s" ? Math.round(m.xp/(m.cooldown/1000) * 100)/100 : n == "level" ? m.min_level : n == "name" ? list[i].name : m[n]; return value};
                        else if (type == "enchant") c_value = function (i, c) {if (!(i > -1) || !list[i]) return 0; var value = c == "name" ? list[i].name : c == "enchant" ? item_base[list[i].enchant.to_enchant].name : list[i].enchant[c]; return value};

                        if (c_value(sortlist[a], value) > 0 || value == "family" && typeof c_value(sortlist[a], value) == "string") subsort1.push(sortlist[a]);
                        else subsort2.push(sortlist[a]);
                    };
                    if (subsort1.length > 0) sortItems(subsort1);
                    if (subsort2.length > 0) sortItems(subsort2);
                    if (reverse) {
                        subsort1.reverse();
                        if (subsort1.length == 0) subsort2.reverse();
                    }
                    for (var b in subsort2) subsort1.push(subsort2[b]);
                    sortlist = subsort1;
                }
            };

            Mods.Wikimd.oldSortList = sortlist;
            if (!populateWiki) return sortlist;
            else Mods.Wikimd.populateWiki(list, sortlist);
        }
    };

    // This function filters the list based on the filter options.
    Mods.Wikimd.populateWikiList = function () {
        var t = getElem("mods_wiki_type");
        var i = getElem("mods_wiki_type_item");
        var i1 = getElem("mods_wiki_type_item_type");
        var i2 = getElem("mods_wiki_type_item_skill");
        var m = getElem("mods_wiki_type_monster");
        var v = getElem("mods_wiki_type_vendor");
        var c = getElem("mods_wiki_type_craft");
        var p = getElem("mods_wiki_type_pet");
        var s = getElem("mods_wiki_type_spell");
        var e = getElem("mods_wiki_type_enchant");
        var c1 = getElem("mods_wiki_type_craft_skill");
        var c2 = getElem("mods_wiki_type_craft_source");
        var n = getElem("mods_wiki_name");
        var r = getElem("mods_wiki_range");
        var l1 = parseInt(getElem("mods_wiki_level_low").value);
        var l2 = parseInt(getElem("mods_wiki_level_high").value);
        var tempList = {};
        var iFormulas = Mods.Wikimd.item_formulas;
        var formulas = Mods.Wikimd.formulas;
        var item;
        if (typeof t.value != "undefined") {
            if (t.value == "item") {
                for (item in iFormulas) { tempList[item] = iFormulas[item] }
                if (i.value == "name") {
                    if (typeof n.value != "undefined") for (item in tempList) {
                        var name = tempList[item].name.toLowerCase();
                        var value = n.value.toLowerCase();
                        if (name.indexOf(value) < 0) { delete tempList[item] }
                    }
                } else {
                    if (i.value == "skill") {
                        if (i2.value != "-1") {
                            for (item in tempList) if (tempList[item].skill.toLowerCase() != i2.value) { delete tempList[item] }
                        } else return false;
                    } else if (i.value == "type") {
                        if (i1.value != "-1") {
                            var value = i1.value;
                            for (item in tempList) {
                                var slot = tempList[item].slot || "none";
                                    slot = slot.toLowerCase();
                                var type = tempList[item].type || "none";
                                    type = type.toLowerCase();
                                var skill = tempList[item].skill || "none";
                                    skill = skill.toLowerCase();
                                    if (!(value.indexOf(slot) > -1 && value.search(type) > -1) && (slot != value && skill != value && type != value)) { delete tempList[item] }
                            }
                        } else return false;
                    };
                    if (r.value != -1) for (item in tempList) {
                        var r_type = "";
                        if (r.value == "level") r_type = tempList[item].level;
                        else r_type = tempList[item].params[r.value] || null;
                        if (r_type == null) { delete tempList[item] }
                        if (typeof l1 == "number" && typeof tempList[item] != "undefined") if (r_type < l1 || !(r_type > -1)) { delete tempList[item] }
                        if (typeof l2 == "number" && typeof tempList[item] != "undefined") if (r_type > l2 || !(r_type > -1)) { delete tempList[item] }
                    }
                }
            } else if (t.value == "monster" || t.value == "vendor") {
                for (item in npc_base) { tempList[item] = npc_base[item] }
                if (t.value == "monster") {
                    for (item in tempList) {
                        if (tempList[item].type != 3) delete tempList[item]
                        else if (m.value == "name") {
                            if (typeof n.value != "undefined") {
                                var s_name = n.value.toLowerCase();
                                var n_name = tempList[item].name.toLowerCase();
                                if (n_name.indexOf(s_name) == -1) { delete tempList[item] }
                            }
                        } else if (m.value == "item") {
                            var hasItem = false;
                            for (var mat in tempList[item].params.drops) {
                                if (typeof parseInt(mat) == "number") {
                                    var drops = tempList[item].params.drops;
                                    var s_name = n.value.toLowerCase();
                                    var m_name = item_base[drops[mat].id].name;
                                        m_name = m_name.toLowerCase();
                                        if (m_name.indexOf(s_name) > -1) { hasItem = true }
                                }
                            }
                            if (!hasItem) { delete tempList[item] }
                        } else {
                            if (r.value != -1 && typeof tempList[item] != "undefined") {
                                var r_type = "";
                                if (r.value == "level") r_type = FIGHT.calculate_monster_level(tempList[item]);
                                else if (r.value == "health") r_type = tempList[item].temp.health;
                                else r_type = tempList[item].temp["total_" + r.value] || null;
                                if (r_type == null) { delete tempList[item] }
                                if (typeof l1 == "number" && typeof tempList[item] != "undefined") if (r_type < l1 || !(r_type > -1)) { delete tempList[item] }
                                if (typeof l2 == "number" && typeof tempList[item] != "undefined") if (r_type > l2 || !(r_type > -1)) { delete tempList[item] }
                            }
                        }
                    }
                } else if (t.value == "vendor") {
                    for (item in tempList) {
                        if (tempList[item].type != 4) delete tempList[item]
                        else if (v.value == "name") {
                            if (typeof n.value != "undefined") for (item in tempList) {
                                var s_name = n.value.toLowerCase();
                                var n_name = tempList[item].name.toLowerCase();
                                if (n_name.indexOf(s_name) == -1) { delete tempList[item] }
                            }
                        } else if (v.value == "item") {
                            var hasItem = false;
                            for (var mat in tempList[item].temp.content) if (typeof parseInt(mat) == "number") {
                                var drops = tempList[item].temp.content;
                                var s_name = n.value.toLowerCase();
                                var m_name = item_base[drops[mat].id].name;
                                    m_name = m_name.toLowerCase();
                                    if (m_name.indexOf(s_name) != -1) { hasItem = true }
                            }
                            if (!hasItem) { delete tempList[item] }
                        }
                    }
                } else { return false }
            } else if (t.value == "craft") {
                for (item in formulas) { tempList[item] = formulas[item] }
                if (c.value == "item") {
                    if (typeof n.value != "undefined") for (item in tempList) {
                        var hasItem = false;
                        var s_name = n.value.toLowerCase();
                        var n_name = tempList[item].name;
                            n_name = n_name.toLowerCase();
                            if (n_name.indexOf(s_name) != -1) {
                            hasItem = true
                        }
                        if (!hasItem) {
                            for (var mat in tempList[item].pattern.requires) if (typeof item_base[mat].name != "undefined") {
                                var m_name = item_base[mat].name;
                                    m_name = m_name.toLowerCase();
                                if (m_name.indexOf(s_name) != -1) { hasItem = true; break }
                            }
                        }
                        if (!hasItem) { delete tempList[item] }
                    }
                } else {
                    if (c.value == "skill") {
                        if (c1.value != -1) {
                            for (item in tempList) if (tempList[item].skill.toLowerCase() != c1.value) { delete tempList[item] }
                        } else { return false }
                    }
                    if (c.value == "source") {
                        if (c2.value != -1) {
                            for (item in tempList) if (tempList[item].object.name.toLowerCase() != c2.value) { delete tempList[item] }
                        } else { return false }
                    }
                    if (r.value != -1) for (item in tempList) {
                        var r_type = "";
                        if (r.value == "base_chance" || r.value == "max_chance") r_type = 100 * tempList[item].pattern[r.value] || 100 * tempList[item].pattern.chance || null;
                        else { r_type = parseInt(tempList[item][r.value]) || null }
                        if (r_type == null) { delete tempList[item] }
                        if (typeof l1 == "number" && typeof tempList[item] != "undefined") if (r_type < l1 || !(r_type > -1)) { delete tempList[item] }
                        if (typeof l2 == "number" && typeof tempList[item] != "undefined") if (r_type > l2 || !(r_type > -1)) { delete tempList[item] }
                    }
                }
            } else if (t.value == "pet") {
                for (item in Mods.Wikimd.pet_family) { tempList[item] = item_base[item] };
                for (item in tempList) {
                    if (p.value == "name") if (typeof n.value != "undefined") {
                        var s_name = n.value.toLowerCase();
                        var n_name = tempList[item].name.toLowerCase();
                        if (n_name.indexOf(s_name) == -1) { delete tempList[item] }
                    } else if (p.value == "family") {
                        if (typeof n.value != "undefined") {
                            var s_name = n.value.toLowerCase();
                            var n_name = Mods.Wikimd.family[item];
                            if (n_name != undefined) {
                                n_name = n_name.toLowerCase();
                                if (n_name.indexOf(s_name) == -1) { delete tempList[item] }
                            } else { delete tempList[item] }
                        }
                    } else {
                        if (r.value != -1 && typeof tempList[item] != "undefined") {
                            var r_type = r.value == -1 ? null : (r.value == "aim" || r.value == "armor" || r.value == "power" || r.value == "magic" || r.value == "speed") ? item_base[tempList[item].b_i].params[r.value] : pets[tempList[item].params.pet].params[r.value];
                            if (r_type == null) { delete tempList[item] }
                            if (typeof l1 == "number" && typeof tempList[item] != "undefined") if (r_type < l1 || !(r_type > -1)) { delete tempList[item] }
                            if (typeof l2 == "number" && typeof tempList[item] != "undefined") if (r_type > l2 || !(r_type > -1)) { delete tempList[item] }
                        }
                    }
                }
            } else if (t.value == "spell") {
                for (item in iFormulas) if (iFormulas[item].magic) { tempList[item] = iFormulas[item] };
                for (item in tempList) {
                    if (s.value == "name") {
                        if (typeof n.value != "undefined") {
                            var s_name = n.value.toLowerCase();
                            var n_name = tempList[item].name.toLowerCase();
                            if (n_name.indexOf(s_name) == -1) { delete tempList[item] }
                        }
                    } else {
                        if (r.value != -1 && typeof tempList[item] != "undefined") {
                            var m_value = tempList[item].magic;
                            var m = r.value == "damage" ? "basic_damage" : r.value == "exp" ? "xp" : r.value == "casts" ? "uses" : r.value;
                            var d_value = r.value == "dmg_s" ? Math.round(m_value.basic_damage/(m_value.cooldown/1000) * 10)/10 : r.value == "cost_s" ? Math.round(tempList[item].params.price/(m_value.uses * m_value.cooldown)) : r.value == "exp_s" ? Math.round(m_value.xp/m_value.uses) : null;
                            var r_type = r.value == -1 ? null : d_value != null ? d_value : r.value == "price" ? tempList[item].params.price : m_value[m];
                            if (r_type == null) { delete tempList[item] }
                            if (typeof l1 == "number" && typeof tempList[item] != "undefined") if (r_type < l1 || !(r_type > -1)) { delete tempList[item] }
                            if (typeof l2 == "number" && typeof tempList[item] != "undefined") if (r_type > l2 || !(r_type > -1)) { delete tempList[item] }
                        }
                    }
                }
            } else if (t.value == "enchant") {
                for (item in iFormulas) if (iFormulas[item].enchant && iFormulas[item].enchant.to_enchant) { tempList[item] = iFormulas[item] };
                for (item in tempList) {
                    if (e.value == "item") {
                        if (typeof n.value != "undefined") {
                            var s_name = n.value.toLowerCase();
                            var n_name = tempList[item].name.toLowerCase();
                            var f_name = tempList[item].enchant.from_enchant;
                                f_name = f_name ? item_base[f_name].name.toLowerCase() : null;
                            var t_name = tempList[item].enchant.to_enchant;
                                t_name = t_name ? item_base[t_name].name.toLowerCase() : null;
                            if (n_name.indexOf(s_name) == -1 && (f_name && f_name.indexOf(s_name) == -1 || !f_name && true) && (t_name && t_name.indexOf(s_name) == -1 || !f_name && true)) { delete tempList[item] }
                        }
                    } else {
                        if (r.value != -1 && typeof tempList[item] != "undefined") {
                            var r_type = r.value == -1 ? null : tempList[item].enchant[r.value] || null;
                            if (r_type == null) { delete tempList[item] }
                            if (typeof l1 == "number" && typeof tempList[item] != "undefined") if (r_type < l1 || !(r_type > -1)) { delete tempList[item] }
                            if (typeof l2 == "number" && typeof tempList[item] != "undefined") if (r_type > l2 || !(r_type > -1)) { delete tempList[item] }
                        }
                    }
                }
            } else { return false }
        } else { return false }
        Mods.Wikimd.newWikiLoad = tempList;
        return tempList;
    };

    // This function creates a value the represents how the current list is sorted. It's used for comparison with the last or next sort/filter that's done to see if the sorting/filtering is the same as it previously was. When it is the same, the wiki inverse-sorts the list.
    Mods.Wikimd.currentSort = function () {
        var t = getElem("mods_wiki_type");
        var i = getElem("mods_wiki_type_item");
        var i1 = getElem("mods_wiki_type_item_type");
        var i2 = getElem("mods_wiki_type_item_skill");
        var m = getElem("mods_wiki_type_monster");
        var v = getElem("mods_wiki_type_vendor");
        var c = getElem("mods_wiki_type_craft");
        var s = getElem("mods_wiki_type_spell");
        var e = getElem("mods_wiki_type_enchant");
        var c1 = getElem("mods_wiki_type_craft_skill");
        var c2 = getElem("mods_wiki_type_craft_source");
        var p = getElem("mods_wiki_type_pet");
        var n = getElem("mods_wiki_name");
        var r = getElem("mods_wiki_range");
        var l1 = parseInt(getElem("mods_wiki_level_low").value);
        var l2 = parseInt(getElem("mods_wiki_level_high").value);
        var sort = "";
            sort = t.value == "monster" ? "mob " : t.value == "vendor" ? "npc " : t.value + " ";
            sort += (t.value == "item" && i.value || t.value == "monster" && m.value || t.value == "vendor" && v.value || t.value == "craft" && c.value || t.value == "pet" && p.value || t.value == "spell" && s.value || t.value == "enchant" && e.value) + " " || "";
            sort += (t.value == "item" && i.value == "name" || t.value == "monster" && m.value != "all" || t.value == "vendor" && v.value != "all" || t.value == "craft" && c.value == "item" || t.value == "pet" && ( p.value == "name" || p.value == "family" ) || t.value == "spell" && s.value == "name" || t.value == "enchant" && e.value == "item" ) && (n.value || "") || "";
            sort += (t.value == "item" && (i.value == "type" && (i1.value + " ") || i.value == "skill" && (i2.value + " "))) || "";
            sort += (t.value == "craft" && (c.value == "skill" && (c1.value + " ") || c.value == "source" && (c2.value + " "))) || "";
            sort += (t.value == "item" && i.value != "name" || t.value == "monster" && m.value == "all" || t.value == "craft" && c.value != "item" || t.value == "pet" && p.value == "all" || t.value == "spell" && s.value == "all" || t.value == "enchant" && e.value == "all" ) && (r.value + " (" + (l1 || "") + "," + (l2 || "") + ")") || "";
        return sort;
    };

    // This function determins which filters should appear based on when a filter is changed. For example, if the first filter is "Item" then the next will be "Item type", whereas if you change the first filter to "Craft", the next filter that appears will be "Craft type". This insures that only relevant filter options appear based on the previous ones that are chosen.
    Mods.Wikimd.loadWikiType = function (lvl, id) {
        if (loadedMods.indexOf("Chatmd") != -1) {
            var go = getElem("mods_wiki_load");
            go.innerHTML = "Go!"
            go.setAttribute("onclick", "javascript:Mods.Wikimd.populateWiki(true);");
        }
        if (lvl === false) return;

        var type = getElem("mods_wiki_type");
        var item = getElem("mods_wiki_type_item");
        var itemType = getElem("mods_wiki_type_item_type");
        var itemSkll = getElem("mods_wiki_type_item_skill");
        var mstr = getElem("mods_wiki_type_monster");
        var vndr = getElem("mods_wiki_type_vendor");
        var crft = getElem("mods_wiki_type_craft");
        var crftSkll = getElem("mods_wiki_type_craft_skill");
        var crftSrce = getElem("mods_wiki_type_craft_source");
        var pets = getElem("mods_wiki_type_pet");
        var spel = getElem("mods_wiki_type_spell");
        var ench = getElem("mods_wiki_type_enchant");
        var name = getElem("mods_wiki_name");
        var rnge = getElem("mods_wiki_range");
        var lvel = getElem("mods_wiki_level");
        var lvl_L = getElem("mods_wiki_level_low");
        var lvl_H = getElem("mods_wiki_level_high");

        var Vtype = type.value;
        var Vitem = item.value;
        var VitemType = itemType.value;
        var VitemSkll = itemSkll.value;
        var Vmstr = mstr.value;
        var Vvndr = vndr.value;
        var Vcrft = crft.value;
        var VcrftSkll = crftSkll.value;
        var VcrftSrce = crftSrce.value;
        var Vpets = pets.value;
        var Vspel = spel.value;
        var Vench = ench.value;
        var Vname = name.value = null;
        var Vrnge = rnge.value;
        var Vlvl_L = lvl_L.value = null;
        var Vlvl_H = lvl_H.value = null;

        var Ditem, DitemType, DitemSkll, Dmstr, Dvndr, Dcrft, DcrftSkll, DcrftSrce, Dpets, Dspel, Dench, Dname, Drnge;

        if (lvl == 0) {
            DitemType = DitemSkll = DcrftSkll = DcrftSrce = Dname = Drnge = "none";

            Ditem = Vtype == "item" && "block" || "none";
            Dmstr = Vtype == "monster" && "block" || "none";
            Dvndr = Vtype == "vendor" && "block" || "none";
            Dcrft = Vtype == "craft" && "block" || "none";
            Dpets = Vtype == "pet" && "block" || "none";
            Dspel = Vtype == "spell" && "block" || "none";
            Dench = Vtype == "enchant" && "block" || "none";
            Drnge = (Vtype == "item" && Vitem != "name" || Vtype == "monster" && Vmstr == "all" || Vtype == "craft" && Vcrft != "item" || Vtype == "pet" && Vpets == "all" || Vtype == "spell" && Vspel == "all" || Vtype == "enchant" && Vench == "all" ) && "block" || "none";
            Dname = (Vtype == "item" && Vitem == "name" || Vtype == "monster" && Vmstr != "all" || Vtype == "vendor" && Vvndr != "all" || Vtype == "craft" && Vcrft == "item" || Vtype == "pet" && Vpets != "all" || Vtype == "spell" && Vspel == "name" || Vtype == "enchant" && Vench == "item" ) && "block" || "none";

        } else if (lvl == 1) {

            DitemType = Vtype == "item" && Vitem == "type" && "block" || "none";
            DitemSkll = Vtype == "item" && Vitem == "skill" && "block" || "none";
            DcrftSkll = Vtype == "craft" && Vcrft == "skill" && "block" || "none";
            DcrftSrce = Vtype == "craft" && Vcrft == "source" && "block" || "none";

            Drnge = (Vtype == "item" && Vitem != "name" || Vtype == "monster" && Vmstr == "all" || Vtype == "craft" && Vcrft != "item" || Vtype == "pet" && Vpets == "all" || Vtype == "spell" && Vspel == "all" || Vtype == "enchant" && Vench == "all" ) && "block" || "none";
            Dname = (Vtype == "item" && Vitem == "name" || Vtype == "monster" && Vmstr != "all" || Vtype == "vendor" && Vvndr != "all" || Vtype == "craft" && Vcrft == "item" || Vtype == "pet" && Vpets != "all" || Vtype == "spell" && Vspel == "name" || Vtype == "enchant" && Vench == "item" ) && "block" || "none";

        } else if (lvl == 2) {

            Drnge = "block";

        }

        var Hrnge = "";
            Hrnge = Vtype != "item" ?        Hrnge : "<option value='-1'>Range</option>            <option value='level'>Level</option>        <option value='price'>Price</option>             <option value='power'>Power</option>              <option value='aim'>Aim</option>             <option value='armor'>Armor</option>        <option value='magic'>Magic</option>        <option value='speed'>Speed</option>";
            Hrnge = Vtype != "monster" ?     Hrnge : "<option value='-1'>Range</option>            <option value='level'>Level</option>        <option value='health'>Health</option>           <option value='accuracy'>ACC</option>             <option value='strength'>STR</option>        <option value='defense'>DEF</option>";
            Hrnge = Vtype != "craft" ?       Hrnge : "<option value='-1'>Range</option>            <option value='level'>Level</option>        <option value='base_chance'>Min%</option>        <option value='max_chance'>Max%</option>          <option value='xp'>Exp</option>";
            Hrnge = Vtype != "pet" ?         Hrnge : "<option value='-1'>Range</option>            <option value='stones'>Req SoE</option>     <option value='xp_required'>Req Exp</option>     <option value='inventory_slots'>Slots</option>    <option value='aim'>Aim</option>             <option value='power'>Power</option>        <option value='armor'>Armor</option>        <option value='magic'>Magic</option>                  <option value='speed'>Speed</option>";
            Hrnge = Vtype != "spell" ?       Hrnge : "<option value='-1'>Range</option>            <option value='level'>Level</option>        <option value='damage'>Damage</option>           <option value='exp'>Exp</option>                  <option value='cooldown'>Cooldown</option>   <option value='price'>Price</option>        <option value='casts'>Casts</option>        <option value='penetration'>Spell Pen</option>        <option value='dmg_s'>Dmg/S</option>        <option value='cost_s'>Cost/S</option>       <option value='exp_s'>Exp/S</option>";
            Hrnge = Vtype != "enchant" ?     Hrnge : "<option value='-1'>Range</option>            <option value='low'>Low %</option>          <option value='med'>Med %</option>               <option value='high'>High %</option>              <option value='sup'>Sup %</option>";

        if (Drnge == "block") { rnge.innerHTML = Hrnge }

        getElem("mods_wiki_type_item").style.display = Ditem;
        getElem("mods_wiki_type_item_type").style.display = DitemType;
        getElem("mods_wiki_type_item_skill").style.display = DitemSkll;
        getElem("mods_wiki_type_monster").style.display = Dmstr;
        getElem("mods_wiki_type_vendor").style.display = Dvndr;
        getElem("mods_wiki_type_craft").style.display = Dcrft;
        getElem("mods_wiki_type_craft_skill").style.display = DcrftSkll;
        getElem("mods_wiki_type_craft_source").style.display = DcrftSrce;
        getElem("mods_wiki_type_pet").style.display = Dpets;
        getElem("mods_wiki_type_spell").style.display = Dspel;
        getElem("mods_wiki_type_enchant").style.display = Dench;
        getElem("mods_wiki_name").style.display = Dname;
        getElem("mods_wiki_range").style.display = Drnge;
        getElem("mods_wiki_level").style.display = Drnge;

        getElem("mods_wiki_range_separate").style.display = Drnge;

    };

    // These functions will load the menus for "Load" and "Options" when those buttons are clicked;
    Mods.loadModMenu_options = function () {
        getElem("mod_load_options").style.display = "none";
        getElem("mod_load_mods_options").style.display = "none";
        getElem("mod_options_options").style.display = "block";
        getElem("mod_options_mods_options").style.display = "block";
        getElem("mod_wiki_options").style.display = "none";
        getElem("mod_wiki_mods_options").style.display = "none";
        Mods.modOptionsOptionsDisplay("expbar");
    };
    Mods.loadModMenu_load = function () {
        getElem("mod_load_options").style.display = "block";
        getElem("mod_load_mods_options").style.display = "block";
        getElem("mod_options_options").style.display = "none";
        getElem("mod_options_mods_options").style.display = "none";
        getElem("mod_wiki_options").style.display = "none";
        getElem("mod_wiki_mods_options").style.display = "none";
    };
    Mods.loadModMenu_wiki = function () {
        getElem("mod_load_options").style.display = "none";
        getElem("mod_load_mods_options").style.display = "none";
        getElem("mod_options_options").style.display = "none";
        getElem("mod_options_mods_options").style.display = "none";
        getElem("mod_wiki_options").style.display = "block";
        getElem("mod_wiki_mods_options").style.display = "block";
    };

    temp();
    Mods.timestamp("wikimd");
};

Load.miscmd = function () {

    modOptions.miscmd.time = timestamp();

    function temp () {
        getElem("penalty_bonus_skill").value = Mods.Miscmd.penalty;
        for (var a = 0; a < 40; a++) getElem("inv_" + a).style.position = "relative";

        //fix style overflow for chest and inventory
        getElem("chest_item", {
            style: {
                overflow: "hidden",
                maxWidth: "185px",
                whiteSpace: "nowrap"
            }
        });

        getElem("inv_name", {
            style: {
                overflow: "hidden",
                maxWidth: "185px",
                whiteSpace: "nowrap",
                cssFloat: "left"
            }
        });

        Mods.Miscmd.toolbar.loadDivs();
        Mods.Miscmd.toolbar.updateToolbar();
        Mods.Miscmd.toolbar.checkTime();
        Mods.Miscmd.ideath.safe_items();
        setCanvasSize();
    }

    penalty_bonus = function () {
        Mods.Miscmd.penalty = getElem("penalty_bonus_skill").value;
        localStorage.penalty_bonus = JSON.stringify(Mods.Miscmd.penalty);
        Mods.Miscmd.oldPenaltyBonus();
    };

    Mods.Miscmd.ideath.sort_values = function () {
        var values = {};
        var list = [];
        var saved = 2;
        var pet;
        for (var a = 0; a < players[0].temp.inventory.length; a++) {
            var item = players[0].temp.inventory[a].id;
            var price = item_base[item].params.price;
            if (item != 855) {
                values[item] = price;
                list.push(parseInt(item));
            } else if (players[0].temp.inventory[a].selected) saved = 7;
        };
        if (!(players[0].temp.created_at < timestamp() - 72E5)) saved = 40;
        if (players[0].pet.enabled) {
            pet = players[0].pet.id;
            pet = pets[pet].params.item_id;
            for (var i = 0; i < list.length; i++) if (list[i] == pet) list.splice(i, 1);
            delete values[pet]
        };
        if (list.length > 0) {
            list.sort(function (a, b) {
                if (values[a] > values[b]) return -1;
                if (values[a] < values[b]) return 1;
                if (item_base[a].name > item_base[b].name) return -1;
                return 1;
            })
        };
        list.splice(saved, 40);
        list.push(pet);
        return list;
    };

    Mods.Miscmd.ideath.safe_items = function () {
        var bgColor = Mods.Miscmd.ideath.bgColor;
        var brColor = Mods.Miscmd.ideath.brColor;
        var list = Mods.Miscmd.ideath.sort_values();
        for (a = 0; a < 40; a++) {
            var b = getElem("inv_" + a);
            b.innerHTML = "&nbsp;";
            if (typeof players[0].temp.inventory[a] != "undefined") {
                var c = players[0].temp.inventory[a];
                for (var i in list) if (c.id == list[i]) {
                    b.innerHTML = "<div class='pointer' title='You keep this item if you die.' style='position: absolute; top: 0%; left: 0%; margin-left: -1px; margin-top: -1px; width: 13%; height: 13%; background-color: " + bgColor + "; border: 1px solid; border-color: " + brColor + "; opacity: .8;'>&nbsp;</div>";
                    list.splice(i, 1);
                }
            }
        }
    };
	
	// Allow clicking bag to keep it open
	oldBigMenuShow = BigMenu.show;
	BigMenu.show = function (a) {
		if (a === 2) Mods.showInv = true;
		else Mods.showInv = false;
		oldBigMenuShow(a)
	}
	
	document.addEventListener('mousedown', function (e) {
		if (Mods.showInv && e.toElement.id === "hud") {
			getElem("inventory").style.zIndex = "199";
			Mods.showBag = !Mods.showBag.valueOf();
			if (Mods.showBag)  getElem("inventory").style.display = "block";
			else getElem("inventory").style.display = "";
		}
	})

    Mods.Miscmd.toolbar.loadDivs = function () {
        createElem("div", wrapper, {
            id: "toolbar_main_holder",
            style: "position: absolute; display: block; height: 2.8%; right: 0%; top: 0%; width: 70.5%; font-size: 0.7em; color: #FFF; z-index: 999; background-color: rgba(0, 0, 0, 0.4); overflow: hidden;",
            innerHTML: "<span id='toolbar_holder' style='position: relative; display: inline-block; float: right; padding-top: 1px; height: 100%; width: 99%;'></span>"
        });

        var ids = ["td_time", "td_stats", "td_quests", "td_inventory_old", "td_location", "td_dpsinfo"];
        for (var a in ids) createElem("span", "toolbar_holder", {
            id: ids[a],
            className: "toolbar_item"
        });

        //add new inventory div
        createElem("div", wrapper, {
            id: "td_inventory",
            style: "position: absolute; text-shadow: -1px -1px #333; border: 1px solid #000; border-radius: 4px; font-weight: normal; z-index: 999999; opacity: .8; background-color: rgba(20, 20, 20, 0.7); pointer-events: none; text-align: center; color: white; font-family: ariel;"
		});
    };

    Mods.Miscmd.toolbar.playerLocation = function () {
        var map = "<span style='color: #BBB;'>" + map_names[players[0].map] + "</span> (" + players[0].i + ", " + players[0].j + ")";
        return map;
    };

    //dps bar
    Mods.Miscmd.toolbar.dpsinfo = function () {
        var a, op;
        if (Mods.Miscmd.dpsmode) a = "<span onclick='javascript:Mods.Miscmd.switchdpsmode();' class='pointer'><span style='color: #BBB;'>DPS:</span> " + Math.round(Mods.Miscmd.avgdps * 100) / 100 + " (" + Math.round(Mods.Miscmd.maxdps * 100) / 100 + ")</span>";
        else {
            if (Mods.Miscmd.avgexp > 10000) op = Math.round(Mods.Miscmd.avgexp / 10) / 100 + "k";
            else op = Math.round(Mods.Miscmd.avgexp);
            a = "<span onclick='javascript:Mods.Miscmd.switchdpsmode();' onMouseOver='Mods.Miscmd.ShowExpPopup(this);' onMouseOut='Mods.Miscmd.HideExpPopup();' class='pointer'><span style='color: #BBB;'>Exp/h:</span> " + op + "</span>";
        }
        return a
    };

    Mods.Miscmd.ShowExpPopup = function (a) {
        if (Mods.Miscmd.dpsmode == false) {
            var xpp = getElem("ww_xp_popup");
            if (!xpp) createElem("div", wrapper, {
                id: "ww_xp_popup",
                className: "xptable menu",
                style: "z-index: 101; visibility: hidden; position: absolute; opacity: 1; padding: 0px;"
            });
            var dpi = getElem("td_dpsinfo");
            xpp.style.left = getElem("td_dpsinfo").getBoundingClientRect().left + "px";
            xpp.style.top = getElem("td_dpsinfo").getBoundingClientRect().bottom + "px";
            var cont = "<table><thead><tr><th>Skill</th><th>Exp/h</th><th>Level in</th></tr></thead><tbody>";

            var resskill = {};
            for (var cn in Mods.Miscmd.adps) {
                if (resskill[Mods.Miscmd.adps[cn].skill]) {
                    resskill[Mods.Miscmd.adps[cn].skill].xp += Mods.Miscmd.adps[cn].xpdelta;
                    if (resskill[Mods.Miscmd.adps[cn].skill].mintime > Mods.Miscmd.adps[cn].time) resskill[Mods.Miscmd.adps[cn].skill].mintime = Mods.Miscmd.adps[cn].time;
                    if (resskill[Mods.Miscmd.adps[cn].skill].maxtime < Mods.Miscmd.adps[cn].time) resskill[Mods.Miscmd.adps[cn].skill].maxtime = Mods.Miscmd.adps[cn].time;
                } else {
                    resskill[Mods.Miscmd.adps[cn].skill] = {};
                    resskill[Mods.Miscmd.adps[cn].skill].mintime = resskill[Mods.Miscmd.adps[cn].skill].maxtime = Mods.Miscmd.adps[cn].time;
                    resskill[Mods.Miscmd.adps[cn].skill].xp = Mods.Miscmd.adps[cn].xpdelta;
                }
            }

            var rcol = true;
            for (var sk in resskill) {
                var exphour = 3600 * resskill[sk].xp / ((resskill[sk].maxtime - resskill[sk].mintime) / 1000);
                if (isFinite(exphour) && exphour != NaN) {
                    var needsxp = Level.xp_for_level(skills[0][sk].level + 1) - skills[0][sk].xp;
                    var percxp = Math.round((skills[0][sk].xp - Level.xp_for_level(skills[0][sk].level)) / (Level.xp_for_level(skills[0][sk].level + 1) - Level.xp_for_level(skills[0][sk].level)) * 100);

                    var secsneeded = Math.floor(needsxp / (exphour / 3600));
                    var hours = parseInt(secsneeded / 3600);
                    var minutes = parseInt(secsneeded / 60) % 60;
                    var seconds = secsneeded % 60;

                    var tneed = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);

                    if (exphour > 10000) exphour = Math.round(exphour / 10) / 100 + "k";
                    else exphour = Math.round(exphour);
                    if (rcol == true) cont += "<tr>";
                    else cont += "<tr class='alt'>";

                    cont += "<td>" + sk + " (" + percxp + "%)</td><td>" + exphour + "</td><td>" + tneed + "</td></tr>";
                    rcol = rcol == true ? false : true;
                };
            }

            cont += "</tbody></table>";

            xpp.innerHTML = cont;
            xpp.style.visibility = "";
        }
    }

    Mods.Miscmd.HideExpPopup = function () {
        getElem("ww_xp_popup").style.visibility = "hidden";
    }

    Mods.Miscmd.switchdpsmode = function () {
        if (Mods.Miscmd.dpsmode) {
            Mods.Miscmd.avgexp = 0;
            Mods.Miscmd.adps = new Array();
            Mods.Miscmd.maxtime = 180000;
            Mods.Miscmd.dpsmode = false;
        } else {
            Mods.Miscmd.maxdps = 0;
            Mods.Miscmd.avgdps = 0;
            Mods.Miscmd.adps = new Array();
            Mods.Miscmd.maxtime = 60000;
            getElem("ww_xp_popup").style.visibility = "hidden";
            Mods.Miscmd.dpsmode = true;
        };
        Mods.Miscmd.toolbar.updateToolbar("dpsinfo");
    };

    Mods.Miscmd.toolbar.playerStats = function () {
        var penetration = false;
        for (var a = 0; a < players[0].params.magic_slots; a++) {
            if (players[0].params.magics[a]) {
                var temp = Magic[players[0].params.magics[a].id].params.penetration;
                penetration = penetration || temp;
                penetration = Math.min(penetration, temp);
            }
        };
        penetration = penetration || 0;
        var stats = "<span style='color: #BBB;'>Stats:</span> " + Math.floor(players[0].temp.total_accuracy) + "A / " + Math.floor(players[0].temp.total_strength) + "S / " + Math.floor(players[0].temp.total_defense) + "D / " + Math.floor(players[0].temp.magic / 1.2 + skills[0].magic.level + penetration) + "M";
        return stats;
    };

    Mods.Miscmd.toolbar.questData = function (active) {
        var active_quests = getElem("quests_form_content").childNodes[0].childNodes[0];
        if (typeof active_quests == "undefined") {
            Quests.show_active();
            active_quests = getElem("quests_form_content").childNodes[0].childNodes[0]
        };
        for (var a in active_quests.childNodes) if (typeof active_quests.childNodes[a].title != "undefined" && /Location/.test(active_quests.childNodes[a].title)) {
            var q = active_quests.childNodes[a];
            if (q.onclick == null) for (var b in quests) if (q.innerHTML.search(quests[b].name) != -1) q.setAttribute("onclick", "Mods.Miscmd.toolbar.questData(\"" + b + "\"); Mods.Miscmd.toolbar.updateToolbar(\"questData\")");
        };
        var active = active || Mods.Miscmd.toolbar.activeQuest || false;
        if (!active || !player_quests[active] || player_quests[active].progress == quests[active].amount) for (var a = [], b = 0; b < player_quests.length; b++) player_quests[b].progress < quests[b].amount && (active = b);
        if (typeof player_quests[active] == "undefined") {
            getElem("td_quests").style.display = "none";
            return;
        };
        getElem("td_quests").style.display = "";
        var quest = "<span style='color: #BBB;'>Quest:</span> (" + player_quests[active].progress + "/" + quests[active].amount + ") " + npc_base[quests[active].npc_id].name;
        Mods.Miscmd.toolbar.activeQuest = active;
        localStorage.activeQuest = JSON.stringify(active);
        return quest;
    };

    Mods.Miscmd.toolbar.currentTime = function () {
        var Time = new Date();
        var hour = Time.getHours();
        var clock = hour < 11 ? " AM" : " PM";
        hour = hour == 0 ? 12 : hour > 12 ? hour - 12 : hour;
        var minute = Time.getMinutes();
        minute = minute.toString();
        minute = minute.length != 2 ? "0" + minute : minute;
        var time = hour + ":" + minute + " " + clock;
        return time;
    };

    Mods.Miscmd.toolbar.invSlots = function () {
        getElem("td_inventory_old").style.display = "none";
        var open = 40 - players[0].temp.inventory.length;
        var popen = "";
        var pmax = "";
        if (players[0].pet.enabled) {
            var popen = players[0].pet.chest.length;
            var pmax = pets[players[0].pet.id].params.inventory_slots;
            popen = pmax - popen;
        };
        var slots = "<span style='color: yellow;'>" + open + "</span>" + (players[0].pet.enabled ? "/" + "<span style='color:yellow;'>" + popen + "</span>" : "");
        /*var inv = "<span style='color: yellow;'>i" + (players[0].pet.enabled ? " / p:</span> " : ":</span> ") + slots;*/
        return slots;
    };

    Mods.Miscmd.toolbar.updateToolbar = function (item) {
        var bar = Mods.Miscmd.toolbar;
        var id = Mods.Miscmd.toolbar.ids;
        if (typeof item != "undefined") getElem(id[item]).innerHTML = bar[item]();
        else for (var item in id) getElem(id[item]).innerHTML = bar[item]();
    };

    Mods.Miscmd.inventoryEquip = function (a, b, d) {
        if (typeof b != "undefined" && item_base[b].name.indexOf("Potion") != -1 && item_base[b].name != "Potion Of Preservation") {
            for (var a = 1; a < 20; a++) {
                Timers.set("new_potion_" + b + "_" + a, function () {
                    var c, p, s, skill, skill_curr, skill_delta, time, holder, end;
                    holder = {};
                    holder[b] = {};
                    for (c in item_base[b].params) {
                        skill = /^boost_(.{1,})$/.exec(c);
                        if (skill) {
                            s = skill[1];
                            p = item_base[b].params[skill[0]];
                            time = timestamp();
                            skill_curr = skills[0][s].level;
                            skill_delta = Math.ceil(skill_curr * p);
                            end = skill_delta * (1000 * 60) + time;
                            holder[b][s] = {percent: p, start: time, delta: skill_delta, end: end}
                        }
                    }
                    if (getElem("mod_potion_" + b) != null) for (var a = 1; a < 20; a++) Timers.clear("new_potion_" + b + "_" + a);
                    else Mods.Miscmd.potions[b] = holder[b];

                }, 1000 * a)
            }
        }
        return false;
    }

    Mods.Miscmd.checkPotions = function () {
        //return false
        var potions, i, j, k, div, item, img, text, skill_value, skill_names, active, display;
        potions = Mods.Miscmd.potions;
        div = getElem("mod_potion_holder");
        skill_names = {accuracy: "ACC", alchemy: "Alch", carpentry: "Crpt", cooking: "Cook", defense: "DEF", farming: "Farm", fishing: "Fish", forging: "Forg", health: "Hlth", jewelry: "Jewl", magic: "Mag", mining: "Mine", strength: "STR", woodcutting: "Wood"}
        if (div !== null) delete wrapper[div]
        if (div == null) createElem("div", wrapper, {
            id: "mod_potion_holder",
            style: "position: absolute; z-index: 999; background: transparent; left: 11%; top: 15%; min-width: 32px; min-height: 32px;"
        });
        for (i in potions) {
            div = getElem("mod_potion_" + i);
            if (div == null) {
                item = item_base[i];
                img = IMAGE_SHEET[item.img.sheet];
                createElem("div", "mod_potion_holder", {
                    id: "mod_potion_" + i,
                    title: item.name,
                    style: "position: relative; display: block; padding: 2px; float: left;",
                    innerHTML: "<div style='background: url(&apos;" + img.url + "&apos;) no-repeat scroll " + -item.img.x * img.tile_width + "px " + -item.img.y * img.tile_height + "px transparent; float: left; width: 32px; height: 32px; margin-left: -16px; left: 50%; position: relative;'>&nbsp;</div><div id='mod_potion_duration_" + i + "' style='float: left; clear: left; font-size: 0.7em; color: #FFF; background: #444; padding: 4px; border: 1px solid #FFF; border-radius: 5px; -moz-border-radius: 5px;'>&nbsp;</div>"
                });
            }
            text = "";
            active = false;
            for (j in potions[i]) {
                skill_value = skills[0][j].current - skills[0][j].level;
                if (skill_value > 0) {
                    text += "<tr><td>" + skill_names[j] + "</td><td><span style='color: #FF0; text-align: right; padding-left: 4px;'>+" + skill_value + "</span></td></tr>";
                    active = true;
                    if (potions[i][j].value != skill_value) {
                        potions[i][j].value = skill_value;
                        display = true
                    }
                } else potions[i][j].value = 0;
            }
            if (display) {
                text = "<table>" + text + "</table>"
                getElem("mod_potion_duration_" + i).innerHTML = text
            } else if (!active) {
                getElem("mod_potion_holder").removeChild(getElem("mod_potion_" + i));
                delete potions[i]
            }
        }
    }

    Mods.Miscmd.socketOn = {
        actions: ["update_quest", "quests", "quest_complete", "my_data", "skills", "inventory", "my_pet_data", "item_drop", "skills", "hit"],
        fn: function (action, data, message) {
            var toolbar = Mods.Miscmd.toolbar.updateToolbar;
            if (action == "update_quest" || action == "quests" || action == "quest_complete") toolbar("questData")
            if (action == "my_pet_data" && data.enabled == false) setCanvasSize();
            if (action == "my_data" || action == "skills") {
                toolbar("playerStats");
                toolbar("playerLocation")
            }
            if (action == "inventory" || action == "my_pet_data" || action == "item_drop") {
                toolbar("invSlots");
                Mods.Miscmd.ideath.safe_items()
            }
            //CHG: ADD DPS TO TOOLBAR

            if (action == "skills" && Mods.Miscmd.dpsmode == false) {
                var newts = timestamp();
                var totdelta = 0;
                var mintime = newts;

                if (data.skill) //single skill packet
                {
                    if (Mods.Miscmd.lastSkill[data.skill]) {
                        if (Mods.Miscmd.lastSkill[data.skill].xp < data.stats.xp) {
                            //write delta with ts is array for each skill
                            var tmpo = new Object();
                            tmpo.skill = data.skill;
                            tmpo.xpdelta = data.stats.xp - Mods.Miscmd.lastSkill[data.skill].xp;
                            tmpo.time = newts;
                            Mods.Miscmd.adps.push(tmpo);
                            totdelta += data.stats.xp - Mods.Miscmd.lastSkill[data.skill].xp;
                        }
                    }
                    Mods.Miscmd.lastSkill[data.skill] = new Object();
                    Mods.Miscmd.lastSkill[data.skill].level = data.stats.level;
                    Mods.Miscmd.lastSkill[data.skill].xp = data.stats.xp;

                } else {

                    for (sk in data) {
                        if (Mods.Miscmd.lastSkill[sk]) {
                            //find the skill(s) that have updated xp
                            if (Mods.Miscmd.lastSkill[sk].xp < data[sk].xp) {
                                //write delta with ts is array for each skill
                                var tmpo = new Object();
                                tmpo.skill = sk;
                                tmpo.xpdelta = data[sk].xp - Mods.Miscmd.lastSkill[sk].xp;
                                tmpo.time = newts;
                                Mods.Miscmd.adps.push(tmpo);
                                totdelta += data[sk].xp - Mods.Miscmd.lastSkill[sk].xp;
                            }
                        }
                        //calculate total xp per skill
                        Mods.Miscmd.lastSkill[sk] = new Object();
                        Mods.Miscmd.lastSkill[sk].xp = data[sk].xp;
                        Mods.Miscmd.lastSkill[sk].level = data[sk].level;
                    }
                }


                if (Mods.Miscmd.adps.length > 0 && Mods.Miscmd.adps[0] && Mods.Miscmd.adps[0].time) {
                    while (Mods.Miscmd.adps[0].time + Mods.Miscmd.maxtime < newts) {
                        Mods.Miscmd.adps.shift();
                    }
                }

                Mods.Miscmd.avgexp = 0;

                for (var cntr = 0; cntr < Mods.Miscmd.adps.length; cntr++) {
                    Mods.Miscmd.avgexp += Mods.Miscmd.adps[cntr].xpdelta;
                    if (Mods.Miscmd.adps[cntr].time < mintime) { mintime = Mods.Miscmd.adps[cntr].time; }
                }

                Mods.Miscmd.avgexp = 3600 * Mods.Miscmd.avgexp / ((newts - mintime) / 1000);

                if (!isFinite(Mods.Miscmd.avgexp) || Mods.Miscmd.avgexp == NaN) Mods.Miscmd.avgexp = 0;

                toolbar("dpsinfo");
            }

            if (action == "hit" && Mods.Miscmd.dpsmode) {
                if (message.target.id == 0) {
                    //received damage
                    //console.debug(a.hit + " Incoming damage");
                }
                else if (message.target.id == selected_object.id) {
                    //damage done
                    //new dps calc

                    var newts = timestamp();
                    var tmpo = new Object();

                    tmpo.damage = message.hit;
                    tmpo.time = newts;
                    var mintime = newts - 5000;


                    Mods.Miscmd.adps.push(tmpo);

                    Mods.Miscmd.avgdps = 0;
                    if (Mods.Miscmd.adps.length > 0 && Mods.Miscmd.adps[0] && Mods.Miscmd.adps[0].time) {
                        while (Mods.Miscmd.adps[0].time + Mods.Miscmd.maxtime < newts) {
                            Mods.Miscmd.adps.shift();
                        }
                        for (var cntr = 0; cntr < Mods.Miscmd.adps.length; cntr++) {
                            Mods.Miscmd.avgdps += Mods.Miscmd.adps[cntr].damage;
                            if (Mods.Miscmd.adps[cntr].time < mintime) { mintime = Mods.Miscmd.adps[cntr].time; }
                        }
                    }

                    Mods.Miscmd.avgdps = Mods.Miscmd.avgdps / ((newts - mintime) / 1000);

                    if (Mods.Miscmd.avgdps > Mods.Miscmd.maxdps) { Mods.Miscmd.maxdps = Mods.Miscmd.avgdps; };
                    toolbar("dpsinfo");

                }

            }
        }
    }

    Quests.show_active = function () {
        Mods.Miscmd.toolbar.oldQuestsShowActive();
        Mods.Miscmd.toolbar.questData();
    };

    Mods.Miscmd.toolbar.checkTime = function () {
        Mods.Miscmd.toolbar.updateToolbar("currentTime");
        Mods.Miscmd.checkPotions();
        Timers.set("check_time", function () {
            Mods.Miscmd.toolbar.checkTime();
        }, 1000);
    };

    inventoryClick = function (a) {
        Mods.Miscmd.ideath.oldInventoryClick(a);
        if (!(Android && timestamp() - lastTap < 500)) {
            Mods.Miscmd.ideath.safe_items();
            Mods.Miscmd.toolbar.updateToolbar("playerStats");
            Mods.Miscmd.toolbar.updateToolbar("invSlots");
        }
    };

    drawMap = function (a, b, d) {
        Mods.Miscmd.toolbar.oldDrawMap(a, b, d);
        Mods.Miscmd.toolbar.updateToolbar("playerLocation");
    };

    Inventory.add = function (a, b, d) {
        Mods.Miscmd.toolbar.updateToolbar("invSlots");
        return Mods.Miscmd.toolbar.oldInventoryAdd(a, b, d);
    };

    Inventory.remove = function (a, b) {
        Mods.Miscmd.toolbar.updateToolbar("invSlots");
        return Mods.Miscmd.toolbar.oldInventoryRemove(a, b);
    };

    Mods.Miscmd.setCanvasSize = function () {
        getElem("toolbar_main_holder").style.fontSize = Mods.fontSize[0];
        var d = wrapper.style.width.replace("px", "") / width,
            e = wrapper.style.height.replace("px", "") / height;
        getElem("td_inventory", {
            style: {
                right: (((players[0].pet.enabled ? 66:34) + (players[0].map == 300 || players[0].map == 16 ? 32:0)) * d) + "px",
                top: (40 * e) + "px",
                width: (28 * d) + "px",
                height: (10 * e) + "px",
                fontSize: Mods.fontSize[0]
            }
        });

    };

    //Adds slider to change the audio volume in "Game options"
    function addVolumeOption() {
        createElem("div", "options_audio", {
            innerHTML: "<br>Volume: <input id='settings_volume_slider' type='range' min='0' max='100' step='5' value='50' onchange='Mods.Miscmd.changeVolume(value)'/>"
        });

        getElem("my_text").style.zIndex = "90";
        var slider = document.getElementById("settings_volume_slider");
        slider.value = localStorage.audioVolume;
    }

    Mods.Miscmd.changeVolume = function(volString) {
        localStorage["audioVolume"] = volString;
        var vol = parseInt(volString);
        try {
            for (var sound in sound_effects) sound_effects[sound].music.setVolume(vol,0);
            for (var map in map_music) map_music[map].music.setVolume(vol,0);
        } catch(e){}
    };

    //Only works well on Google Chrome
    if(navigator.userAgent.match(/chrome/i)) {
        addVolumeOption();
        Mods.Miscmd.changeVolume(localStorage.audioVolume);
    }

    //Adds speed stat to "equipment status" - Mod coded by Kiwi
    function addSpeedStat()
    {
       var oldShow_Skills = BigMenu.show_skills;
       BigMenu.show_skills = function(){
          oldShow_Skills();
          //speed stats on equips goes from 0 to 60; the param speed goes from 150 to 180;
          document.getElementById("status_speed").innerHTML = " " + 2 * (players[0].params.speed - 150);
       }

       var speedStatus = document.createElement("span");
       addClass(speedStatus,"skill");
       speedStatus.innerHTML = "Speed";
       var innerSpeedStatus = document.createElement("span");
       addClass(innerSpeedStatus,"level");
       innerSpeedStatus.id = "status_speed";
       innerSpeedStatus.innerHTML = 0;
       speedStatus.appendChild(innerSpeedStatus);
          document.getElementById("skills_menu").insertBefore(speedStatus,document.getElementById("skill_name").parentNode);
    }

    addSpeedStat();

    temp();
    Mods.timestamp("miscmd");
};

Load.chatmd = function () {

    modOptions.chatmd.time = timestamp();

    function temp () {
        getElem("chat").style.opacity = ".9";
        getElem("my_text").style.opacity = "1";

        createElem("div", "filters_form", {
            id: "mod_filters",
            style: "padding-top: 23px; width: 50%; float: left;"
        });

        var ids = {"spam": "Spam information", "color": "Colored channels", "modcolor": "Chat moderator color", "chattimestamp": "Chat timestamps", "urlfilter": "Disable URL in chat"};
        for (var i in ids) {
            createElem("span", "mod_filters", {
                id: "filter_" + i,
                className: "wide_link pointer green",
                innerHTML: ids[i],
                onclick: "ChatSystem.filter_toggle('" + i + "')"
            });
            if (i == "color") {
                createElem("span", "mod_filters", {
                    style: "margin-top: 5px; margin-bottom: 5px; padding-left: 5px; width: 100%; font-size: .9em; display: inline-block; vertical-align: middle;",
                    innerHTML: "<input type='checkbox' id='filter_channel_only' onclick='Mods.Chatmd.filter_checks();'><span style='position: absolute; padding-top: 5px; padding-left: 3px;'>Color channel only</span>"
                });
                createElem("span", "mod_filters", {
                    style: "margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-left: 5px; width: 100%; font-size: .9em; display: inline-block; vertical-align: middle;",
                    innerHTML: "<input type='checkbox' id='filter_highlight_friends' onclick='Mods.Chatmd.filter_checks();'><span style='position: absolute; padding-top: 5px; padding-left: 3px;'>Highlight friends</span>"
                });
            }
        }

        getElem("filter_channel_only").checked = JSON.parse(localStorage.colorChannel);
        getElem("filter_highlight_friends").checked = JSON.parse(localStorage.highlightFriends);

        getElem("filter_attempt").style.paddingTop = "20px";
        getElem("filter_attempt").parentNode.style.width = "50%";
        getElem("filter_attempt").parentNode.style.cssFloat = "left";
        getElem("filter_attempt").parentNode.childNodes[1].style.position = "absolute";
        getElem("filters_form").style.width = "328px";

        Mods.Chatmd.game_tips = {0: [
            "In defensive fight mode, each point of damage you deal will give 2 xp to the defense skill and 1 xp to health.",
            "In accurate fight mode, each point of damage you deal will give 2 xp to the accuracy skill and 1 xp to health.",
            "In aggressive fight mode, each point of damage you deal will give 2 xp to the strength skill and 1 xp to health.",
            "In controlled fight mode, combat xp is equally divided between strength, defense and accuracy. Health will always get 1 xp per damage dealt.",
            "Ingame wiki mod is a precious resource to plan your adventure. Access it typing /wiki in the chat line.",
            "You can turn tips off from the Game Options menu.",
            "If you like RPG MO, consider writing a review and gaining free MOS: http://forums.mo.ee/viewtopic.php?t=3870.",
            "There is no Cow Level.",
            "The Enhanced Map mod adds several details to the game map, including key resource spots, travel points and boss locations.",
            "The Enhanced Market mod adds several helpers for player's market operations, including the ability to resubmit expired offers and compare your equip with the one that is for sale.",
            "Your health slowly regenerates over time: you gain 1 health every minute.",
            "The more health a mob has, the more time it takes to respawn.",
            "Be ready to be hunted down and fight for your life if you go into No Man's Land, the RPG MO PvP area.",
            "Don't sell raw fish you get, cook it!",
            "Better boots can increase your movement speed.",
            "Food heals you. You can get food by killing chickens or by fishing. You may have to cook the food.",
            "The forums have a lot of information about this game. You can access them on http://forums.mo.ee/ .",
            "Are you lost? Access zone maps and much more on RPG MO Atlas: http://tinyurl.com/rpgmoatlas",
            "If you die, you will lose all the items you are carrying but the two most valuable. Beware!",
            "Potion of Preservation is a special potion that must be equipped and it will be consumed on death, allowing you to save a total of 7 items from your inventory.",
            "If you have a pet equipped and you die you keep the pet. If it is not equipped, but it's in your inventory you may lose it.",
            "When you die you do not lose the items you placed in your pet's inventory.",
            "You can buy an island deed from the farmer in Dorpat.",
            "To see your combat level, mouse over yourself. To see the level of monsters, position the mouse pointer over them.",
            "Public chat is global, meaning everyone online can read it!",
            "Players chat is white, moderators chat is green and developers/admins chat is orange.",
            "To whisper, type /w \"[playersname]\" followed by the message.",
            "Type /online to bring up a list of players currently online.",
            "Type /played to see how much time has passed since your first login.",
            "Type /penalty to view or spend your current penalty points.",
            "Type /wiki to access the ingame wiki database.",
            "Enable the Enhanced Market mod to gain access to the Trade Channel ($$).",
            "Type /xp to check the duration of an ongoing x2 Experience event.",
            "x2 Experience server events are triggered by players using special x2 pots from the MOS market.",
            "Need help? Don't be afraid to ask! RPG MO is full of helpful players! Further help and guides can be found in the game forums.",
            "Higher accuracy will allow you to equip better weapons.",
            "Higher defense will allow you to equip better armor.",
            "Higher health will allow you to equip better jewelry.",
            "Do not ignore Captchas! If you fail or ignore them you will get -5 penalty points and you will go to jail.",
            "If you end up in jail, only a Game Moderator or an Admin can decide to free you.",
            "You can save up to 5 captcha points. Using Captcha points will add XP to a skill of your choice.",
            "You can access the player's market through the chest.",
            "Some pets can be purchased from the pet vendor in Dorpat. Better pets are usually available on the market ",
            "A pet can extend your inventory space by 4, 8, 12 or 16 slots. Pets also give stats boosts.",
            "Cross-chatting is bad. If someone is speaking in a chat channel, make sure you answer in the same one.",
            "The best way to make money is through gathering professions (especially mining). There will always be players looking for iron, sand or coal in the player's market!",
            "Through the market you can place \"buy\" or \"sell\" offers, and you can check buy and sell offers from other players.",
            "MOS is a special currency that can be acquired with real money. It allows you to buy special items from the MOS store.",
            "The MOS Store is accessible from the \"Buy items and coins\" link at the bottom right of page.",
            "To use magic you need to buy and equip a magic pouch, fill it with spell scrolls (all available at Dorpat Magician NPC) and engage in combat.",
            "You are allowed to have a max of 5 accounts (alts), but trading items or materials between your own accounts is not allowed.",
            "Please check RPG MO Code of Conduct on http://forums.mo.ee/viewtopic.php?t=3083"
        ]};

        createElem("div", "options_game", {
            innerHTML: "<span class='wide_link pointer' id='settings_tips' onclick='Mods.Chatmd.Tipstoggle();'>Tips (on)</span>"
        });
        createElem("div", "options_game", {
            innerHTML: "<span class='wide_link pointer' id='settings_enableallchatts' onclick='Mods.Chatmd.Timestamptoggle();'>Timestamps on all chat lines (off)</span>"
        });

        Mods.Chatmd.ModCh.createDiv();
        Mods.Chatmd.Tipstoggle();
        Mods.Chatmd.ScheduleTips();
        Mods.Chatmd.Timestamptoggle();

        if (iOS) getElem("mod_text").style.left = "63px";

        if (getElem("checkbox_tabs").checked && Mods.loadedMods.indexOf("Tabs") == -1) Mods.loadSelectedMods("tabs");
        Contacts.add_channel("{M}");
		
		channel_names.push(Mods.Chatmd.ModCh.channel); Contacts.add_channel(Mods.Chatmd.ModCh.channel);

        addChatText("Several new chat commands available. Type /help to see a list and usage instructions.", void 0, COLOR.TEAL);

    }

    Mods.Chatmd.blockChat = function (a, b, d, e, f) {
        var blocked = {"#ping;": true, "@mods ": false};
        var block = false;
        var type;
        if (e == "whisper") {
            for (var item in blocked) {
                if (a.indexOf(item) == 0 && (b == players[0].name && blocked[item] || !blocked[item])) block = true;
                if (block && item == "@mods " && b != players[0].name) {
                    var message = a.replace(item, "");
                    var color = Player.is_mod(b) ? COLOR.GREEN : Player.is_admin(b) ? COLOR.ORANGE : "#EAE330";
                    addChatText(message, b, color, "chat", Mods.Chatmd.ModCh.channel);
                    return true;
                } else {
                    var delay = timestamp() - (Mods.Chatmd.delay || 0);
                    if (block) {
                        if (delay > 1000) {
                            Mods.Chatmd.delay = timestamp();
                            Mods.Chatmd.message(a);
                        }
                        return true
                    }
                }
            }
        };
        return false
    };

    Mods.Chatmd.message = function (message) {
        if (message == "#ping;") {
            var ping = timestamp() - Mods.Chatmd.ping;
            ping = "Your ping is: " + ping + "ms";
            addChatText(ping, void 0, COLOR.TEAL)
        }
    };

    addChatText = function (a, b, d, e, f, g, h) {
        var id;
        var chat_obj;
        if(typeof a == 'object'){
            b = a.user || a.name;
            d = a.color;
            e = a.type;
            f = a.lang;
            g = a.to;
            h = a.server;
            id = a.id;
            chat_obj = JSON.clone(a);
            a = a.text;
        }
        var blockChat = Mods.Chatmd.blockChat(a, b, d, e, f, g);
        if (blockChat) return;
        var spamItems = [
            "You are under attack!",
            "Cannot do that yet.",
            "Cannot do that yet",
            "Cannot do that yet.!",
            "I think that I'm missing something.",
            "You feel a bit better.",
            "Your inventory is full!",
            "Your inventory is full.",
            "You feel more experienced.",
            "I need a seed to do that.",
            "I need a rake to do that.",
            selected_object && selected_object.activities && selected_object.activities[0] == ACTIVITIES.INSPECT && (selected_object.params.desc || "It's a " + selected_object.name),
            "Not enough free space in magic pouch!",
            players[0].params.magic_slots && "This pouch has a limit " + thousandSeperate(players[0].params.magic_slots * 1E3) + " of each spell!",
            "You need a magic pouch!",
            "Your chest is full!"
        ];
        for (var spam = 0; spam < spamItems.length; spam++) if (a === spamItems[spam]) {
            e = "spam";
            break
        };
        if (e && e == "spam") {
            if (timestamp() - last_cannot_message < 1E3) return;
            last_cannot_message = timestamp();
        };

        var sendcurrent = true;

        //CHG ADD LOAD Mods.Tabs.wwTabContent.history based on Mods.Tabs.wwCurrentTabs.filters
        //commands only go to current tab
        //SKIP IF TAB IS NOT LOADED
        var ts = new Date();

        if (loadedMods.indexOf("Tabs") > -1) {
            if (!(Contacts.ignores.indexOf(b) > -1)) {
                for (var cnt in Mods.Tabs.wwCurrentTabs) {

                    //verify target filters and channels
                    //no need: timestamps and friend highlight
                    var send = true;

                    //filter unsubscribed channels
                    if (f != undefined) {
                        if (!Contacts.channels[f]) {
                            send = false;
                        }
                    }



                    if (send) {
                        //filter on channels
                        for (var ln in Mods.Tabs.wwCurrentTabs[cnt].channels) {
                            if (ln == f && Mods.Tabs.wwCurrentTabs[cnt].channels[ln] == false) {
                                send = false;
                                if (Mods.Tabs.wwCurrentTabs[cnt].id == Mods.Tabs.wwactiveTab) { sendcurrent = false; }
                                break;
                            }
                        }
                    }

                    //filter on filters
                    if (send) {
                        if (Mods.Tabs.wwCurrentTabs[cnt].filter_skillattempts == true && e == "attempt") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_skillfails == true && e == "fails") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_playerchat == true && e == "chat") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_whispering == true && e == "whisper") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_joinleave == true && e == "join_leave") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_loot == true && e == "loot") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_magic == true && e == "magic") { send = false; }
                        else if (Mods.Tabs.wwCurrentTabs[cnt].filter_spam == true && (e == "spam")) { send = false; }

                        if (Mods.Tabs.wwCurrentTabs[cnt].filter_chatmoderator == true && d == COLOR.GREEN) { d = COLOR.WHITE };
                        if (d == COLOR.WHITE && Mods.Tabs.wwCurrentTabs[cnt].filter_coloredchannels == false && Mods.Tabs.wwCurrentTabs[cnt].filter_coloredonly == false) { d = Mods.Chatmd.colorChat(f); }

                    }

                    //send only in current tab
                    if (send && ((b == undefined || b == null) && f == undefined && (e == undefined || e == "chat") || (e == "spam" || e == "exists" || e == "cannot" || e == "duel"))) {
                        if (Mods.Tabs.wwCurrentTabs[cnt].id != Mods.Tabs.wwactiveTab) send = false;
                    };
                    if (send) {
                        Mods.Tabs.wwTabContent[Mods.Tabs.findWithAttr(Mods.Tabs.wwTabContent, "id", Mods.Tabs.wwCurrentTabs[cnt].id)].history.push({
                            text: a,
                            user: b,
                            color: d,
                            lang: f,
                            type: e,
                            to: g,
                            server: h,
                            id: id,
                            chattimestamp: (ts.getHours() < 10 ? "0" : "") + ts.getHours() + ":" + (ts.getMinutes() < 10 ? "0" : "") + ts.getMinutes() + ":" + (ts.getSeconds() < 10 ? "0" : "") + ts.getSeconds()
                        });

                        if (Mods.Tabs.wwTabContent[cnt].history.length > Chat.max_chat_history) { Mods.Tabs.wwTabContent[cnt].history.splice(0, 1) };

                        //warningflash
                        if (Mods.Tabs.wwCurrentTabs[cnt].id != Mods.Tabs.wwactiveTab) {
                            Mods.Tabs.Warning(Mods.Tabs.wwCurrentTabs[cnt].id);
                        }
                    }
                }
            } else {
                if(typeof g != 'undefined'){
                    if(timestamp()-(last_ignore_message || 0) > 10000){
                        last_ignore_message = timestamp();
                        Socket.send('message', {
                            data: '/w "' + b + '" [Ignored] This player does not receive your messages.',
                            lang: getElem('current_channel').value,
                            silent: true
                        });
                    }
                }
                return;
            }
        }

        if (sendcurrent == false) { return; }


        var gr = d == COLOR.GREEN ? true : false;
        var mf = chat_filters.modcolor == true ? true : false;
        var ch = !getElem("filter_channel_only").checked ? true : false;
        var cc = !chat_filters.color ? true : false;
        cc && d != COLOR.ORANGE && (ch && mf || ch && !gr) && (d = Mods.Chatmd.colorChat(f) || d || COLOR.WHITE);
        !ch && mf && gr && (d = COLOR.WHITE);
        if(typeof chat_obj == 'object'){
            chat_obj.text = a;
            chat_obj.user = b;
            chat_obj.color = d;
            chat_obj.lang = f;
            chat_obj.type = e;
            chat_obj.to = g;
            chat_obj.server = h;
            Mods.Chatmd.addChatText(chat_obj, b, d, e, f, g, h);
        } else {
        Mods.Chatmd.addChatText(a, b, d, e, f, g, h);
        }
    };

    Chat.add_line = function (a) {
        if (typeof chat_history[a] == "object") {
            var b = document.createElement("div"),
                d = COLOR.WHITE,
                f = "";
            b.style.display = "block";
            b.className = "chat_text";
            if (chat_history[a].color) d = chat_history[a].color;
            b.style.color = d;
            var color = COLOR.WHITE;
            var cc = !chat_filters.color ? true : false;
            typeof chat_history[a].lang != "undefined" && (cc && (color = Mods.Chatmd.colorChat(chat_history[a].lang)) || (color = COLOR.WHITE)) && (f = f + ("<span style='color: " + color + ";'>[" + chat_history[a].lang + "]</span>"));

            //CHG: timestamp
            if (!chat_filters.chattimestamp == true && (chat_history[a].user || Mods.Chatmd.enableallchatts == 1)) {
                if (chat_history[a].chattimestamp) {
                    f = "(" + chat_history[a].chattimestamp + ") " + f;
                }
                else {
                    var ts = new Date(); // for now
                    f = "(" + (ts.getHours() < 10 ? "0" : "") + ts.getHours() + ":" + (ts.getMinutes() < 10 ? "0" : "") + ts.getMinutes() + ":" + (ts.getSeconds() < 10 ? "0" : "") + ts.getSeconds() + ") " + f;
                }
            }
            if(chat_history[a].id){
                b.id = "chat_"+chat_history[a].id;
            }
            if (chat_history[a].user && chat_history[a].type != "join_leave") {
                if ((chat_history[a].type == "whisper" || chat_history[a].lang == undefined) && (chat_history[a].user != players[0].name || chat_history[a].to != players[0].name)) {
                    var whispNames = Mods.Chatmd.whispNames;
                    var tar = chat_history[a].user != players[0].name ? chat_history[a].user : chat_history[a].to;
                    var index = whispNames.indexOf(tar);
                    if (index > -1) whispNames.splice(index, 1);
                    whispNames.unshift(tar);
                }
                var name = chat_history[a].user;
                var friend = false;
                var highlight_color = "#FFFF00";
                var col = getElem("filter_highlight_friends").checked ? "<span style='color: " + highlight_color + ";'>" : "";
                var wto = chat_history[a].to && chat_history[a].to != players[0].name ? "to " : "";
                var to_unsanitized = chat_history[a].to && chat_history[a].to != players[0].name ? chat_history[a].to : chat_history[a].user;
                var to = to_unsanitized && to_unsanitized.sanitizeChat();
                var highlight = !chat_filters.friends || false;
                for (var q in Contacts.friends) if (Contacts.friends[q].name == to) friend = true;
                var h_o = friend && highlight && (wto + col + "&lt;</span>") || (wto + "&lt;");
                var h_c = friend && highlight && (col + "&gt;</span> ") || "&gt; ";
                f = f + (h_o + to + h_c);
                var onclick_func = (function(_to){
                  return function(e){
                    Mods.Chatmd.ModCh.nameClick(_to)
                  }
                }(to_unsanitized));
                b.onclick = onclick_func;
                b.oncontextmenu = function (b) {
                    Mods.Chatmd.chatContext(b,to_unsanitized);
                }

                Chat.set_visible();
            };

            d = chat_history[a].text.sanitizeChat().replace(/%26/g, "&amp;");

            // adjusted login/out events
            var event = /( has joined the game.| has left the game.)/.exec(d);
            if (chat_history[a].type == "join_leave" || event && chat_history[a].lang == undefined && chat_history[a].type != "whisper" && chat_history[a].user == undefined) {
                var name = chat_history[a].user || d.replace(event[0], "");
                name = name.trim();
                var text = event[0] || (" " + chat_history[a].text);
                var friend = false;
                var ignore = Contacts.ignores.indexOf(name) == -1 ? false : true;
                for (var q in Contacts.friends) if (Contacts.friends[q].name == name) friend = true;
                if (friend && !chat_filters.friends) d = "<span style='color: #FFFF00'>" + name + "</span>" + text;
                else d = name + text
                if (ignore) return;
                b.onclick = new Function("Mods.Chatmd.ModCh.nameClick('" + name + "')");
                b.oncontextmenu = function (b) {
                    Mods.Chatmd.chatContext(b,name);
                }
            }

            // $$ colored offers
            if (chat_history[a].lang == "$$" && cc) {
                //Mods.consoleLog(d);
                var replace = {"'": "`", "Necklace": "Neck.", "Medallion": "Medal.", "Platinum ": "Plat. ", "Pet ": "", " Scroll": "", "Enchanted ": "Ench. ", "Platemail": "Plate.", "Helmet": "Helm.", "Superior ": "Sup. ", " Permission": "", " Of": ":", "Defense": "Def.", "Accuracy": "Acc.", "Strength": "Str.", "Farming": "Farm.", "Woodcutting": "WC.", "Jewelry": "Jewel.", "Cooking": "Cook.", "Carpentry": "Carp.", "Alchemy": "Alch.", "Fishing ": "Fish. ", " Fishing": " Fish.", "Medium ": "Med. ", "Teleport": "Tele."};
                for (var s in replace) d = d.replace(RegExp(s, "g"), replace[s]);
                var invert = {};
                for (var s in replace) if (replace[s] != "" && replace[s] != " Fish") invert[replace[s]] = s;
                var offers = /(\d[km]? )([^|]{1,})( for )/g;
                var update = offers.exec(d);
                for (var q = 0; q < d.length; q++) {
                    if (update === null) break;
                    var reduced_item_name = update[2];
                    var item = reduced_item_name;
                    //Mods.consoleLog(item);
                    for (var s in invert) item = item.replace(s, invert[s]);
                    //Mods.consoleLog(update);
                    //Mods.consoleLog(item);
                    var id = false;
                    //checks if there is a name in item_base containing the string item
                    for (var s in item_base) if (item_base[s].name.replace("Pet ", "").toLowerCase().indexOf(item.toLowerCase()) == 0) {
                        //checks if the replaced name in item_base matches reduced_item_name
                        var temp_name = item_base[s].name;
                        for (var s2 in replace) temp_name = temp_name.replace(RegExp(s2, "g"), replace[s2]);
                        if (temp_name === reduced_item_name) {
                            id = s;
                            break;
                        };
                    };
                    var type = /^\[BUY\]/.test(d) ? 1 : 0;
                    var color = /^\[BUY\]/.test(d) ? "#62E7B7" : "#E7A762";
                    //Mods.consoleLog(id);
                    d = d.replace(update[0], update[1] + '<span class="pointer" item_id="' + id + '" onclick="Mods.Chatmd.marketSearch(' + type + ', false, ' + id + '); Mods.Chatmd.block_hidden=true;" style="color: ' + color + ';">' + update[2] + '</span>' + update[3]);
                    update = offers.exec(d);
                };
                //Mods.consoleLog(d);
                d = d.replace(/\[SELL\]/, "<span style='color: #E7A762;'>[SELL]</span>");
                d = d.replace(/\[BUY\]/, "<span style='color: #62E7B7;'>[BUY]</span>");
                /\[SELL\]/.test(d) && (d = d.replace(/\|/g, "<span style='color: #E7A762;'>|</span>"));
                /\[BUY\]/.test(d) && (d = d.replace(/\|/g, "<span style='color: #62E7B7;'>|</span>"));
            }

            chat_history[a].lang == "EN" && (d = d.filterChat("EN"));
            if (chat_filters.urlfilter == true) {
                b.innerHTML = f + Mods.Chatmd.urlify(d);
            } else {
                b.innerHTML = f + d;
            };

            //colors to /online
            if (/Currently online\(/.test(d) && !chat_history[a].user) {

                //var aAdmins = ["margus", "reside", "merit", "fiendish"];
                //var aMods = ["katt", "kemikaalikeijo", "paxli", "roryajax", "woofy", "nox", "dendrek", "roase", "bmw9191", "witwiz", "trishula", "foxy"];

                //CHG: SORT & color friends
                var init = d.split("):")[0];
                var lst = d.split("):")[1].split(",").sort();

                b.innerHTML = init + "):";

                for (var ouser in lst) {
                    var cuser = lst[ouser].trim().sanitize();
                    var uspan = document.createElement("span");
                    uspan.innerHTML = " " + cuser;
                    if (Player.is_admin(cuser)) {
                        uspan.style.color = COLOR.ORANGE;
                    }
                    else if (Player.is_mod(cuser)) {
                        uspan.style.color = COLOR.GREEN;
                    }
                    else if (Mods.findWithAttr(Contacts.friends, "name", cuser)) {
                        uspan.style.color = "yellow";
                    }
                    uspan.onclick = new Function("Mods.Chatmd.ModCh.nameClick('" + cuser + "');");

                    uspan.oncontextmenu = function (b) {
                        Mods.Chatmd.chatContext(b, this.innerHTML.trim());
                    }

                    b.appendChild(uspan);
                    if(ouser<lst.length-1)
                        b.insertAdjacentHTML('beforeend', ',');
                }
            };

            addClass(b, "scrolling_allowed");
            a = false;
            getElem("chat").scrollHeight - getElem("chat").offsetHeight - getElem("chat").scrollTop <= 20 && (a = true);
            getElem("chat").appendChild(b);
            if (a) getElem("chat").scrollTop = getElem("chat").scrollHeight
        }
    };

    Mods.Chatmd.set_hidden = function () {
        if (Mods.Chatmd.block_hidden == true) {
            Mods.Chatmd.block_hidden = false;
            return false
        } else return true;
    }

    Mods.Chatmd.marketSearch = function (type, category, item) {
        if (!selected || selected && selected.name != "Chest" || typeof type == "undefined" || typeof item == "undefined") return;
        Market.open(selected);
        if (type == 1 || type == 0) getElem("market_search_type").value = type;
        else return;
        if (category !== false && category > -1 && category < 11) getElem("market_search_category").value = category;
        else if (item_base[item] != undefined) getElem("market_search_category").value = parseInt(item_base[item].b_t);
        else return;
        if (item_base[item] != undefined) getElem("market_search_item").value = item;
        else if (category > 8 && typeof item == "string") getElem("market_search_name").value = item;
        else return;
        Market.client_search();
    }

    Mods.Chatmd.ModCh.nameClick = function (name) {
        if (GAME_STATE != GAME_STATES.CHAT) return;
        var mod_text = getElem("mod_text");
        var my_text = getElem("my_text");
        if (getElem("current_channel").value == Mods.Chatmd.ModCh.channel) {
            var data = Mods.Chatmd.ModCh.targets();
            //Mods.consoleLog("name click");
            //Mods.consoleLog(data);
            if (data) {
                var targets = data.targets;
                var message = data.message;
                if (targets.indexOf(name) == -1) targets.push(name);
                else targets.splice(targets.indexOf(name), 1);
                var str = JSON.stringify(targets);
                //Mods.consoleLog(str);
                str = str.replace(/"/g, "");
                mod_text.value = "@" + str + " " + message;
                my_text.value = "/@mods @" + str + " " + message;
                Chat.update_string();
            } else {
                mod_text.value = "/w \"" + name + "\" ";
                my_text.value = "/w \"" + name + "\" ";
                Chat.update_string();
            }
        } else {
            my_text.value = "/w \"" + name + "\" ";
            Chat.update_string();
        }
    };

    Mods.Chatmd.ModCh.targets = function () {
        if (GAME_STATE == GAME_STATES.CHAT && getElem("current_channel").value == Mods.Chatmd.ModCh.channel) {
            var message = getElem("mod_text").value;
            var has_target = /^@/.test(message);
            if (has_target) {
                var test1 = /^@ ?([\[\(][^\]\)]{0,}[\]\)])/;
                var test2 = /^@ ?"([^"]{0,})"/;
                var test3 = /^@ ?([^ ]{1,}) /;
                var data = test1.exec(message) || test2.exec(message) || test3.exec(message);
                //Mods.consoleLog(data);
                if (!data) return {targets: [], message: ""};
                message = message.slice(data[0].length).trim().replace(/""/, "\"\"");
                var targets = data[1];
                targets == "" && (targets = "[]");
                targets = targets.replace(/",? ?"/g, "\",\"").replace(/^\(/, "[").replace(/\)$/, "]").replace(/^\[/, "[\"").replace(/, ?/g, "\",\"").replace(/]$/, "\"]").replace(/""/g, "\"");
                targets == "[\"]" && (targets = "[]");
                !/^\[.*\]$/.test(targets) && (targets = "[\"" + targets + "\"]");
                //Mods.consoleLog(targets);
                targets = JSON.parse(targets);
                var data = {
                    message: message,
                    targets: targets
                };
                return data;
                //Mods.consoleLog(targets)
            }
        } return false;
    };

    Mods.Chatmd.chatContext = function (b,name) {
        b.preventDefault();
        b.clientX = b.clientX || b.pageX || b.touches[0].pageX;
        b.clientY = b.clientY || b.pageY || b.touches[0].pageY;
        var d = name,
            e = function (a) {
                Socket.send("message", {
                    data: a,
                    lang: getElem("current_channel").value
                })
            };

        var a1 = [];
        if (mod_initialized){
            a1=[{
                name: d,
                method: "Mute",
                func: function () {
                    e("/mute " + d)
                }
            }, {
                name: d,
                method: "Unmute",
                func: function () {
                    e("/unmute " +
                        d)
                }
            }, {
                name: d,
                method: "Kick",
                func: function () {
                    e("/kick " + d)
                }
            }, {
                name: d,
                method: "Ban",
                func: function () {
                    Popup.prompt("Ban. Are you sure?", function () {
                        e("/ban " + d)
                    })
                }
            }];

            if(b.srcElement.id){
              a1.push({name: d, method: 'Remove Message', func: function(){
                Socket.send('remove_line',{
                  line: b.srcElement.id.replace( /^\D+/g, '')
                });
                Popup.prompt("Also mute "+d+"?", function(){
                  e('/mute '+d);
                }, null_function);
              }});
            }
        }

        a1 = a1.concat([{
            name: d,
            method: "Add Friend",
            func: function () {
                //if not already friend
                if (!Mods.findWithAttr(Contacts.friends, "name", d)) {
                    Socket.send("contacts", {
                        sub: "add_friend",
                        name: d
                    });
                    Contacts.list_add("friends", d);
                    addChatText(d + " was added as friend.", void 0, COLOR.TEAL);
                } else
                    addChatText(d + " is already in your friends list.", void 0, COLOR.TEAL);
            }
        }, {
            name: d,
            method: "Remove Friend",
            func: function () {
                if (Mods.findWithAttr(Contacts.friends, "name", d)) {
                    Socket.send("contacts", {
                        sub: "remove_friend",
                        name: d
                    });
                    Contacts.list_remove("friends", d);
                    addChatText(d + " was removed from your friends list.", void 0, COLOR.TEAL);
                }
                else
                    addChatText(d + " is not in your friends list!", void 0, COLOR.TEAL);
            }
        }, {
            name: d,
            method: "Ignore",
            func: function () {
                if (Contacts.ignores.indexOf(d) == -1) {
                    Contacts.ignore_player(d);
                    !Player.is_admin(d) && !Player.is_mod(d) && addChatText(d + " was added to your ignore list!", void 0, COLOR.TEAL);
                }
            }
        }, {
            name: d,
            method: "Whisper",
            func: function () {
                hasClass(getElem("my_text"), "hidden") && ChatSystem.toggle();
                getElem("my_text").value = '/w "' + d + '" ';
            }
        }, {
            name: "",
            method: "Cancel",
            func: function () {
                addClass(getElem("action_menu"), "hidden");
            }
        }]);

        ActionMenu.custom_create(b, a1);

    }

    Mods.Chatmd.urlify = function (text) {
        //var urlRegex = /(https?:\/\/[^\s]+)/g;
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

        return text.replace(urlRegex, '<a href="$1" target="_blank" onclick="event.stopPropagation();this.blur();" style="color:yellow;">$1</a>');
    };

    Mods.Chatmd.colorChat = function (channel) {
        var typing = false;
        var colorChat = Mods.Chatmd.colors;
        var channels = Mods.Chatmd.default_channels;
        var ischannel = false;
        if (typeof channel == "boolean" && !channel) typing = true;
        else if (typeof channel == "string" && channels[channel]);
        else if (typeof channel == "string") {
            for (var a in channel_names) if (channel_names[a] == channel) {
                ischannel = true;
                break;
            }
            if (!ischannel) return false;
        } else return false;
        typing && (channel = getElem("current_channel").value);
        var color = colorChat[channel] || channels[channel] && colorChat["default"] || colorChat["none"];
        chat_filters.color && (color = COLOR.WHITE)
        if (typing) {
            var test_value = /^\//.test(getElem("my_text").value);
            var test_valch = /^\//.test(getElem("mod_text").value);
            var test_modch = /^\/\@mods/.test(getElem("my_text").value);
            getElem("current_channel").style.color = color;
            (!test_value || test_modch) && (getElem("my_text").style.color = color);
            (!test_valch && !(test_value && !test_modch)) && (getElem("mod_text").style.color = color);
            return false
        } else return color
    };

    Mods.Chatmd.chatCommand = function (keyCode) {
        var value = getElem("my_text").value;
        var mvalue = getElem("mod_text").value;
        var my_text = getElem("my_text");
        var mod_text = getElem("mod_text");
        // changes & to %26 if not /^\/id /
        !/^\/id /.test(value) && /&/g.test(value) && (my_text.value = value.replace(/&/g, "%26"));
        !/^\/id /.test(mvalue) && /&/g.test(mvalue) && (mod_text.value = mvalue.replace(/&/g, "%26"));
        // cycles through whisper targets if pressing up / down
        if (/^\/w /.test(value) && Mods.Chatmd.cycleWhisper && (keyCode == 33 || keyCode == 34)) {
            var target = /(^\/w (\"([^\"]{1,})\"|([^ ]{1,})))/.exec(value);
            var whispNames = Mods.Chatmd.whispNames;
            if (target) {
                var oldTarget = target[3];
                var newTarget = "";
                var index = whispNames.indexOf(oldTarget);
                // Pgup = 33, Pgdown = 34
                if (keyCode == 33) newTarget = index == -1 || index == whispNames.length -1 ? whispNames[0] : whispNames[index + 1];
                if (keyCode == 34) newTarget = index == -1 || index == 0 ? whispNames[whispNames.length - 1] : whispNames[index - 1];
                value = value.replace(target[2], '"' + newTarget + '"');
                my_text.value = value;
                mod_text.value = value;

            }
        }
        var current_channel = getElem("current_channel")
        var test_value = /^\//.test(value);
        var test_modch = /^\/\@mods/.test(value);
        var test_valch = /^\//.test(mod_text.value);
        if (chat_filters.color) {
            my_text.style.color = COLOR.WHITE;
            current_channel.style.color = COLOR.WHITE;
            mod_text.style.color = COLOR.WHITE;
        };
        Mods.Chatmd.colorChat(false)
        if (current_channel.value == Mods.Chatmd.ModCh.channel) Mods.Chatmd.ModCh.chatCommand(test_value, test_modch, test_valch, value, my_text, mod_text)
        else {
            //current_channel.style.color = "";
            if (current_channel.value != Mods.Chatmd.ModCh.channel && (test_modch || !hasClass(mod_text, "hidden"))) {
                //Mods.consoleLog("not in mod_text, just left : " + mod_text.value + " : " + my_text.value);
                my_text.value = my_text.value.replace("/@mods ", "");
                removeClass(my_text, "hidden");
                my_text.focus();
                mod_text.value = "";
                addClass(mod_text, "hidden");
                //Mods.Chatmd.colorChat(false)
            } else if (test_value) {
                !chat_filters.color && (my_text.style.color = COLOR.TEAL) && (mod_text.style.color = COLOR.TEAL);
                if (/^\/r/.test(value)) {
                    Mods.Chatmd.chat.whisp(value);
                    Chat.update_string();
                }
            } else mod_text.value = my_text.value;
        }
        Chat.update_string();
    };

    Mods.Chatmd.ModCh.chatCommand = function (test_value, test_modch, test_valch, value, my_text, mod_text) {
        if (!(Player.is_mod(players[0].name) || Player.is_admin(players[0].name))) {
            if (Mods.Chatmd.ModCh.delay == false || Mods.Chatmd.ModCh.delay + 6E5 < timestamp()) {
                Mods.Chatmd.ModCh.delay = timestamp();
                addChatText("Use this channel to report issues of in-game abuse or harassment to the Chat Moderators.", void 0, COLOR.ORANGE);
            }
        }
        if (/^@ ?< ?$/g.test(mod_text.value)) {
            for (var a = (chat_history.length - 1); a >= 0; a--) if (chat_history[a].lang == "{M}" && /^@\(/.test(chat_history[a].text)) {
                var test = /(^@\([^\)]{1,}\)) /g.exec(chat_history[a].text);
                var text = test[1];
                mod_text.value = text;
                my_text.value = "/@mods " + text;
                Chat.update_string()
                return;
            }
            mod_text.value = mod_text.value.replace(/^@ ?\< ?/, "@() ");
            my_text.value = "/@mods " + mod_text.value;
        } else if (!test_modch && !test_valch) {
            if (my_text.value == "/" || mod_text.value == "/") my_text.value = "";
            //Mods.consoleLog("in channel, not setup for it : " + mod_text.value + " : " + my_text.value);
            mod_text.value = mod_text.value.length > 0 ? mod_text.value : my_text.value;
            !test_value && (my_text.value = "/@mods " + my_text.value);
            addClass(my_text, "hidden");
            my_text.blur();
            removeClass(mod_text, "hidden");
            mod_text.focus();
            //mod_text.style.color = COLOR.GREEN;
        } else if (test_valch) {
            //Mods.consoleLog("mod_text has /: " + mod_text.value + " : " + my_text.value);
            if (/^\/r/.test(mod_text.value)) Mods.Chatmd.chat.whisp(mod_text.value);
            my_text.value = mod_text.value;
            !chat_filters.color && (mod_text.style.color = COLOR.TEAL) && (my_text.style.color = COLOR.TEAL);
        } else my_text.value = "/@mods " + mod_text.value;
    }

    Mods.Chatmd.ModCh.createDiv = function () {
        if (getElem("mod_text") == null) createElem("input", wrapper, {
            id: "mod_text",
            type: "text",
            className: "hidden",
            style: "font-family: Brawler, cursive; font-size: 13px; position: absolute; left: 55px; bottom: 23px; z-index: 90; opacity: 1; color: " + COLOR.GREEN + "; text-shadow: 1px 1px 1px #3A3A3A;\
                background: #9C9C9C; border: 0px; padding: 0px; margin: 0px;",
            setAttributes: {
                autocomplete: "off",
                size: "1",
                onkeypress: "javascript: Chat.update_string();",
                maxlength: "154"
            }
        });
    };

    Mods.Chatmd.ModCh.listener = function (keyCode) {
        if (GAME_STATE == GAME_STATES.CHAT) {
            if (!hasClass(getElem("mod_text"), "hidden") && (keyCode == 8 || keyCode == 46) && /^\//.test(getElem("my_text").value) && getElem("mod_text").value == "") getElem("my_text").value = "";
            Mods.Chatmd.chatCommand(keyCode)
        } else {
            addClass(getElem("mod_text"), "hidden");
            getElem("mod_text").value = "";
            getElem("mod_text").blur()
        }
    };

    Mods.Chatmd.setCanvasSize = function () {
        var b = Math.min(16, Math.round(16 * current_ratio_y));
        getElem("mod_text").style.bottom = 25 * current_ratio_y + "px";
        getElem("mod_text").style.fontSize = b + "px";
    };

    Mods.Chatmd.ModCh.sendWhisper = function (message) {
        var data = Mods.Chatmd.ModCh.targets();
        var message = getElem("mod_text").value;
        var targets = "";
        var list = "";
        if (data) {
            message = data.message;
            targets = data.targets;
            if (targets.length != 0) list = "@(" + targets.toString() + ") ";
        }
        if (message == "" || message == " ") return;
        var name, whisper, color;
        var excluded = {"kemikaalikeijo": 1, "reside": 1}
        for (name in online_players) {
            whisper = false;
            if ((!excluded[name] || targets.indexOf(name) != -1) && (Player.is_mod(name) || Player.is_admin(name) || targets.indexOf(name) != -1)) whisper = true;
            var tars = Player.is_mod(name) || Player.is_admin(name) ? list : "";
            if (whisper) Socket.send("message", {
                data: "/w \"" + name + "\" @mods " + tars + message
            });
        }
        var color = Player.is_mod(players[0].name) ? COLOR.GREEN : Player.is_admin(players[0].name) ? COLOR.ORANGE : "#EAE330";
        addChatText(list + message, players[0].name, color, "chat", Mods.Chatmd.ModCh.channel)
    };

    Chat.update_string = function () {
         getElem("my_text").size = Math.max(getElem("my_text").value.length, 1);
         getElem("mod_text").size = Math.max(getElem("mod_text").value.length, 1);
    };

    window_onclick = function (e) {
        if(e && e.target && e.target.id == 'current_channel'){
            return;
        }
        if (captcha) getElem("recaptcha_response_field").focus();
        else if (GAME_STATE == GAME_STATES.CHAT) {
            if (!hasClass(getElem("mod_text"), "hidden")) getElem("mod_text").focus();
            else getElem("my_text").focus()
        }
    };
    window.onclick = window_onclick;

    Mods.Chatmd.filter_checks = function () {
        var ch = getElem("filter_channel_only").checked;
        var hf = getElem("filter_highlight_friends").checked;
        localStorage.colorChannel = JSON.stringify(ch);
        localStorage.highlightFriends = JSON.stringify(hf);
        if (loadedMods.indexOf("Tabs") > -1) {
            var ct = Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", Mods.Tabs.wwactiveTab)];
            ct.filter_coloredonly = getElem("filter_channel_only").checked;
            ct.filter_highlightfriends = getElem("filter_highlight_friends").checked;
            localStorage.CurrentTabs = JSON.stringify(Mods.Tabs.wwCurrentTabs);
        }
    };

    ChatSystem.filters_init = function () {
        var a = ["attempt", "fails", "chat", "whisper", "join_leave", "loot", "magic", "spam", "color", "modcolor", "chattimestamp", "urlfilter"],
            b;
        for (b in a) {
            var d = getElem("filter_" + a[b]);
            removeClass(d, "green");
            removeClass(d, "red");
            chat_filters[a[b]] ? addClass(d, "red") : addClass(d, "green")
        }

        //if tabbed chat enabled, save filter for tab
        if (loadedMods.indexOf("Tabs") > -1) Mods.Tabs.SaveCurrent();
    };

    Mods.Chatmd.socketOn = {
        actions: ["message", "login"],
        fn: function (action, data) {
            if (data && data.type == "whisper" && data.name != players[0].name) Mods.Chatmd.afkReply(data.name, data.message);
            if (action == "login" && data && data.status == "ok") Contacts.add_channel("{M}");
        }
    }

    Mods.Chatmd.eventListener = {
        keys: {"keydown": [KEY_ACTION.SEND_CHAT], "keyup": [true]},
        fn: function (type, keyCode, keyMap) {
            if (type == "keydown") {
                if (keyMap == KEY_ACTION.SEND_CHAT && getElem("my_text").value != "" && !Mods.Chatmd.blockCommand) {
                    //Mods.consoleLog("down " + Mods.Chatmd.blockCommand);
                    !isTouchDevice() && (Mods.Chatmd.blockCommand = true);
                    Timers.set("unblockCommand", function () {
                        Mods.Chatmd.blockCommand = false;
                    }, 1000);
                    Mods.Chatmd.chatCommands(getElem("my_text").value);
                }
            }
            if (type == "keyup") {
                Mods.Chatmd.ModCh.listener(keyCode)
                if (keyMap == KEY_ACTION.SEND_CHAT && !isTouchDevice()) {
                    //Mods.consoleLog("up " + Mods.Chatmd.blockCommand);
                    Mods.Chatmd.blockCommand = false;
                }
            }
        }
    }

    Mods.Chatmd.newDrawObject = function (a, b) {
        var target;
        if (b.b_t == BASE_TYPE.PLAYER) {
            target = npc_base[102];
            var t = Math.random();
            Mods.Chatmd.mooDelay[b.name] = Mods.Chatmd.mooDelay[b.name] || 1;
            var time = timestamp() - Mods.Chatmd.mooDelay[b.name];
            if (time > 1000) {
                Mods.Chatmd.mooDelay[b.name] = timestamp();
                t > 0.7 && Mods.Chatmd.m00(b);
            }
        }
        else { target = b };
        Mods.Chatmd.oldDrawObject(a, target);
    }; updateBase();

    Mods.Chatmd.m00 = function (b) {
        var m = ["m00", "m00!", "mU", "m00", "m00", "m00", "m00", "m00!", "moo", "MOO", "moo", "mOO", "M00"];
        var r = Math.max(1, Math.ceil(Math.random() * m.length)) - 1;
        var f = getElem("enemy_hit").cloneNode(true);
        var g = translateTileToCoordinates(b.i, b.j);
        f.id = "moo_" + b.id + (new Date).getTime();
        removeClass(f, "hidden");
        f.innerHTML = "<div id='enemy_burst' style='display: block; position: relative; background: #000000; text-align: center; width: 35px; height: 35px; -webkit-transform: rotate(20deg); -moz-transform: rotate(20deg); -ms-transform: rotate(20deg); -o-transform: rotate(20deg);'></div><div id='enemy_damage' class='damage' style='font-size: 16px; top: 4px;'></div>";
        f.childNodes[0].innerHTML = "<div style='position: absolute; background: #000000; top: 0; left: 0; height: 35px; width: 35px; -webkit-transform: rotate(135deg); -moz-transform: rotate(135deg); -ms-transform: rotate(135deg); -o-transform: rotate(135deg)'></div>";
        f.childNodes[1].innerHTML = m[r];
        f.style.background = "#000000";
        f.style.width = "35px";
        f.style.height = "35px";
        //f.childNodes[1].style.width = "33px";
        //f.childNodes[1].style.height = "33px";
        wrapper.appendChild(f);
        f.style.left = (g.x + 16 + players[0].mx) * current_ratio_x + "px";
        f.style.top = (g.y - 40 + players[0].my - 20) * current_ratio_y + "px";
        addClass(f, "opacity_100");
        setTimeout(function () {
            decreaseOpacity(f, 150, 10);
        }, 150)
    };

    drawObject = function (a, b) {
        Mods.Chatmd.oldDrawObject(a, b)
    }; Draw.clear(ctx.players_show); updateBase();

    Mods.Chatmd.afkReply = function (user, text) {
        var holder = Mods.Chatmd.afkHolder = Mods.Chatmd.afkHolder || {};
        var message = Mods.Chatmd.afkMessage;
        if (message != "" && user != players[0].name && (holder[user] == undefined || timestamp() - holder[user] > 300000) && !/^@mods ?/.test(text)) {
            holder[user] = timestamp();
            Socket.send("message", {
                data: "/w \"" + user + "\" " + message
            })
        }
    }

    Mods.Chatmd.wiki = function (content, input) {
        if (!(typeof input === "object" && typeof content === "object" && input[0] === "wiki")) return;
        var q = { "item": "item", "mob": "monster", "npc": "vendor", "craft": "craft", "pet": "pet", "spell": "spell", "enchant": "enchant" },
            div = function (t1, t2, t3) {
                var t2 = typeof t2 == "string" ? "_" + t2 : "";
                var t3 = typeof t2 == "string" && typeof t3 == "string" ? "_" + t3 : "";
                return getElem("mods_wiki_" + t1 + t2 + t3)
            },
            load = Mods.Wikimd.loadWikiType;
        if (input[1]) {
            div("type").value = q[input[1]] || input[1];
            load(0, "type");
            if (input[2] && content[input[1]] && content[input[1]][input[2]] != undefined) {
                div("type", q[input[1]] || input[1]).value = input[2];
                load(1, input[1]);
                if (input.text && content[input[1]][input[2]] === "text") div("name").value = typeof input.text === "string" ? input.text : null;
                if (input.value && content[input[1]][input[2]] === "value") {
                    div("type", q[input[1]] || input[1], input[2]).value = typeof input.value === "string" ? input.value : -1;
                    load(2, input[2]);
                }
                if (input.range && (content[input[1]][input[2]] === "range" || content[input[1]][input[2]] === "value")) {
                    div("range").value = input.range;
                    div("level", "low").value = input.min > -1 ? input.min : null;
                    div("level", "high").value = input.max > -1 ? input.max : null;
                }
            }
        }
        removeClass(getElem("mods_form"), "hidden");
        Mods.Wikimd.populateWiki(true);
        Mods.loadModMenu_wiki && Mods.loadModMenu_wiki();
    }

    Mods.Chatmd.chat = {
        findcom: function (message, target) { //find command
            if (typeof Mods.Newmap === 'undefined' || typeof Mods.Newmap.POI === 'undefined' || typeof Mods.Newmap.POIfind === 'undefined')
                return ("Enhanced Map mod not loaded! You need to load it first before using the /find command.");

            Mods.Newmap.POIfind = [];
            HUD.drawMinimap();
            var target = target.toLowerCase().trim();
            if (target === "") return "Use /find [monster] or [material] to get the map or coordinates of what you're looking for.";
            var string = "", string2 = "";
            var foundCurrentMap = false;
            var foundOtherMap = false;
            var mapName = {0:"Dorpat", 1:"Dungeon", 2:"Narwa", 3:"Whiland", 4:"Reval", 5:"Rakblood", 6:"Blood River", 7:"Hell", 8:"Clouds", 9:"Heaven", 10:"Cesis", 11:"Walco", 13:"Pernau", 14:"Fellin Island", 15:"Dragon's Lair", 16:"No Man's Land", 17:"Ancient Dungeon", 18: "Lost Woods"};
            var foundIn = {};
            for (var i in mapName) foundIn[i] = false;
            var disamb = {"fir log": "fir", "fir tree": "fir", "fir wood": "fir", "oak log": "oak", "oak tree": "oak", "oak wood": "oak", "willow log": "willow", "willow tree": "willow", "willow wood": "willow", "maple log": "maple", "maple tree": "maple", "maple wood": "maple", "spirit wood": "spirit log", "spirit tree": "spirit log", "blue palm log": "blue palm", "blue palm tree": "blue palm", "blue palm wood": "blue palm", "magic oak log": "magic oak", "magic oak tree": "magic oak", "magic oak wood": "magic oak", "iron ore": "iron", "iron chunk": "iron", "silver ore": "silver", "silver chunk": "silver", "gold ore": "gold", "gold chunk": "gold", "white gold ore": "white gold", "white gold chunk": "white gold", "azure ore": "azure", "azure chunk": "azure", "azurite": "azure", "azurite ore": "azure", "azurite chunk": "azure", "platinum ore": "platinum", "platinum chunk": "platinum", "fire stone ore": "fire stone", "fire stone chunk": "fire stone", "firestone": "fire stone", "firestone ore": "fire stone", "firestone chunk": "fire stone", "gandalf": "gandalf the grey", "overlord": "orc overlord", "phoenix": "flame phoenix", "vortex": "chaos vortex", "dorpat": "transfer to dorpat", "dungeon": "dorpat mine", "reval": "transfer to reval", "cesis": "transfer to cesis", "ancient dungeon": "transfer to ancient dungeon", "pernau": "transfer to pernau", "whiland": "transfer to whiland", "clouds": "transfer to clouds", "heaven": "transfer to heaven", "lost woods": "transfer to lost woods", "forest maze": "transfer to lost woods", "rakblood": "transfer to rakblood", "no man's land": "transfer to no man's land", "pvp": "transfer to no man's land", "narwa": "transfer to narwa", "blood river": "transfer to blood river", "hell": "transfer to hell", "fellin island": "transfer to fellin island", "dragon's lair": "transfer to dragon's lair"};
            if (target in disamb) target = disamb[target];

            //checks if target is in Mods.Newmap.POI[0] database
            for (var i in Mods.Newmap.POI[0]) {
                if (typeof Mods.Newmap.POI[0][i].name !== 'undefined' && Mods.Newmap.POI[0][i].name.toLowerCase() === target) {
                    if (current_map == Mods.Newmap.POI[0][i].mapid) {
                        if (foundCurrentMap) string = string + ", ";
                        foundCurrentMap = true;
                        string = string + "(" + Mods.Newmap.POI[0][i].x + ", " + Mods.Newmap.POI[0][i].y + ")";
                        Mods.Newmap.POIfindMap = current_map;
                        Mods.Newmap.POIfind.push({color: "#FF0000", i: Mods.Newmap.POI[0][i].x, j: Mods.Newmap.POI[0][i].y});
                    }
                    else {
                        if (!foundIn[ Mods.Newmap.POI[0][i].mapid ]) {
                            if (foundOtherMap) string2 = string2 + ", ";
                            foundOtherMap = true;
                            foundIn[ Mods.Newmap.POI[0][i].mapid ] = true;
                            string2 = string2 + mapName[ Mods.Newmap.POI[0][i].mapid ];
                        }
                    }
                }
            }
            target = target.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
            if (foundCurrentMap) {
                string = "Found " + target + " in " + mapName[current_map] + " at coordinates: " + string + ". ";
                HUD.drawMinimap();
            }
            if (foundOtherMap) {
                if (foundCurrentMap)
                    string = string + "Found in: " + string2 + ".";
                else
                    string = string + "Found " + target + " in: " + string2 + ".";
            }
            if (foundCurrentMap || foundOtherMap)
                return (string);

            string = target + " not found.";
            return (string);
        },
        dailylogin: function (message, target) {
            var days = players[0].temp.consecutive_logins;
            return ("Daily rewards counter: " + days + " day" + sOrNoS(days) + ".");
        },
        online: function (message, target) {
            var target = target.toLowerCase();
            var targets = "";
            for (var player in online_players) if (player.indexOf(target) != -1) targets += player + ", ";
            if (targets.length > 0) return ("Online players (" + target + "): " + targets.slice(0, -2));
            else return ("No players online matching " + target);
        },
        tip: function (a) {
            var reply = "TIP: " + Mods.Chatmd.game_tips[0][Math.floor(Math.random() * Mods.Chatmd.game_tips[0].length)];
            return reply;
        },
        played: function (message, target) {
            if (target != "") return "";
            var time = timestamp() - players[0].temp.created_at;
            var reply = "Time since you started playing: " + Mods.timeConvert(time/1000) + ".";
            return reply;
        },
        friend: function (message, target) {
            //getTarget(friend);
            var add = true;
            var toadd = false;
            var notCommand = true;
            for (var a in Contacts.friends) if (Contacts.friends[a].name == target) { add = false; break };
            add && Contacts.add_friend(target);
            !add && Contacts.remove_friend(target);
            toadd = add && "added to " || "removed from ";
            reply = target + " has been " + toadd + "your friend's list.";
            if (toadd == "removed from ") return false;
            return reply;
        },
        ignore: function (message, target) {
            //getTarget(ignore);
            var add = true;
            var toadd = false;
            var notCommand = true;
            for (var a in Contacts.ignores) if (Contacts.ignores[a] == target) { add = false; break };
            add && Contacts.ignore_player(target);
            !add && Contacts.remove_ignore(target);
            toadd = add && "added to " || "removed from ";
            reply = target + " has been " + toadd + "your ignore list.";
            if (toadd == "removed from " || Player.is_admin(target) || Player.is_mod(target)) return false;
            return reply;
        },
        join: function (message, target) {
            //getTarget(join);
            var add = true;
            var toadd = false;
            var notCommand = true;
            Contacts.channels[target] && (add = false);
            var exists = false;
            for (var a in channel_names) if (channel_names[a] == target) { exists = true; break };
            add && exists && Contacts.add_channel(target);
            toadd = add && exists && " have added channel " || !add && exists && " are already in channel " || " cannot join channel ";
            reply = "You" + toadd + target + ".";
            return reply;
        },
        leave: function (message, target) {
            //getTarget(join);
            var add = true;
            var toadd = false;
            var notCommand = true;
            !Contacts.channels[target] && (add = false) && (toadd = " are not in channel ");
            var exists = false;
            for (var a in channel_names) if (channel_names[a] == target) { exists = true; break };
            add && exists && Contacts.remove_channel(target);
            toadd = add && exists && " have left channel " || !toadd && " are not in channel " || toadd;
            reply = "You" + toadd + target + ".";
            return reply;
        },
        whisp: function (message) {
            var whispNames = Mods.Chatmd.whispNames;
            if (whispNames.length > 0) {
                message = message.slice(2);
                message = "/w \"" + whispNames[0] + "\"" + message;
                getElem("my_text").value = message;
                !hasClass(getElem("mod_text"), "hidden") && (getElem("mod_text").value = message);
                return false;
            } else reply = "You have no target to whisper.";
            return reply;
        },
        ping: function (message) {
            Mods.Chatmd.ping = timestamp();
            Socket.send("message", {
                data: "/w \"" + players[0].name + "\" #ping;"
            });
            return false;
        },
        mods: function (message) {
            removeClass(getElem("mods_form"), "hidden");
            return false;
        },
        wiki: function (message, target) {
            target = target.trim().toLowerCase();
            var input, content, trans, revrs, search, values, ranges, srch, vals, rnge;
            input = {};
            input[0] = "wiki";
            content = {
                "item": {"all": "range", "skill": "value", "type": "value", "name": "text"},
                "mob": {"all": "range", "name": "text", "item": "text"},
                "npc": {"all": "range", "name": "text", "item": "text"},
                "craft": {"all": "range", "skill": "value", "source": "value", "item": "text"},
                "pet": {"all": "range", "name": "text", "family": "text"},
                "spell": {"all": "range", "name": "text"},
                "enchant": {"all": "range", "item": "text"}
            }
            trans = {mob: "monster", npc: "vendor"}
            revrs = {monster: "mob", vendor: "npc"}
            search = /^(item|mob|monster|npc|vendor|craft|pet|spell|enchant) ?(skill|type|name|item|family|all|source)?( (.*))?/gi;
            values = /^([^ ]{1,})( (.*))?/gi;
            ranges = /^([^ ]{1,}) (\(|min ?|from ?|)?=?(\d{1,})? ?(, ?|to ?|max ?)?=?(\d{1,})?/gi;
            srch = search.exec(target);
            if (!srch) {
                Mods.Chatmd.wiki(content, input);
                return false
            }
            input[1] = revrs[srch[1]] || srch[1];
            input[2] = revrs[srch[2]] || srch[2];
            function srch_txt (val) {
                if (!val) return;
                input.text = val;
            }
            function srch_rng (val) {
                if (!val) return;
                rnge = ranges.exec(val);
                if (!rnge) return;
                input.range = rnge[1];
                input.min = rnge[3];
                input.max = rnge[5];
            }
            function srch_val (val) {
                if (!val) return;
                vals = values.exec(val);
                if (!vals) return;
                input.value = vals[1];
                srch_rng(vals[3]);
            }
            if (content[input[1]][input[2]] == "text") srch_txt(srch[4]);
            if (content[input[1]][input[2]] == "range") srch_rng(srch[4]);
            if (content[input[1]][input[2]] == "value") srch_val(srch[4]);
            Mods.Chatmd.wiki(content, input);
            return false
        },
        moo: function (message, target) {
            for (var a = 1; a < 120; a++) Timers.clear("m00 chat" + a);
            //getTarget(moo);
            target = parseInt(target);
            target = typeof target == "number" && target > 0 ? Math.min(120, Math.max(Math.ceil(target), 1)) : 10;
            reply = "m000000000000000000";
            drawObject = function (a, b) {
                Mods.Chatmd.newDrawObject(a, b)
            };
            drawMap(false, true, false);
            updateBase();
            HUD.drawMinimap();
            Timers.set("m00", function () {
                drawObject = function (a, b) {
                    Mods.Chatmd.oldDrawObject(a, b)
                };
                drawMap(false, true, false);
                updateBase();
                //HUD.drawMinimap()
            }, target * 1000);
            for (var time = 1; time < (target * 10) ; time++) Timers.set("m00 chat" + time, function () {
                drawMap(false, true, false);
                updateBase();
                //HUD.drawMinimap()
            }, time * 100)
        },
        timer: function (message, target) {
            //getTarget(timer);
            var type = /set/.test(message) && "set" || /start/.test(message) && "start" || /clear/.test(message) && "clear" || false;
            var name = type == "set" && /.{1,}set/.exec(message)[0].replace(" set", "") || type == "start" && /.{1,}start/.exec(message)[0].replace(" start", "") || type == "clear" && /.{1,}clear/.exec(message)[0].replace(" clear", "") || false;
            name = typeof name == "string" && name.length > 7 && name.slice(7, 100) || "default";
            var start_name = name && " (" + name.toLowerCase() + ") " || "";
            var end_name = name && " (" + name.toLowerCase() + ") " || "";
            target = type == "set" && /(?=set).{1,}/.exec(message)[0].replace("set ", "") || false;
            if (type == "set") {
                var total = 0;
                var remove;
                var temp;
                var handle = { "hrs": { 0: false, 1: ["hours", "hour", "hrs", "hr", "h"], 2: 3600 }, "min": { 0: false, 1: ["minutes", "minute", "mins", "min", "m"], 2: 60 }, "sec": { 0: false, 1: ["seconds", "second", "secs", "sec", "s"], 2: 1 } };
                for (var a in handle) {
                    var h = handle[a];
                    var t = false;
                    var found = found || false;
                    for (var b in h[1]) if (RegExp(h[1][b]).test(target)) {
                        t = h[1][b];
                        h[0] = RegExp(".{1,}" + t).exec(target);
                        h[0] = h[0][0];
                        target = target.replace(h[0], "");
                        h[0] = h[0].replace(t, "");
                        h[0] = parseFloat(h[0]);
                        h[0] = h[0] > 0 && h[0] * h[2] || 0;
                        total += h[0];
                        found = true;
                    };
                };
                if (!found) target = parseInt(target) || false;
                else target = Math.ceil(total);
            };
            if (type == "set" && typeof target == "number" && target > 0) {
                Mods.Chatmd.runTimer.set[name.toLowerCase()] = [timestamp(), target * 1000];
                Timers.set("set_timer" + name.toLowerCase(), function (a) {
                    addChatText("Countdown " + end_name + "has expired. It has been " + target + " seconds.", void 0, COLOR.TEAL);
                    delete Mods.Chatmd.runTimer.set[name.toLowerCase()];
                }, target * 1000);
                reply = "Countdown " + start_name + "started for " + Mods.timeConvert(target) + ".";
            } else if (type == "start") {
                Mods.Chatmd.runTimer.start[name] = [timestamp()];
                reply = "Timer " + start_name + "started.";
            } else if (type == "clear") {
                delete Mods.Chatmd.runTimer.start[name];
                delete Mods.Chatmd.runTimer.set[name];
                Timers.clear("set_timer" + name.toLowerCase());
                reply = "Timer/Countdown " + start_name + "has been deleted.";
            } else {
                //addChatText("Timer's running:", void 0, COLOR.ORANGE);
                var runTimer = Mods.Chatmd.runTimer;
                var hasTimer = false;
                var hasTimers = false;
                for (var q = 0; q < 2; q++) {
                    var initial, type, ending;
                    if (q == 0) {
                        type = "start";
                        initial = "Timers elapsed:";
                        ending = "."
                    } else {
                        type = "set";
                        initial = "Countdowns running:";
                        ending = " left."
                    };
                    addChatText(initial, void 0, COLOR.TEAL);
                    for (var a in runTimer[type]) {
                        hasTimer = true;
                        hasTimers = true;
                        var time = runTimer[type][a][0] > 0 && timestamp() - runTimer[type][a][0] - (runTimer[type][a][1] || 0) || false;
                        runTimer[type][a][1] > 0 && (time = -1 * time);
                        if (time > 0) time = "- Timer (" + a + ") running: " + Mods.timeConvert(time / 1000) + ending;
                        else time = "- Timer (" + a + ") has already elapsed.";
                        addChatText(time, void 0, COLOR.TEAL);
                    };
                    !hasTimer && addChatText("- No timers running...", void 0, COLOR.TEAL);
                    hasTimer = false;
                }
                !hasTimers && (reply = "No timers have been started. Try /timer name start (to start a timer) or /timer name set # (to start a countdown).") || (reply = false);
            };
            localStorage.timer = JSON.stringify(Mods.Chatmd.runTimer);
            return reply;
        },
        modch: function (message, target) {
            Mods.Chatmd.ModCh.sendWhisper(target);
        },
        ttlxp: function (message, target) {
            var target = target.trim();
            var values = /^(\d{1,}) ?(\-|to)? ?(\d{1,})?$/.exec(target);
            if (!values) {
                values = 0;
                for (var a in skills[0]) if (skills[0][a].xp) values += Math.floor(skills[0][a].xp);
                return ("Your total experience for all skills is: " + thousandSeperate(Math.round(values)));
            }
            var val1 = values[1];
            var val2 = values[3] || 1;
            var vals = Math.min(val1, val2) + " to " + Math.max(val1, val2);
            val1 = Level.xp_for_level(val1);
            val2 = Level.xp_for_level(val2);
            var value = Math.max(val1, val2) - Math.min(val1, val2);
            value = thousandSeperate(value);
            var reply = "Total exp needed to go from level " + vals + ": " + value;
            return reply;
        },
        id: function (message, target) {
            var target, content, sep, bse, prm, t, i, j, k, p, base, params, type, value, string, substr, amount, name, data, count, tar;
            substr = [], data = {}, name = "";
            bse = /^(item_base|objects_data|object_base|ground_base|npc_base|players|Magic|quests|pets|IMAGE_SHEET|skills|FORGE_FORMULAS|CARPENTRY_FORMULAS|sprite|countries)? ?(.*)/;
            sep = /^(([^!=><&]{1,})?(!|!=|<=|>=|=|<|>))? ?([^!=><&]{1,})/g;
            prm = /([^.]{1,})/g;
            target = bse.exec(target);
            base = target[1] != undefined && window[target[1]] != undefined ? window[target[1]] : window.item_base;
            bse = target[1] || "item_base";
            target = target[2].replace(/\+/g, "\\+").replace(/\-/g, "\\-").replace(/\'/g, "\\'").split(/ ?& ?/g);
            if (!target || !base) return "No items match the given values. (error: no parameters)";
            for (i in base) data[i] = base[i];
            for (i in target) {
                sep.lastIndex = 0;
                content = sep.exec(target[i].trim());
                if (content && content[4]) {
                    params = content[2] ? content[2].match(prm) : ["name"];
                    type = content[3] && content[3] != "=" && content[3] != "!" && content[3] != "!=" && content[4] > -1 ? content[3] : content[3] == "!" || content[3] == "!=" ? "!=" : "=";
                    value = content[4];
                    str = type == "!=" ? "!" + value : type == "=" ? value.replace(/\\/g, "") : type == ">=" ? value + "+" : type == "<=" ? value + "-" : type == ">" ? (parseInt(value) + 1) + "+" : (parseInt(value) - 1) + "-";
                    substr.push(params.toString().replace(/,/g, ".") + "=" + str);
                    for (j in data) {
                        p = data[j];
                        for (k in params) if (typeof p == "object") p = p[params[k]];
                        if (p != undefined && typeof p !== "object" && p !== null && (value > -1 && p > -1) && (type == "=" && p == value || type == ">" && p > value || type == "<" && p < value || type == ">=" && p >= value || type == "<=" && p <= value || type == "!=" && p != value) || !(value > -1 && p > -1) && (type !== "!=" && RegExp(value, "gi").test(p) || type === "!=" && RegExp(value, "gi").test(p) == false));
                        else delete data[j];
                    }
                }
            }
            substr = substr.toString().replace(/ ,/g, "; ");
            substr = (substr.length > 0 ? "." : "") + substr + ": ";
            count = 0;
            for (i in data) {
                count += 1;
                if (count > 20) {
                    name += "(too many results), ";
                    break
                } else name += data[i].name + " (" + i + "), ";
            }
            if (name.length > 0) return (bse + substr + name.slice(0, -2));
            else return (bse + substr + "No items match the given values.");
        },
        help: function (message, target) {
            var target = target.trim() || "";
            var commands = {
                afk: "Use /afk or /afk [message] to set an automatic reply to people who whisper you if you are away from the keyboard. While your status is \"AFK\" if you type /afk again, the automatic replies will be disabled.",
                combats: "Use /combats to see everyones combat level.",
                cathedral: "Use /cathedral to see how much time is left before can start another cathedral run.",
                daily: "Use /daily to see the number of consecutive days you have on your daily rewards counter.",
                find: "Use /find [monster] or [material] to get the map or coordinates of what you're looking for.",
                friend: "Use /friend [player] to quickly add or remove a player to/from your friends list. Example: /friend dendrek",
                help: "Use /help to see a list of the mod chat commands. To read a description of a command, type /help [command].",
                id: "[For Debuggin] Use /id [object] [params=] [value] to lookup information in the game's database. This command is mostly for debugging purposes and will not be useful to most players.",
                ignore: "Use /ignore [player] to quickly add or remove a player to/from your ignore list. Example: /ignore dumbplayer",
                join: "Use /join [CH] (where CH is a valid channel name, written with capital letters, such a EN, DE, 18, etc) to join that channel. Example: /join EN",
                leave: "Use /leave [CH] (where CH is a valid channel name, written with capital letters, such as EN, DE, 18, etc) to leave taht channel. Example: /leave EN",
                maintenance: "Use /maintenance to see how much time is left till the next maintenance restart.",
                m00: "Use /m00 to see ... something happen. You can extend the duration of this command with /m00 # (such as /m00 30 to set the duration to 30 seconds).",
                obj: "[For Debugging] Use /obj [object] [id] [params] to list the params of the object[id]. This command is mostly for debugging purposes and may not be useful to most players.",
                online: "Use /online to see a list of players who are currently online.",
                o: "Use /o [player] to check if that player is online. It's a fast way to confirm a players online-status without scanning the /online list. Example: /o dendrek",
                penalty: "Use /penalty to see how many captcha points you have stored. You can save up to 5 points. If you reach -5 points, you'll go to jail.",
                ping: "Use /ping to see how much of a delay (called latency) there is between you and your computer. Every 1000ms equals a 1 second delay.",
                played: "Use /played to see how long it has been since you created your current character. This is a measure of time since you started, not of actual time played.",
                r: "Use /r to reply to the last person who whispered you. If a player has whispered you recently enough, /r will immediately change to /w \"playername\". Additionally, use PageUp and PageDown to cycle through previous whisper targets.",
                savemap: "Use /savemap to save current map into a .PNG file. Caution, might take a while to generate! Not to be used with a mobile device.",
                saveplayer: "Use /saveplayer to save current player into a .PNG file.",
                world: "Use /world to see which world you are in currently. Use /world x to connecto another world, change x into any available world number.",
                timer: {
                    desc: "Use /timer to check already created timers. You can also create a countdown (using /timer set), a clock (using /timer start), remove timers (using /timer clear), and even give timers names (using /timer [name] [command]). Type /help timer [set/start/clear/name] for more details.",
                    set: "The command /timer set #[time type] starts a timer for \"#\" seconds. If time type is specified (examples: seconds/secs/sec/s, /minutes/mins/min/m, /hours/hrs/hr/h) the # will be in that time interval. Example: /timer set 30m starts a timer for 30minutes. /timer set 1h 20m 15s can also be done.",
                    start: "The command /timer start starts a \"clock\" from the current moment that counts up. You can check how much time has passed by typing /timer at any time.",
                    clear: "The command /timer [name] clear cancels all existing timers that have the specified \"name\" value. If name is not included, /timer clear cancels all existing \"default\" timers. See /help timer name for more details on naming timers.",
                    name: "Timers can be named in this way: /timer [name] [command]. The commands are set, start, and clear. The name can be practically anything, but it CANNOT contain the words set, start or clear. Examples: /timer orc overlord set 20m (starts a countdown named 'orc overlord'), /timer see how long this takes start (starts a clock named 'see how long this takes')."
                },
                totalexp: "Use /totalexp to see the total experience you've gathered. Or use /totalexp # to see how much exp would be required to go from level 1 to the level specified. Or use /totalexp # to # to see how much exp is required to go from the lower level to the higher. Example: /totalexp 90 to 95",
                totalvalue: "Use /totalvalue to see a total wiki value of all items in your inventory, chest, pet, as well as your coins.",
                xp: "Use /xp to see if a 2x experience event is currently running, and to see the duration if one is.",
                wiki: {
                    desc: "Use /wiki to open up the in-game wiki. You can also perform a search using this command. /wiki [option1] [option2] [option3] etc, where each option \"fills\" in one of the search boxes in the wiki. For more details, type /help wiki options.",
                    options: "The wiki has dropdown boxes that must be filled in. To do a wiki search using the /wiki command, you must \"fill in\" each box with an appropriate value. Examples of wiki searches: /wiki item name bronze pants, /wiki mob item superior armor enchant, /wiki npc name magician, /wiki craft item iron bar. The different parts of the search must be included for it to work."
                }
            };
            var cmds = " List of commands: ";
            var cmd1 = "(";
            var cmd2 = "(";
            for (var i in commands) {
                cmd1 += i + "|";
                cmds += i + ", ";
                for (var j in commands[i]) if (j != "desc" && !(j >= 0) && cmd2.indexOf(j) == -1) cmd2 += j + "|";
            }
            cmds = cmds.slice(0, -2) + ".";
            cmd1 = cmd1.slice(0, -1) + ")";
            cmd2 = cmd2.slice(0, -1) + ")";
            //Mods.consoleLog(cmd1);
            //Mods.consoleLog(RegExp(cmd1, "g").exec(target));
            //Mods.consoleLog(cmd2);
            //Mods.consoleLog(RegExp(cmd2, "g").exec(target));
            commands.help += cmds;
            var targets = RegExp(cmd1 + " ?" + cmd2 + "?", "g").exec(target);
            //Mods.consoleLog(targets);
            var reply = "";
            if (targets && commands[targets[1]] && commands[targets[1]][targets[2]]) reply = commands[targets[1]][targets[2]];
            else if (targets && commands[targets[1]]) reply = commands[targets[1]].desc || commands[targets[1]];
            else reply = commands.help;
            return reply;
        },
        obj: function (message, target) {
            var values, content, base, param, value, params, sep, tst, string, b, p, a, i;
            values = "";
            content = /(item_base|objects_data|object_base|ground_base|npc_base|players|Magic|quests|pets|IMAGE_SHEET|skills|FORGE_FORMULAS|CARPENTRY_FORMULAS|sprite|countries)? ?\[?(\d{1,})\]? ?(.*)/g.exec(target);
            sep = /([^.]{1,})/g;
            if (!content) return "No items match the given value. (error: no parameters)";
            base = content[1] || "item_base";
            params = content[3] ? content[3].match(sep) : [];
            value = content[2];
            if (window[base] == undefined || typeof value == "undefined") return "No items match the given values. (error: base = undefined)";
            string = params.toString().replace(/,/g, ".");
            values = base + "[" + value + "]" + (string.length > 0 ? "." + string : "") + ": ";
            b = window[base][value];
            for (i in params) if (typeof b != "undefined") b = b[params[i]];
            if (typeof b === "undefined") return "No items match the given values."
            else if (typeof b !== "object" && b !== null) values += b + ", ";
            else for (i in b) {
                if (typeof b[i] == "object") values += i + " (object), ";
                else values += i + " (" + b[i] + "), ";
            }
            return values.slice(0, -2);
        },
        afk: function (message, target) {
            var reply = "";
            var value = "";
            message = message.replace(/^\/afk ?/g, "").trim();
            value = "[AFK]: " + (message.length > 0 ? message : "I am away from my keyboard.");
            if (Mods.Chatmd.afkMessage == "" || Mods.Chatmd.afkMessage != value && message != "") {
                Mods.Chatmd.afkMessage = value;
                Mods.Chatmd.afkHolder = {};
                reply = "You are now [AFK] : \"" + target + "\"";
            } else {
                Mods.Chatmd.afkMessage = "";
                reply = "You are no longer [AFK]";
            }
            Player.update_healthbar();
            return reply;
        },
        ttval: function (message, target) {
            var value, a;
            value = 0;
            for (a in players[0].temp.inventory) value += item_base[players[0].temp.inventory[a].id].params.price;
            for (a in players[0].pet.chest) value += item_base[players[0].pet.chest[a]].params.price;
            for (a in chests[0]) value += chests[0][a].count * item_base[chests[0][a].id].params.price;
            value += players[0].temp.coins;
            value = thousandSeperate(value);
            return ("The total value of items in your Chest/Inv/Pet + Coins is: " + value);
        },
        tele: function (message, target) {
            var x, y, tar;
            tar = target.split(/ /g) || [target];
            x = players[0].i + (parseInt(tar[0]) || 0);
            y = players[0].j + (parseInt(tar[1]) || 0);
            Socket.send("message", {
                data: "/level " + players[0].map + " " + x + " " + y
            })
        }
    };

    Mods.Chatmd.chatCommands = function (message) {
        var m = {
            online: "/o ", played: "/played", friend: "/friend ", ignore: "/ignore ", join: "/join ", leave: "/leave ", whisp: "/r ", ping: "/ping", mods: "/mods", wiki: "/wiki", moo: "/m00",
            timer: "/timer", tip: "/tip", modch: "/@mods ", ttlxp: "/totalexp", id: "/id ", help: "/help", obj: "/obj ", afk: "/afk", ttval: "/totalvalue", tele: "/tele ", dailylogin: "/daily", findcom: "/find"
        };
        var type;
        var target;
        var reply;
        var add = true;
        var toadd = false;
        var notCommand = true;
        for (var a in m) if (RegExp("^" + m[a]).test(message)) {
            notCommand = false;
            type = a;
            break
        };
        if (notCommand) return message;
        function getTarget(inp_type) {
            target = message.replace(inp_type, "");
            target = target.replace("\"", "");
            target = target.replace("\"", "");
        };
        getTarget(m[type]);
        reply = Mods.Chatmd.chat[type](message, target);
        reply && addChatText(reply, void 0, COLOR.TEAL);
        return false;
    };

    //tips scheduling and option
    Mods.Chatmd.ScheduleTips = function () {
        //send tip every 10 minutes
        var tipsenabled = Mods.Chatmd.tipsenabled;
        if (tipsenabled == 0) Mods.Chatmd.chatCommands("/tip");
        setTimeout(function () {
            Mods.Chatmd.ScheduleTips();
        }, 600000);
    }

    Mods.Chatmd.Tipstoggle = function () {
        var s = getElem("settings_tips");
        var tipsenabled = Mods.Chatmd.tipsenabled;
        switch (tipsenabled) {
            case 0:
                s.innerHTML = "Tips (off)";
                Mods.Chatmd.tipsenabled = 1;
                break;
            default:
                s.innerHTML = "Tips (on)";
                Mods.Chatmd.tipsenabled = 0;
                break;
        }
        //save to localstorage
        localStorage["tipsenabled"] = JSON.stringify(Mods.Chatmd.tipsenabled);

    }

    //chat timestamp options
    Mods.Chatmd.Timestamptoggle = function () {
        var s = getElem("settings_enableallchatts");

        switch (Mods.Chatmd.enableallchatts) {
            case 0:
                s.innerHTML = "Timestamps on all chat lines (on)";
                Mods.Chatmd.enableallchatts = 1;
                break;
            default:
                s.innerHTML = "Timestamps on all chat lines (off)";
                Mods.Chatmd.enableallchatts = 0;
                break;
        }
        //save to localstorage
        localStorage["enableallchatts"] = JSON.stringify(Mods.Chatmd.enableallchatts);

    }

    temp();
    Mods.timestamp("chatmd");
}

Load.newmap = function () {
    modOptions.newmap.time = timestamp();

    Mods.Newmap.drawMinimapLarge = Mods.Newmap.drawMinimapLarge || HUD.drawMinimapLarge;

    Mods.Newmap.drawMinimap = Mods.Newmap.drawMinimap || HUD.drawMinimap;

    //create container for map coordinates
    createElem("div", wrapper, {
        id: "mods_newmap_coords",
        style: "visibility: hidden; z-index: 300; left: 35%; top: 20px; position: absolute; color #FFF; text-align: middle; font-size: 20px; text-shadow: #555 1px 1px 1px; pointer-events: none;"
    });

    //create container for map popup info
    createElem("div", wrapper, {
        id: "mods_newmap_popup",
        style: "visibility: hidden; z-index: 49; position: absolute; color: #FFF; border-radius: 4px; text-align: middle; font-size: 12px; background-color: #666; padding: 4px; pointer-events: none;"
    });

    //create button for showing zone map
    createElem("div", wrapper, {
        id: "mods_zone_buttondiv",
        style: "visibility: hidden; z-index: 49; position: absolute; top: 40px; left: 3px; font-size: 8px;",
        innerHTML: "<button id='mods_zone_button' class='market_select pointer' onclick='Mods.Newmap.ShowZone();'>World Map</button>"
    });
    var zonemapvisible = false;

    //current_map
    //Dorpat 0;Dungeon 1;Narwa 2;Whiland 3;Reval 4;Rakblood 5;Blood River 6;Hell 7;Clouds 8;Cesis 10;Walco 11;Pernau 13;Fellin Island 14;Dragon's Lair 15;Heaven 9;Ancient Dungeon 17; No Man's Land 16; Lost Woods 18;
    Mods.Newmap.POI = {0: [
        //DORPAT: 0
        { mapid: 0, name: "Dorpat Town", type: "CITY", x: 20, y: 20 },
        { mapid: 0, name: "Dorpat Outpost", type: "CITY", x: 83, y: 38 },
        { mapid: 0, name: "Fishing Net", description: "5 fishing", type: "RESOURCE", icon: "net", x: 32, y: 5 },
        { mapid: 0, name: "Fishing Rod", description: "1 fishing", type: "RESOURCE", icon: "fish", x: 16, y: 8 },
        { mapid: 0, name: "Fishing Rod", description: "1 fishing", type: "RESOURCE", icon: "fish", x: 91, y: 33 },
        { mapid: 0, name: "Wooden Harpoon", description: "50 fishing", type: "RESOURCE", icon: "woodharp", x: 95, y: 5 },
        { mapid: 0, name: "Steel Harpoon", description: "63 fishing", type: "RESOURCE", icon: "steelharp", x: 81, y: 90 },
        { mapid: 0, name: "Sand", description: "1 mining", type: "RESOURCE", icon: "spade", x: 73, y: 73 },
        { mapid: 0, name: "Silver", description: "25 mining", type: "RESOURCE", icon: "pick", x: 69, y: 79 },
        { mapid: 0, name: "Fir", description: "1 woodcutting", type: "RESOURCE", icon: "wood", x: 24, y: 27 },
        { mapid: 0, name: "Fir", description: "1 woodcutting", type: "RESOURCE", icon: "wood", x: 88, y: 32 },
        { mapid: 0, name: "Cactus", description: "5 woodcutting", type: "RESOURCE", icon: "wood", x: 3, y: 88 },
        { mapid: 0, name: "Willow", description: "20 woodcutting", type: "RESOURCE", icon: "wood", x: 87, y: 88 },
        { mapid: 0, name: "Oak", description: "10 woodcutting", type: "RESOURCE", icon: "wood", x: 67, y: 23 },
        { mapid: 0, name: "Minotaur Maze", description: "Access to Minotaur Cave dungeon", type: "POI", x: 21, y: 87 },
        { mapid: 0, name: "Dorpat Castle", description: "Access to Dorpat Castle dungeon", type: "POI", x: 50, y: 59 },
        { mapid: 0, name: "Miner's Guild", description: "Requires Mining guild permission and 65 mining.", type: "POI", x: 56, y: 14 },
        { mapid: 0, name: "Skeleton Dungeon", description: "", type: "POI", x: 66, y: 29 },
        { mapid: 0, name: "Transfer to Whiland", description: "Leads to Rakblood, No Man's Land (PvP)", type: "TRAVEL", x: 92, y: 15 },
        { mapid: 0, name: "Cow", type: "MOB", icon: 102, x: 41, y: 11 },
        { mapid: 0, name: "Moth", type: "MOB", icon: 280, x: 48, y: 17 },
        { mapid: 0, name: "Orc Warrior", type: "MOB", icon: 4, x: 75, y: 18 },
        { mapid: 0, name: "Thief", type: "MOB", icon: 185, x: 80, y: 9 },
        { mapid: 0, name: "Minotaur", type: "MOB", icon: 6, x: 18, y: 90 },
        { mapid: 0, name: "Apeman", type: "MOB", icon: 119, x: 50, y: 78 },
        { mapid: 0, name: "Dwarf Mage", type: "MOB", icon: 7, x: 63, y: 52 },
        { mapid: 0, name: "Gray Wizard", type: "MOB", icon: 0, x: 15, y: 50 },
        { mapid: 0, name: "Black Rat", type: "MOB", icon: 8, x: 80, y: 78 },
        { mapid: 0, name: "Dragonfly", type: "MOB", icon: 120, x: 90, y: 41 },
        { mapid: 0, name: "Orc Mage", type: "MOB", icon: 13, x: 83, y: 56 },
        { mapid: 0, name: "Explorer", type: "MOB", icon: 187, x: 45, y: 60 },
        { mapid: 0, name: "Paladin", type: "MOB", icon: 25, x: 50, y: 63 },
        { mapid: 0, name: "Ridder", type: "MOB", icon: 201, x: 43, y: 58 },
        { mapid: 0, name: "Bronze Golem", type: "MOB", icon: 60, x: 70, y: 69 },
        { mapid: 0, name: "Iron Golem", type: "MOB", icon: 62, x: 65, y: 75 },
        { mapid: 0, name: "Sand Golem", type: "MOB", icon: 162, x: 71, y: 72 },
        { mapid: 0, name: "White Rat", type: "MOB", icon: 1, x: 28, y: 33 },
        { mapid: 0, name: "Hen", type: "MOB", icon: 101, x: 9, y: 31 },
        { mapid: 0, name: "Green Wizard", type: "MOB", icon: 3, x: 48, y: 46 },
        { mapid: 0, name: "Chicken", type: "MOB", icon: 100, x: 16, y: 34 },
        { mapid: 0, name: "Dorpat Mine", description: "", type: "TRAVEL", x: 9, y: 23 },
        { mapid: 0, name: "Transfer to Walco", description: "", type: "TRAVEL", x: 94, y: 83 },
        { mapid: 0, name: "Transfer to Reval", description: "Leads to Cesis, Pernau", type: "TRAVEL", x: 6, y: 89 },
        { mapid: 0, name: "Transfer to Clouds", description: "Leads to Heaven. Requires wings.", type: "TRAVEL", x: 43, y: 92 },
        //DUNGEON: 1
        { mapid: 1, name: "Big Treasure Chest", description: "Use spare keys from search dungeons here.", type: "POI", x: 26, y: 8 },
        { mapid: 1, name: "Campfire", type: "POI", x: 90, y: 41 },
        { mapid: 1, name: "Clay", description: "0 mining", icon: "pick", type: "RESOURCE", x: 15, y: 27 },
        { mapid: 1, name: "Tin", description: "1 mining", icon: "pick", type: "RESOURCE", x: 6, y: 16 },
        { mapid: 1, name: "Tin", description: "1 mining", icon: "pick", type: "RESOURCE", x: 31, y: 18 },
        { mapid: 1, name: "Copper", description: "1 mining", icon: "pick", type: "RESOURCE", x: 4, y: 9 },
        { mapid: 1, name: "Copper", description: "1 mining", icon: "pick", type: "RESOURCE", x: 34, y: 18 },
        { mapid: 1, name: "Cage", description: "35 fishing", icon: "cage", type: "RESOURCE", x: 19, y: 25 },
        { mapid: 1, name: "Gold", description: "45 mining", icon: "pick", type: "RESOURCE", x: 30, y: 90 },
        { mapid: 1, name: "Iron", description: "25 mining", icon: "pick", type: "RESOURCE", x: 67, y: 14 },
        { mapid: 1, name: "Iron", description: "25 mining", icon: "pick", type: "RESOURCE", x: 34, y: 21 },
        { mapid: 1, name: "Coal", description: "40 mining", icon: "pick", type: "RESOURCE", x: 55, y: 70 },
        { mapid: 1, name: "Coal", description: "40 mining", icon: "pick", type: "RESOURCE", x: 33, y: 24 },
        { mapid: 1, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 9, y: 23 },
        { mapid: 1, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 66, y: 29 },
        { mapid: 1, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 40, y: 56 },
        { mapid: 1, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 22, y: 88 },
        { mapid: 1, name: "White Rat", type: "MOB", icon: 1, x: 14, y: 21 },
        { mapid: 1, name: "Moth", type: "MOB", icon: 280, x: 10, y: 27 },
        { mapid: 1, name: "Cave Bat", type: "MOB", icon: 196, x: 21, y: 33 },
        { mapid: 1, name: "Cave Worm", type: "MOB", icon: 197, x: 14, y: 52 },
        { mapid: 1, name: "Black Rat", type: "MOB", icon: 8, x: 11, y: 66 },
        { mapid: 1, name: "Sapphire Dragon", type: "MOB", icon: 14, x: 25, y: 84 },
        { mapid: 1, name: "Ghost Dragon", type: "MOB", icon: 23, x: 9, y: 85 },
        { mapid: 1, name: "Efreet", type: "MOB", icon: 22, x: 91, y: 79 },
        { mapid: 1, name: "Ruby Dragon", type: "MOB", icon: 27, x: 41, y: 87 },
        { mapid: 1, name: "Cursed Dragon", type: "MOB", icon: 26, x: 60, y: 90 },
        { mapid: 1, name: "Adult Ruby Dragon", type: "MOB", icon: 184, x: 86, y: 91 },
        { mapid: 1, name: "King Ruby Dragon", type: "MOB", icon: 24, x: 91, y: 87 },
        { mapid: 1, name: "Ridder", type: "MOB", icon: 201, x: 44, y: 56 },
        { mapid: 1, name: "Crusader", type: "MOB", icon: 200, x: 53, y: 59 },
        { mapid: 1, name: "Dark Knight", type: "MOB", icon: 29, x: 58, y: 64 },
        { mapid: 1, name: "Paladin", type: "MOB", icon: 25, x: 61, y: 61 },
        { mapid: 1, name: "Holy Warrior", type: "MOB", icon: 30, x: 55, y: 79 },
        { mapid: 1, name: "Scholar", type: "MOB", icon: 202, x: 62, y: 80 },
        { mapid: 1, name: "Enchanter", type: "MOB", icon: 204, x: 79, y: 67 },
        { mapid: 1, name: "Skeleton", type: "MOB", icon: 10, x: 74, y:29 },
        { mapid: 1, name: "Vampire", type: "MOB", icon: 11, x: 72, y:16 },
        { mapid: 1, name: "Ghost", type: "MOB", icon: 9, x: 71, y:44 },
        { mapid: 1, name: "Spirit", type: "MOB", icon: 135, x: 74, y:49 },
        { mapid: 1, name: "Energy Ghost", type: "MOB", icon: 137, x: 58, y:46 },
        { mapid: 1, name: "Baby Minotaur Skeleton", type: "MOB", icon: 68, x: 43, y:34 },
        { mapid: 1, name: "Skeleton Knight", type: "MOB", icon: 67, x: 32, y:56 },
        { mapid: 1, name: "Skeleton Lord", type: "MOB", icon: 176, x: 32, y:67 },
        { mapid: 1, name: "Vampire Lord", type: "MOB", icon: 28, x: 54, y:19 },
        { mapid: 1, name: "Hydra", type: "MOB", icon: 17, x: 75, y:17 },
        { mapid: 1, name: "Gnoll Warrior", type: "MOB", icon: 16, x: 65, y:10 },
        { mapid: 1, name: "Skeleton Mage", type: "MOB", icon: 177, x: 85, y:20 },
        { mapid: 1, name: "Gnoll Mage", type: "MOB", icon: 199, x: 89, y:35 },
        //NARWA: 2
        { mapid: 2, name: "Narwa Town", type: "CITY", x: 68, y: 37 },
        { mapid: 2, name: "Water Altar", type: "POI", x: 61, y: 75 },
        { mapid: 2, name: "Wooden Harpoon", description: "50 fishing", icon: "woodharp", type: "RESOURCE", x: 78, y: 30 },
        { mapid: 2, name: "Transfer to Rakblood", description: "Leads to Whiland", type: "TRAVEL", x: 19, y: 81 },
        { mapid: 2, name: "Transfer to Fellin Island", description: "Requires ticket. Leads to Dragon's Lair", type: "TRAVEL", x: 78, y: 38 },
        { mapid: 2, name: "Transfer to Blood River", description: "Leads to Hell. Requires wings.", type: "TRAVEL", x: 86, y: 81 },
        { mapid: 2, name: "Sailor", description: "(NPC) Shop", type: "POI", x: 74, y: 38 },
        { mapid: 2, name: "Coal", description: "40 mining", icon: "pick", type: "RESOURCE", x: 92, y: 11 },
        { mapid: 2, name: "Frozen Spirit", type: "MOB", icon: 57, x: 82, y:93 },
        { mapid: 2, name: "Frozen Spirit", type: "MOB", icon: 57, x: 72, y:78 },
        { mapid: 2, name: "Frozen Spirit", type: "MOB", icon: 57, x: 95, y:56 },
        { mapid: 2, name: "Ice Lizard", type: "MOB", icon: 55, x: 77, y:82 },
        { mapid: 2, name: "Ice Wyvern", type: "MOB", icon: 259, x: 91, y:80 },
        { mapid: 2, name: "Ice Giant", type: "MOB", icon: 54, x: 79, y:54 },
        { mapid: 2, name: "Frozen Bat", type: "MOB", icon: 53, x: 82, y:58 },
        { mapid: 2, name: "Frozen Bat", type: "MOB", icon: 53, x: 84, y:18 },
        { mapid: 2, name: "Ice Golem", type: "MOB", icon: 56, x: 90, y:49 },
        { mapid: 2, name: "Ice Troglodyte", type: "MOB", icon: 52, x: 85, y:34 },
        { mapid: 2, name: "Wind Protector", type: "MOB", icon: 58, x: 93, y:17 },
        { mapid: 2, name: "Snow Troll Defender", type: "MOB", icon: 71, x: 51, y:53 },
        { mapid: 2, name: "Snow Troll Knight", type: "MOB", icon: 69, x: 62, y:64 },
        { mapid: 2, name: "King Elemental Dragon", type: "MOB", icon: 76, x: 52, y:89 },
        { mapid: 2, name: "King Elemental Dragon", type: "MOB", icon: 76, x: 32, y:86 },
        { mapid: 2, name: "Wind Elemental", type: "MOB", icon: 51, x: 18, y:57 },
        { mapid: 2, name: "Wind Elemental", type: "MOB", icon: 51, x: 25, y:90 },
        { mapid: 2, name: "Wind Elemental", type: "MOB", icon: 51, x: 10, y:72 },
        { mapid: 2, name: "Wind Elemental", type: "MOB", icon: 51, x: 9, y:44 },
        { mapid: 2, name: "Wind Elemental", type: "MOB", icon: 51, x: 32, y:29 },
        { mapid: 2, name: "Adult Elemental Dragon", type: "MOB", icon: 74, x: 32, y:82 },
        { mapid: 2, name: "Baby Elemental Dragon", type: "MOB", icon: 74, x: 23, y:74 },
        { mapid: 2, name: "Snow Troll Assassin", type: "MOB", icon: 70, x: 18, y:47 },
        { mapid: 2, name: "Snow Gungan Priest", type: "MOB", icon: 72, x: 44, y:34 },
        { mapid: 2, name: "Snow Gungan Priest", type: "MOB", icon: 72, x: 66, y:25 },
        { mapid: 2, name: "Bear", type: "MOB", icon: 104, x: 8, y:18 },
        { mapid: 2, name: "Polar Bear", type: "MOB", icon: 189, x: 18, y:14 },
        { mapid: 2, name: "Snow Gungan Lord", type: "MOB", icon: 73, x: 48, y:16 },
        //WHILAND: 3
        { mapid: 3, name: "Whiland Town", type: "CITY", x: 28, y: 93 },
        { mapid: 3, name: "Earth Altar", type: "POI", x: 42, y: 39 },
        { mapid: 3, name: "Wandering Farmer", description: "(NPC) Shop", type: "POI", x: 10, y: 29 },
        { mapid: 3, name: "Transfer to Dorpat", type: "TRAVEL", x: 4, y: 5 },
        { mapid: 3, name: "Transfer to Rakblood", description: "Leads to Narwa", type: "TRAVEL", x: 71, y: 46 },
        { mapid: 3, name: "Transfer to No Man's Land", description: "PVP Area", type: "TRAVEL", x: 90, y: 12 },
        { mapid: 3, name: "Transfer to Lost Woods", type: "TRAVEL", x: 8, y: 90 },
        { mapid: 3, name: "Oak", description: "10 woodcutting", type: "RESOURCE", icon: "wood", x: 19, y: 85 },
        { mapid: 3, name: "Deer", type: "MOB", icon: 103, x: 10, y:13 },
        { mapid: 3, name: "Boletus", type: "MOB", icon: 34, x: 12, y:7 },
        { mapid: 3, name: "Bear", type: "MOB", icon: 104, x: 25, y:87 },
        { mapid: 3, name: "Silver Shroom", type: "MOB", icon: 35, x: 75, y:42 },
        { mapid: 3, name: "Blue Magic Shroom", type: "MOB", icon: 33, x: 37, y:81 },
        { mapid: 3, name: "Avatar's Shroom", type: "MOB", icon: 38, x: 42, y:87 },
        { mapid: 3, name: "Russula", type: "MOB", icon: 31, x: 39, y:77 },
        { mapid: 3, name: "Grizzly Bear", type: "MOB", icon: 188, x: 58, y:69 },
        { mapid: 3, name: "Golden Shroom", type: "MOB", icon: 36, x: 67, y:76 },
        { mapid: 3, name: "Lava Shroom", type: "MOB", icon: 41, x: 80, y:66 },
        { mapid: 3, name: "Dark Shroom", type: "MOB", icon: 32, x: 84, y:70 },
        { mapid: 3, name: "Fire Shroom", type: "MOB", icon: 40, x: 80, y:30 },
        { mapid: 3, name: "Dry-Rotted Shroom", type: "MOB", icon: 37, x: 55, y:60 },
        { mapid: 3, name: "Poisoned Shroom", type: "MOB", icon: 39, x: 75, y:31 },
        //REVAL: 4
        { mapid: 4, name: "Reval Town", type: "CITY", x: 16, y: 24 },
        { mapid: 4, name: "Orc Overlord", type: "BOSS", cblevel: 450, x: 71, y: 87 },
        { mapid: 4, name: "Snake Maze", type: "POI", x: 59, y: 43 },
        { mapid: 4, name: "Flash Altar", type: "POI", x: 62, y: 65 },
        { mapid: 4, name: "Jewelry Guild", description: "Requires Jewelry Guild permission and 60 jewelry.", type: "POI", x: 48, y: 56 },
        { mapid: 4, name: "Sand", description: "1 mining", icon: "spade", type: "RESOURCE", x: 8, y: 34 },
        { mapid: 4, name: "Gold", description: "45 mining", icon: "pick", type: "RESOURCE", x: 66, y: 38 },
        { mapid: 4, name: "Gold", description: "45 mining", icon: "pick", type: "RESOURCE", x: 45, y: 56 },
        { mapid: 4, name: "Silver", description: "25 mining", icon: "pick", type: "RESOURCE", x: 42, y: 55 },
        { mapid: 4, name: "Clay", description: "1 mining", icon: "pick", type: "RESOURCE", x: 46, y: 59 },
        { mapid: 4, name: "Coal", description: "40 mining", icon: "pick", type: "RESOURCE", x: 91, y: 67 },
        { mapid: 4, name: "Cactus", description: "5 woodcutting", type: "RESOURCE", icon: "wood", x: 10, y: 12 },
        { mapid: 4, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 93, y: 7 },
        { mapid: 4, name: "Transfer to Cesis", description: "Leads to Ancient Dungeon", type: "TRAVEL", x: 40, y: 91 },
        { mapid: 4, name: "Transfer to Pernau", description: "", type: "TRAVEL", x: 83, y: 87 },
        { mapid: 4, name: "Lion", type: "MOB", icon: 190, x: 56, y: 82 },
        { mapid: 4, name: "Chaos Vortex", type: "MOB", icon: 174, x: 91, y: 81 },
        { mapid: 4, name: "Desert Runner", type: "MOB", icon: 44, x: 33, y: 12 },
        { mapid: 4, name: "Cyclops Knight", type: "MOB", icon: 43, x: 51, y: 30 },
        { mapid: 4, name: "Orc King", type: "MOB", icon: 46, x: 87, y: 68 },
        { mapid: 4, name: "Kobalos", type: "MOB", icon: 304, x: 89, y: 23 },
        { mapid: 4, name: "Sand Golem", type: "MOB", icon: 162, x: 83, y: 84 },
        { mapid: 4, name: "King Cobra", type: "MOB", icon: 48, x: 62, y: 36 },
        { mapid: 4, name: "Fire Viper", type: "MOB", icon: 49, x: 60, y: 49 },
        { mapid: 4, name: "Fire Imp", type: "MOB", icon: 47, x: 34, y: 36 },
        { mapid: 4, name: "Fire Imp", type: "MOB", icon: 47, x: 65, y: 72 },
        { mapid: 4, name: "Fire Ant", type: "MOB", icon: 50, x: 13, y: 82 },
        { mapid: 4, name: "Desert Orc", type: "MOB", icon: 45, x: 83, y: 46 },
        //RAKBLOOD: 5
        { mapid: 5, name: "Rakblood Town", type: "CITY", x: 34, y: 68 },
        { mapid: 5, name: "Coal", type: "RESOURCE", icon: "pick", description: "40 mining", x: 56, y: 20 },
        { mapid: 5, name: "Fishing Master", type: "POI", x: 47, y: 76 },
        { mapid: 5, name: "Transfer to Whiland", description: "Leads to Dorpat", type: "TRAVEL", x: 38, y: 21 },
        { mapid: 5, name: "Transfer to Narwa", description: "Leads to Fellin Island, Blood River", type: "TRAVEL", x: 88, y: 91 },
        { mapid: 5, name: "Fishing Rod", description: "1 fishing", type: "RESOURCE", icon: "fish", x: 46, y: 79 },
        { mapid: 5, name: "Assassin", type: "MOB", icon: 186, x: 25, y: 79 },
        { mapid: 5, name: "Explorer", type: "MOB", icon: 187, x: 23, y: 66 },
        { mapid: 5, name: "Dark Orc", type: "MOB", icon: 66, x: 34, y: 83 },
        { mapid: 5, name: "Bronze Golem", type: "MOB", icon: 60, x: 14, y: 42 },
        { mapid: 5, name: "Azure Golem", type: "MOB", icon: 59, x: 17, y: 12 },
        { mapid: 5, name: "Iron Golem", type: "MOB", icon: 62, x: 55, y: 25 },
        { mapid: 5, name: "Coal Golem", type: "MOB", icon: 61, x: 72, y: 20 },
        { mapid: 5, name: "Steel Golem", type: "MOB", icon: 63, x: 65, y: 45 },
        { mapid: 5, name: "Thief", type: "MOB", icon: 185, x: 37, y: 44 },
        { mapid: 5, name: "Rock Spirit", type: "MOB", icon: 64, x: 45, y: 85 },
        { mapid: 5, name: "Mutated Hydra", type: "MOB", icon: 65, x: 51, y: 88 },
        { mapid: 5, name: "Giant Troll", type: "MOB", icon: 303, x: 60, y: 79 },
        { mapid: 5, name: "Emerald Dragon", type: "MOB", icon: 126, x: 63, y: 71 },
        //Blood River: 6
        { mapid: 6, name: "Blood River Town", type: "CITY", x: 35, y: 86 },
        { mapid: 6, name: "Fire Altar", type: "POI", x: 78, y: 42 },
        { mapid: 6, name: "Platinum", description: "75 mining", icon: "pick", type: "RESOURCE", x: 43, y: 46 },
        { mapid: 6, name: "Azure", description: "60 mining", icon: "pick", type: "RESOURCE", x: 63, y: 33 },
        { mapid: 6, name: "Azure", description: "60 mining", icon: "pick", type: "RESOURCE", x: 85, y: 49 },
        { mapid: 6, name: "Transfer to Narwa", description: "Leads to Fellin Island, Rakblood", type: "TRAVEL", x: 29, y: 42 },
        { mapid: 6, name: "Transfer to Hell", description: "", type: "TRAVEL", x: 59, y: 21 },
        { mapid: 6, name: "Blood Lizard", type: "MOB", icon: 90, x: 61, y: 25 },
        { mapid: 6, name: "Flame Phoenix", type: "MOB", icon: 87, x: 45, y: 46 },
        { mapid: 6, name: "Flame Phoenix", type: "MOB", icon: 87, x: 65, y: 31 },
        { mapid: 6, name: "Efreet", type: "MOB", icon: 22, x: 61, y: 37 },
        { mapid: 6, name: "Efreet", type: "MOB", icon: 22, x: 52, y: 60 },
        { mapid: 6, name: "Efreet", type: "MOB", icon: 22, x: 30, y: 29 },
        { mapid: 6, name: "Cursed Dragon", type: "MOB", icon: 26, x: 55, y: 74 },
        { mapid: 6, name: "Fire Imp", type: "MOB", icon: 47, x: 65, y: 88 },
        { mapid: 6, name: "Fire Imp", type: "MOB", icon: 47, x: 23, y: 53 },
        { mapid: 6, name: "Ruby Dragon", type: "MOB", icon: 27, x: 75, y: 83 },
        { mapid: 6, name: "Ruby Dragon", type: "MOB", icon: 27, x: 39, y: 14 },
        { mapid: 6, name: "Ruby Dragon", type: "MOB", icon: 27, x: 32, y: 67 },
        { mapid: 6, name: "Adult Ruby Dragon", type: "MOB", icon: 184, x: 82, y: 30 },
        { mapid: 6, name: "Adult Ruby Dragon", type: "MOB", icon: 184, x: 73, y: 15 },
        { mapid: 6, name: "King Ruby Dragon", type: "MOB", icon: 24, x: 90, y: 60 },
        { mapid: 6, name: "King Ruby Dragon", type: "MOB", icon: 24, x: 28, y: 81 },
        { mapid: 6, name: "Fire Centipede", type: "MOB", icon: 159, x: 89, y: 82 },
        { mapid: 6, name: "Flame Wyvern", type: "MOB", icon: 181, x: 75, y: 65 },
        { mapid: 6, name: "Fire Behemoth", type: "MOB", icon: 88, x: 69, y: 48 },
        { mapid: 6, name: "Archdevil", type: "MOB", icon: 19, x: 19, y: 16 },
        { mapid: 6, name: "Archdevil", type: "MOB", icon: 19, x: 17, y: 36 },
        { mapid: 6, name: "Fire Viper", type: "MOB", icon: 49, x: 18, y: 87 },
        //Hell: 7
        { mapid: 7, name: "Hell Town", type: "CITY", x: 31, y: 22 },
        { mapid: 7, name: "Diablo", type: "BOSS", cblevel: 800, x: 11, y: 79 },
        { mapid: 7, name: "Platinum", description: "75 mining", icon: "pick", type: "RESOURCE", x: 48, y: 38 },
        { mapid: 7, name: "King Ruby Dragon", type: "MOB", icon: 24, x: 8, y: 69 },
        { mapid: 7, name: "King Ruby Dragon", type: "MOB", icon: 24, x: 27, y: 53 },
        { mapid: 7, name: "Adult Ruby Dragon", type: "MOB", icon: 184, x: 80, y: 29 },
        { mapid: 7, name: "Adult Ruby Dragon", type: "MOB", icon: 184, x: 89, y: 15 },
        { mapid: 7, name: "Ruby Dragon", type: "MOB", icon: 27, x: 40, y: 12 },
        { mapid: 7, name: "Ruby Dragon", type: "MOB", icon: 27, x: 16, y: 76 },
        { mapid: 7, name: "Ghost Dragon", type: "MOB", icon: 23, x: 31, y: 59 },
        { mapid: 7, name: "Flame Phoenix", type: "MOB", icon: 87, x: 11, y: 48 },
        { mapid: 7, name: "Flame Phoenix", type: "MOB", icon: 87, x: 85, y: 87 },
        { mapid: 7, name: "Fire Spirit", type: "MOB", icon: 191, x: 81, y: 82 },
        { mapid: 7, name: "Fire Behemoth", type: "MOB", icon: 88, x: 13, y: 15 },
        { mapid: 7, name: "Fire Behemoth", type: "MOB", icon: 88, x: 81, y: 63 },
        { mapid: 7, name: "Fire Dragon", type: "MOB", icon: 253, x: 71, y: 90 },
        { mapid: 7, name: "Fire Dragon", type: "MOB", icon: 253, x: 51, y: 34 },
        { mapid: 7, name: "Cursed Dragon", type: "MOB", icon: 26, x: 59, y: 34 },
        { mapid: 7, name: "Hell Angel", type: "MOB", icon: 91, x: 47, y: 89 },
        { mapid: 7, name: "Blood Lizard", type: "MOB", icon: 90, x: 26, y: 90 },
        { mapid: 7, name: "Blood Lizard", type: "MOB", icon: 90, x: 59, y: 67 },
        { mapid: 7, name: "Blood Lizard", type: "MOB", icon: 90, x: 43, y: 58 },
        { mapid: 7, name: "Blood Lizard", type: "MOB", icon: 90, x: 34, y: 54 },
        { mapid: 7, name: "Lava Shroom", type: "MOB", icon: 41, x: 89, y: 47 },
        { mapid: 7, name: "Lava Shroom", type: "MOB", icon: 41, x: 89, y: 12 },
        { mapid: 7, name: "Lava Shroom", type: "MOB", icon: 41, x: 55, y: 6 },
        { mapid: 7, name: "Fire Shroom", type: "MOB", icon: 40, x: 87, y: 25 },
        { mapid: 7, name: "Fire Shroom", type: "MOB", icon: 40, x: 85, y: 10 },
        { mapid: 7, name: "Fire Shroom", type: "MOB", icon: 40, x: 57, y: 8 },
        { mapid: 7, name: "Flame Wyvern", type: "MOB", icon: 181, x: 86, y: 31 },
        { mapid: 7, name: "Flame Wyvern", type: "MOB", icon: 181, x: 57, y: 11 },
        { mapid: 7, name: "Archdevil", type: "MOB", icon: 19, x: 59, y: 23 },
        { mapid: 7, name: "Flaming Giant", type: "MOB", icon: 89, x: 70, y: 45 },
        { mapid: 7, name: "Ruby Dragon", type: "MOB", icon: 27, x: 85, y: 45 },
        { mapid: 7, name: "Efreet", type: "MOB", icon: 22, x: 31, y: 32 },
        { mapid: 7, name: "Efreet", type: "MOB", icon: 22, x: 21, y: 49 },
        { mapid: 7, name: "Azure", description: "60 mining", icon: "pick", type: "RESOURCE", x: 34, y: 65 },
        { mapid: 7, name: "Azure", description: "60 mining", icon: "pick", type: "RESOURCE", x: 62, y: 48 },
        { mapid: 7, name: "Fire Stone", description: "80 mining", icon: "pick", type: "RESOURCE", x: 89, y: 93 },
        { mapid: 7, name: "Transfer to Blood River", description: "", type: "TRAVEL", x: 40, y: 57 },
        //CLOUDS: 8
        { mapid: 8, name: "Clouds Town", type: "CITY", x: 60, y: 72 },
        { mapid: 8, name: "Acid Dragon Lord", type: "BOSS", cblevel: 3987, x: 46, y: 37 },
        { mapid: 8, name: "Air Altar", type: "POI", x: 13, y: 68 },
        { mapid: 8, name: "White Gold", description: "55 mining", icon: "pick", type: "RESOURCE", x: 52, y: 18 },
        { mapid: 8, name: "Beholder", type: "MOB", icon: 114, x: 29, y: 17 },
        { mapid: 8, name: "Beholder", type: "MOB", icon: 114, x: 24, y: 32 },
        { mapid: 8, name: "Archangel", type: "MOB", icon: 18, x: 23, y: 44 },
        { mapid: 8, name: "Blood Battlemage", type: "MOB", icon: 116, x: 35, y: 49 },
        { mapid: 8, name: "Griffin", type: "MOB", icon: 107, x: 38, y: 30 },
        { mapid: 8, name: "Baby Griffin", type: "MOB", icon: 106, x: 40, y: 16 },
        { mapid: 8, name: "Royal Griffin", type: "MOB", icon: 110, x: 9, y: 91 },
        { mapid: 8, name: "Baby Griffin", type: "MOB", icon: 106, x: 22, y: 81 },
        { mapid: 8, name: "Griffin", type: "MOB", icon: 107, x: 35, y: 90 },
        { mapid: 8, name: "Naga", type: "MOB", icon: 109, x: 32, y: 68 },
        { mapid: 8, name: "Ettin", type: "MOB", icon: 115, x: 70, y: 46 },
        { mapid: 8, name: "Chaos Vortex", type: "MOB", icon: 174, x: 84, y: 39 },
        { mapid: 8, name: "Chaos Vortex", type: "MOB", icon: 174, x: 93, y: 67 },
        { mapid: 8, name: "Beholder King", type: "MOB", icon: 113, x: 83, y: 29 },
        { mapid: 8, name: "Beholder King", type: "MOB", icon: 113, x: 86, y: 46 },
        { mapid: 8, name: "Royal Griffin", type: "MOB", icon: 110, x: 89, y: 21 },
        { mapid: 8, name: "Royal Griffin", type: "MOB", icon: 110, x: 53, y: 15 },
        { mapid: 8, name: "Wind Protector", type: "MOB", icon: 58, x: 84, y: 11 },
        { mapid: 8, name: "Beholder King", type: "MOB", icon: 113, x: 70, y: 30 },
        { mapid: 8, name: "Archangel", type: "MOB", icon: 18, x: 65, y: 15 },
        { mapid: 8, name: "Archangel", type: "MOB", icon: 18, x: 11, y: 40 },
        { mapid: 8, name: "Naga", type: "MOB", icon: 109, x: 57, y: 30 },
        { mapid: 8, name: "Royal Griffin", type: "MOB", icon: 110, x: 54, y: 44 },
        { mapid: 8, name: "Zeus", icon: 99, type: "MOB", x: 71, y: 54 },
        { mapid: 8, name: "Zeus", icon: 99, type: "MOB", x: 77, y: 43 },
        { mapid: 8, name: "Zeus", icon: 99, type: "MOB", x: 76, y: 36 },
        { mapid: 8, name: "Zeus", icon: 99, type: "MOB", x: 94, y: 33 },
        { mapid: 8, name: "King Sapphire Dragon", icon: 112, type: "MOB", x: 63, y: 78 },
        { mapid: 8, name: "Adult Sapphire Dragon", icon: 111, type: "MOB", x: 53, y: 77 },
        { mapid: 8, name: "King Black Dragon", icon: 108, type: "MOB", x: 57, y: 61 },
        { mapid: 8, name: "King Ruby Dragon", icon: 24, type: "MOB", x: 60, y: 68 },
        { mapid: 8, name: "Ruby Dragon", icon: 27, type: "MOB", x: 69, y: 70 },
        { mapid: 8, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 13, y: 19 },
        { mapid: 8, name: "Transfer to Heaven", description: "", type: "TRAVEL", x: 83, y: 83 },
        //HEAVEN: 9
        { mapid: 9, name: "Heaven Town", type: "CITY", x: 58, y: 16 },
        { mapid: 9, name: "Demon Portal", type: "BOSS", cblevel: 1500, x: 38, y: 9 },
        { mapid: 9, name: "Nephilim Warrior", type: "BOSS", cblevel: 3000, x: 26, y: 89 },
        { mapid: 9, name: "White Gold", description: "55 mining", icon: "pick", type: "RESOURCE", x: 79, y: 18 },
        { mapid: 9, name: "Zeus", icon: 99, type: "MOB", x: 89, y: 40 },
        { mapid: 9, name: "Flame Phoenix", icon: 87, type: "MOB", x: 73, y: 41 },
        { mapid: 9, name: "Unicorn", icon: 245, type: "MOB", x: 69, y: 59 },
        { mapid: 9, name: "Thunder Bird", icon: 258, type: "MOB", x: 8, y: 70 },
        { mapid: 9, name: "Gandalf The Grey", icon: 98, type: "MOB", x: 58, y: 45 },
        { mapid: 9, name: "Gandalf The Grey", icon: 98, type: "MOB", x: 81, y: 15 },
        { mapid: 9, name: "Thunder Angel", icon: 269, type: "MOB", x: 54, y: 62 },
        { mapid: 9, name: "Confused Merlin", icon: 95, type: "MOB", x: 53, y: 74 },
        { mapid: 9, name: "Dwarf Battlemage", icon: 94, type: "MOB", x: 29, y: 66 },
        { mapid: 9, name: "Dwarf Battlemage", icon: 94, type: "MOB", x: 88, y: 29 },
        { mapid: 9, name: "Merlin", icon: 96, type: "MOB", x: 85, y: 33 },
        { mapid: 9, name: "King Black Dragon", icon: 108, type: "MOB", x: 25, y: 79 },
        { mapid: 9, name: "Young Gandalf", icon: 97, type: "MOB", x: 40, y: 31 },
        { mapid: 9, name: "Young Gandalf", icon: 97, type: "MOB", x: 75, y: 85 },
        { mapid: 9, name: "Battlemage", icon: 93, type: "MOB", x: 16, y: 50 },
        { mapid: 9, name: "Battlemage", icon: 93, type: "MOB", x: 66, y: 82 },
        { mapid: 9, name: "Adult Sapphire Dragon", icon: 111, type: "MOB", x: 20, y: 24 },
        { mapid: 9, name: "Adult Sapphire Dragon", icon: 111, type: "MOB", x: 81, y: 51 },
        { mapid: 9, name: "King Sapphire Dragon", icon: 112, type: "MOB", x: 39, y: 44 },
        { mapid: 9, name: "King Sapphire Dragon", icon: 112, type: "MOB", x: 23, y: 11 },
        { mapid: 9, name: "King Sapphire Dragon", icon: 112, type: "MOB", x: 85, y: 68 },
        { mapid: 9, name: "King Gilded Dragon", icon: 244, type: "MOB", x: 84, y: 84 },
        { mapid: 9, name: "Death Angel", icon: 105, type: "MOB", x: 39, y: 13 },
        { mapid: 9, name: "Transfer to Clouds", description: "", type: "TRAVEL", x: 15, y: 21 },
        //CESIS: 10
        { mapid: 10, name: "Cesis Town", type: "CITY", x: 58, y: 64 },
        { mapid: 10, name: "Fishing Rod", description: "1 fishing", type: "RESOURCE", icon: "fish", x: 51, y: 66 },
        { mapid: 10, name: "Ancient Hydra", type: "BOSS", cblevel: 1000, x: 60, y: 89 },
        { mapid: 10, name: "Maple", description: "35 woodcutting", icon: "wood", type: "RESOURCE", x: 71, y: 32 },
        { mapid: 10, name: "Blue Palm", description: "55 woodcutting", icon: "wood2", type: "RESOURCE", x: 10, y: 38 },
        { mapid: 10, name: "Magic Oak", description: "65 woodcutting", icon: "wood2", type: "RESOURCE", x: 77, y: 28 },
        { mapid: 10, name: "Transfer to Reval", description: "Leads to Dorpat, Pernau", type: "TRAVEL", x: 48, y: 17 },
        { mapid: 10, name: "Transfer to Ancient Dungeon", description: "", type: "TRAVEL", x: 20, y: 92 },
        { mapid: 10, name: "King Emerald Dragon", type: "MOB", icon: 128, x: 52, y: 35 },
        { mapid: 10, name: "King Emerald Dragon", type: "MOB", icon: 128, x: 57, y: 80 },
        { mapid: 10, name: "Grass Killer", type: "MOB", icon: 195, x: 31, y: 13 },
        { mapid: 10, name: "Grass Killer", type: "MOB", icon: 195, x: 17, y: 69 },
        { mapid: 10, name: "Grass Killer", type: "MOB", icon: 195, x: 42, y: 61 },
        { mapid: 10, name: "Barbarian Shaman", type: "MOB", icon: 132, x: 31, y: 33 },
        { mapid: 10, name: "Barbarian Shaman", type: "MOB", icon: 132, x: 76, y: 21 },
        { mapid: 10, name: "Emerald Dragon", type: "MOB", icon: 126, x: 21, y: 48 },
        { mapid: 10, name: "Emerald Dragon", type: "MOB", icon: 126, x: 35, y: 64 },
        { mapid: 10, name: "Barbarian Berserker", type: "MOB", icon: 133, x: 26, y: 75 },
        { mapid: 10, name: "Barbarian Berserker", type: "MOB", icon: 133, x: 84, y: 85 },
        { mapid: 10, name: "Emerald Plant", type: "MOB", icon: 194, x: 17, y: 83 },
        { mapid: 10, name: "Adult Emerald Dragon", type: "MOB", icon: 127, x: 9, y: 31 },
        { mapid: 10, name: "Adult Emerald Dragon", type: "MOB", icon: 127, x: 39, y: 82 },
        { mapid: 10, name: "Grass Snake", type: "MOB", icon: 130, x: 43, y: 46 },
        { mapid: 10, name: "Grass Snake", type: "MOB", icon: 130, x: 77, y: 15 },
        { mapid: 10, name: "Grass Snake", type: "MOB", icon: 130, x: 78, y: 39 },
        { mapid: 10, name: "Baby Emerald Dragon", type: "MOB", icon: 125, x: 36, y: 21 },
        { mapid: 10, name: "Baby Emerald Dragon", type: "MOB", icon: 125, x: 48, y: 27 },
        { mapid: 10, name: "Baby Emerald Dragon", type: "MOB", icon: 125, x: 56, y: 23 },
        { mapid: 10, name: "Naga", type: "MOB", icon: 109, x: 67, y: 17 },
        { mapid: 10, name: "Naga", type: "MOB", icon: 109, x: 87, y: 18 },
        { mapid: 10, name: "Barbarian Ghost", type: "MOB", icon: 131, x: 19, y: 34 },
        { mapid: 10, name: "Barbarian Ghost", type: "MOB", icon: 131, x: 82, y: 51 },
        { mapid: 10, name: "Poisonous Behemoth", type: "MOB", icon: 193, x: 12, y: 18 },
        { mapid: 10, name: "Poisonous Behemoth", type: "MOB", icon: 193, x: 75, y: 75 },
        { mapid: 10, name: "Moss Wyvern", type: "MOB", icon: 129, x: 64, y: 84 },
        //WALCO: 11
        { mapid: 11, name: "Walco Town", type: "CITY", x: 22, y: 29 },
        { mapid: 11, name: "Reaper", type: "BOSS", cblevel: 600, x: 45, y: 70 },
        { mapid: 11, name: "Spirit Log", description: "45 woodcutting", icon: "wood2", type: "RESOURCE", x: 29, y: 44 },
        { mapid: 11, name: "Spirit Log", description: "45 woodcutting", icon: "wood2", type: "RESOURCE", x: 58, y: 33 },
        { mapid: 11, name: "Spirit Log", description: "45 woodcutting", icon: "wood2", type: "RESOURCE", x: 38, y: 21 },
        { mapid: 11, name: "Transfer to Dorpat", description: "", type: "TRAVEL", x: 9, y: 84 },
        { mapid: 11, name: "Shadow Ghost", type: "MOB", icon: 134, x: 16, y: 79 },
        { mapid: 11, name: "Ghost", type: "MOB", icon: 9, x: 23, y: 68 },
        { mapid: 11, name: "Poltergeist", type: "MOB", icon: 136, x: 20, y: 39 },
        { mapid: 11, name: "Energy Ghost", type: "MOB", icon: 137, x: 32, y: 20 },
        { mapid: 11, name: "Skeleton Assassin", type: "MOB", icon: 138, x: 30, y: 35 },
        { mapid: 11, name: "Ghost Dragon", type: "MOB", icon: 23, x: 67, y: 53 },
        { mapid: 11, name: "Vampire", type: "MOB", icon: 11, x: 55, y: 45 },
        { mapid: 11, name: "Vampire Lord", type: "MOB", icon: 28, x: 46, y: 74 },
        { mapid: 11, name: "Spirit", type: "MOB", icon: 135, x: 23, y: 55 },
        { mapid: 11, name: "Skeleton", type: "MOB", icon: 10, x: 76, y: 39 },
        { mapid: 11, name: "Skeleton Knight", type: "MOB", icon: 67, x: 79, y: 70 },
        //PERNAU: 13
        { mapid: 13, name: "Campfire", description: "(There is no chest in Pernau)", type: "POI", x: 90, y: 13 },
        { mapid: 13, name: "Pharaoh", type: "BOSS", cblevel: 1300, x: 12, y: 45 },
        { mapid: 13, name: "Transfer to Reval", description: "", type: "TRAVEL", x: 85, y: 80 },
        { mapid: 13, name: "Transfer to Pernau Desert", description: "", type: "TRAVEL", x: 40, y: 57 },
        { mapid: 13, name: "Transfer to Pernau Desert", description: "", type: "TRAVEL", x: 11, y: 23 },
        { mapid: 13, name: "Transfer to Pernau Pyramid", description: "", type: "TRAVEL", x: 37, y: 42 },
        { mapid: 13, name: "Transfer to Lion's Den", description: "Leads to Pharaoh", type: "TRAVEL", x: 55, y: 10 },
        { mapid: 13, name: "Transfer to Lion's Den", description: "", type: "TRAVEL", x: 6, y: 32 },
        { mapid: 13, name: "Transfer to Pharaoh", description: "", type: "TRAVEL", x: 7, y: 12 },
        { mapid: 13, name: "Shopkeeper", type: "POI", x: 15, y: 8 },
        { mapid: 13, name: "Mummy", type: "MOB", icon: 163, x: 83, y: 88 },
        { mapid: 13, name: "Mummy", type: "MOB", icon: 163, x: 53, y: 71 },
        { mapid: 13, name: "White Wall", type: "MOB", icon: 179, x: 88, y: 92 },
        { mapid: 13, name: "White Wall", type: "MOB", icon: 179, x: 88, y: 69 },
        { mapid: 13, name: "White Wall", type: "MOB", icon: 179, x: 91, y: 55 },
        { mapid: 13, name: "White Wall", type: "MOB", icon: 179, x: 80, y: 55 },
        { mapid: 13, name: "Rotting Mummy", type: "MOB", icon: 164, x: 87, y: 74 },
        { mapid: 13, name: "Rotting Mummy", type: "MOB", icon: 164, x: 66, y: 81 },
        { mapid: 13, name: "Rotting Mummy", type: "MOB", icon: 164, x: 76, y: 71 },
        { mapid: 13, name: "Skeleton Lord", type: "MOB", icon: 176, x: 92, y: 74 },
        { mapid: 13, name: "Skeleton Lord", type: "MOB", icon: 176, x: 82, y: 58 },
        { mapid: 13, name: "Skeleton Lord", type: "MOB", icon: 176, x: 59, y: 91 },
        { mapid: 13, name: "Skeleton Lord", type: "MOB", icon: 176, x: 33, y: 64 },
        { mapid: 13, name: "Skeleton Mage", type: "MOB", icon: 177, x: 91, y: 64 },
        { mapid: 13, name: "Skeleton Mage", type: "MOB", icon: 177, x: 79, y: 66 },
        { mapid: 13, name: "Phantom Skull", type: "MOB", icon: 170, x: 80, y: 62 },
        { mapid: 13, name: "Ice Mummy", type: "MOB", icon: 165, x: 76, y: 81 },
        { mapid: 13, name: "Ice Mummy", type: "MOB", icon: 165, x: 58, y: 85 },
        { mapid: 13, name: "Ice Mummy", type: "MOB", icon: 165, x: 41, y: 71 },
        { mapid: 13, name: "DarkElf Mage", type: "MOB", icon: 161, x: 58, y: 89 },
        { mapid: 13, name: "Skeleton King", type: "MOB", icon: 175, x: 60, y: 91 },
        { mapid: 13, name: "Skeleton King", type: "MOB", icon: 175, x: 24, y: 65 },
        { mapid: 13, name: "Emerald Mummy", type: "MOB", icon: 166, x: 62, y: 62 },
        { mapid: 13, name: "Emerald Mummy", type: "MOB", icon: 166, x: 43, y: 59 },
        { mapid: 13, name: "Sand Golem", type: "MOB", icon: 162, x: 45, y: 85 },
        { mapid: 13, name: "White Hard Wall", type: "MOB", icon: 180, x: 39, y: 67 },
        { mapid: 13, name: "White Hard Wall", type: "MOB", icon: 180, x: 36, y: 64 },
        { mapid: 13, name: "White Hard Wall", type: "MOB", icon: 180, x: 40, y: 61 },
        { mapid: 13, name: "White Hard Wall", type: "MOB", icon: 180, x: 46, y: 54 },
        { mapid: 13, name: "Flame Phoenix", type: "MOB", icon: 87, x: 27, y: 69 },
        { mapid: 13, name: "Sand Centipede", type: "MOB", icon: 157, x: 32, y: 36 },
        { mapid: 13, name: "Rock Centipede", type: "MOB", icon: 158, x: 39, y: 19 },
        { mapid: 13, name: "Fire Centipede", type: "MOB", icon: 159, x: 55, y: 33 },
        { mapid: 13, name: "Gilded Mummy", type: "MOB", icon: 167, x: 61, y: 40 },
        { mapid: 13, name: "Gilded Mummy", type: "MOB", icon: 167, x: 59, y: 19 },
        { mapid: 13, name: "Skeletal Dragon", type: "MOB", icon: 160, x: 66, y: 45 },
        { mapid: 13, name: "Diamond Mummy", type: "MOB", icon: 169, x: 79, y: 21 },
        { mapid: 13, name: "Deathstalker Scorpion", type: "MOB", icon: 171, x: 75, y: 43 },
        { mapid: 13, name: "Deathstalker Scorpion", type: "MOB", icon: 171, x: 89, y: 45 },
        { mapid: 13, name: "Emperor Scorpion", type: "MOB", icon: 172, x: 82, y: 41 },
        { mapid: 13, name: "War Elephant", type: "MOB", icon: 173, x: 85, y: 25 },
        { mapid: 13, name: "Amethyst Mummy", type: "MOB", icon: 168, x: 66, y: 13 },
        { mapid: 13, name: "Lion", type: "MOB", icon: 190, x: 16, y: 15 },
        { mapid: 13, name: "Earth Dragon", type: "MOB", icon: 251, x: 11, y: 34 },
        { mapid: 13, name: "Fire Dragon", type: "MOB", icon: 253, x: 19, y: 45 },
        { mapid: 13, name: "Void Dragon", type: "MOB", icon: 254, x: 7, y: 43 },
        //FELLIN ISLAND: 14
        { mapid: 14, name: "Fishing Guild", description: "Requires Fishing Guild permission and 80 fishing.", type: "POI", x: 79, y: 32 },
        { mapid: 14, name: "Transfer to Dragon's Lair", description: "", type: "TRAVEL", x: 41, y: 48 },
        { mapid: 14, name: "Transfer to Narwa", description: "", type: "TRAVEL", x: 10, y: 26 },
        { mapid: 14, name: "Gilded Dragon", type: "MOB", icon: 248, x: 45, y: 51 },
        { mapid: 14, name: "Naga", type: "MOB", icon: 109, x: 26, y: 63 },
        { mapid: 14, name: "Poisonous Behemoth", type: "MOB", icon: 193, x: 36, y: 77 },
        { mapid: 14, name: "Barbarian Berserker", type: "MOB", icon: 133, x: 69, y: 74 },
        { mapid: 14, name: "Barbarian Shaman", type: "MOB", icon: 132, x: 58, y: 63 },
        { mapid: 14, name: "Skeletal Dragon", type: "MOB", icon: 160, x: 72, y: 49 },
        { mapid: 14, name: "Barbarian Ghost", type: "MOB", icon: 131, x: 68, y: 34 },
        { mapid: 14, name: "Grass Snake", type: "MOB", icon: 130, x: 51, y: 27 },
        { mapid: 14, name: "Behemoth", type: "MOB", icon: 20, x: 31, y: 35 },
        { mapid: 14, name: "Dragonfly", type: "MOB", icon: 120, x: 36, y: 18 },
        { mapid: 14, name: "Fishing Rod", description: "1 fishing", type: "RESOURCE", icon: "fish", x: 87, y: 31 },
        { mapid: 14, name: "Iron Fishing Rod", description: "65 fishing", type: "RESOURCE", icon: "ironrod", x: 89, y: 31 },
        { mapid: 14, name: "Fishing Net", description: "5 fishing", type: "RESOURCE", icon: "net", x: 88, y: 36 },
        { mapid: 14, name: "Cage", description: "35 fishing", icon: "cage", type: "RESOURCE", x: 85, y: 32 },
        { mapid: 14, name: "Wooden Harpoon", description: "50 fishing", type: "RESOURCE", icon: "woodharp", x: 88, y: 27 },
        { mapid: 14, name: "Steel Harpoon", description: "63 fishing", type: "RESOURCE", icon: "steelharp", x: 87, y: 81 },
        //DRAGON'S LAIR: 15
        { mapid: 15, name: "Dragon's Lair Outpost", description: "", type: "CITY", x: 49, y: 45 },
        { mapid: 15, name: "Chaotic Dragon", description: "", type: "BOSS", cblevel: 2100, x: 71, y: 21 },
        { mapid: 15, name: "Fire Stone", description: "80 mining", icon: "pick", type: "RESOURCE", x: 77, y: 69 },
        { mapid: 15, name: "Ruby Dragon", type: "MOB", icon: 27, x: 49, y: 34 },
        { mapid: 15, name: "Adult Ruby Dragon", type: "MOB", icon: 184, x: 48, y: 24 },
        { mapid: 15, name: "King Ruby Dragon", type: "MOB", icon: 24, x: 54, y: 16 },
        { mapid: 15, name: "Fire Dragon", type: "MOB", icon: 253, x: 68, y: 17 },
        { mapid: 15, name: "Ghost Dragon", type: "MOB", icon: 23, x: 61, y: 43 },
        { mapid: 15, name: "Skeletal Dragon", type: "MOB", icon: 160, x: 72, y: 39 },
        { mapid: 15, name: "Void Dragon", type: "MOB", icon: 254, x: 77, y: 57 },
        { mapid: 15, name: "Emerald Dragon", type: "MOB", icon: 126, x: 40, y: 40 },
        { mapid: 15, name: "Adult Emerald Dragon", type: "MOB", icon: 127, x: 32, y: 39 },
        { mapid: 15, name: "King Emerald Dragon", type: "MOB", icon: 128, x: 16, y: 42 },
        { mapid: 15, name: "Adult Black Dragon", type: "MOB", icon: 249, x: 50, y: 57 },
        { mapid: 15, name: "King Black Dragon", type: "MOB", icon: 108, x: 48, y: 74 },
        { mapid: 15, name: "Metal Dragon", type: "MOB", icon: 252, x: 62, y: 69 },
        { mapid: 15, name: "Metal Dragon", type: "MOB", icon: 252, x: 74, y: 72 },
        { mapid: 15, name: "Earth Dragon", type: "MOB", icon: 251, x: 12, y: 77 },
        { mapid: 15, name: "Gilded Dragon", type: "MOB", icon: 248, x: 39, y: 55 },
        { mapid: 15, name: "Adult Gilded Dragon", type: "MOB", icon: 250, x: 31, y: 65 },
        { mapid: 15, name: "King Gilded Dragon", type: "MOB", icon: 244, x: 24, y: 62 },
        { mapid: 15, name: "Transfer to Fellin Island", description: "", type: "TRAVEL", x: 44, y: 45 },
        //NO MAN'S LAND: 16
        { mapid: 16, name: "No Man's Land Outpost", description: "Chest", type: "CITY", x: 15, y: 23 },
        { mapid: 16, name: "World Tree", description: "", type: "BOSS", cblevel: 3775, x: 84, y: 42 },
        { mapid: 16, name: "Fire Stone", description: "80 mining", icon: "pick", type: "RESOURCE", x: 79, y: 93 },
        { mapid: 16, name: "Fir", description: "1 woodcutting", type: "RESOURCE", icon: "wood", x: 10, y: 19 },
        { mapid: 16, name: "Tin", description: "1 mining", icon: "pick", type: "RESOURCE", x: 16, y: 37 },
        { mapid: 16, name: "Copper", description: "1 mining", icon: "pick", type: "RESOURCE", x: 17, y: 35 },
        { mapid: 16, name: "Iron", description: "25 mining", icon: "pick", type: "RESOURCE", x: 26, y: 42 },
        { mapid: 16, name: "Coal", description: "40 mining", icon: "pick", type: "RESOURCE", x: 55, y: 18 },
        { mapid: 16, name: "Loot Master", description: "(NPC)", type: "POI", x: 8, y: 25 },
        { mapid: 16, name: "Loot Master", description: "(NPC)", type: "POI", x: 21, y: 11 },
        { mapid: 16, name: "Legendary Breeding Master", description: "(NPC) Shop", type: "POI", x: 11, y: 88 },
        { mapid: 16, name: "PVP Shopkeeper", description: "(NPC) Shop", type: "POI", x: 82, y: 66 },
        { mapid: 16, name: "Transfer to Whiland", description: "", type: "TRAVEL", x: 13, y: 14 },
        { mapid: 16, name: "Novice Knight", type: "MOB", icon: 271, x: 18, y: 27 },
        { mapid: 16, name: "Knight", type: "MOB", icon: 272, x: 23, y: 29 },
        { mapid: 16, name: "Knight", type: "MOB", icon: 272, x: 49, y: 48 },
        { mapid: 16, name: "Baron", type: "MOB", icon: 273, x: 73, y: 17 },
        { mapid: 16, name: "Baron", type: "MOB", icon: 273, x: 19, y: 52 },
        { mapid: 16, name: "Baron", type: "MOB", icon: 273, x: 14, y: 78 },
        { mapid: 16, name: "Baron", type: "MOB", icon: 273, x: 62, y: 53 },
        { mapid: 16, name: "Baron", type: "MOB", icon: 273, x: 54, y: 56 },
        { mapid: 16, name: "Baron", type: "MOB", icon: 273, x: 65, y: 39 },
        { mapid: 16, name: "Earl", type: "MOB", icon: 274, x: 22, y: 61 },
        { mapid: 16, name: "Earl", type: "MOB", icon: 274, x: 15, y: 89 },
        { mapid: 16, name: "Earl", type: "MOB", icon: 274, x: 74, y: 43 },
        { mapid: 16, name: "Prince", type: "MOB", icon: 276, x: 43, y: 87 },
        { mapid: 16, name: "Prince", type: "MOB", icon: 276, x: 82, y: 72 },
        { mapid: 16, name: "King", type: "MOB", icon: 277, x: 62, y: 81 },
        { mapid: 16, name: "King", type: "MOB", icon: 277, x: 84, y: 87 },
        { mapid: 16, name: "Marquis", type: "MOB", icon: 275, x: 74, y: 67 },
        { mapid: 16, name: "Marquis", type: "MOB", icon: 275, x: 68, y: 51 },
        { mapid: 16, name: "Marquis", type: "MOB", icon: 275, x: 81, y: 42 },
        { mapid: 16, name: "Marquis", type: "MOB", icon: 275, x: 52, y: 64 },
        //ANCIENT DUNGEON: 17
        { mapid: 17, name: "Ancient Dungeon Outpost", description: "Chest", type: "CITY", x: 44, y: 91 },
        { mapid: 17, name: "Ancient Magician", description: "(NPC) Shop", type: "POI", x: 46, y: 86 },
        { mapid: 17, name: "Ancient Furniture Master", description: "(NPC) Shop", type: "POI", x: 48, y: 90 },
        { mapid: 17, name: "Cannibal Plant", description: "", type: "BOSS", cblevel: 2100, x: 41, y: 49 },
        { mapid: 17, name: "Pyrohydra", description: "", type: "MOB", icon: 283, x: 13, y: 78 },
        { mapid: 17, name: "Diamond Plant", description: "", type: "MOB", icon: 285, x: 27, y: 63 },
        { mapid: 17, name: "Diamond Plant", description: "", type: "MOB", icon: 285, x: 48, y: 48 },
        { mapid: 17, name: "Emerald Plant", description: "", type: "MOB", icon: 194, x: 45, y: 48 },
        { mapid: 17, name: "Grass Killer", description: "", type: "MOB", icon: 195, x: 29, y: 85 },
        { mapid: 17, name: "Earth Dragon", description: "", type: "MOB", icon: 251, x: 55, y: 47 },
        { mapid: 17, name: "Earth Dragon", description: "", type: "MOB", icon: 251, x: 56, y: 73 },
        { mapid: 17, name: "Beholder Overseer", description: "", type: "MOB", icon: 288, x: 48, y: 32 },
        { mapid: 17, name: "Hydra Dragon", description: "", type: "MOB", icon: 286, x: 19, y: 26 },
        { mapid: 17, name: "Earthstorm", description: "", type: "MOB", icon: 284, x: 13, y: 42 },
        { mapid: 17, name: "Unicorn", description: "", type: "MOB", icon: 245, x: 73, y: 19 },
        { mapid: 17, name: "Queen Lizard", description: "", type: "MOB", icon: 282, x: 77, y: 56 },
        { mapid: 17, name: "Blood Spirit", description: "", type: "MOB", icon: 281, x: 69, y: 36 },
        { mapid: 17, name: "Blood Spirit", description: "", type: "MOB", icon: 281, x: 70, y: 81 },
        { mapid: 17, name: "Transfer to Cesis", description: "", type: "TRAVEL", x: 79, y: 87},
        //LOST WOODS: 18
        { mapid: 18, name: "Cave Crawler", description: "", type: "BOSS", cblevel: 434, x: 50, y: 35 },
        { mapid: 18, name: "Giant Cyclops", description: "", type: "BOSS", cblevel: 450, x: 36, y: 76 },
        { mapid: 18, name: "Venus Flytrap", description: "", type: "BOSS", cblevel: 545, x: 79, y: 80 },
        { mapid: 18, name: "Grizzly Bear", description: "", type: "MOB", icon: 188, x: 83, y: 19 },
        { mapid: 18, name: "Poisoned Shroom", description: "", type: "MOB", icon: 39, x: 78, y: 40 },
        { mapid: 18, name: "Golden Shroom", description: "", type: "MOB", icon: 36, x: 70, y: 36 },
        { mapid: 18, name: "Dry-Rotted Shroom", description: "", type: "MOB", icon: 37, x: 72, y: 24 },
        { mapid: 18, name: "Avatar's Shroom", description: "", type: "MOB", icon: 38, x: 65, y: 21 },
        { mapid: 18, name: "Archdevil", description: "", type: "MOB", icon: 19, x: 55, y: 30 },
        { mapid: 18, name: "Archdevil", description: "", type: "MOB", icon: 19, x: 17, y: 82 },
        { mapid: 18, name: "Behemoth", description: "", type: "MOB", icon: 20, x: 35, y: 83 },
        { mapid: 18, name: "Ettin King", description: "", type: "MOB", icon: 21, x: 63, y: 78 },
        { mapid: 18, name: "Transfer to Whiland", description: "", type: "TRAVEL", x: 86, y: 14}
    ]};

    Mods.Newmap.POIfind = []; //Coordinates of the red targets in the minimap
    Mods.Newmap.POIfindMap = 0; //Map of the targets

    HUD.drawMinimap = function () {
        if (minimap) return;

        //hide zone button
        getElem("mods_zone_buttondiv").style.visibility = "hidden";
        //call original
        Mods.Newmap.drawMinimap();
        //fills transparent white border
        var r_a = 0.3;
        (ctx.hud.fillStyle = "rgba(255, 255, 255, " + r_a + ")", ctx.hud.fillRect(0,14,76,4), ctx.hud.fillRect(76,14,4,76), ctx.hud.fillRect(0,90,80,4), ctx.hud.fillRect(0,18,4,72), ctx.globalAlpha=0);
        //adds N (North)
        ctx.hud.font = "9px Arial";
        ctx.hud.textAlign = 'center';
        ctx.hud.fillStyle = "black";
        ctx.hud.fillText("N", 39.5, 18);
        ctx.hud.fillStyle = "yellow";
        ctx.hud.fillText("N", 40.5, 19);

        //center coordinates of the minimap
        var cx = 39.5, cy = 53.5;

        //orders players to show friends last (so that friends will be always visible)
        //yellow for friend, green for me, white for ignored and blue for everyone else
        var ordTargets = [], myFriends = [];
        for (var e in players) {
            if (typeof players[e] != "undefined" && players[e].b_t == BASE_TYPE.PLAYER) {
                if (players[e].me) ordTargets.push({color: "#55FF55", i: players[e].i, j: players[e].j});
                else if (Mods.findWithAttr(Contacts.friends, "name", players[e].name)) myFriends.push({color: "yellow", i: players[e].i, j: players[e].j});
                else if (Contacts.ignores.indexOf(players[e].name) >= 0) ordTargets.push({color: "#FFFFFF", i: players[e].i, j: players[e].j});
                else ordTargets.push({color: "#00BFFF", i: players[e].i, j: players[e].j});
            }
        }
        //adds red targets from the /find command
        for (var e in myFriends) ordTargets.push(myFriends[e]);
        if (typeof Mods.Newmap !== "undefined" && typeof Mods.Newmap.POIfind !== "undefined") {
            if (Mods.Newmap.POIfindMap == current_map)
                for (var e in Mods.Newmap.POIfind)
                    ordTargets.push(Mods.Newmap.POIfind[e]);
            //clears targets if user goes to another map
            else Mods.Newmap.POIfind = [];
        }

        for (var e in ordTargets) {
            col = ordTargets[e].color;
            d = translateTileToCoordinates(ordTargets[e].i + 13, ordTargets[e].j - 9);
            a = map_increase * 3;
            a = (Math.round((d.x - dest_x) / 8) / 1.01 + 0.5 | 0) - a;
            d = 14 + Math.round((d.y - dest_y) / 8);
            //if (1<a && (15<d && 79>a && 93>d && col=="#FFFFFF")) col = "#00BFFF";

            var outside = false;
            var angle = 0;
            //coordinates of the triangles (used for players outside the range of the minimap)
            //var triang = [{x:0,y:0},{x:-4,y:-4},{x:-4,y:4}], triang2 = [{x:-1,y:0},{x:-3,y:-2},{x:-3,y:2}];
            //var triang = [{x:0,y:0},{x:-5,y:-4},{x:-5,y:4}], triang2 = [{x:-1,y:0},{x:-4,y:-2},{x:-4,y:2}];
            var triang = [{x:0,y:0},{x:-6,y:-3},{x:-6,y:3}], triang2 = [{x:-1,y:0},{x:-5,y:-1},{x:-5,y:1}];

            //calculates position in minimap border if player is outside of the range of the minimap
            if (a < 2) {
                angle = Math.atan2(d-cy,a-cx);
                outside = true;
                d = (d-cy)*(cx-0)/(cx-a)+cy;
                a = 0
            }
            if (d < 16) {
                if (!outside) {
                    angle = Math.atan2(d-cy,a-cx);
                    outside = true;
                }
                a = (a-cx)*(cy-14)/(cy-d)+cx;
                d = 14
            }
            if (a > 78) {
                if (!outside) {
                    angle = Math.atan2(d-cy,a-cx);
                    outside = true;
                }
                d = (d-cy)*(cx-79)/(cx-a)+cy;
                a = 79
            }
            if (d > 92) {
                if (!outside) {
                    angle = Math.atan2(d-cy,a-cx);
                    outside = true;
                }
                a = (a-cx)*(cy-93)/(cy-d)+cx;
                d = 93
            }
            if (outside) {
                var newTriang = [{x:0,y:0},{x:0,y:0},{x:0,y:0}], newTriang2 = [{x:0,y:0},{x:0,y:0},{x:0,y:0}];
                var cos = Math.cos(angle), sin = Math.sin(angle);
                for (var i in newTriang) {
                    newTriang[i].x = a + cos*triang[i].x - sin*triang[i].y;
                    newTriang[i].y = d + sin*triang[i].x + cos*triang[i].y;
                    newTriang2[i].x = a + cos*triang2[i].x - sin*triang2[i].y;
                    newTriang2[i].y = d + sin*triang2[i].x + cos*triang2[i].y;
                }
                ctx.hud.closePath();
                ctx.hud.fillStyle="#000000";
                ctx.hud.beginPath();
                ctx.hud.moveTo(newTriang[0].x,newTriang[0].y);
                ctx.hud.lineTo(newTriang[1].x,newTriang[1].y);
                ctx.hud.lineTo(newTriang[2].x,newTriang[2].y);
                ctx.hud.fill();
                ctx.hud.closePath();
                ctx.hud.fillStyle=col;
                ctx.hud.beginPath();
                ctx.hud.moveTo(newTriang2[0].x,newTriang2[0].y);
                ctx.hud.lineTo(newTriang2[1].x,newTriang2[1].y);
                ctx.hud.lineTo(newTriang2[2].x,newTriang2[2].y);
                ctx.hud.fill();
                ctx.hud.closePath();
            }
            else {
                (ctx.hud.fillStyle="#000000", ctx.hud.fillRect(a-2,d-2,4,4));
                (ctx.hud.fillStyle=col, ctx.hud.fillRect(a-1,d-1,2,2));
            }
        }
    }

    HUD.drawMinimapLarge = function () {
        //call original
        Mods.Newmap.drawMinimapLarge();
        if (!minimap) return;

        //show zone button
        getElem("mods_zone_buttondiv").style.visibility = "";
        zonemapvisible = false;
        //draw POI
        var currentPOI = Mods.Newmap.FilterObj(Mods.Newmap.POI[0], { mapid: current_map });
        Mods.Newmap.HotSpots = [];


        //initialize resources images
        var icon, col;

        for (var poi in currentPOI) {
            var d = Mods.Newmap.translateMapCoords(currentPOI[poi].x + 6, currentPOI[poi].y - 3);
            var a = (Math.round((d.x)) / 1.01 + 0.5 | 0);
            d = 14 + Math.round((d.y));
            icon = null;

            switch (currentPOI[poi].type) {
                case "CITY":
                    col = "#55FF55";
                    currentPOI[poi].icon = "city";
                    break;
                case "BOSS":
                    col = "red";
                    currentPOI[poi].icon = "boss";
                    break;
                case "MOB":
                    col = "transparent";
                    icon = npc_base[currentPOI[poi].icon].img;
                    var cm = npc_base[currentPOI[poi].icon];
                    currentPOI[poi].cblevel = npc_base[currentPOI[poi].icon].params.combat_level;
                    currentPOI[poi].description = cm.temp.total_accuracy + "A, " + cm.temp.total_strength + "S, " + cm.temp.total_defense + "D, " + cm.params.health + "Hp";
                    break;

                case "POI":
                    col = "yellow";
                    currentPOI[poi].icon = "poi";
                    break;
                case "TRAVEL":
                    col = "#00FFFF";
                    currentPOI[poi].icon = "travel";
                    break;
                case "RESOURCE":
                    col = "#F5A9E1";
                    break;
                default:
                    col = "yellow";
            }

            switch (currentPOI[poi].icon) {
                case "spade":
                    icon = item_base[286].img;
                    break;
                case "pick":
                    icon = item_base[23].img;
                    break;
                case "wood":
                    icon = item_base[22].img;
                    break;
                case "wood2":
                    icon = item_base[152].img;
                    break;
                case "fish": //rod
                    icon = item_base[7].img;
                    break;
                case "ironrod":
                    icon = item_base[1036].img;
                    break;
                case "net":
                    icon = item_base[124].img;
                    break;
                case "cage":
                    icon = item_base[127].img;
                    break;
                case "woodharp":
                    icon = item_base[125].img;
                    break;
                case "steelharp":
                    icon = item_base[126].img;
                    break;
                case "city":
                    icon = {
                        sheet: IMAGE_SHEET.MISC,
                        x: 2,
                        y: 7
                    };
                    break;
                case "boss":
                    icon = item_base[544].img;
                    break;
                case "poi":
                    icon = item_base[1132].img;
                    break;
                case "travel":
                    icon = {
                        sheet: IMAGE_SHEET.SICOS,
                        x: 0,
                        y: 5
                    };
                    break;
            }

            //ctx.hud.fillStyle = col;
            //ctx.hud.fillRect(a - 3, d - 3, 6, 6);
            if (col != "transparent") {
                ctx.hud.beginPath();
                ctx.hud.arc(a, d, 6, 0, 2 * Math.PI, false);
                ctx.hud.fillStyle = col;
                ctx.hud.fill();
                ctx.hud.lineWidth = 1;
                ctx.hud.strokeStyle = '#000000';
                ctx.hud.stroke();
            }
            if (icon) {
                var f,cx,cy;
                if(typeof icon.hash == "string"){
                    icon.x = 12;
                    icon.y = 10;
                    var i = getBodyImgNoHalo(icon.hash);
                    f = {img:[i], tile_width: 1, tile_height: 1};
                    f.url = i.toDataURL();
                    cx = 12;
                    cy = 10;
                } else {
                    f = IMAGE_SHEET[icon.sheet];
                    //var img = new Image;
                    cx = icon.x;
                    cy = icon.y;
                }
                var ax = a, ay = d;

                var isize = col == "transparent" ? 16 : 12;

                ctx.hud.drawImage(f.img[0], cx * f.tile_width, cy * f.tile_height, 32, 32, ax - (isize / 2), ay - (isize / 2), isize, isize);
                //img.src = f.url;
            }

            Mods.Newmap.HotSpots.push({ x: a, y: d, desc: (icon ? "<div style='width:32px;height:32px;float:left;background:url(\"" + f.url + "\") no-repeat scroll " + -icon.x * f.tile_width + "px " + -icon.y * f.tile_height + "px transparent;display: inline-block;margin: 0px;padding: 0px;'>&nbsp;</div>" : "") + currentPOI[poi].type + ": <b>" + currentPOI[poi].name + (currentPOI[poi].cblevel ? " (" + currentPOI[poi].cblevel + ")" : "") + "</b><br/>" + (currentPOI[poi].description ? currentPOI[poi].description : "") });
        }

        //draw player position
        Mods.Newmap.BlinkPos("#55FF55");

        //draw other players if in dungeon or nml or cathedral
        if (["Dungeon quest", "No Man's Land", "Cathedral", "Every Man's Land"].indexOf(map_names[players[0].map]) != -1) for (var pl in players) if (players[pl].b_t == BASE_TYPE.PLAYER && !players[pl].me) {
            var d = Mods.Newmap.translateMapCoords(players[pl].i + 6, players[pl].j - 3);
            var a = (Math.round((d.x)) / 1.01 + 0.5 | 0);
            d = 14 + Math.round((d.y));
            var text = players[pl].name;
            ctx.hud.font = "9px Arial";
            ctx.hud.textAlign = 'center';
            ctx.hud.fillStyle = "#000";
            ctx.hud.fillText(text, a - 1, d - 1);
            ctx.hud.fillStyle = "yellow";
            ctx.hud.fillText(text, a, d);
        }

        getElem("hud").addEventListener('mousemove', Mods.Newmap.MapCoords, false);
    };

    Mods.Newmap.ShowZone = function () {
        getElem("mods_zone_buttondiv").style.visibility = "hidden";
        zonemapvisible = true;
        var imageObj = new Image();
        imageObj.onload = function () {
            ctx.hud.drawImage(imageObj, ctx.hud.canvas.width * 0.1, ctx.hud.canvas.height * 0.05, ctx.hud.canvas.width * 0.8, ctx.hud.canvas.height * 0.9);
        };
        imageObj.src = 'https://rpgimg.r.worldssl.net/world_map_mods.jpg';
    };

    Mods.Newmap.BlinkPos = function (col, px, py) {
        if (!minimap || zonemapvisible) return;
        if (!px && !py) {
            var d = Mods.Newmap.translateMapCoords(players[0].i + 6, players[0].j - 3);
            var a = (Math.round((d.x)) / 1.01 + 0.5 | 0);
            d = 14 + Math.round((d.y));
            px = a;
            py = d;
        }
        ctx.hud.fillStyle = col;
        ctx.hud.fillRect(px - 2, py - 2, 4, 4);
        setTimeout(function () { Mods.Newmap.BlinkPos((col == "red" ? "#55FF55" : "red"), px, py); }, 1000);
    }

    Mods.Newmap.translateMapCoords = function (a, b) {
        return {
            x: (b) * half_tile_width_round + (a) * half_tile_width_round + dest_x,
            y: (a) * half_tile_height_round - (b) * half_tile_height_round + dest_y
        }
    }

    Mods.Newmap.FilterObj = function (arr, criteria) {
        return arr.filter(function (obj) {
            return Object.keys(criteria).every(function (c) {
                return obj[c] == criteria[c];
            });
        });
    }

    Mods.Newmap.MouseTranslate = function (a, b) {
        var d = width / wrapper.style.width.replace("px", ""),
        e = height / wrapper.style.height.replace("px", ""),
        a = a * d - dest_x,
        b = b * e - dest_y,
        e = 2 * a / (tile_width),
        f = 2 * b / (tile_height * 1.18),
        d = Math.round((e + f) / 2) - 1,
        e = Math.round((e - f) / 2);
        return {
            i: d,// + dx,
            j: e// + dy
        }
    }

    Mods.Newmap.MapCoords = function (evt) {
        var cdiv = getElem("mods_newmap_coords");
        var pdiv = getElem("mods_newmap_popup");
        evt.clientX = evt.clientX || evt.pageX || evt.touches && evt.touches[0].pageX;
        evt.clientY = evt.clientY || evt.pageY || evt.touches && evt.touches[0].pageY;

        var d1 = translateMousePositionToScreenPosition(evt.clientX, evt.clientY);
        var d = Mods.Newmap.MouseTranslate((evt.clientX * 1.15), (evt.clientY));
        var cx = Math.round(d.i) - 9;
        var cy = Math.round(d.j) + 7;
        //cdiv.innerHTML = evt.clientX + " / " + evt.clientY + " (" + cx + "/" + cy + ")<br/>" + d1.x + " " + d1.y;
        if (cx > -1 && cy > -1 && cx < 101 && cy < 101)
            cdiv.innerHTML = cx + "/" + cy;
        var popvis = false;
        //check if we are above a hot spot
        for (var hs in Mods.Newmap.HotSpots) if (Math.abs(d1.x - Mods.Newmap.HotSpots[hs].x) < 5 && Math.abs(d1.y - Mods.Newmap.HotSpots[hs].y) < 5) {
            //show popup
            //console.debug("popup");
            pdiv.style.top = evt.clientY + "px";
            pdiv.style.left = (evt.clientX + 10) + "px";
            pdiv.innerHTML = Mods.Newmap.HotSpots[hs].desc;
            popvis = true;
            break;
        }

        cdiv.style.visibility = "";
        if (popvis) pdiv.style.visibility = "";
        else pdiv.style.visibility = "hidden";
        if (!minimap || zonemapvisible) {
            getElem("hud").removeEventListener('mousemove', Mods.Newmap.MapCoords, false);
            cdiv.style.visibility = "hidden";
            pdiv.style.visibility = "hidden";
        }
    }

    Mods.timestamp("newmap");

}

Load.newmarket = function () {

    modOptions.newmarket.time = timestamp();

    function temp () {

        createElem("div", "options_game", {
            innerHTML: "<span class='wide_link pointer' id='settings_tradechannel' onclick='Mods.Newmarket.Tradetoggle();'>Trade Chat: join automatically</span>"
        });

        Mods.Newmarket.Tradetoggle();

        //adds trade channel
        channel_names.unshift("$$");

        if (Mods.Newmarket.tradechatmode == 0) Contacts.add_channel("$$");

        //prevent scrolling of market header
        getElem("market").style.overflowY = "";
        getElem("market_search_results").style.overflowY = "auto";
        getElem("market_search_results").style.maxHeight = "230px";
        var sp = getElem("market_search_max_price").parentElement;
        sp.getElementsByClassName("market_select pointer")[0].onclick = function () {
            Market.client_search();
            getElem('market_search_results').scrollTop = 0;
        }

        //adds "enable popup"
        var lcx = document.createElement("span");
        lcx.style.verticalAlign = "middle";
        lcx.style.fontSize = "10px";
        lcx.innerHTML = "<input type='checkbox' id='chk_enable_mktpopup' onclick='Mods.Newmarket.togglepopup();'> Popup"
        sp.insertBefore(lcx, getElem("market_search_item").nextSibling);

        getElem("chk_enable_mktpopup").checked = !Mods.Newmarket.popup;

        //adds next page
        var lt = document.createElement("span");
        lt.innerHTML = "<button onclick='Mods.Newmarket.next();' class='market_select pointer'>Next &gt;&gt;</button>";
        sp.appendChild(lt);

        market_client_new_offer_template = Handlebars.compile("<table style='text-align: left;border: 1px #666666 solid;width: 100%;margin: 0px;margin-top: 20px;' class='table'><tbody><tr class='offer_line'><td>Type</td><td><select id='market_new_offer_search_type' onchange='Market.client_update_new_offer_items();' class='market_select'><option value='0'>Sell</option><option value='1'>Buy</option></select></td></tr><tr class='offer_line even'><td style='width: 96px;'>Item</td><td id='market_new_offer_items'></td></tr><tr class='offer_line'><td>To</td><td><input type='text' class='market_select' placeholder='Everybody' id='market_offer_player' list='player_datalist'/></td></tr><tr class='offer_line even'><td>Price</td><td><input type='number' id='market_new_offer_price' onchange='Market.client_new_offer_update_total_cost();' class='market_select'/></td></tr><tr class='offer_line'><td>Amount</td><td><input id='market_new_offer_amount' type='number' autocomplete='off' style='width:85px;' value='1' class='market_select' onchange='Market.client_new_offer_update_total_cost();'> of <span id='market_new_offer_count'>0</span> <button onclick='javascript:Mods.Newmarket.MaxQuantity()' class='market_select pointer' style='margin: 0px;font-weight: bold;'>Max</button></td></tr><tr class='offer_line even'><td>Total cost</td><td><b id='market_new_offer_total_cost'>0</b></td></tr><tr class='offer_line'><td></td><td><button onclick='javascript:Market.client_make_offer()' class='market_select pointer' style='margin: 0px;font-weight: bold;'>Make offer</button></td></tr></tbody></table><span>* Each offer lasts 24 hours. You can have {{market_offers}} active offers.{{#can_upgrade_market_offers}}<br/><button class='market_select pointer' onclick='Market.upgrade_offers_dialog();'>Upgrade</button>{{/can_upgrade_market_offers}}</span><div style='position: absolute;bottom: 2px;right: 4px;'>You have <span id='market_new_offer_player_coins'></span> coins</div>");

        market_client_search_results_template = Handlebars.compile("<table class='table scrolling_allowed'><tbody class='scrolling_allowed'><tr class='scrolling_allowed'><th>Item</th><th>Count</th><th>Price</th><th>User</th></tr>{{#each results}}<tr class='{{this.classes}} scrolling_allowed {{#if this.to_player}}green{{/if}}' onclick='Market.client_open_offer({{this.id}})' onmouseenter='Mods.Newmarket.details({{this.id}})' onmouseleave='Mods.Newmarket.hidedetails()'>  <td item_id='{{this.item_id}}' class='scrolling_allowed'>{{item_name this.item_id}}</td>  <td  class='scrolling_allowed'>{{this.count}}</td>  <td class='scrolling_allowed'>{{item_price this.price}}</td>  <td class='scrolling_allowed'>{{this.player}}</td></tr>{{/each}}</tbody></table>");

        market_client_open_offer_template = Handlebars.compile("<table style='text-align: left;border: 1px #666666 solid;width: 100%;margin: 0px;margin-top: 20px;' class='table'><tbody><tr class='offer_line'><td style='width: 96px;'>Item</td><td><span style='vertical-align: bottom;margin-right: 4px;padding-bottom: -26px;line-height: 32px;'>{{item_name this.item_id}}</span><div style='{{item_image this.item_id}}width: 32px;height: 32px;display: inline-block;margin: 0px;padding: 0px;'>&nbsp;</div></td></tr><tr class='offer_line even'><td>About</td><td>{{item_stats this.item_id}}</td></tr><tr class='offer_line'><td>Dealer</td><td>{{this.player}}</td></tr><tr class='offer_line even'><td>Price</td><td>{{item_price this.price}}</td></tr><tr class='offer_line'><td>Amount</td><td><input id='market_offer_amount' type='number' autocomplete='off' style='width:85px;' value='1' class='market_select' onchange='Market.client_update_total_cost({{this.id}});'> of {{this.count}} <button onclick='javascript:Mods.Newmarket.MaxQuantity({{this.id}})' class='market_select pointer' style='margin: 0px;font-weight: bold;'>Max</button> {{#if this.type}}{{{mod_quantity this.id}}}{{/if}}</td></tr><tr class='offer_line even'><td>Total cost</td><td><b id='market_total_cost'>{{item_price this.price}}</b></td></tr><tr class='offer_line'><td></td><td><button onclick='javascript:Market.client_accept_offer({{this.id}})' class='market_select pointer' style='margin: 0px;font-weight: bold;'>{{#if this.type}}Sell{{else}}Buy{{/if}}</button></td></tr></tbody></table><div style='position: absolute;bottom: 2px;right: 4px;'>You have <span id='market_offer_player_coins'></span> coins</div>");

        market_announce_template = Handlebars.compile("\
            <div style='border: 1px solid #666; padding: 5px; margin-bottom: 5px; background-color: #111;'>\
                <span style='color: #FF0; font-weight: bold; padding-top: 3px; line-height: 20px;'>Announces: \
                    <span id='announce_queued_amount' style='color: #FFF;'>0</span>\
                </span>\
                <button class='market_select pointer scrolling_allowed' onclick='Mods.Newmarket.transaction_announce(&apos;announce&apos;)' style='font-size: 0.7em; float: right; margin-bottom: 8px;'>Announce!</button>\
                <button class='market_select pointer scrolling_allowed' onclick='Mods.Newmarket.transaction_announce(&apos;remove&apos;)' style='font-size: 0.7em; float: right;'>Cancel</button>\
                <button class='market_select pointer scrolling_allowed' onclick='Mods.Newmarket.transaction_announce(&apos;select&apos;)' style='font-size: 0.7em; float: right;'>Select All</button>\
                <button id='announce_expand' class='market_select pointer scrolling_allowed' onclick='Mods.Newmarket.transaction_announce(&apos;expand&apos;)' style='font-size: 0.7em; float: right;'>Expand</button>\
                <table id='announce_queued_list' class='table scrolling_allowed hidden' style='text-align: center; min-width: 100%; max-width: 100%'>\
                    <tbody>\
                        <tr>\
                            <th style='text-align: left; padding-bottom: 5px;' colspan='2'>Queued Item</th>\
                            <th>To</th>\
                            <th style='text-align: right; padding-right: 3px;'>Remove</th>\
                        </tr>\
                        {{#each results}}\
                        <tr style='padding-top: 2px;'>\
                            <td style='padding-right: 2px;'>{{#if this.type}}<div style='color: #0F0; padding-bottom: 8px;'>[B]</div>{{else}}<div style='color: #0F0; padding-bottom: 8px'>[S]</div>{{/if}}</td>\
                            <td style='text-align: left; padding-top: 2px'>{{this.name}}</td>\
                            <td style='padding-top: 2px;'>{{#if this.to}}{{this.to}}{{else}}<span style='color: #F2A2F2;'>$$</span>{{/if}}</td>\
                            <td style='padding-left: 8px;'>\
                                <button class='market_select pointer scrolling_allowed' onclick='Mods.Newmarket.transaction_remove_announce({{this.id}})' style='font-size: 0.7em; float: right;'>Remove</button>\
                            </td>\
                        </tr>\
                        {{/each}}\
                    </tbody>\
                </table>\
            </div>\
        ");

        market_client_transaction_offers_template = Handlebars.compile("\
            <div id='announce_list' class='hidden'>&nbsp;</div>\
            Offers\
            <table class='table scrolling_allowed'>\
                <tbody>\
                    <tr>\
                        <th style='width:12%; text-align:center'>Type</th>\
                        <th style='width:15%; text-align:center;'>Item</th>\
                        <th style='width:4%'>#</th>\
                        <th style='width:19%;'>Price</th>\
                        <th style='text-align:center; width:18%; padding-left:9px;'>To</th>\
                        <th style='width:21%; text-align:left; padding-left:9px; position:relative;'>Select\
                            <select id='market_drop_default' class='market_select scrolling allowed' style='width:70px; position:absolute; top:-68%; right:-51%; font-weight:bold;' onchange='Mods.Newmarket.transaction_select()'> \
                                <option class='scrolling_allowed' value='delete'{{select_states 'default' 'delete'}}>Delete</option>\
                                <option class='scrolling_allowed' value='resubmit'{{select_states 'default' 'resubmit'}}>Resubmit</option>\
                                <option class='scrolling_allowed' value='announce'{{select_states 'default' 'announce'}}>Announce</option>\
                                <option class='scrolling_allowed' value='edit'{{select_states 'default' 'edit'}}>Edit</option>\
                            </select>\
                        </th>\
                        <th style='width:10%;'></th>\
                    </tr>\
                    {{#each results}}\
                    <tr class='scrolling_allowed {{this.classes}} {{#if this.available}}green{{else}}red{{/if}}' {{#if this.available}}{{#if this.count}}{{else}}style='color:yellow'{{/if}}{{/if}}>\
                        <td class='scrolling_allowed' style='vertical-align:middle; text-align:center;'>{{#if this.type}}Buy{{else}}Sell{{/if}}</td>\
                        <td class='scrolling_allowed' style='vertical-align:middle; position:relative;'>\
                            <div title='{{item_name this.item_id}}' item_id='{{this.item_id}}' style='{{item_image this.item_id}} width:32px; height:32px; display:inline-block; margin:0px; margin-left:14px; padding:0px; float:left; '></div>\
                        </td>\
                        <td class='scrolling_allowed' style='vertical-align:middle'>{{this.count}}</td>\
                        <td class='scrolling_allowed' style='vertical-align:middle;'>{{item_price this.price}}</td>\
                        <td class='scrolling_allowed' style='vertical-align:middle; text-align:left; padding-left:12px;'>{{#if this.to_player}}{{this.to_player}}{{else}}Everyone{{/if}}</td>\
                        <td class='scrolling_allowed' style='text-align:left; position:relative;'>\
                            <select id='market_drop_{{this.id}}' class='market_select scrolling allowed' style='margin:0px; margin-left:9px; width:70px; position: absolute; top: 25%;'>\
                                <option class='scrolling_allowed' value='delete'{{select_states this.id 'delete'}}>Delete</option>\
                                {{#if this.count}}\
                                <option class='scrolling_allowed' value='resubmit'{{select_states this.id 'resubmit'}}>Resubmit</option>\
                                {{#if this.available}}\
                                <option class='scrolling_allowed' value='announce'{{select_states this.id 'announce'}}>Announce</option>\
                                {{/if}}{{/if}}\
                                <option class='scrolling_allowed' value='edit'{{select_states this.id 'edit'}}>Edit</option>\
                            </select>\
                        </td>\
                        <td class='scrolling_allowed' style='text-align:center; position:relative'>\
                            <button class='market_select pointer scrolling_allowed' onclick='Mods.Newmarket.transaction_click({{this.id}})' style='position: absolute; left: 6px; top: 25%;'>Go</button>\
                        </td>\
                    </tr>\
                    {{/each}}\
                </tbody>\
            </table>\
        ");
    };

    Handlebars.registerHelper("mod_quantity", function (a) {
        var ret = "";
        var off = market_results.filter(function (obj) {
            return obj.id == a;
        });

        if (off.length > 0) {
            var itemid = off[0].item_id;
            var itemcount = off[0].count;
            var mycount = Mods.Newmarket.OwnedQuantity(itemid);
            ret = "You own: <span style='color:" + (mycount >= itemcount ? "lightgreen" : (mycount == 0 ? "red" : "yellow")) + "'>" + mycount + "</span>";
        }

        return ret;
    });

    Handlebars.registerHelper("select_states", function (id, option) {
        if (option == Mods.Newmarket.states[id]) {
            return "selected=\'selected\'";
        }
    });

    Mods.Newmarket.transaction_announce = function (type) {
        var type = typeof type == "string" ? type : false;
        if (type) switch (type) {
            case "announce":
                var list = getElem("announce_list");
                Mods.Newmarket.submitAnnouncement(true);
                Mods.Newmarket.announceList = {};
                addClass(list, "hidden");
                list.innerHTML = "&nbsp;";
                break;
            case "expand":
                var list = getElem("announce_queued_list");
                var btn = getElem("announce_expand");
                if (hasClass(list, "hidden")) {
                    removeClass(list, "hidden");
                    btn.innerHTML = "Collapse"
                } else {
                    addClass(list, "hidden");
                    btn.innerHTML = "Expand"
                };
                break;
            case "select":
                var mark = market_transaction_offers;
                var hasItem = 0;
                for (var id in mark) if (mark[id].count && mark[id].available) {
                    Mods.Newmarket.gatherAnnounce(mark[id].id);
                    hasItem++
                }
                var hide = hasClass(getElem("announce_queued_list"), "hidden");
                getElem("announce_list").innerHTML = market_announce_template({results: Mods.Newmarket.announceList})
                getElem("announce_queued_amount").innerHTML = hasItem;
                if (hide === null) removeClass(getElem("announce_queued_list"), "hidden");
                break;
            case "remove":
                var list = getElem("announce_list");
                Mods.Newmarket.announceList = {};
                addClass(list, "hidden");
                list.innerHTML = "&nbsp;";
                break;
        }
    };

    Mods.Newmarket.transaction_remove_announce = function (itemID) {
        delete Mods.Newmarket.announceList[itemID];
        var hasItem = 0;
        for (var i in Mods.Newmarket.announceList) hasItem++;
        if (hasItem > 0) {
            var hide = hasClass(getElem("announce_queued_list"), "hidden");
            getElem("announce_list").innerHTML = market_announce_template({results: Mods.Newmarket.announceList})
            getElem("announce_queued_amount").innerHTML = hasItem;
            if (hide === null) removeClass(getElem("announce_queued_list"), "hidden");
        } else {
            getElem("announce_list").innerHTML = "&nbsp;";
            addClass(getElem("announce_list"), "hidden");
        }
    };

    Mods.Newmarket.offerFilter = function (itemID, amount, price) {
        if (item_base[itemID] == undefined || typeof parseInt(amount) != "number" || amount < 1 || typeof parseInt(price) != "number") return "";
        var name = "";
        var holder = "";
        var replace = {"'": "`", "Necklace": "Neck.", "Medallion": "Medal.", "Platinum ": "Plat. ", "Pet ": "", " Scroll": "", "Enchanted ": "Ench. ", "Platemail": "Plate.", "Helmet": "Helm.", "Superior ": "Sup. ", " Permission": "", " Of": ":", "Defense": "Def.", "Accuracy": "Acc.", "Strength": "Str.", "Farming": "Farm.", "Woodcutting": "WC.", "Jewelry": "Jewel.", "Cooking": "Cook.", "Carpentry": "Carp.", "Alchemy": "Alch.", "Fishing ": "Fish. ", " Fishing": " Fish.", "Medium ": "Med. ", "Teleport": "Tele."};
        holder = item_base[itemID].name;
        // shortens names
        for (var l in replace) holder = holder.replace(l, replace[l]);
        name += holder;
        // shortens amount and price
        for (var i = 0; i < 2; i++) {
            if (i == 0) holder = amount;
            else holder = price;
            if (holder/3E8 > 1) return "";
            else if (holder/1E6 >= 1) holder = Math.round(holder/1E6 * 10)/10 + "m";
            else if (holder/1E5 >= 1) holder = Math.round(holder/1E3) + "k";
            else if (holder/1E3 >= 1) holder = Math.round(holder/1E3 * 10)/10 + "k";
            if (i == 0) name = holder + " " + name;
            else name += " for " + holder + (amount > 1 ? " ea" : "");
        }
        return name;
    }

    Mods.Newmarket.gatherAnnounce = function (marketID) {
        var isOff = false;
        var mark = market_transaction_offers;
        for (var i in mark) if (mark[i].id == marketID) {
            mark = mark[i];
            isOff = true;
            break;
        }
        if (!isOff || mark.count == 0 || mark.available == 0) return;
        var id = mark.id;
        var itemID = mark.item_id;
        var amount = mark.count;
        var price = mark.price;
        var to_player = mark.to_player || false;
        var type = mark.type;
        var name = Mods.Newmarket.offerFilter(itemID, amount, price);
        var list = Mods.Newmarket.announceList;
        if (name.length == 0) return;
        list[marketID] = {id: id, to: to_player, name: name, type: type};
        var hide = hasClass(getElem("announce_queued_list"), "hidden");
        getElem("announce_list").innerHTML = market_announce_template({results: Mods.Newmarket.announceList})
        removeClass(getElem("announce_list"), "hidden");
        if (hide === null) removeClass(getElem("announce_queued_list"), "hidden");
        var a = 0;
        for (var i in list) a++;
        getElem("announce_queued_amount").innerHTML = a;
    }

    // max chat length: 160 characters;
    Mods.Newmarket.clearAnnouncement = function () {
        var list = getElem("announce_list");
        Mods.Newmarket.announceList = {};
        addClass(list, "hidden");
        list.innerHTML = "&nbsp;";
        Mods.Newmarket.announces = {
            messages: [],
            count: 0
        }
    }

    Mods.Newmarket.submitAnnouncement = function (perform) {
        var list, max, announces, buy, sell, whisper, count, type, id, name, to, item, text;
        if (perform === false) {
            Mods.Newmarket.clearAnnouncement();
            return
        };
        max = Mods.Newmarket.max_lines;
        list = Mods.Newmarket.announceList;
        announces = Mods.Newmarket.announces.messages = [];
        buy = "[BUY] "; sell = "[SELL] "; whisper = {}; count = 0;
        for (id in list) {
            //Mods.consoleLog(list[id].type + ", " + list[id].name);
            if (list[id].to !== false) {
                type = list[id].type == 0 ? " [SELL] " : " [BUY] ";
                whisper[list[id].to] = whisper[list[id].to] || [];
                whisper[list[id].to].push(type + list[id].name);
            } else if (list[id].type == 1) {
                if (list[id].name.length + buy.length + 3 < 160) buy += list[id].name + " | ";
                else {
                    announces.push(buy.slice(0, -3));
                    buy = "[BUY] " + list[id].name + " | ";
                    count++
                }
            } else if (list[id].type == 0) {
                if (list[id].name.length + sell.length + 3 < 160) sell += list[id].name + " | ";
                else {
                    announces.push(sell.slice(0, -3));
                    sell = "[SELL] " + list[id].name + " | ";
                    count++
                }
            }
        }
        if (buy.length - 7 > 0) {announces.push(buy.slice(0, -3)); count++}
        if (sell.length - 7 > 0) {announces.push(sell.slice(0, -3)); count++}
        Mods.Newmarket.announces.count = count;
        name = "";
        for (to in whisper) {
            name = "/w \"" + to + "\" My offer:";
            for (item in whisper[to]) {
                if (name.length + whisper[to][item].length + 6 < 160) name += whisper[to][item] + ", ";
                else {
                    name = capitaliseFirstLetter(name.slice(0, -2)) + " is up.";
                    announces.push(name);
                    name = "/w \"" + to + "\" My offer:";
                }
            }
            name = capitaliseFirstLetter(name.slice(0, -2)) + " is up."
            if (name.length > ("/w \"" + to + "\"").length) announces.push(name);
        }
        if (announces.length == 0) return;
        else announces.sort(function (a, b) { if (/^\[BUY\]/.test(b) || /^\/w/.test(a)) return 1; if (/^\/w/.test(b) || /^\[BUY\]/.test(a)) return -1; return 0 });
        function fn () {
            var announces = Mods.Newmarket.announces.messages,
                count = Mods.Newmarket.announces.count,
                max = Mods.Newmarket.max_lines,
                channel_stop = false,
                ann, delay, channel_stop;
            if (announces.length > 0) for (ann in announces) {
                if (announces[ann].indexOf("/w") == 0) Socket.send("message", {data: announces[ann]});
                else {
                    if (Contacts.channels["$$"] !== true) {
                        !channel_stop && addChatText("You must be subscribed to [$$] to use the Announce feature. Go to: Contacts - Channels - [$$] Subscribe.", void 0, COLOR.TEAL);
                        channel_stop = true;
                    } else {
                        if (!Mods.Newmarket.allowTrade()) {
                            delay = 36E5 + Mods.Newmarket.times[0] - timestamp();
                            delay = Math.round(delay/6E4 * 10)/10 + " minutes";
                            addChatText("You have submitted " + max + " or more offers within the last hour. You must wait at least " + delay + " before you can submit another offer.", void 0, COLOR.TEAL);
                            //addChatText("It is recommended you group offers together (by clicking Announce on more than one offer) before submitting to avoid this limitation in the future.", void 0, COLOR.TEAL);
                            break;
                        } else {
                            Mods.Newmarket.times.push(timestamp());
                            Socket.send("message", {
                                data: announces[ann],
                                lang: "$$"
                            });
                            localStorage["announceBlock"] = JSON.stringify(Mods.Newmarket.times);
                        }
                    }
                }
            }
            Mods.Newmarket.announceList = {};
        }
        // confirmWindow(alert, fn, text, title, fn_no, check_text, check_fn)
        // call confirmWindow if count > 0; else submit messages;
        if (count > 0) {
            Mods.Newmarket.allowTrade(false);
            text = (count == 1 ? "This Announcement uses 1 $$ line. " : count > 1 ? "These Announcements use " + count + " $$ lines. " : "") + "You have " + (max - Mods.Newmarket.times.length) + " available this hour. Post this Announcement?";
            Popup.prompt(text, fn, Mods.Newmarket.clearAnnouncement)
        } else fn();
    };

    Mods.Newmarket.allowTrade = function (change) {
        var max = Mods.Newmarket.max_lines;
        var length = Mods.Newmarket.times.length;
        while (Mods.Newmarket.times[0] + 36E5 < timestamp()) Mods.Newmarket.times.splice(0, 1);
        if (Mods.Newmarket.times.length != length) localStorage["announceBlock"] = JSON.stringify(Mods.Newmarket.times);
        if (Mods.Newmarket.times.length >= max) {
            if (change && getElem("current_channel").value == "$$") {
                if (Contacts.channels.EN == true) getElem("current_channel").value = "EN";
                else if (Contacts.channels["18"] == true) getElem("current_channel").value = "18";
                else {
                    var hasChannel = false;
                    for (var ch in Contacts.channels) if (ch != "$$") {
                        getElem("current_channel").value = Contacts.channels[ch];
                        hasChannel = true;
                        break
                    };
                    if (!hasChannel) {
                        Contacts.add_channel("EN");
                        getElem("current_channel").value = "EN"
                    }
                }
            };
            return false
        };
        return true
    };

    Mods.Newmarket.transaction_click = function (itemID) {
        Mods.Newmarket.saveSelectValue();
        // affects the specified transaction's state
        var type = getElem("market_drop_" + itemID).value;
        type == "delete" && Market.client_cancel_offer(itemID);
        type == "resubmit" && Market.client_extend_offer(itemID);
        type == "announce" && Mods.Newmarket.gatherAnnounce(itemID);
        type == "edit" && Mods.Newmarket.edit(itemID);
    };

    Mods.Newmarket.transaction_select = function () {
        var value = getElem("market_drop_default").value;
        var off = market_transaction_offers;
        for (var item in off) {
            var id = off[item].id;
            var div = getElem("market_drop_"+id);
            if (typeof value != "undefined" && typeof id == "number") {
                if (value == "delete" || value == "edit") div.value = value;
                else if (parseInt(off[item].count) > 0) {
                    if (off[item].available && value == "announce") div.value = value;
                    else if (value == "resubmit") div.value = value;
                }
            }
        }
        Mods.Newmarket.saveSelectValue();
    };

    Mods.Newmarket.saveSelectValue = function () {
        var off = market_transaction_offers;
        var states = Mods.Newmarket.states = {};
        for (var item in off) {
            var div = getElem("market_drop_" + off[item].id)
            if (div && div.value) states[off[item].id] = div.value;
        };
        states["default"] = getElem("market_drop_default").value;
    };

    Mods.Newmarket.OwnedQuantity = function (a) {
        var myitems = Mods.findWithAttr(chest_content, "id", a);
        return myitems && chest_content[myitems].count > 0 ? chest_content[myitems].count : 0;
    };

    Mods.Newmarket.MaxQuantity = function (a) {

        if (a) {
            //if buy offer, sets to max quantity
            //if sell offer, set to max owned if < of max quantity
            var off = market_results.filter(function (obj) {
                return obj.id == a;
            });
            if (off.length > 0) {

                var offertype = off[0].type;
                var itemid = off[0].item_id;
                var itemcount = off[0].count;

                if (offertype == 0) getElem("market_offer_amount").value = itemcount;
                else {
                    var mycount = Mods.Newmarket.OwnedQuantity(itemid);
                    getElem("market_offer_amount").value = mycount < itemcount ? mycount : itemcount;
                }
                Market.client_update_total_cost(off[0].id);
            }
        } else {
            getElem("market_new_offer_amount").value = getElem("market_new_offer_count").innerText;
            Market.client_new_offer_update_total_cost();
        }
    }

    Mods.Newmarket.Tradetoggle = function () {
        var s = getElem("settings_tradechannel");

        switch (Mods.Newmarket.tradechatmode) {
            case 0:
                s.innerHTML = "Trade Chat: manual join";
                Mods.Newmarket.tradechatmode = 1;
                break;
            default:
                s.innerHTML = "Trade Chat: join automatically";
                Mods.Newmarket.tradechatmode = 0;
                break;
        }
        //save to localstorage
        localStorage["tradechatmode"] = JSON.stringify(Mods.Newmarket.tradechatmode);

    }

    Mods.Newmarket.next = function () {
        getElem("market_search_results").scrollTop = 0;
        if (market_results.length == 50) {
            getElem("market_search_min_price").value = market_results[market_results.length - 1].price;
            getElem("market_search_max_price").value = "";
            Market.client_search();
        }
    };

    Mods.Newmarket.togglepopup = function () {
        Mods.Newmarket.popup = !Mods.Newmarket.popup;
        localStorage["marketpopup"] = JSON.stringify(Mods.Newmarket.popup);
    }

    Mods.Newmarket.equippedinslot = function (a) {
        var inv = players[0].temp.inventory;
        for (var i in inv) if (inv[i].selected == true && item_base[inv[i].id].params.slot == a) return inv[i].id;
        return null;
    };

    Mods.Newmarket.hidedetails = function (a) {
        var pop = getElem("market_offer_popup");
        if (pop) pop.style.visibility = "hidden";
    }

    Mods.Newmarket.details = function (a) {
        if (Mods.Newmarket.popup) return;
        //if popup div is not yet defined, define it
        if (getElem("market_offer_popup") == null) {
            var lt = document.createElement("div");
            lt.className = "marketpopup menu";
            lt.id = "market_offer_popup";
            lt.style.opacity = "1";
            getElem("wrapper").appendChild(lt);
        }

        var off = market_results.filter(function (obj) {
            return obj.id == a;
        });



        var pop = getElem("market_offer_popup");
        var dt = (new Date(off[0].available_until) - new Date());
        dt = dt / 1000;
        dt = Math.floor(dt / 3600) + "h " + Math.floor((dt % 3600) / 60) + "m";

        var itm = item_base[off[0].item_id],
        imgs = IMAGE_SHEET[itm.img.sheet];
        var out = "<div style=\"background:url('" + imgs.url + "') no-repeat scroll " + -itm.img.x * imgs.tile_width + "px " + -itm.img.y * imgs.tile_height + "px transparent;width: 32px;height: 32px;display: inline-block;margin: 0px;padding: 0px;float:left;\">&nbsp;</div>";
        out += "<table>";

        out += "<tr><td colspan=2>" + (off[0].type == 0 ? "<span style='color:lightgreen'>[You are BUYING]" : "<span style='color:orange'>[You are SELLING]") + " <b>" + itm.name + "</b></span></td></tr>";
        out += "<tr><td colspan=2 style='color:#3BEEEE'>" + Items.info(off[0].item_id) + "</td></tr>";

        if (off[0].type == 1) {
            //show amount of items in chest
            var myitems = Mods.findWithAttr(chest_content, "id", off[0].item_id);
            var mycount = myitems && chest_content[myitems].count > 0 ? chest_content[myitems].count : 0;
            out += "<tr><td style='color:#CCC'>You own:</td><td style='color:" + (mycount > 0 ? "#00FF00'>" : "#FF0000'>") + mycount + "</td></tr>"
        } else if ((itm.b_t == ITEM_CATEGORY.WEAPON || itm.b_t == ITEM_CATEGORY.ARMOR || itm.b_t == ITEM_CATEGORY.JEWELRY || itm.b_t == ITEM_CATEGORY.PET) && itm.params.wearable) {
            var comp = Mods.Newmarket.equippedinslot(itm.params.slot);
            if (comp)
                out += "<tr><td colspan=2 style='color:#F3A7BD'>" + Items.info(comp) + "<br/>Comparing: " + item_base[comp].name + "</td></tr>";
        }

        out += "<tr><td style='color:#CCC'>Dealer:</td><td style='color:" + (Mods.findWithAttr(Contacts.friends, "name", off[0].player) ? "#FFFF00'>(friend) " : "'>") + off[0].player + "</td></tr>";

        out += "<tr><td style='color:#CCC'>Expires in:</td><td>" + dt + "</td></tr>" + "<tr><td style='color:#CCC'>Price:</td><td>" + thousandSeperate(off[0].price) + " vs " + thousandSeperate(itm.params.price) + " (wiki) = " + Math.round(off[0].price / itm.params.price * 100) + "%</td></tr>";

        out += "<tr><td style='color:#CCC'>Quantity:</td><td>" + off[0].count + " (total " + thousandSeperate(off[0].count * off[0].price) + " coins)</td></tr>";
        out += "</table>";

        pop.innerHTML = out;

        var market = getElem("market");
        pop.style.position = "absolute";
        pop.style.zIndex = "310";
        pop.style.left = market.offsetLeft - 25 + "px";
        pop.style.top = market.offsetTop - 25 + "px";
        pop.style.visibility = "";

    };

    Mods.Newmarket.edit = function (a) {

        var off = market_transaction_offers.filter(function (obj) {
            return obj.id == a;
        });

        if (off.length > 0) {
            var offertype = off[0].type;
            var itemid = off[0].item_id;
            var toplayer = off[0].to_player == null ? "" : off[0].to_player;
            var offprice = off[0].price;
            var itemcount = off[0].count;

            //first we cancel offer
            Market.client_cancel_offer(a);

            setTimeout(function () {
                //reopen market window
                Market.open();

                //show the new offer window
                Market.client_new_offer();
                getElem("market_new_offer_search_type").value = offertype;
                Market.client_update_new_offer_items();
                getElem("market_new_offer_items_item").value = itemid;
                Market.client_update_new_offer_item_change();
                getElem("market_offer_player").value = toplayer;
                getElem("market_new_offer_price").value = offprice;
                getElem("market_new_offer_amount").value = itemcount;
                Market.client_new_offer_update_total_cost();
            }, 1000);
        };
    };

    Mods.Newmarket.eventListener = {
        keys: {"keyup": [KEY_ACTION.SWITCH_LANGUAGE_UP, KEY_ACTION.SWITCH_LANGUAGE_DOWN], "keydown": [KEY_ACTION.SEND_CHAT]},
        fn: function (type, keyCode, keyMap) {
            if (type == "keyup") {
                if (GAME_STATE == GAME_STATES.CHAT && getElem("current_channel").value == "$$") {
                    var allow = Mods.Newmarket.allowTrade()
                    if (!allow) {
                        if (keyMap == KEY_ACTION.SWITCH_LANGUAGE_UP) getElem("current_channel").selectedIndex = Math.max(0, getElem("current_channel").selectedIndex - 1);
                        if (keyMap == KEY_ACTION.SWITCH_LANGUAGE_DOWN) getElem("current_channel").selectedIndex = Math.min(getElem("current_channel").length - 1, getElem("current_channel").selectedIndex + 1);
                        if (getElem("current_channel").value == "$$") Mods.Newmarket.allowTrade(true);
                    }
                }
            }
            if (type == "keydown") {
                 if (GAME_STATE == GAME_STATES.CHAT && getElem("current_channel").value == "$$" && keyMap == KEY_ACTION.SEND_CHAT && getElem("my_text").value.length > 0 && !/^\//.test(getElem("my_text").value)) {
                    Mods.Newmarket.times.push(timestamp());
                    localStorage["announceBlock"] = JSON.stringify(Mods.Newmarket.times);
                }
            }
        }
    }

    Mods.Newmarket.socketOn = {
        actions: ["market_transaction_offers", "login"],
        fn: function (action, data) {
            if (action == "market_transaction_offers") {
                var list, hasItem, id;
                if (!hasClass(getElem("market"), "hidden")) Timers.set("confirm_submit", function () {
                    Mods.Newmarket.checkSubmit(true);
                    Timers.clear("confirm_submit");
                }, 1000);
                list = Mods.Newmarket.announceList;
                hasItem = 0;
                for (id in list) hasItem++;
                if (hasItem > 0) {
                    getElem("announce_list").innerHTML = market_announce_template({results: Mods.Newmarket.announceList});
                    getElem("announce_queued_amount").innerHTML = hasItem;
                    removeClass(getElem("announce_list"), "hidden");
                }
            }
            if (action == "login" && data && data.status == "ok" && Mods.Newmarket.tradechatmode) Contacts.add_channel("$$");
        }
    }

    Mods.Newmarket.resubmit = function (a) {

        if (Timers.running("checkSubmit") || Mods.Newmarket.submitHolder.length > 0) return;

        var off = market_transaction_offers.filter(function (obj) {
            return obj.id == a;
        });

        if (off.length > 0) {
            Mods.Newmarket.submitHolder.push(off[0])
            var likeoff = market_transaction_offers.filter(function (obj) {
                return (obj.item_id == off[0].item_id && obj.type == off[0].type && obj.to_player == off[0].to_player && obj.price == off[0].price && obj.count == off[0].count)
            });
            Mods.Newmarket.submitHolder[0].likeoff = likeoff.length;
            Mods.Newmarket.submitHolder[0].timer = 0;
            Market.client_cancel_offer(a);
            Timers.clear("checkSubmit");

        };
    };

    Mods.Newmarket.checkSubmit = function (submit, count) {
        //Mods.consoleLog(submit);
        var holder = Mods.Newmarket.submitHolder;
        //Mods.consoleLog(timestamp());
        //submit && Mods.consoleLog(holder[0]);
        if (holder.length == 0 || holder[0] && holder[0].id == undefined || holder[0] && holder[0].timer > 0) {
            var cancel = true;
            if (holder[0] && holder[0].timer > 0) {
                var likeoff = market_transaction_offers.filter(function (obj) {
                    return (obj.item_id == holder[0].item_id && obj.type == holder[0].type && obj.to_player == holder[0].to_player && obj.price == holder[0].price && obj.count == holder[0].count)
                });
                //Mods.consoleLog(likeoff.length + " " + holder[0].likeoff)
                if (likeoff.length >= holder[0].likeoff) cancel = false;
            }
            if (cancel) {
                if (holder.length != 0) {
                    Mods.consoleLog("resubmit failed: timeout");
                    addChatText("Failed to resubmit", void 0, COLOR.TEAL);
                }
                Mods.Newmarket.submitQueued = false;
                Timers.clear("checkSubmit");
                Mods.Newmarket.submitHolder = [];
                return;
            }
        };
        if (!submit) return;
        var likeoff = market_transaction_offers.filter(function (obj) {
            return (obj.item_id == holder[0].item_id && obj.type == holder[0].type && obj.to_player == holder[0].to_player && obj.price == holder[0].price && obj.count == holder[0].count)
        });
        //Mods.consoleLog("likeoff, holder.likeoff: " + likeoff.length + ", " + holder[0].likeoff);
        var confirm = likeoff.length >= holder[0].likeoff ? true : false;
        var sameoff = market_transaction_offers.filter(function (obj) {
            return obj.id == holder[0].id;
        });
        if (confirm && sameoff.length == 0) {
            addChatText("Your offer for " + holder[0].count + " " + item_base[holder[0].item_id].name + (holder[0].to_player == "" || holder[0].to_player == null ? "" : " to " + holder[0].to_player) + " was resubmitted.", void 0, COLOR.TEAL);
            Mods.Newmarket.submitHolder = [];
            Mods.Newmarket.submitQueued = false;
            return;
        } else if (sameoff.length > 0) {
            Mods.Newmarket.submitHolder = [];
            Mods.Newmarket.submitQueued = false;
            return;
        }
        var itemChest = chests[0].filter(function (obj) {
            return obj.id == holder[0].item_id;
        });
        var itemCount = itemChest.length > 0 ? parseInt(itemChest[0].count) : 0;
        //Mods.consoleLog("item, count, required: " + itemChest[0].id + ", " + itemCount + ", " + holder[0].count);
        if (itemChest.length > 0 && itemCount >= holder[0].count) {
            Mods.Newmarket.submitHolder.length > 0 && setTimeout(function() {
                var holder = Mods.Newmarket.submitHolder;
                if (holder.length != 0) {
                    var queued = {
                        type: holder[0].type,
                        item_id: holder[0].item_id,
                        to_player: holder[0].to_player != null ? holder[0].to_player : "",
                        price: holder[0].price,
                        count: holder[0].count,
                        target_id: chest_npc.id
                    }
                    //Mods.consoleLog(queued);
                    Socket.send("market_make_new_offer", queued);
                    //Mods.consoleLog("attempt submit: " + queued.item_id);
                }
            }, 100);
            Mods.Newmarket.submitHolder[0].timer += 1;
        };
        if (holder[0].id) Timers.set("resubmit", function () {
            Market.client_transactions()
            //Mods.Newmarket.checkSubmit(true)
        }, Mods.Newmarket.submitHolder[0].timer * 2000 + 1500);
    };

    temp();
    Mods.timestamp("newmarket");
};

Load.tabs = function () {

    modOptions.tabs.time = timestamp();

    function temp () {
        //load variables
        var wrapp = getElem("wrapper");
        var ch = getElem("chat");
        var list = document.createElement("ul");
        list.id = "tabs";
        list.style.top = ch.offsetTop - 49 + "px";

        //create saved tabs
        if (localStorage.CurrentTabs != undefined) Mods.Tabs.wwCurrentTabs = JSON.parse(localStorage.CurrentTabs) || [];
        else {

            //create the data and content for default tab

            Mods.Tabs.wwCurrentTabs.push({
                id: "tabs_default",
                name: "Default",
                activechannel: "EN",
                channels: JSON.parse(JSON.stringify(Contacts.channels)),
                filter_skillattempts: chat_filters.attempt || false,
                filter_skillfails: chat_filters.fails || false,
                filter_playerchat: chat_filters.chat || false,
                filter_whispering: chat_filters.whisper || false,
                filter_joinleave: chat_filters.join_leave || false,
                filter_loot: chat_filters.loot || false,
                filter_magic: chat_filters.magic || false,
                filter_spam: chat_filters.spam || false,
                filter_coloredchannels: chat_filters.color || false,
                filter_coloredonly: getElem("filter_channel_only") ? getElem("filter_channel_only").checked : false,
                filter_highlightfriends: getElem("filter_highlight_friends") ? getElem("filter_highlight_friends").checked : false,
                filter_chatmoderator: chat_filters.modcolor || false,
                filter_timestamps: chat_filters.chattimestamp || false,
                filter_urlfilter: chat_filters.urlfilter || false
            });

            //Mods.Tabs.wwTabContent.push({
            //    id: "tabs_default",
            //    history: []
            //;
        }

        Mods.Tabs.wwactiveTab = "tabs_default";

        if (Mods.Tabs.wwCurrentTabs.length > 0) {
            for (i in Mods.Tabs.wwCurrentTabs) {
                var st = document.createElement("li");
                st.id = Mods.Tabs.wwCurrentTabs[i].id;
                if (Mods.Tabs.wwCurrentTabs[i].id == Mods.Tabs.wwactiveTab) {
                    st.style.selected = "true";
                    st.className = "selected";
                    chat_filters.attempt = Mods.Tabs.wwCurrentTabs[i].filter_skillattempts || false;
                    chat_filters.fails = Mods.Tabs.wwCurrentTabs[i].filter_skillfails || false;
                    chat_filters.chat = Mods.Tabs.wwCurrentTabs[i].filter_playerchat || false;
                    chat_filters.whisper = Mods.Tabs.wwCurrentTabs[i].filter_whispering || false;
                    chat_filters.join_leave = Mods.Tabs.wwCurrentTabs[i].filter_joinleave || false;
                    chat_filters.loot = Mods.Tabs.wwCurrentTabs[i].filter_loot || false;
                    chat_filters.magic = Mods.Tabs.wwCurrentTabs[i].filter_magic || false;
                    chat_filters.spam = Mods.Tabs.wwCurrentTabs[i].filter_spam || false;
                    chat_filters.color = Mods.Tabs.wwCurrentTabs[i].filter_coloredchannels || false;
                    if (getElem("filter_channel_only")) { getElem("filter_channel_only").checked = Mods.Tabs.wwCurrentTabs[i].filter_coloredonly };
                    if (getElem("filter_highlight_friends")) { getElem("filter_highlight_friends").checked = Mods.Tabs.wwCurrentTabs[i].filter_highlightfriends; }
                    chat_filters.modcolor = Mods.Tabs.wwCurrentTabs[i].filter_chatmoderator || false;
                    chat_filters.chattimestamp = Mods.Tabs.wwCurrentTabs[i].filter_timestamps || false;
                    chat_filters.urlfilter = Mods.Tabs.wwCurrentTabs[i].filter_urlfilter || false;
                };
                st.innerHTML = Mods.Tabs.wwCurrentTabs[i].name;
                st.onclick = function () { Mods.Tabs.showTab(this); }
                st.oncontextmenu = function (a) { Mods.Tabs.rclick(a) };
                list.appendChild(st);
                Mods.Tabs.wwTabContent.push({
                    id: Mods.Tabs.wwCurrentTabs[i].id,
                    history: Mods.Tabs.wwCurrentTabs[i].id == Mods.Tabs.wwactiveTab ? chat_history.slice(0) : []
                });
            }

        }

        //create add page
        var v = document.createElement("li");
        v.id = "tabs_add";
        v.innerHTML = "[+]";
        v.onclick = function (a) {
            //add new tab
            if (Mods.Tabs.wwCurrentTabs.length < Mods.Tabs.wwMaxTabs) {
                Popup.input_prompt("Please enter new chat tab name:", function (tabname) {
                    if (tabname && tabname != "") {
                        var newid = Mods.Tabs.add(tabname);
                        var tb = new Object();
                        tb.name = tabname;
                        tb.channels = JSON.parse(JSON.stringify(Contacts.channels));
                        tb.id = newid;

                        //save filters for new tab
                        tb.filter_skillattempts = chat_filters.attempt || false;
                        tb.filter_skillfails = chat_filters.fails || false;
                        tb.filter_playerchat = chat_filters.chat || false;
                        tb.filter_whispering = chat_filters.whisper || false;
                        tb.filter_joinleave = chat_filters.join_leave || false;
                        tb.filter_loot = chat_filters.loot || false;
                        tb.filter_magic = chat_filters.magic || false;
                        tb.filter_spam = chat_filters.spam || false;
                        tb.filter_coloredchannels = chat_filters.color || false;
                        tb.filter_coloredonly = getElem("filter_channel_only") ? getElem("filter_channel_only").checked : false;
                        tb.filter_highlightfriends = getElem("filter_highlight_friends") ? getElem("filter_highlight_friends").checked : false;
                        tb.filter_chatmoderator = chat_filters.modcolor || false;
                        tb.filter_timestamps = chat_filters.chattimestamp || false;
                        tb.filter_urlfilter = chat_filters.urlfilter || false;
                        tb.activechannel = getElem("current_channel").value != "" ? getElem("current_channel").value : "EN";
                        Mods.Tabs.wwCurrentTabs.push(tb);
                    }
                    localStorage.CurrentTabs = JSON.stringify(Mods.Tabs.wwCurrentTabs);
                });
            }
        };
        list.appendChild(v);
        wrapp.appendChild(list);

        if (list.childNodes.length > Mods.Tabs.wwMaxTabs) {
            getElem("tabs_add").style.display = "none";
        };

        //set default channel on load
        var deftb = Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", Mods.Tabs.wwactiveTab)];
        if (deftb.activechannel) { getElem("current_channel").value = deftb.activechannel; }


        //add resizable chat window div

        var resdiv = getElem("chat_resize");
        if (resdiv == undefined) {
            //create the resize div
            resdiv = document.createElement("div");
            resdiv.id = "chat_resize";
            resdiv.style.height = "4px";
            resdiv.style.width = "99.5%"
            resdiv.style.top = getElem("chat").offsetTop - 4 + "px";
            resdiv.style.left = "5px";
            resdiv.style.position = "absolute";
            resdiv.style.display = "block";
            resdiv.style.cursor = "n-resize";
            resdiv.style.zIndex = "100"
            resdiv.draggable = "true";

            resdiv.addEventListener('touchmove', function (e) {
                if (timestamp() - Mods.Tabs.chat_resize_timestamp > 100) {
                    //get mouse relative coords
                    var y = e.changedTouches[0].pageY;

                    var perc = (last_updated.set_canvas_size_new_height - y) / last_updated.set_canvas_size_new_height
                    //reposition resdiv
                    if (perc < 1 && perc > 0)
                        Mods.Tabs.chat_size_percent = perc;

                    //resize chat
                    Mods.setCanvasSize();
                    Mods.Tabs.chat_resize_timestamp = timestamp();
                };
            }, false);


            resdiv.ondrag = function (a) {
                //only fire every 100 ms
                if (timestamp() - Mods.Tabs.chat_resize_timestamp > 100) {
                    //get mouse relative coords
                    var y = a.pageY;

                    var perc = (last_updated.set_canvas_size_new_height - y) / last_updated.set_canvas_size_new_height;
                    //reposition resdiv
                    if (perc < 1 && perc > 0)
                        Mods.Tabs.chat_size_percent = perc;

                    //resize chat
                    Mods.setCanvasSize();
                    Mods.Tabs.chat_resize_timestamp = timestamp();

                    //scroll to bottom
                    getElem("chat").scrollTop = getElem("chat").scrollHeight;
                };
            }
            getElem("wrapper").appendChild(resdiv);
        };

    }

    Chat.set_visible = function () {
        if (Mods.Tabs.set_visible() == false) return false;
        var tabs = getElem("tabs");
        tabs.style.top = getElem("chat").offsetTop - 49 + "px";
        tabs.style.visibility = "";
        getElem("chat_resize").style.visibility = "";
        getElem("chat_resize").style.top = getElem("chat").offsetTop - 4 + "px";
        return true;
    }

    Chat.remove_line = function(a,b){
        var d=document.getElementById("chat_"+a);
        if(d){
            var e=/(.*?)&gt;/g.exec(d.innerHTML)[0];
            d.innerHTML=e+" &lt;Message removed by "+b+"&gt;"
        }
        for(e in chat_history)
            if(chat_history[e]&&chat_history[e].id&&chat_history[e].id==a){
                chat_history[e].text="<Message removed by "+b+">";
                break
            }
        for(tab in Mods.Tabs.wwTabContent)
            for(e in Mods.Tabs.wwTabContent[tab].history)
                if(Mods.Tabs.wwTabContent[tab].history[e]&&Mods.Tabs.wwTabContent[tab].history[e].id&&Mods.Tabs.wwTabContent[tab].history[e].id==a)
                    Mods.Tabs.wwTabContent[tab].history[e].text="<Message removed by "+b+">";
    }

    ChatSystem.toggle = function () {
        Mods.Tabs.chattoggle();
        var tabs = getElem("tabs");
        tabs.style.top = getElem("chat").offsetTop - 49 + "px";
        getElem("chat_resize").style.top = getElem("chat").offsetTop - 4 + "px";
    }

    //adds a new chat tab
    Mods.Tabs.add = function (a) {
        var tabs = getElem("tabs");
        var d = document.createElement("li");
        var n = tabs.childNodes.length - 1;
        while (getElem("tabs_" + n)) {
            n++;
        }
        d.id = "tabs_" + n;
        d.innerHTML = "" + a + "";
        d.onclick = function () { Mods.Tabs.showTab(this); }
        d.oncontextmenu = function (a) { Mods.Tabs.rclick(a) };
        var md = getElem("tabs_add");
        tabs.insertBefore(d, md);
        Mods.Tabs.wwTabContent.push({
            id: "tabs_" + n,
            history: []
        });


        //hide + if max pages
        if (tabs.childNodes.length > Mods.Tabs.wwMaxTabs) {
            getElem("tabs_add").style.display = "none";
        }

        return (d.id);
    }

    //right-click on tab: rename, add or remove channels or delete
    Mods.Tabs.rclick = function (a) {
        var seltab = a.target;

        if (seltab != null) {
            var b = seltab.innerText,
                d = elementXY(seltab.id),
                e = getElem("action_menu");
            addClass(e, "hidden");

            e.style.left = d.left + 20 + "px";
            e.innerHTML = "<span class='line' onclick='Mods.Tabs.showTab(getElem(\"" + seltab.id + "\"));ChatSystem.filters();addClass(getElem(\"action_menu\"),\"hidden\");'>Edit Tab Filters</span>";
            if (seltab.id != "tabs_default") { e.innerHTML += "<span class='line' onclick='Mods.Tabs.rename(\"" + seltab.id + "\");addClass(getElem(\"action_menu\"),\"hidden\");'>Rename<span class='item'>" + b + "</span></span><span class='line' onclick='Mods.Tabs.remove(\"" + seltab.id + "\");addClass(getElem(\"action_menu\"),\"hidden\");'>Remove<span class='item'>" + b + "</span></span>"; }

            if (Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", seltab.id)].filter_playerchat == false) {
                var activec = Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", seltab.id)].channels;

                for (var i in Contacts.channels) {
                    if (activec[i] == undefined || activec[i] == true) {
                        e.innerHTML += "<span class='line' onclick='Mods.Tabs.switchtabchannel(\"" + seltab.id + "\", \"" + i + "\", false);addClass(getElem(\"action_menu\"),\"hidden\");'>Hide Channel <span class='item'>" + i + "</span></span>";
                    }
                    else {
                        e.innerHTML += "<span class='line' onclick='Mods.Tabs.switchtabchannel(\"" + seltab.id + "\", \"" + i + "\", true);addClass(getElem(\"action_menu\"),\"hidden\");'>Show Channel <span class='item'>" + i + "</span></span>";
                    }
                }
            }

            e.style.top = d.top - (e.childElementCount * 22) + "px";
            e.innerHTML.length > 0 && removeClass(e, "hidden");

        }

    }

    Mods.Tabs.switchtabchannel = function (a, b, c) {
        //a=tab ID, b=laguage, c=true/false
        var activec = Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", a)].channels;
        activec[b] = c;
        localStorage.CurrentTabs = JSON.stringify(Mods.Tabs.wwCurrentTabs);

    }

    Mods.Tabs.rename = function (a) {

        var curtab = getElem(a);
        if (curtab != undefined) {
            Popup.input_prompt("Please enter new chat tab name:", function (tabname) {
                if (tabname && tabname != "") {
                    Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "name", curtab.innerText)].name = tabname;
                    curtab.innerText = tabname;
                    localStorage.CurrentTabs = JSON.stringify(Mods.Tabs.wwCurrentTabs);
                }
            });
        }
    }

    Mods.Tabs.remove = function (a) {
        var curtab = getElem(a);
        if (curtab != undefined) {
            //if curtab is the active tab, switch to general
            if (Mods.Tabs.wwactiveTab == a) {
                var ch = getElem("tabs_default");
                Mods.Tabs.showTab(ch);
                Mods.Tabs.wwactiveTab = "tabs_default";
            }


            Mods.Tabs.wwCurrentTabs.splice(Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "name", curtab.innerText), 1);
            localStorage.CurrentTabs = JSON.stringify(Mods.Tabs.wwCurrentTabs);
            var d = curtab.parentNode;
            d.removeChild(curtab);
            getElem("tabs_add").style.display = "";
        }
    }

    Mods.Tabs.findWithAttr = function (array, attr, value) {
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
    }

    Mods.Tabs.chatrefill = function (a) {
        //clears chat and refills with tab history
        chat_history = a.slice(0);
        getElem("chat").innerHTML = "";
        resyncRequired = 100;
        Chat.resync();
    }

    Mods.Tabs.Warning = function (a) {
        getElem(a).className = "warning";
        setTimeout(function () {
            if (getElem(a).className != "selected") getElem(a).className = "";
        }, 350);
        setTimeout(function () {
            if (getElem(a).className != "selected") getElem(a).className = "warning";
        }, 700);

    }

    Mods.Tabs.SaveCurrent = function () {
        //save current filter into active chat
        var tb = Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", Mods.Tabs.wwactiveTab)];
        tb.filter_skillattempts = chat_filters.attempt || false;
        tb.filter_skillfails = chat_filters.fails || false;
        tb.filter_playerchat = chat_filters.chat || false;
        tb.filter_whispering = chat_filters.whisper || false;
        tb.filter_joinleave = chat_filters.join_leave || false;
        tb.filter_loot = chat_filters.loot || false;
        tb.filter_magic = chat_filters.magic || false;
        tb.filter_spam = chat_filters.spam || false;
        tb.filter_coloredchannels = chat_filters.color || false;
        tb.filter_coloredonly = getElem("filter_channel_only") ? getElem("filter_channel_only").checked : false;
        tb.filter_highlightfriends = getElem("filter_highlight_friends") ? getElem("filter_highlight_friends").checked : false;
        tb.filter_chatmoderator = chat_filters.modcolor || false;
        tb.filter_timestamps = chat_filters.chattimestamp || false;
        tb.filter_urlfilter = chat_filters.urlfilter || false;
        tb.activechannel = getElem("current_channel").value != "" ? getElem("current_channel").value : "EN";
        localStorage.CurrentTabs = JSON.stringify(Mods.Tabs.wwCurrentTabs);
    }

    Mods.Tabs.LoadCurrent = function () {
        var newtb = Mods.Tabs.wwCurrentTabs[Mods.Tabs.findWithAttr(Mods.Tabs.wwCurrentTabs, "id", Mods.Tabs.wwactiveTab)];
        chat_filters.attempt = newtb.filter_skillattempts || false;
        chat_filters.fails = newtb.filter_skillfails || false;
        chat_filters.chat = newtb.filter_playerchat || false;
        chat_filters.whisper = newtb.filter_whispering || false;
        chat_filters.join_leave = newtb.filter_joinleave || false;
        chat_filters.loot = newtb.filter_loot || false;
        chat_filters.magic = newtb.filter_magic || false;
        chat_filters.spam = newtb.filter_spam || false;
        chat_filters.color = newtb.filter_coloredchannels || false;
        if (getElem("filter_channel_only")) getElem("filter_channel_only").checked = newtb.filter_coloredonly;
        if (getElem("filter_highlight_friends")) getElem("filter_highlight_friends").checked = newtb.filter_highlightfriends;
        chat_filters.modcolor = newtb.filter_chatmoderator || false;
        chat_filters.chattimestamp = newtb.filter_timestamps || false;
        chat_filters.urlfilter = newtb.filter_urlfilter || false;

        if (newtb.activechannel && Contacts.channels[newtb.activechannel]) { getElem("current_channel").value = newtb.activechannel; }
    }

    Mods.Tabs.showTab = function (a) {

        var selectedId = a.id;

        //close filter tab if open
        getElem("filters_form").style.display = "none";

        Mods.Tabs.SaveCurrent();

        // Highlight the selected tab, and dim all others.
        // Also applies tab filters and replaces chat content with prev history
        if (getElem("tabs") != undefined && Mods.Tabs.wwactiveTab != a.id) {
            var contentDivs = getElem("tabs").childNodes;
            for (var id in contentDivs) {

                if (contentDivs[id].id == selectedId) {
                    contentDivs[id].className = 'selected';
                    //chat_history = Mods.Tabs.wwTabContent[Mods.Tabs.findWithAttr(Mods.Tabs.wwTabContent, "id", selectedId)].history;
                    Mods.Tabs.wwactiveTab = selectedId;
                    //load filters
                    Mods.Tabs.LoadCurrent();

                    Mods.Tabs.chatrefill(Mods.Tabs.wwTabContent[Mods.Tabs.findWithAttr(Mods.Tabs.wwTabContent, "id", selectedId)].history);

                } else {
                    if (contentDivs[id].className == "selected") contentDivs[id].className = "";
                }
            }
        }

    }

    temp();
    Mods.timestamp("tabs");
}

Load.farming = function () {

    modOptions.farming.time = timestamp();

    // creates queue window if it doesn't exist
    Mods.Farming.loadDivs = function () {
        if (getElem("mods_farming_holder") == null) {
            createElem("div", wrapper, {
                id: "mods_farming_holder",
                className: "menu",
                style: "position: absolute; display: none; z-index: 999;",
                innerHTML: Mods.Farming.farming_queue_template()
            })
            createElem("div", wrapper, {
                id: "mods_farming_options",
                className: "menu",
                style: "position: absolute; display: none; z-index: 999;",
                innerHTML: Mods.Farming.farming_queue_option_template(),
                onclick: "Mods.Farming.queueOptions();"
            })
        }
    };
    Mods.Farming.loadDivs();

    Mods.Farming.eventListener = {
        keys: {"keyup": [KEY_ACTION.CTRL], "keydown": [KEY_ACTION.CTRL, 32]},
        fn: function (type, keyCode, keyMap) {
            if (players[0].map === 300 && players[0].name == players[0].params.island) {
                if (type === "keyup") {
                    if (keyMap === KEY_ACTION.CTRL) Mods.Farming.ctrlHeld(false);
                }
                if (type === "keydown") {
                    if (keyMap === KEY_ACTION.CTRL) {
                        Mods.Farming.ctrlHeld(true);
                        Timers.set("clear_ctrl_farm", function () {
                            Mods.Farming.eventListener.fn("keyup", false, KEY_ACTION.CTRL)
                        }, 1000)
                    }
                    if (players[0].map == 300 && keyCode === 32) {
                        if (Mods.Farming.queuePaused === false) Mods.Farming.pauseQueue(false, false, true);
                        else Mods.Farming.pauseQueue(false, false, false)
                    }
                }
            }
        }
    }

    moveInPath = function (a) {
        if (a.me && players[0].map == 300 && Mods.Farming.options.mods_farming_opt_stop && (Mods.Farming.queuePaused === 1 || Mods.Farming.ctrlPressed)) {
            players[0].path = [];
            selected = selected_object = {i: null, j: null};
        } else Mods.Farming.oldMoveInPath(a);
    };

    Mods.Farming.queueOptions = function (showOptions) {
        if (showOptions === true) {
            var div = getElem("mods_farming_options");
            var state = div.style.display;
            if (state == "none") div.style.display = "block";
            else div.style.display = "none";
        }
        var divs = {"mods_farming_opt_stop": false, "mods_farming_opt_save": true, "mods_farming_opt_equipped": true};
        var options = Mods.Farming.options;
        for (var item in divs) {
            var div = getElem(item);
            if (options[item] === undefined) {
                var state = divs[item];
                options[item] = state;
                div.checked = state;
            } else if (showOptions === false) {
                div.checked = options[item];
            } else {
                var state = div.checked;
                options[item] = state;
            }
        }
        localStorage.farming_options = JSON.stringify(options);
    };

    Mods.Farming.queueOptions(false);

    Mods.Farming.findExtendedPath = function (location) {
        if (typeof location === "object" && location.i != undefined && location.j != undefined) {
            var temp_map_increase = map_increase;
            map_increase = 200;
            players[0].path = findPathFromTo(players[0], selected_object, players[0]);
            map_increase = temp_map_increase;
        } else return [];
    };

    Mods.Farming.ctrlHeld = function (state) {
        Mods.Farming.ctrlPressed = state === true ? true : false;
        Mods.Farming.actionState();
        Mods.Farming.pauseQueue(null, true);
    };

    // adjusts dimensions of queue window and font size
    Mods.Farming.setCanvasSize = function () {
        getElem("mods_farming_holder", {
            style: {
                display: players[0].map == 300 && players[0].name == players[0].params.island ? "block" : "none",
                left: (6 + (getElem("magic_slots").style.display == "block" || getElem("magic_slots").style.left == "" ? 38 : 0)) + "px",
                top: Math.ceil(105 * current_ratio_y) + "px",
                width: "145px",
                fontSize: ".7em"
            }
        });
        getElem("mods_farming_queue").style.height = Math.round(120 * current_ratio_y) + "px";
        getElem("mods_farming_options", {
            style: {
                display: players[0].map != 300 || players[0].name != players[0].params.island ? "none" : getElem("mods_farming_options").style.display,
                left: (18 + parseInt(getElem("mods_farming_holder").style.width.replace("px","")) + (getElem("magic_slots").style.display == "block" || getElem("magic_slots").style.left == "" ? 38 : 0)) + "px",
                top: Math.ceil(105 * current_ratio_y) + "px",
                width: "145px",
                fontSize: ".7em",
                height: ""
            }
        });
        if (players[0].map != 300 && !Mods.Farming.options.mods_farming_opt_save) Mods.Farming.cancelQueue();
    };

    // changes the "Action" state on the queue window
    Mods.Farming.actionState = function () {
        var state;
        if (Mods.Farming.ctrlPressed || Mods.Farming.queuePaused === 1) state = "Queuing";
        else if (Mods.Farming.queuePaused === true) state = "Paused";
        else state = "Active";
        getElem("mods_farming_action").innerHTML = state;
    };

    // minimizes the queue window
    Mods.Farming.hideQueue = function (node, setTrue) {
        var queue = getElem("mods_farming_queue");
        var hide = node || getElem("mods_farming_opt_hide");
        if (Mods.Farming.queueHidden || setTrue) {
            Mods.Farming.queueHidden = false;
            queue.style.display = "";
            hide.innerHTML = "Hide queued window";
        } else {
            Mods.Farming.queueHidden = true;
            queue.style.display = "none";
            hide.innerHTML = "Show queued window";
        }
    };

    // stops the queues from performing, or changes the state of the queue/pause/resume button
    Mods.Farming.pauseQueue = function (node, queue, state) {
        var sortedQueue = Mods.Farming.sortedQueue;
        var queue_id = sortedQueue[0];
        if (queue) {
            if (!Mods.Farming.ctrlPressed) Timers.set("farm_check", function (queue_id) {Mods.Farming.checkQueue(0, queue_id)}, Math.min(Math.max(players[0].temp.animate_until - timestamp(), 50), 500));
        } else {
            var states = {"true": "1", "1": "false", "false": "true"};
            Mods.Farming.queuePaused = states[state] != undefined ? JSON.parse(states[state]) : Mods.Farming.queuePaused;
            var pause = getElem("farming_queue_button");
            if (Mods.Farming.queuePaused === true) {
                // active
                Mods.Farming.queuePaused = false;
                Timers.set("farm_check", function (queue_id) {Mods.Farming.checkQueue(0, queue_id)}, Math.min(Math.max(players[0].temp.animate_until - timestamp(), 50), 500));
                pause.innerHTML = "(queue)";
                Mods.Farming.actionState();
            } else if (Mods.Farming.queuePaused === 1) {
                // pause
                Mods.Farming.queuePaused = true;
                pause.innerHTML = "(resume)";
                Mods.Farming.actionState();
            } else {
                // queue
                Mods.Farming.queuePaused = 1;
                pause.innerHTML = "(pause)";
                players[0].path = [];
                Mods.Farming.actionState();
            }
        }
    };

    // adds items to the queue when they are clicked
    Mods.Farming.addToQueue = function (space) {
        if (typeof space != "object" || space.id == undefined) return;
        var queue = Mods.Farming.queue;
        var id = space.b_i;
        var obj_id = space.id;
        var object = space.name;
        var slot = space.i + "_" + space.j + "_" + id;
        if (!Mods.Farming.canPerform(obj_id, false)) return;
        var actions = {333: {slot: 3, name: "Seed", action: "seed"}, 332: {slot: 4, name: "Rake", action: "rake"}};
        action = actions[id] != undefined ? actions[id].action : "harvest";
        var delay = id == 333 || id == 332 ? 1000 : 2000;
        queue[slot] = {id: slot, obj_id: obj_id, item_id: id, i: space.i, j: space.j, delay: delay, action: action};
        Mods.Farming.sortedQueue.push(slot);
        getElem("mods_farming_queue").innerHTML += Mods.Farming.farming_queue_action_template({slot: slot, action: capitaliseFirstLetter(action), object: object, i: space.i, j: space.j});
        if (Mods.Farming.sortedQueue.length == 1 || !Timers.running("farm_queue") || timer_holder["farm_queue"] > 5000 || players[0].path.length == 0) Timers.set("farm_queue", function(slot) {Mods.Farming.performQueue(0, Mods.Farming.sortedQueue[0])}, Math.max(players[0].temp.animate_until - timestamp(), 50));
        getElem("mods_farming_total").innerHTML = Mods.Farming.sortedQueue.length;
    };

    // checks if an item can be added to the queue, or if an item can be performed once the queue reaches it
    Mods.Farming.canPerform = function (obj_id, check_length) {
        var check_length = check_length != null? check_length : true;
        if (check_length && Mods.Farming.sortedQueue.length == 0) return false;
        var object = objects_data[obj_id];
        if (object == undefined) return false;
        var id = object.b_i;
        var actions = {333: {slot: 3, name: "Seed", action: "seed"}, 332: {slot: 4, name: "Rake", action: "rake"}};
        var action = actions[id] || undefined;
        var perform = false;
        if (action != undefined) {
            var temp_inv = players[0].temp.inventory;
            for (var item in temp_inv) if (temp_inv[item].selected && item_base[temp_inv[item].id] && actions[id] && (item_base[temp_inv[item].id].name.indexOf(actions[id].name) > -1
                || actions[id].name == "Seed" && item_base[temp_inv[item].id].name == "Bag of Worms")) { // Bag of Worms does not have a seed in name
                perform = true;
                break
            }
            if (!perform && Mods.Farming.options.mods_farming_opt_equipped === false) perform = 1;
        } else {
            var duration = object_base[id].params.duration;
            if (duration) {
                var time = (secondstamp() - object.params.secondstamp)/60 || duration;
                if (time >= duration) {
                    if (players[0].temp.inventory.length < 40) perform = true;
                    else if (Mods.Farming.options.mods_farming_opt_equipped === false) perform = 1;
                }
            }
        }
        return perform;
    };

    // activates an item once the queue reaches it
    Mods.Farming.performQueue = function (index, queue_id) {
        if (players[0].map != 300) Mods.Farming.pauseQueue(false, false, true);
        var index = typeof index == "number" ? index : 0;
        var queue_id = typeof queue_id == "string" ? queue_id : Mods.Farming.sortedQueue[0] || null;
        var sortedQueue = Mods.Farming.sortedQueue;
        var queue = Mods.Farming.queue;
        var item = queue[sortedQueue[0]];
        if (Mods.Farming.ctrlPressed || Mods.Farming.queuePaused) return;
        if (sortedQueue[index] != queue_id) {
            Mods.Farming.checkQueue(0, sortedQueue[0]);
            return
        }
        var canPerform = Mods.Farming.canPerform(item.obj_id);
        if (item == undefined || canPerform !== true) {
            Mods.Farming.pauseQueue(false, false, true);
            return;
        }
        var object = objects_data[item.obj_id];
        if (object == undefined || object.i != item.i || object.j != item.j) return;
        selected = selected_object = object;
        Mods.Farming.findExtendedPath(selected_object);
        if (players[0].path.length == 0) ActionMenu.act(0);
        var delay = item.delay + Math.min(Math.max(players[0].temp.animate_until - timestamp(), 50), 500);
        var queue_0 = sortedQueue[0];
        if (players[0].path.length > 0) moveInPath(players[0]);
        Timers.set("farm_check", function (queue_0, delay) {Mods.Farming.checkQueue(0, queue_0)}, delay)
    };

    // checks if an item should be activated or deleted once the queue gets to it
    Mods.Farming.checkQueue = function (index, queue_id) {
        var index = typeof index == "number" ? index : 0;
        var queue_id = typeof queue_id == "string" ? queue_id : (Mods.Farming.sortedQueue[0] || null);
        var queue = Mods.Farming.queue;
        var sortedQueue = Mods.Farming.sortedQueue;
        var obj = queue[sortedQueue[index]];
        var obj_id = (obj != undefined) ? obj.obj_id : null;
        if (sortedQueue.length == 0) {
            Mods.Farming.cancelQueue();
            return
        } else if (obj == undefined) {
            Mods.Farming.deleteFromQueue(index);
            return
        } else if (typeof index != "number" || typeof queue_id != "string") {
            Timers.set("farm_check", function (queue_id) {Mods.Farming.checkQueue(0, queue_id)}, Math.min(Math.max(players[0].temp.animate_until - timestamp(), 50), 500));
            return
        }
        var object_g = obj_g(on_map[300][obj.i][obj.j]);
        if (obj_id != object_g.id) {
            Mods.Farming.deleteFromQueue(index, true);
        } else if (inDistance(players[0].i, players[0].j, object_g.i, object_g.j)) {
            selected_object = object_g;
            ActionMenu.act(0);
            Mods.Farming.deleteFromQueue(index, true);
        } else if (sortedQueue[index] == queue_id) {
            Mods.Farming.performQueue(index, queue_id);
        } else if (sortedQueue.length > 0 && !Timers.running("farm_queue")) Mods.Farming.performQueue(0, sortedQueue[0]);
    };

    // deletes an item from the queue if it's been completed or if it doesn't exist
    Mods.Farming.deleteFromQueue = function (index, perform) {
        var perform = perform || false;
        var obj = Mods.Farming.queue[Mods.Farming.sortedQueue[index]];
        var obj_id = (obj != undefined) ? obj.obj_id : null;
        var object_g = obj_g(on_map[300][obj.i][obj.j]);
        if (Mods.Farming.sortedQueue.length > 0 && index < Mods.Farming.sortedQueue.length && !Mods.Farming.queuePaused && !Mods.Farming.ctrlPressed && object_g.id != obj_id) {
            var id = Mods.Farming.sortedQueue[index];
            delete Mods.Farming.queue[id];
            Mods.Farming.sortedQueue.splice(index,1);
            getElem("mods_farming_"+id) != null && getElem("mods_farming_queue").removeChild(getElem("mods_farming_"+id));
            perform && Mods.Farming.checkQueue(index, Mods.Farming.sortedQueue[index]);
            Timers.set("farming_queue_active", function() {Mods.Farming.checkQueue(0)}, 2500);
            getElem("mods_farming_total").innerHTML = Mods.Farming.sortedQueue.length;
        }
    };

    // resets the entire queue
    Mods.Farming.cancelQueue = function () {
        Timers.clear("farm_check");
        Timers.clear("farm_queue");
        Mods.Farming.queue = {};
        Mods.Farming.sortedQueue = [];
        getElem("mods_farming_queue").innerHTML = "<span style='width: 100%; float: left; display: inline-block; font-weight: bold; color: #999;'><span>Action:&nbsp;&nbsp;Object</span><span style='float: right;'>Coords</span></span>";
        getElem("mods_farming_total").innerHTML = Mods.Farming.sortedQueue.length;
    };

    // new functions for rake/seed/harvest
    DEFAULT_FUNCTIONS.rake = function (a, b) {
        if (Mods.Farming.performActivity(a)) {
            Mods.Farming.oldDefault.rake(a, b);
            Timers.set("farming_queue_active", function() {Mods.Farming.checkQueue(0)}, 2500);
        } else {
            Mods.Farming.findExtendedPath(a);
            players[0].path.length > 0 && moveInPath(players[0]);
        }
    };
    DEFAULT_FUNCTIONS.seed = function (a, b) {
        if (Mods.Farming.performActivity(a)) {
            Mods.Farming.oldDefault.seed(a, b);
            Timers.set("farming_queue_active", function() {Mods.Farming.checkQueue(0)}, 2500);
        } else {
            Mods.Farming.findExtendedPath(a);
            players[0].path.length > 0 && moveInPath(players[0]);
        }
    };
    DEFAULT_FUNCTIONS.harvest = function (a, b) {
        if (Mods.Farming.performActivity(a)) {
            Mods.Farming.oldDefault.harvest(a, b);
            Timers.set("farming_queue_active", function() {Mods.Farming.checkQueue(0)}, 2500);
        } else {
            Mods.Farming.findExtendedPath(a);
            players[0].path.length > 0 && moveInPath(players[0]);
        }
    };

    // checks if rake/seed/harvest should be performed
    // does not perform if: queueing (ctrlPressed || queuePaused === 1), map != 300
    Mods.Farming.performActivity = function (object) {
        if (players[0].map == 300) {
            var sel_obj = obj_g(object);
            if (!building_mode_enabled && (Mods.Farming.queuePaused !== true || Mods.Farming.ctrlPressed) && sel_obj.id && sel_obj.params && (sel_obj.params.rotate != undefined || sel_obj.params.carpentry_item_id == 753)) {
                var queue = Mods.Farming.queue;
                var queue_id = sel_obj.i + "_" + sel_obj.j + "_" + sel_obj.b_i;
                queue[queue_id] == undefined && Mods.Farming.addToQueue(sel_obj);
                if (!(Mods.Farming.ctrlPressed || Mods.Farming.queuePaused === 1) && Mods.Farming.sortedQueue.length > 0) {
                    if (sel_obj.id == selected_object.id) return true;
                    var item = Mods.Farming.queue[Mods.Farming.sortedQueue[0]];
                    var object = obj_g(on_map[300][item.i][item.j]);
                    object && (selected = selected_object = object);
                    if (typeof selected_object == "object" && selected_object.activities && selected_object.activities[0] && selected_object.activities[0].length > 0) ActionMenu.act(0);
                    if (object.id != sel_obj.id) return false;
                } else {
                    return false;
                }
            } else if (!(sel_obj && sel_obj.id && sel_obj.params && (sel_obj.params.rotate != undefined || sel_obj.params.carpentry_item_id == 753))) {
                return false;
            }
        }
        return true;
    };

    Mods.Farming.inventoryEquip = function (a, b, d) {
        if (b == 779 && players[0].map == 300) {
            selected = selected_object = obj_g(on_map[300][9][10]);
            Mods.Farming.findExtendedPath(selected);
            if (players[0].path.length > 0) moveInPath(players[0]);
            else ActionMenu.act(0);
        }
        return false;
    };

    Mods.timestamp("farming");

}

// This is used to give all sub-elements of a chosen parent element a specific className. For example, "scrolling_allowed" needs to appear on all sub-elements of a parent element where "scrolling_allowed" is intended. Bad things happen when any single sub-element doesn't contain this class. This function insures that issue can't occur. But this function is also somewhat inefficient and can take a long time to run for any parentNode that has sufficiently many childNodes.
Mods.elemClass = function (elemClass, parent, path) {
    var scroll = elemClass;
    if (typeof path === "object") {
        if (typeof path.className === "undefined") return false
    } else if (typeof parent != "undefined") {
        var path = getElem(parent);
        if (typeof path !== "object") return false;
        else if (typeof path.className === "undefined") return false
    } else return false;
    addClass(path, scroll);
    for (var node in path.childNodes) if (typeof path.childNodes[node] === "object") {
        var sub_path = path.childNodes[node];
        Mods.elemClass(scroll, null, sub_path);
    }
};

Mods.confirmClass = function (elemClass, parent, path) {
    var scroll = elemClass;
    if (typeof path === "object") {
        if (typeof path.className === "undefined") return false
    } else if (typeof parent != "undefined") {
        ClassValue = 0;
        var path = getElem(parent);
        if (typeof path !== "object") return false;
        else if (typeof path.className === "undefined") return false
    } else return false
    !hasClass(path, scroll) && ClassValue++;
    for (var node in path.childNodes) if (typeof path.childNodes[node] === "object") {
        var sub_path = path.childNodes[node];
        Mods.confirmClass(scroll, null, sub_path);
    }
};

Mods.initialize();
Mods.loadModOptions();
Mods.elemClass("scrolling_allowed", "mods_form");
Mods.initializeOptionsMenu();
// getElem("mods_link").innerHTML = "<span class='common_link' onclick='javascript: getElem(&apos;mods_form&apos;).style.display = &apos;block&apos;;BigMenu.show(-1);' style='width:120px;'>Options Menu</span><br><span class='common_link' onclick='javascript: getElem(&apos;pet_form&apos;).style.display = &apos;block&apos;;BigMenu.show(-1);' style='width:120px;'>Pet Inventory</span>"
getElem("mods_link").innerHTML = "Wiki &amp; Mods menu";
getElem("mods_link").setAttribute("onclick", "javascript: removeClass(getElem(\'mods_form\'),\'hidden\'); BigMenu.show(-1);");
getElem("mods_link").style.display = "";
// TopIcons.show("pet");
Mods.consoleLog("Ready: RPG MO Dendrek & WitWiz Mods Pack version " + Mods.version);